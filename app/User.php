<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password','first_name','last_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
//get associations having many to many relationship with user table
    public function getAssociation()
    {
        return $this->belongsToMany('App\association', 'users_associations', 'users_id', 'associations_id');
    }

    //get Specilities/Categories having many to many relationship with user table
    public function getSpecialities()
    {
        return $this->belongsToMany('App\Catagory', 'users_specialities', 'users_id', 'specialities_id');
    }
    public function getProcedures()
    {
     return $this->belongsToMany('App\Procedure', 'user_procedures', 'user_id', 'procedure_id');
    }

    public function getAlbums()
        {
         return $this->hasMany('App\Album');
        }
    public function getConditions()
    {
         return $this->belongsToMany('App\Models\Conditions','user_conditions','user_id','condition_id'); //Profile is your profile model
    }
    



public function isAdmin()
        {
            return Auth::user()->user_role;
        }
         
    public function userID()
        {
            return $this->user_id; 
        }
        
            public function hasRole($userID, $roles)
        {
            return  User::where('user_role', $roles)->where('id', $userID)->get();
        }
        
}
