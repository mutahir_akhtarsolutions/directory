<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
 protected $fillable = [ 'user_id',
						 'album_name',
						 'album_description',
						 'procedure_id',
						 'sort_order','album_image'];

   public function getGalleries()
        {
          return $this->hasMany('App\Models\Image');
        }
}
