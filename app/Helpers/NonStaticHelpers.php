<?php // Code within app\Helpers\Helper.php Mutahir Shah

    namespace App\Helpers;
    use View;
    use DB;
    use Input;
    use Validator;
    use Redirect;
    use Cartalyst\Sentinel\Native\Facades\Sentinel;
    use Cartalyst\Sentinel\Laravel\Facades\Activation;
    use App\Logic\Image\ImageRepository;
    use Intervention\Image\Facades\ImageFacades; // Use this if you want facade style code
    use App\Models\sitesetting;
    class NonStaticHelpers
    {
        public function __construct()
        {
            
        }
       public function resize($path,$image, $size)
    {
        try 
        {
            $extension      =   $image->getClientOriginalExtension();
            $imageRealPath  =   $image->getRealPath(); 
            $thumbName      =   rand(111111,999999).'_'. $image->getClientOriginalName();
            
            //$imageManager = new ImageManager(); // use this if you don't want facade style code
            //$img = $imageManager->make($imageRealPath);
        
            $img = ImageFacades::make($imageRealPath); // use this if you want facade style code
            $img->resize(intval($size), null, function($constraint) {
                 $constraint->aspectRatio();
            });
            $img->save($path. $thumbName);
            return $thumbName;
        }
        catch(Exception $e)
        {
            return false;
        }

    }

    //------------------------------------- validation function.

    public  function validatorGeneral($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    } 
    //-----------------------------------------------------------------
    public static function siteSettings()
    {

    $settings = sitesetting::where('visible', 1)->first();
    return $settings;
}

    }
?>