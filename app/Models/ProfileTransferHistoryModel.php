<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ProfileTransferHistoryModel extends Model
{
   protected $table = 'profile_to_transferhistory';
}
