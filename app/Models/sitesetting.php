<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sitesetting extends Model
{
    //protected $table    = 'newsletter_recipients';
    public    $timestamps  = true;
    protected $fillable    =  ['site_title','site_copyright','site_email','contact_email','general_email','contact_phone','contact_cell','site_url','address','site_keywords','site_metatags','site_metadescriptions','site_facon'];
    protected $guarded = ['id', '_token'];
}
