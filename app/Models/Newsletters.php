<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletters extends Model
{
	protected $table    = 'newsletter_recipients';
    public $timestamps  = true;
    protected $fillable =  ['first_name','last_name','email'];

}
