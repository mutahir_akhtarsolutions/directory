<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class schedule_profile extends Model
{
     public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['profile_id', 'days','open','from','to','break_time','break_duration'];
}
