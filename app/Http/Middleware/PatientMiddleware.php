<?php

namespace App\Http\Middleware;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Closure; 
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Redirect;

class PatientMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
      try {
            if ( false === Sentinel::check() )
            {
                if ($request->ajax())
                {
                    return response('Unauthorized.', 401);
                }
                else
                {
                    return redirect()->guest('auth/login');
                }
            }
            else if (false === Sentinel::hasAccess('patient'))
            {
                return redirect::to('auth/login')->withErrors(['Only visitor/Patient can access this page.']);
            }else return $next($request);
        }catch (NotActivatedException $e) {

            return redirect('reactivate')
                ->with('user', $e->getUser())
                ->withErrors([Sentinel::getUser()->id =>'Account is not activated!']);
        }






          
    }
 

     
}
