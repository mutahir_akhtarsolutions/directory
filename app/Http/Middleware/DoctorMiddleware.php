<?php

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Closure;

class DoctorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (Sentinel::check()) {
             
          $role    = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 
         if($role->slug == 'doctor')
         {          
           return $next($request);
         }
         else
            {
               
                return view('auth.login')->withErrors('You are not allowed here.');
            }

        } else {
             return view('auth.login')->withErrors('Your session is exprired.');
             
         } 

    }
}
