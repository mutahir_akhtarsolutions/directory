<?php namespace App\Http\Middleware;

use Closure;
#use Illuminate\Contracts\Auth\Guard;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;

class Authenticate {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        try {
            if ( false === Sentinel::check() )
            {
                if ($request->ajax())
                {
                    return response('Unauthorized.', 401);
                }
                else
                {
                    return redirect()->guest('auth/login');
                }
            }
        }catch (NotActivatedException $e) {

            return redirect('reactivate')
                ->with('user', $e->getUser())
                ->withErrors([Sentinel::getUser()->id =>'Account is not activated!']);
        }

		return $next($request);
	}

}
