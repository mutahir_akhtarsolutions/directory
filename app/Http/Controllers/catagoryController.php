<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Catagory;
use View;
use Input;
use Validator;
use Redirect;
use DB;

class catagoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return View('catagory.index', array( 'Catagories' => Catagory::paginate(10) ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return $this->showForm('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->processForm('create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return $this->showForm('update', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {

        //
        if ($Catagory = Catagory::find($id))
        {
          if($Catagory->parent_id != 0){
            $Catagory->delete();
            return Redirect::to('catagory')->withSuccess('Catagory is successfully deleted');
          }else {
            return Redirect::to('catagory')->withErrors('Catagory is not deleted. It is parent catagory.');
          }

        }

        return Redirect::to('catagory');
    }


    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        //$catagory = null;

        if ($id)
        {

            if ( ! $catagory = Catagory::find($id))
            {

                return Redirect::to('catagory');
            }
        }else {
          //$catagory=DB::table('specialities')->where('parent_id', 0)->get();
          $catagory = Catagory::where('parent_id', 0)->get();

        }

        if($id){
            $catagories =   Catagory::where('parent_id', 0)->get();

            /*echo '<pre>';
            print_r($catagories);die;*/


        }

        return View::make('catagory.form', compact('mode', 'catagory', 'catagories'));
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {


        $input = Input::all();


        if ($id)
        {


          $rules = [
              'name' => 'required'
          ];

            $Catagory = Catagory::find($id);
            $messages = $this->validateCatagory($input, $rules);

            if ($messages->isEmpty())
            {
                $input['parent_id'] = $input['Catagory'];
                $Catagory->fill($input);
                $Catagory->save();
            }
        }
        else
        {
          $rules = [
              'name' => 'required|unique:specialities'
          ];

            $messages = $this->validateCatagory($input, $rules);

            if ($messages->isEmpty())
            {

              $input['parent_id'] = $input['Catagory'];
              $Catagory = Catagory::create($input);
                //$role = $this->roles->create($input);

                /*if(!empty($role))
                {
                    $role->parent_id = self::MEMBER_ROLE_ID;
                    $role->save();
                    $input['role_id'] = $role->id;
                    $package = package::create($input);
                }*/
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::to('catagory');
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a catagory.
     *
     * @param  array  $data
     * @param  mixed  $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateCatagory($data, $packages)
    {

        $validator = Validator::make($data, $packages);

        $validator->passes();

        return $validator->errors();
    }



}
