<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/22/2015
 * Time: 10:29 PM
 */
namespace App\Http\Controllers;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

class AuthorizedController extends Controller {

    public function __construct()
    {
        $this->beforeFilter('auth');

        $this->user = Sentinel::getUser();
    }

}
