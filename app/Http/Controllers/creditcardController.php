<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\creditcard;
use View;
use Input;
use Validator;
use Redirect;

class creditcardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('creditcard.index', array( 'creditcards' => creditcard::paginate(10)));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //var_dump('there i go in the CREATE of creditcardController');die;
        return $this->showForm('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->processForm('create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($creditcard = creditcard::find($id))
        {
            $creditcard->delete();

            return Redirect::to('creditcard');
        }

        return Redirect::to('creditcard');


    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        $creditcard = null;
        if ($id)
        {
            if ( ! $creditcard = creditcard::find($id))
            {
                return Redirect::to('creditcard');
            }
        }
        return View::make('creditcard.form', compact('mode', 'creditcard'));
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = Input::all();

        $rules = [
            'cc_name' => 'required'
        ];

        if ($id)
        {
            $creditcard = creditcard::find($id);

            $messages = $this->validatePackage($input, $rules);

            if ($messages->isEmpty())
            {
              $imageName = Input::file('cc_logo')->getClientOriginalName();
              Input::file('cc_logo')->move('images/catalog', $imageName);
              $input['cc_logo'] = $imageName;

                $creditcard->fill($input);
                $creditcard->save();
            }
        }
        else
        {
            $messages = $this->validatePackage($input, $rules);

            if ($messages->isEmpty())
            {
                    $imageName = Input::file('cc_logo')->getClientOriginalName();
                    Input::file('cc_logo')->move('images/catalog', $imageName);
                    $input['cc_logo'] = $imageName;
                    $package = creditcard::create($input);

            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::to('creditcard');
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }
    /**
     * Validates a package.
     *
     * @param  array  $data
     * @param  mixed  $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validatePackage($data, $packages)
    {
        $validator = Validator::make($data, $packages);

        $validator->passes();

        return $validator->errors();
    }
}
