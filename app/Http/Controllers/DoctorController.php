<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use DB;
use View;
use Input;
use App\Profile;
use Redirect;
use App\Catagory;
use App\association;
use App\Album;
use App\Models\ProfileTransferHistoryModel;
// import the Intervention Image Manager Class 
use Intervention\Image\Facades\Image; // Use this if you want facade style code
class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $user_obj;
    public function __construct()
    {
        $this->user_obj = new User();

    }
    public function index()
    {
        $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];

         $collection['created_at']     = Sentinel::getUser()->created_at;
         $collection['allMyComments']  = DB::table('view_rating_doctors')->where('doctor_id', Sentinel::getUser()
        ->id)->where('reviews_type','<>','Archive')->get(); 

            return View::make('doctors.index', compact('collection'));
    }

     public function profile ()
        {
         $collection['countries']      = DB::table('country')->get();
         $collection['states']         = DB::table('state')->get();
         $collection['cities']         = DB::table('city')->get();
         $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
         $collection['first_name']     = Sentinel::getUser()->first_name;
         $collection['last_name']      = Sentinel::getUser()->last_name;
         $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
         $collection['created_at']     = Sentinel::getUser()->created_at;
         return View::make('doctors.editMemberProfile',compact('Profile','collection'));

        }
        public function showSpeciliaty()
        {
            $user_id = Sentinel::getUser()->id;
            $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
            $collection['first_name']     = Sentinel::getUser()->first_name;
            $collection['last_name']      = Sentinel::getUser()->last_name;
            $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];

             $collection['created_at']    = Sentinel::getUser()->created_at;
             $collection['data']          = User::find($user_id)->getSpecialities()->get(); 

            return View::make('doctors.specility', compact('collection'));

        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function moveProfile()
    {
            $user_id                      = Sentinel::getUser()->id;
            $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
            $collection['first_name']     = Sentinel::getUser()->first_name;
            $collection['last_name']      = Sentinel::getUser()->last_name;
            $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];

             $collection['created_at']    = Sentinel::getUser()->created_at;
            
             $collection['data']          = Profile::select('id','business_name')->where('parent_id', 0)->where('id','<>', $collection['Profile']->parent_id)->get(); 

            return View::make('doctors.transfer_form_reques', compact('collection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $rules = ['business_name' => 'required'];
        $messages      = $this->validatorGeneral($request->all() , $rules);

        $data          = Profile::select('id','business_name','parent_id')
          ->where('business_name', trim($input['business_name']))->first(); 
          $checkRequest = Profile::where('id', $input['profile_id'])
          ->where('transfer_request_to_parent','<>', 0)->count(); 
       if($checkRequest <= 0 ) {
        if(count($data) > 0)
        {
            if ($messages->isEmpty())
            {
             Profile::where('id', $input['profile_id'])
                 ->update(['transfer_request_to_parent' => $data->id]);
                return Redirect::to('/doctor/moveprofile')
                ->withSuccess('Request Sent Successfully.');
            }
        } else {
            return Redirect::back()->withInput()->withErrors('No Clinic Exists With This Name.');
            
        } 
    } else return Redirect::back()->withInput()->withErrors('Your Request Is Pending.');

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user_id = Sentinel::getUser()->id;
        $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['data']           = User::find($user_id)->getProcedures()->get();
        return View::make('doctors.procedures', compact('collection'));
    } 

    public function showAssociations()
    {
        $user_id = Sentinel::getUser()->id;
        $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['data']           = User::find($user_id)->getAssociation()->get(); 
        return View::make('doctors.associations', compact('collection'));
    }


    public function addEditAssociations()
    {
        $user_id = Sentinel::getUser()->id;
        $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['data']           = User::find($user_id)->getAssociation()->get(); 
        $collection['association_names'] = association::select('name')->get();

        $collection['associations'] = DB::table('association')->join('users_associations', 'association.id', '=', 'users_associations.associations_id')->select('association.name','users_associations.*')
    ->where('users_id',$user_id)->get();

        return View::make('doctors.associations_form', compact('collection'));
    }

    public function updateAssociations(Request $request)
    {
        ///UPDATE ASSOCIATIONS 

        $user_id = Sentinel::getUser()->id;
        $data = Input::all(); 
      
        if(!empty(@$data['association'])):// If all the associations are removed then delete all

        $errors = array_filter(@$data['association']);
        $totalAssociations = count($data['association']);
        if($totalAssociations >= 1 && !empty($errors)): 

             DB::table('users_associations')->where('users_id', '=', $user_id)->delete();

        for($a = 0; $a < $totalAssociations; $a++):

          $association                =  $data['association'][$a]; //association
          $association_inputs['name'] = $association;   

          $associations               = association::select('id')
          ->whereRaw('UPPER(name) ="'.strtoupper($association).'"')->first();

          if(count($associations) > 0) {
             $association_last_id = $associations->id;
          }

           $users_associations  = DB::table('users_associations')
           ->insert([
           'users_id' => $user_id,
           'associations_id' => @$association_last_id,
          'availability'=>$data['availability'][$a],'from_time'=>$data['from_time'][$a],'to_time'=>$data['to_time'][$a],'associations_order'=>$a,'date_added'=>date("Y-m-d H:i:s")]
        ); 
          endfor;
          endif;

      else: 
        DB::table('users_associations')
            ->where('users_id', '=', $user_id)
            ->delete();
endif;
             return Redirect::back()->withInput()->withSuccess('Record Saved.');
          // ASSOCIATIONS ENDS HERE..................

    }


 public function appointments()
    {
            $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first();  
      $collection['first_name']     = Sentinel::getUser()->first_name;
      $collection['last_name']      = Sentinel::getUser()->last_name;
      $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
      $collection['created_at']     = Sentinel::getUser()->created_at;

      $collection['allAppointments']  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.schedule_time','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.profile_doctor_id',$collection['Profile']->id)
            ->get(); 

      return View::make('doctors.appointments',compact('collection'));


    }
    public function ajaxSearchAppointments(Request $request)
    {
      $html = '';
      //$html .= $request->search_by_date;
      if($request->search_by_date == '' || !isset($request->search_by_date)):
        $searchByDate = date("Y-m-d");
        else:
          $searchByDate = \Carbon\Carbon::createFromFormat('d/m/Y', $request->search_by_date)->format('Y-m-d');
      endif;     
      $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first();
       $collection['allApts']  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.schedule_time','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.profile_doctor_id',$collection['Profile']->id)
            ->whereDate('schedule_date', '=', $searchByDate)
            ->get();
      $html .= '<table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr><th>Sr.No</th>
                    <th>Patients Name</th> 
                    <th>Date</th>
                    <th>Time</th>
                    <th>Current Status</th>  
                    <th>Change Status</th> 
                    
                  </tr>
                </thead>
                <tbody>';                
                $counter = 1;
                foreach($collection['allApts'] as $doctor):
                   $html .= '<tr>
                  <td>'.$counter.'</td><td>'.$doctor->first_name.' '.$doctor->last_name.'</td>
                   <td>'.\Carbon\Carbon::createFromFormat('Y-m-d', $doctor->schedule_date)->format('d-m-Y').'</td>
                    <td>'.\Carbon\Carbon::createFromFormat('H:i', $doctor->schedule_time)->format('h:i A').'</td><td id="td_'.$doctor->id.'">';
                    switch ($doctor->status) {
                      case 1:
                        $html .= 'Unapproved';
                        break;
                      case 2:
                        $html .=  'Approved';
                        break;
                      case 3:
                        $html .=  'Canceled';
                        break;
                      case 4:
                        $html .=  'Pending';
                        break;
                      default:
                         $html .=  'Pending';
                        break;
                    }                   
                    $html .='</td>
                   <td>                    
                    <div title="" data-toggle="tooltip" class="box-tools" data-original-title="Status">
                    <div data-toggle="btn-toggle" class="btn-group">
                      <button class="btn btn-default btn-sm active approve" type="button" data-id="'.$doctor->id.'" data-value="2" title="Active">
                      <i class="fa fa-square text-green"></i></button>
                      <button class="btn btn-default btn-sm approve" type="button" data-id="'.$doctor->id.'" data-value="1" title="Inactive"><i class="fa fa-square text-red"></i></button>

                      <button class="btn btn-default btn-sm approve" type="button" data-id="'.$doctor->id.'" data-value="3" title="Reject"><i class="fa fa-square text-blue"></i></button>
                    </div>
                  </div>

                   </td> 
                  </tr>';
                  $counter ++;
                endforeach;
                $html .= '</tbody>
                <tfoot>
                  <tr>
                    <th>Sr.No</th>
                    <th>Patients Name</th> 
                    <th>Date</th>
                     <th>Time</th>
                     <th>Current Status</th>  
                    <th>Change Status</th> 
                  </tr>
                </tfoot>
              </table>';
              return $html;
               }


public function myAlbums ()
    {
        $user_id                      = Sentinel::getUser()->id;
        $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['data']           = User::find($user_id)->getAlbums()->orderBy('sort_order')->get();
        return View::make('doctors.gallery', compact('collection'));
    }



    protected function showForm()

    {   
        $user_id                      = Sentinel::getUser()->id;
        $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
          $collection['user_procedure'] = User::find($user_id)->getProcedures()->get();
      return View::make('doctors.gallery_form' , compact('collection'));
    }
    public function saveForm (Request $request)
    {
        $input = $request->all();
        $rules = [
            'album_name' => 'required'
        ];
        $input['user_id'] =  Sentinel::getUser()->id;
        $messages = $this->validatorGeneral($input , $rules);
        $imageName = '';
        
        if ($messages->isEmpty())
            {

        if(Input::file('album_image') != ''):
        $image          =   $request->file('album_image');
        $resizedImage   =   $this->resize($image, 100);
       
           $extention   =  Input::file('album_image')->getClientOriginalExtension();
            // $imageName = Input::file('album_image')->getClientOriginalName();
           $imageName = $resizedImage;
            Input::file('album_image')->move('images/gallery/album/full_size', $imageName);
         endif;

         $input['album_image'] = $imageName;
            
            Album::create($input); 
            return Redirect::to('/doctor/gallery')->withSuccess('Record Saved.');
        }

        return Redirect::back()->withInput()->withErrors($messages);

    }


    private function resize($image, $size)
    {
        try 
        {
            $extension     = $image->getClientOriginalExtension();
            $imageRealPath = $image->getRealPath(); 
            $thumbName     = rand(111111,999999).'_'. $image->getClientOriginalName();
            
            //$imageManager = new ImageManager(); // use this if you don't want facade style code
            //$img = $imageManager->make($imageRealPath);
        
            $img = \Intervention\Image\Facades\Image::make($imageRealPath); // use this if you want facade style code
            $img->resize(intval($size), null, function($constraint) {
                 $constraint->aspectRatio();
            });
            $img->save('images/gallery/album/icon_size/'. $thumbName);
            return $thumbName;
        }
        catch(Exception $e)
        {
            return false;
        }

    }


     protected function editForm($id)

    {   
        $user_id                      = Sentinel::getUser()->id;
        $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['albums']         = Album::where('id',$id)->first();
        $collection['user_procedure'] = User::find($user_id)->getProcedures()->get();
      return View::make('doctors.gallery_edit_form' , compact('collection'));
    }

public function updateAlbum (Request $request)
    {
        $input    = $request->all();
        $rules    = [
            'album_name' => 'required'];
        $messages = $this->validatorGeneral($input , $rules);
        $album    = Album::find($input['album_id']);  
        if ($messages->isEmpty())
            { 

            if(Input::file('album_image') != ''):
                    $image          =   $request->file('album_image');
                    $resizedImage   =   $this->resize($image, 100);
                    $extention      =  Input::file('album_image')->getClientOriginalExtension();
                        // $imageName = Input::file('album_image')->getClientOriginalName();
                    $imageName      = $resizedImage;
                    Input::file('album_image')->move('images/gallery/album/full_size', $imageName);
                        $input['album_image'] = $imageName;
                else: $input['album_image']   = $album->album_image;
                endif;
            $album->fill($input);
            $album->save(); 
        return Redirect::to('/doctor/gallery')->withSuccess('Record Saved.');
        }
        return Redirect::back()->withInput()->withErrors($messages);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
         $collection['Profile']       = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['categories']     = Catagory::get();

        return View::make('doctors.speciality_edit', compact('collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request)
    { 
        $input = $request->all(); 
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required'
        ];


        $messages = $this->validatorGeneral($request->all() , $rules); 
        $Profile         = Profile::find($input['profile_id']); 
            if ($messages->isEmpty())
            {
                if (@$input['avatar'] != '' && !empty($input['avatar']) && isset($input['avatar'])) {

                
               \File::delete('images/catalog/'.$Profile->avatar);                
                $imageName           = Input::file('avatar')->getClientOriginalName();
                Input::file('avatar')->move('images/catalog', $imageName);
                $input['avatar']     = $imageName;
              }
              else
              {
                 $input['avatar'] = $Profile->avatar;
 
              }

            User::where('id', $input['user_id'])
            ->update(['first_name' => $input['first_name'] ,'last_name'=>$input['last_name']]);
                $Profile->fill($input);
                $Profile->save();
            } 

        if ($messages->isEmpty())
        {
            return Redirect::to('/doctor/profile')->withSuccess('Record Updated Successfully.');
        }

        return Redirect::back()->withInput()->withErrors($messages);

    }


    public function settings ()
{

  $collection['Profile']               = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
         $collection['first_name']     = Sentinel::getUser()->first_name;
         $collection['last_name']      = Sentinel::getUser()->last_name;
         $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
         $collection['created_at']     = Sentinel::getUser()->created_at;
 return View::make('doctors.changepassword',compact('Profile','collection'));

}
  public function changepassword(Request $request)
    {            
      $input = Input::except('_token'); 
       
      $rules = array(       
          'password'              => 'required|min:6|confirmed',
          'password_confirmation' => 'required|min:6'         
          // required and has to match the password field
      );
       
     $messages = $this->validatorGeneral($input, $rules);

      if ($messages->isEmpty())
      {
        user::where('id', Sentinel::getUser()->id)
        ->update(array('password' => bcrypt($input['password']) 
                            ));  
        return Redirect::to('doctor/profile/settings')
        ->withSuccess('Password changed successfully.');
      }
      else
      {  
       return redirect()->back()->withErrors($messages);
          
      }
    
    
    }


// SHOW THE JOINING LIST OF DOCTORS TO MEMBER PROFILE.
    public function dortorsRequestToJoin()
    {
        $role    = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 

        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];

         $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['Profile'] = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
        $collection['doctors'] = Profile::where('id',$collection['Profile']->transfer_request_to_parent)->get();

          return View::make('doctors.request_to_join_clinic', compact('collection'));
        
    } 


    public function TransferAproveByDoctor()
    {    
     $Profile = Profile::select('id','parent_id','transfer_request_to_parent')->where('user_id', Sentinel::getUser()->id)->first();
        
        Profile::where('id', $Profile->id)
                    ->update(['transfer_request_to_parent' => 0,
                    'transfer_request_approved'=>1,
                    'parent_id'=>$Profile->transfer_request_to_parent,
                    'old_parent'=>$Profile->parent_id]);
        ProfileTransferHistoryModel::insert(
           ['profile_id'    => $Profile->id,
          'old_parent'         => $Profile->parent_id,
          'new_parent'         => $Profile->transfer_request_to_parent,
          'created_at'=> \Carbon\Carbon::now()->toDateTimeString(),
          'updated_at'=> \Carbon\Carbon::now()->toDateTimeString()]);         
        return redirect()->back()->withSuccess('Changes Saved.');
    }


     public function trasferHistory()
    {

       $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];

         $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['Profile'] = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
        $collection['doctors'] = Profile::where('id',$collection['Profile']->transfer_request_to_parent)->get();


      $collection['allhistory']  = DB::select('SELECT p.id userId,p.Practice_id user_practice,ptt.old_parent userOldParent,ptt.new_parent userNewParent,ptt.created_at trasferDate,pp.business_name newParentBusiness,ppp.business_name oldParentBusiness FROM profile p JOIN profile_to_transferhistory ptt ON ptt.profile_id =p.id JOIN profile pp ON ptt.new_parent=pp.id JOIN profile ppp ON ptt.old_parent=ppp.id WHERE p.id='.$collection['Profile']->id);   
       
    return View::make('doctors.trasferhistory', compact('collection'));

    }



    public function socialLinks()
    {
        $logedInUser = Sentinel::getUser()->id;
        $role    = Sentinel::findById($logedInUser)->roles->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['Profile']        = Profile::where('user_id', $logedInUser)->first();

        $collection['checkSocialLinks'] = DB::table('users_socials')->where('user_id',$logedInUser)->orderBy('sort_order')->get();
          return View::make('doctors.social_links_form', compact('collection'));

    }
    public function socialLinksSaved(Request $request)
    {
        $data = $request->all(); 
        $imageName = ''; 
        for($i = 0; $i<count($data['sociallinks']); $i++)
        {
            $imageName = '';
        if(isset($data['sociallinks'][$i]) && $data['sociallinks'][$i] != ''):

          if(isset($data['link_id'][$i]) && $data['link_id'][$i] != ''):

            $social_links = DB::table('users_socials')
            ->where('id',$data['link_id'][$i])->first(); 
         
            if(Input::file('socialicon')[$i] == ''):
             $imageName = $social_links->icon;
            else:

            \File::delete('images/socials/'.$social_links->icon);
             $imageName = Input::file('socialicon')[$i]->getClientOriginalName();
             Input::file('socialicon')[$i]->move('images/socials', $imageName); 

         endif;
        DB::table('users_socials')->where('id', $data['link_id'][$i])
             ->where('user_id', Sentinel::getUser()->id)->update(
           ['link'         => $data['sociallinks'][$i],
          'icon'         => $imageName,
          'sort_order'   => $data['sort_order'][$i]]);

          else:
             if(Input::file('socialicon')[$i] != ''):
             $imageName = Input::file('socialicon')[$i]->getClientOriginalName();
             Input::file('socialicon')[$i]->move('images/socials', $imageName);
         endif;

           DB::table('users_socials')->insert(
           ['user_id'    => Sentinel::getUser()->id,
          'link'         => $data['sociallinks'][$i],
          'icon'         => $imageName,
          'sort_order'   => $data['sort_order'][$i]]); 
         endif;


        endif;



        }
        return redirect()->back()->withSuccess('Changes Saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeSocials($id)
    {
        
            $social_links = DB::table('users_socials')
            ->where('id',$id)->first();

            \File::delete('images/socials/'.$social_links->icon);
         DB::table('users_socials')->where('id', $id)
         ->where('user_id', Sentinel::getUser()->id)->delete();

    }


    protected function validatorGeneral($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    } 



  public function checkStatusAppointment()
    {
      $dataId    =  Input::get('dataId');
      $dataValue =  Input::get('dataValue');
      DB::table('schedule_profile_to_patient')->where('id',$dataId)->update(['status'=>$dataValue]);
      switch ($dataValue) {
                      case 1:
                        echo 'Unapproved';
                        break;
                      case 2:
                        echo 'Approved';
                        break;
                      case 3:
                        echo 'Canceled';
                        break;
                      case 4:
                        echo 'Pending';
                        break;
                      default:
                         echo 'Pending';
                        break;
      }
    }

    public function GetAllAppointments()
  {
    $Profile       = Profile::where('user_id', Sentinel::getUser()->id)->first();
    $appointments  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.profile_doctor_id',$Profile->id)->get(); 
            $calendarAppointments = array(); 
            foreach($appointments as $a) {
              if($a->schedule_date < date("Y-m-d"))
              {
                if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                  
                     $backgroundColor = "#f39c12";
                     $borderColor     = "#f39c12";
                   
                }
              }
            elseif($a->schedule_date == date("Y-m-d"))
            {
               if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
              $backgroundColor   = "#dd4b39"; //red
              $borderColor       = "#dd4b39"; //red
           }
            }      

            else {

              //NEXT DAY date('Y-m-d', strtotime($a->schedule_date . ' +3 day')

              if((date("Y-m-d", time() + 86400) == $a->schedule_date) OR ((date("Y-m-d", time() + 172800) == $a->schedule_date))
                OR ((date("Y-m-d", time() + 259200) == $a->schedule_date)) OR ((date("Y-m-d", time() + 345600) == $a->schedule_date)))
                {    if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                     $backgroundColor = "#00a65a";
                     $borderColor     = "#00a65a";}
                }
                else { 
                   if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                     $backgroundColor = "#0073b7";
                     $borderColor     = "#0073b7";
                   }
                   }
               }


         
                    $event              = array(
                      'id'              => $a->id,
                      'title'           => $a->first_name.' '.$a->last_name,
                      'start'           => $a->schedule_date,
                      'end'             => $a->schedule_date,
                      'backgroundColor' => $backgroundColor, //yellow
                      'borderColor'     => $borderColor //yellow
                      );
                      array_push($calendarAppointments, $event);
   }

    return response()->json($calendarAppointments);
  }


   public function GetAvailableAppointments()
  {
    $Profile       = Profile::where('user_id', Sentinel::getUser()->id)->first();
    $appointments  = DB::table('schedule_profile')
                   ->where('profile_id',$Profile->id)
                   ->get(); 
            $calendarAppointments = array();
            $day_count = 0; 
            for($i = 0; $i<6;$i++):
            foreach($appointments as $a) {
              $from_current_date_tonext = date('Y-m-d', strtotime("+".$day_count." days"));

            $weekday = date('l', strtotime($from_current_date_tonext));
            $open    = $this->checkDayAvailable(strtolower($weekday),$Profile->id);
              if($open == 1){              
                  $weekday         = 'Available';
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
              }
              else {
                    $weekday           = 'Not Available';                 
                    $backgroundColor   = "#dd4b39"; //red
                    $borderColor       = "#dd4b39"; //red
                }
                $event                  = array(
                      'id'              => $a->id,
                      'title'           => $weekday,
                      'start'           => $from_current_date_tonext, 
                      'backgroundColor' => $backgroundColor,  
                      'borderColor'     => $borderColor,
                      'cust_date'       => $from_current_date_tonext
                      ); 
                array_push($calendarAppointments, $event);
                $day_count ++;
              }
 endfor;

    return response()->json($calendarAppointments);
  }



private function checkDayAvailable($day,$profile_id)
{
  $appointments  = DB::table('schedule_profile')
                   ->where('days', $day)
                   ->where('profile_id',$profile_id)
                   ->first(); 
  return $appointments->open;
}
  public function rescheduleAppointments($id)
  { 
    $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first();  
    $collection['first_name']     = Sentinel::getUser()->first_name;
    $collection['last_name']      = Sentinel::getUser()->last_name;
    $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
    $collection['created_at']     = Sentinel::getUser()->created_at;

    $collection['allAppointments']  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.schedule_time','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.id',$id)
            ->get(); 

      return View::make('doctors.rescheduleappointments',compact('collection'));


    } 






public function schedualAppointment(Request $request)
  {
    $message                 = '';
    $error                   = false;
    $data                    = $request->all();
    $data['user_patient_id'] = Sentinel::getUser()->id;
    $data['created_at']      = \Carbon\Carbon::now();
    $data['updated_at']      = \Carbon\Carbon::now();
    $data['schedule_date']   = date_create($data['schedule_date']);
 $collection['Profile']      = Profile::where('user_id', Sentinel::getUser()->id)->first();  
    $data['schedule_date']   = date_format($data['schedule_date'],"Y-m-d");
    $date                    = \Carbon\Carbon::createFromFormat("Y-m-d", $data['schedule_date']); 
    $checkScheduleOnDay = DB::table('schedule_profile')
                        ->where('days',''.strtolower($date->format("l")).'')
                        ->where('profile_id',$collection['Profile']->id)
                        ->where('open',1)
                        ->count();
  $checkApointMentQuota = DB::table('schedule_profile_to_patient')
                       ->where('profile_doctor_id',$collection['Profile']->id)
                       ->whereDate('schedule_date', '=', $data["schedule_date"])
                       ->count();
  $checkApointAlready = DB::table('schedule_profile_to_patient')
                     ->where('profile_doctor_id',$collection['Profile']->id)
                     ->where('user_patient_id',21) 
                     ->whereDate('schedule_date', '=', $data["schedule_date"])->count();
    $checkApointOnthisDate = DB::table('schedule_profile_to_patient') 
                           ->where('profile_doctor_id',$collection['Profile']->id) 
                           ->whereDate('schedule_date', '=', $data["schedule_date"])
                           ->lists('schedule_time'); 
 

  if($checkScheduleOnDay == 0):
    $message = 'No scheduale available on this date.';
    $error   = true; 
  endif; 
  if($checkApointMentQuota > 50):
    $message = 2;
    $error   = true;
    endif;
    if($checkApointAlready > 0):
    $message = 3;
    $error   = true;
    endif;
    if(date('Y-m-d') > $data["schedule_date"]):
    $message = 4;
    $error   = true;
    endif; 
    if($error == false):
      unset($data['doctors_id']);
      $getScheduleOnDay = DB::table('schedule_profile')->select('from','to','break_time','break_duration')
                     ->where('days',''.strtolower($date->format("l")).'')
                     ->where('profile_id',$collection['Profile']->id)
                     ->where('open',1)->first();
    //DB::table('schedule_profile_to_patient')->insert($data);
    //$message = 6; 
    $duration  = 15;  // split by 30 mins                          
    $array_of_time      = array ();
    $array_ofbreak_time = array ();
    $start_time         = strtotime ($getScheduleOnDay->from); //change to strtotime
    $end_time           = strtotime ($getScheduleOnDay->to); //change to strtotime
    $add_mins           = $duration * 60;
    $gapArray           = array();
    $html               = '<tr>';
    $tdcounter          = 0;
      while ($start_time <= $end_time) // loop between time
      {
        $check_time_forbreak    = date ("H:i", $start_time); 
        if($check_time_forbreak == date ("H:i", strtotime ($getScheduleOnDay->break_time))){ 
        $bstarttime             = $getScheduleOnDay->break_time; 
        preg_match("/([0-9]{1,2}):([0-9]{1,2})/", $getScheduleOnDay->break_duration, $match);
        $hour                   = $match[1];
        $min                    = $match[2];

        $bendtime = date('H:i',strtotime('+'.$hour.' hour +'.$min.' minutes',strtotime($getScheduleOnDay->break_time)));

        $gapArray   = $this->returnBreakArray($bstarttime,$bendtime,$add_mins);
        }
        if (!in_array($check_time_forbreak, $gapArray)){
            // $array_of_time [] = date ("H:i", $start_time);
        if(count($checkApointOnthisDate) > 0){
          if (!in_array($check_time_forbreak, $checkApointOnthisDate)){
             if($tdcounter%8 == 0 ){
              $html .= '</tr><tr>';
              $tdcounter=0;
             }
             $html .= '<td><a href="'.url().'/" data-id="'.$collection['Profile']->id.'" data-value="'.date ("H:i", $start_time).'" data-date="'.$data["schedule_date"].'" class="book-appointment">'.date ("H:i", $start_time).'</a></td>';
              $tdcounter ++;
            }// end if
          }
          else {
             if($tdcounter%8 == 0 ){
              $html .= '</tr><tr>';
              $tdcounter=0;
             }
             $html .= '<td><a href="'.url().'/" data-id="'.$collection['Profile']->id.'" data-value="'.date ("H:i", $start_time).'" data-date="'.$data["schedule_date"].'" class="book-appointment">'.date ("H:i", $start_time).'</a></td>';
              $tdcounter ++;
          }
        }// end of if.
         $start_time += $add_mins; // to check endtime
       
      }
    $html               .= '</tr>';

    $message = $html;
    endif;

    return $message;
  }


// Get available days
    function GetAvailableDays() {
        //return response()->json(BookingDateTime::all());
        $message                 = '';
    $error                   = false;
    $data                    = Input::all();
    $data['user_patient_id'] = Sentinel::getUser()->id;
    $data['created_at']      = \Carbon\Carbon::now();
    $data['updated_at']      = \Carbon\Carbon::now();
    //$data['schedule_date']   = date_create($data['schedule_date']);

    //$data['schedule_date']   = date_format($data['schedule_date'],"Y-m-d");
   // $date                    = Carbon::createFromFormat("Y-m-d", $data['schedule_date']);   
  $checkApointAlready = DB::table('schedule_profile_to_patient')
                     ->where('profile_doctor_id',$data['profile_doctor_id'])
                     ->where('user_patient_id',$data['user_patient_id']) 
                     ->whereDate('schedule_date', '=', $data["schedule_date"])->count(); 
          if($checkApointAlready > 0):
          $message = 1;
          $error   = true;
          endif;
          if(date('Y-m-d') > $data["schedule_date"]):
          $message = 2;
          $error   = true;
          endif;
         // if($data['user_patient_id'] == $data['doctors_id']):
         // $message = 3;
         // $error   = true;
         // endif;
          if($error == false):
            DB::table('schedule_profile_to_patient')
            ->where('id', $data["patient_id"])
            ->where('profile_doctor_id', $data['profile_doctor_id'])
            ->update(
            [  'schedule_date' => $data["schedule_date"],
               'schedule_time' => $data["schedule_time"],
               'created_at'    => $data['created_at'],
               'updated_at'    => $data['updated_at']
            ]);
            $message = 3;
            endif;
         return $message;
    }


private function returnBreakArray($bstarttime = '02:00',$bendtime = '03:30',$add_mins = 30)
{ 
    $bstart_time    = strtotime ($bstarttime); //change to strtotime
    $bend_time      = strtotime ($bendtime); //change to strtotime

    while ($bstart_time <= $bend_time) // loop between time
    {
       $array_ofbreak_time[] = date ("H:i", $bstart_time);
       $bstart_time += $add_mins; // to check endtime
    }
    return $array_ofbreak_time;

}

}
