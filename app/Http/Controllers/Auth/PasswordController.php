<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords; 
use Input;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }



    public function postEmail(Request $request)
{
    $this->validate($request, ['email' => 'required']);

    $response = Password::sendResetLink($request->only('email'), function($message)
    { 
        $message->from('mutahir@pkteam.com');
    });

    switch ($response)
    {
        case PasswordBroker::RESET_LINK_SENT:
            return redirect()->back()->with('status', trans($response));

        case PasswordBroker::INVALID_USER:
            return redirect()->back()->withInput()->withErrors(['email' => trans($response)]);
    }
}
}
