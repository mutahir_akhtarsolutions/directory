<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	 */

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */

	//protected $redirectTo = '/account';
	protected $redirectAfterLogout = '/auth/login';

	public function __construct() {

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data) {

		return Validator::make($data, [
				//'name' => 'required|max:255',
				'first_name' => 'max:255',
				'last_name'  => 'max:255',
				'email'      => 'required|email|max:255|unique:users',
				'password'   => 'required|confirmed|min:6',
			]);
	}

	/**
	 * Handle a registration request for the application by using sentinel framework.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postRegister(Request $request) {

		$validator = $this->validator($request->all());
		if ($validator->fails()) {
			$this->throwValidationException(
				$request, $validator
			);
		}

		//@todo: this should be move to config
		$autoActivate = false;

		if ($user = Sentinel::register($request->all(), $autoActivate)) {
			$patientsRole = Sentinel::findRoleByName('Patient');

			$patientsRole->users()->attach($user);

			if (false === $autoActivate) {
				$activation = Activation::create($user);
				$code       = $activation->code;
				$sent       = \Mail::send('sentinel.emails.activate', compact('user', 'code'), function ($m) use ($user) {
						$m->from('junaid@pkteam.com', 'Nation Wide Physicians');
						$m->to($user->email)->subject('Activate Your Account');
					});
				if ($sent === 0) {
					return redirect()->back()->withInput()->withErrors('Failed to send activation email.');
				}
				return redirect($this->redirectPath())
				                     ->withSuccess('Your account was successfully created. You might login now.')
				                     ->with('userId', $user->getUserId());
			} else {
				$credentials = $this->getCredentials($request);
				Sentinel::authenticate($credentials);

				return redirect($this->redirectPath());
			}
		}
		return redirect()->back()->withInput()->withErrors('An error occured while registering.');
	}

	/**
	 * Handle a login request to the application by using sentinel framework.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request) {

		try {

			$this->validate($request, [
					$this->loginUsername()=> 'required', 'password' => 'required',
				]);

			// If the class is using the ThrottlesLogins trait, we can automatically throttle
			// the login attempts for this application. We'll key this by the username and
			// the IP address of the client making these requests into this application.
			$throttles = $this->isUsingThrottlesLoginsTrait();

			if ($throttles && $this->hasTooManyLoginAttempts($request)) {
				return $this->sendLockoutResponse($request);
			}

			$credentials = $this->getCredentials($request);

			if (Sentinel::authenticate($credentials, $request->has('remember'))) {
				return $this->handleUserWasAuthenticated($request, $throttles);
			}

			$errors = $this->getFailedLoginMessage();
		}
		 catch (NotActivatedException $e) {
			return redirect('reactivate')->with('user', $e->getUser())->withErrors('Account is not activated!');
		}
		 catch (ThrottlingException $e) {
			$delay  = $e->getDelay();
			$errors = "Your account is blocked for {$delay} second(s).";
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		if ($throttles) {
			$this->incrementLoginAttempts($request);
		}

		return redirect($this->loginPath())
		                     ->withInput($request->only($this->loginUsername(), 'remember'))
		->withErrors([
				$this->loginUsername()=> $errors,
			]);
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout() {
		Sentinel::logout();

		return redirect(property_exists($this, 'redirectAfterLogout')?$this->redirectAfterLogout:'/');
	}
}
