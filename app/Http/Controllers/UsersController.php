<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Form;
use View; 
use Input;
use App\Profile;
use Validator;
use Redirect;
use DB;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\User;
class UsersController extends AuthorizedController {

    /**
     * Holds the Sentinel Users repository.
     *
     * @var \Cartalyst\Sentinel\Users\EloquentUser
     */
    protected $users;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->users = Sentinel::getUserRepository();
    }

    /**
     * Display a listing of users.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $users = $this->users->createModel()->paginate();
  
       // User::with('roles')->get();

        return View::make('sentinel.users.index', ['users' => $users]);
    }  
    /**
     * Show the form for creating new user.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        return $this->processForm('create');
    }

    /**
     * Show the form for updating user.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    /**
     * Handle posting of the form for updating user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        if ($user = $this->users->createModel()->find($id))
        {
            $user->delete();

            return Redirect::to('users');
        }

        return Redirect::to('users');
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        if ($id)
        {
            if ( ! $user = $this->users->createModel()->find($id))
            {
                return Redirect::to('users');
            }
        }
        else
        {
            $user = $this->users->createModel();
        }

        $roles = Sentinel::getRoleRepository()->createModel()->lists('name', 'id');

        return View::make('sentinel.users.form', compact('mode', 'user', 'roles'));
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {
        $input = array_filter(Input::all());

        $rules = [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required|unique:users'
        ];

        if ($id)
        {
            $user = $this->users->createModel()->find($id); 

            $rules['email'] .= ",email,{$user->email},email";

            $messages = $this->validateUser($input, $rules);

            if ($messages->isEmpty())
            { 
                DB::table('role_users')->where('user_id', $id)
                     ->update(['role_id' => $input['role']]);
                $this->users->update($user, $input);
            }
        }
        else
        {
            $messages = $this->validateUser($input, $rules);

            if ($messages->isEmpty())
            {
                $user = $this->users->create($input);

                $code = Activation::create($user);

                Activation::complete($user, $code);

              DB::table('role_users')->insert(
              ['user_id' => $user->id, 'role_id' => $input['role']]);
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::to('users')->withSuccess('User was successfully saved');
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a user.
     *
     * @param  array  $data
     * @param  mixed  $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateUser($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

    // EDIT PROFILE CODE STARTS HERE.

    public function editProfile($id)
    {  
        $collection['countries']      = DB::table('country')->get();
        $collection['states']         = DB::table('state')->get();
        $collection['cities']         = DB::table('city')->get();

        $collection['Profile'] = DB::table('profile')->where('user_id',$id)->first();

        return View::make('sentinel.users.profile', compact('user','collection'));

    }
     public function updateProfile($id)

    {

     $rules = [
            'phone' => 'required',
            'mobile'  => 'required' 
        ]; 
            $input    = array_filter(Input::all()); 
            $messages = $this->validateUser($input, $rules);
            $Profile  = Profile::find($input['profile_id']); 
            if ($messages->isEmpty())
            {

            if (@$input['avatar'] != '' && !empty($input['avatar']) && isset($input['avatar'])) {

                
               \File::delete('images/catalog/'.$Profile->avatar);                
                $imageName           = Input::file('avatar')->getClientOriginalName();
                Input::file('avatar')->move('images/catalog', $imageName);
                $input['avatar']     = $imageName;
              }
              else
              {
                 $input['avatar'] = $Profile->avatar;
 
              } 
                $Profile->fill($input);
                $Profile->save();
             return Redirect::to('/users')->withSuccess('Record updated successfully.');
            } 

        return Redirect::back()->withInput()->withErrors($messages);
    }


     public function checkStatusUsers()
        {
          $dataId      =  Input::get('dataId');
          $dataValue   =  Input::get('dataValue');
          $user        = Sentinel::findById($dataId);
          $activation  = Activation::exists($user);
          Activation::removeExpired();
          if($activation):
            echo ' Activation Link Already Sent.';
        else:
              if (Activation::completed($user))
            {
                return ' User is already activated.';
            }else{
              $activation = Activation::create($user);
              echo ' Activation Link Sent Successfully.';
          }
          endif;
      }

       public function removeActivationLink()
        {
          $dataId      =  Input::get('dataId'); 
          $user        = Sentinel::findById($dataId);
          $activation  = Activation::exists($user);
          Activation::removeExpired();
          if($activation): 
            $activation  = Activation::remove($user);

            echo ' Activation has been removed.';
        else:  
             $activation  = Activation::remove($user);
              
              echo ' Activation deactivated and removed';
          endif;
      }

       public function manualActivateUser()
        {
            $dataId             =  Input::get('dataId'); 
            $user               = Sentinel::findById($dataId);
            $activation_exists  = Activation::exists($user);
            if($activation_exists):

                if (Activation::complete($user, $activation_exists->code))
                        {
                            return ' Activation was successfull';
                        }
                        else
                        {
                            return ' Activation not found or not completed.';
                        }

            else:
                    if (Activation::completed($user))
                        {
                            return ' Already Activated.';
                        }
                        else
                        {
                            return ' Activation Not Found.';
                        }
            endif;
        }
          

}
