<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Reviews;
use Input;
use DB;
use View;
use Redirect;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $collection['allMyComments'] = DB::table('view_rating_doctors')->paginate();  
        return View::make('ratings.index',compact('collection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo '<pre>';
        print_r(Input::all());
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $commenter_id          = Sentinel::getUser();
        $commenter_id          = $commenter_id->id;
        $input['doctor_id']    = $request->doctor_id;
        $checkUserComments     = Reviews::where('doctor_id',$input['doctor_id'])
                                 ->where('user_id', $commenter_id)->count(); 
        if($checkUserComments == 0)
        {
            $input['behaviour']    = $request->rating;
            $input['availability'] = $request->rating1;
            $input['enviroment']   = $request->rating2;
            $average               = ($input['behaviour']+$input['availability']+$input['enviroment'])/3;
            
            $input['user_id']      = $commenter_id;
            $input['rating']       = $average;
            $input['comment']      = $request->comment;
            $reviewObj             = Reviews::create($input);
            $insertedId            = $reviewObj->id;
            $input['review_id']    = $insertedId;
            DB::table('users_to_reviews')->insert(['doctor_id' => $input['doctor_id'],
            'user_id'      => $input['user_id'],
            'review_id'    => $insertedId,
            'behaviour'    => $input['behaviour'],
            'availability' => $input['availability'],
            'enviroment'   => $input['enviroment'],
            'created_at'   => date("Y-m-d H:i:s")
            ]);
        return redirect()->back();
    }// end of checking the comment if previously inserted.
    else echo 'Already Rated.';
    return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $collection['allMyComments'] = DB::table('view_rating_doctors')
        ->where('id',$id)->first(); 
        $html = '
          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr> 
                    <th colspan="2">Full Information</th> 
                  </tr>
                </thead>
                <tbody>
                  <tr> 
                    <th>Doctor Name</th>
                    <td>'.$collection['allMyComments']->doctor_name.'</td>
                  </tr>
                  <tr>
                    <th>Commenter Name</th>
                    <td>'.$collection['allMyComments']->visitor_name.'</td> 
                  </tr>
                  <tr>
                    <th>Ratting</th>
                    <td>'.$collection['allMyComments']->rating.'</td> 
                  </tr>
                  <tr> 
                    <th>Comments</th>
                    <td>'.$collection['allMyComments']->comment.'</td>
                  </tr>
                   <tr> 
                    <th>Status</th>
                    <td>';
                   // $x = $valid ? 'yes' : 'no';
                    $x = $collection['allMyComments']->approved == 1 ? 'Approved': 'Disapproved';

                    $html .= $x.'</td>
                  </tr>
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->'; 
          echo $html;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $collection['allMyComments'] = DB::table('view_rating_doctors')
        ->where('id',$id)->first();  
       echo json_encode($collection['allMyComments']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {  
        Reviews::where('doctor_id', $request->input('doctor_id'))
        ->where('user_id', $request->input('user_id'))
        ->update(array('reviews_type'=>'Archive')); 

        $reviews = Reviews::create($request->all());
        return Redirect::back()->withSuccess('Record updated succcessfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aproveDisaprove()
    {   
        $input = Input::all(); 
        Reviews::where('id', $input['review_id'])
                     ->update(['approved' => $input['approve']]);

        return redirect()->back();
    }




      protected function validateUser($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}
