<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use App\Profile; 
use View;
class MemeberController extends Controller
{
    //getRegister method for Memeber.
    public function getRegister(){
     return View::make('members.register', compact('row'));
    }

    /**
     * Handle a registration request for the application by using sentinel framework.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function postRegister(Request $request)
    { 
        $inputs = $request->all();
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        //@todo: this should be move to config
        $autoActivate = false;

        if( $user = Sentinel::register($request->all(), $autoActivate ) )
        {

            //Sentinel::getRoleRepository()->findById($inputs['role']); 
            //$memberRole = Sentinel::findRoleByName('member');
            $memberRole = Sentinel::findRoleById($inputs['role']);
            $memberRole->users()->attach($user);
            Profile::insert(['user_id' => $user->getUserId()]);
            if( false === $autoActivate )
            {
                $activation = Activation::create($user);
                $code       = $activation->code;
                $sent       = \Mail::send('sentinel.emails.activate', compact('user', 'code'), function($m) use ($user)
                {
                    $m->from('mutahir@pkteam.com', 'Nation Wide Physicians');
                    $m->to($user->email)->subject('Activate Your Account');
                });
                if ($sent === 0)
                {
                    return redirect()->back()->withInput()
                    ->withErrors('Failed to send activation email.');
                }
                return redirect('auth/login')
                    ->withSuccess('Your account was successfully created. You might login now.')
                    ->with('userId', $user->getUserId());
            }else{
                $credentials = $this->getCredentials($request);
                Sentinel::authenticate($credentials);

                return redirect($this->redirectPath());
            }
        }
        return redirect()->back()->withInput()
        ->withErrors('An error occured while registering.');
    }

    public function getprofile(){
      return View('profile');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {

        return Validator::make($data, [
            //'name' => 'required|max:255',
            'first_name' => 'max:255',
            'last_name' => 'max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }





}
