<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller; 
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\Helpers\MyHelper;
use View;
use App\Procedure;
use App\Catagory;
    class ProceduresController extends Controller
    {    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // protected $roles;
     public function __construct()
    { 

       // $this->roles = Sentinel::getRoleRepository()->createModel();
    }

    public function index()
    {
        //
         return View('procedure.index', array( 'procedures' => procedure::paginate(10)));

         //return MyHelper::helperShowAll($template_blade = 'procedure.index' , $table_name = 'procedures' ,  $dbobject = 'procedures');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
        $rules = ['procedure_name' => 'required|unique:procedures'];
        return MyHelper::helperProcessForm('create', $table_name = 'procedures' , $routes = 'procedures', $rules,  $id = null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return $this->showForm('update', $id);
       // return MyHelper::helperShowForm('update',$table_name = 'procedures',$template_blade = 'procedure.form',
         //   $routes = 'procedures', $id , $array_name_passtoview = 'procedures');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 

         $rules = ['procedure_name' => 'required'];

        return MyHelper::helperProcessForm('update', $table_name = 'procedures' , $routes = 'procedures', $rules,  $id , $field_id = 'id');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return MyHelper::helperDelete($id , $field_id = 'id' , $table_name = 'procedures', $routes= 'procedures');
    }

     public static function showForm($mode, $id = null)
    {
        $procedures = null;
        $categories = Catagory::orderBy('name')->get();
        if ($id)
        {
            if ( ! $procedures = Procedure::find($id))
            {
                return Redirect::to('procedures');
            }
        } 
        return View::make('procedure.form', compact('mode', 'procedures','categories'));
    }

 }
