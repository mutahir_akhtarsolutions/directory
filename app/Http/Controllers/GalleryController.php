<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Validator;
use Cartalyst\Sentinel\Native\Facades\Sentinel; 
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use DB;
use View;
use Input; 
use Redirect;  
use App\Album;
use App\Models\Image;
use App\Logic\Image\ImageRepository;
use Intervention\Image\Facades\ImageFacades; // Use this if you want facade style code


class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $image;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->image = $imageRepository;
    }

    public function index($id)
    {
        $user_id                      = Sentinel::getUser()->id;
        $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['data']           = Album::find($id)->getGalleries()->orderBy('sort_order')->get();
        return View::make('doctors.gallery_manager', compact('collection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
         $user_id                      = Sentinel::getUser()->id;
        $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
      return View::make('doctors.gallery_manage_form' , compact('collection'));
    }

     public function postUpload()
    {
        $photo = Input::all();
        $response = $this->image->upload($photo);
        return $response;

    }

     protected function editForm($id)

    {   
        $user_id                      = Sentinel::getUser()->id;
        $collection['Profile']        = Profile::where('user_id', $user_id)->first(); 
        $collection['first_name']     = Sentinel::getUser()->first_name;
        $collection['last_name']      = Sentinel::getUser()->last_name;
        $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
        $collection['created_at']     = Sentinel::getUser()->created_at;
        $collection['images']         = Image::where('id',$id)->first(); 
      return View::make('doctors.gallery_manage_edit_form' , compact('collection'));
    }

public function updateImage (Request $request)
    {
        $input    = $request->all();
        $rules    = [
            'filename' => 'required'];
        $messages = $this->validatorGeneral($input , $rules);
        $album    = Image::find($input['image_id']);  
        if ($messages->isEmpty())
            { 

            if(Input::file('original_name') != ''):
                    $image          =   $request->file('original_name');
                    $resizedImage   =   $this->resize($image, 100);
                    $extention      =  Input::file('original_name')->getClientOriginalExtension();
                        // $imageName = Input::file('album_image')->getClientOriginalName();
                    $imageName      = $resizedImage;
                    Input::file('original_name')->move('images/gallery/full_size', $imageName);
                        $input['original_name'] = $imageName;
                else: $input['original_name']   = $album->original_name;
                endif;
            $album->fill($input);
            $album->save(); 
        return Redirect::to('/doctor/managegalleries/'.$input['album_id'])->withSuccess('Record Saved.');
        }
        return Redirect::back()->withInput()->withErrors($messages);

    }



     private function resize($image, $size)
    {
        try 
        {
            $extension      =   $image->getClientOriginalExtension();
            $imageRealPath  =   $image->getRealPath(); 
            $thumbName      =   rand(111111,999999).'_'. $image->getClientOriginalName();
            
            //$imageManager = new ImageManager(); // use this if you don't want facade style code
            //$img = $imageManager->make($imageRealPath);
        
            $img = ImageFacades::make($imageRealPath); // use this if you want facade style code
            $img->resize(intval($size), null, function($constraint) {
                 $constraint->aspectRatio();
            });
            $img->save('images/gallery/icon_size/'. $thumbName);
            return $thumbName;
        }
        catch(Exception $e)
        {
            return false;
        }

    }

    public function deleteUpload()
    {

        $filename = Input::get('id');

        if(!$filename)
        {
            return 0;
        }

        $response = $this->image->delete( $filename );

        return $response;
    }

     public function deleteImage()
    {   $id = Input::get('id');
        $data = Image::select('original_name')->where('id', '=', $id)->first();
        \File::delete('images/gallery/full_size/'.$data->original_name);
        \File::delete('images/gallery/icon_size/'.$data->original_name);
        Image::where('id', '=', $id)->delete();
    }

    
public function sortImages(Request $request)
{

     $sectionids = $request->sectionsid;
     $count = 1;
        if (is_array($sectionids)) {
            foreach ($sectionids as $sectionid) {
            Image::where('id', $sectionid)
            ->update(['sort_order' => $count]);
            $query  = "Update sections SET display_order = $count";
            $query .= " WHERE id='".$sectionid."'";
            // your DB query here
            $count++;            
            }
           
            echo '{"status":"success"}';
        } else {
            echo '{"status":"failure", "message":"No Update happened. Could be an internal error, please try again."}';
        }
}

public function sortAlbums(Request $request)
{

     $sectionids = $request->sectionsid;
     $count = 1;
        if (is_array($sectionids)) {
            foreach ($sectionids as $sectionid) {
            Album::where('id', $sectionid)
            ->update(['sort_order' => $count]);
            $query  = "Update sections SET display_order = $count";
            $query .= " WHERE id='".$sectionid."'";
            // your DB query here
            $count++;            
            }
           
            echo '{"status":"success"}';
        } else {
            echo '{"status":"failure", "message":"No Update happened. Could be an internal error, please try again."}';
        }
}


  public function deleteAlbums()
    {   $id = Input::get('id'); 
         $data = Album::where('id', '=', $id)->delete();
        $data = Image::where('album_id', '=', $id)->delete();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     protected function validatorGeneral($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    } 

}
