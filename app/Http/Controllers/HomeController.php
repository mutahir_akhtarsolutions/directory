<?php namespace App\Http\Controllers;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use DB;
use Auth;
use View;
use App\Profile; 
use App\Country;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
	if ($user = Sentinel::getUser())
    {
		if ( \Cartalyst\Sentinel\Native\Facades\Sentinel::hasAccess('admin') !== false ) {

		    return view('home');
	}
	elseif ($user->inRole('patient'))// LATERLY CONVERT THIS TO SWITCH STATEMENT.
        { 
          $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
		  $collection['first_name']     = Sentinel::getUser()->first_name;
		  $collection['last_name']      = Sentinel::getUser()->last_name;
		  $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
		 $collection['created_at']      = Sentinel::getUser()->created_at;
		 $collection['allMyComments']   = DB::table('view_rating_doctors')->where('user_id', Sentinel::getUser()
        ->id)->get(); 			
            return View::make('patients.membermanagment', compact('collection'));
        }
        elseif ($user->inRole('doctor'))// LATERLY CONVERT THIS TO SWITCH STATEMENT.
        {
        $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
		$collection['first_name']     = Sentinel::getUser()->first_name;
		$collection['last_name']      = Sentinel::getUser()->last_name;
		$collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];

		$collection['created_at']     = Sentinel::getUser()->created_at;

        $collection['allMyComments']  = DB::table('view_rating_doctors')
                                      ->where('doctor_id', Sentinel::getUser()->id)
                                      ->where('reviews_type','<>','Archive')->get(); 

		return View::make('doctors.index', compact('collection'));
        }
	else {
		if (empty(DB::table('profile')->where('user_id', '=', Sentinel::getUser()->id)->get())) {

			$country = DB::table('country')->get();
			$state   = DB::table('state')->get();
			$cities  = DB::table('city')->get();
            $data    = array('mem_id'=>Sentinel::getUser()->id, 'first_name'=>
            	Sentinel::getUser()->first_name, 'last_name'=>Sentinel::getUser()
            	->last_name);
			 $mode = 'create';
			 //return View::make('buildMemberProfile')->with('data', $data, 'mode');
			return View::make('buildMemberProfile', compact('mode', 'data', 'country', 'state','cities'));
    }else {
	    	$Profile = Profile::where('user_id', Sentinel::getUser()->id)->first();
	        $role    = Sentinel::findRoleById($Profile->id); 
			$doctors = Profile::where('parent_id',$Profile->id)->get();
			//return View::make('member')->with('Profile', $Profile,'doctors');
			return View::make('members.membermanagment', compact('Profile', 'doctors'));
		}
	}
}
	// END LOGIN CHECK
	}

	//function for coming soon page
		public function comingSoon($value='')
		{
			$business_Profile	= Profile::where('user_id', Sentinel::getUser()->id)->first();
			return View::make('comingSoon', compact('business_Profile'));

		}

}
