<?php
 namespace App\Http\Controllers;

use Illuminate\Pagination\Paginator; 
use Illuminate\Pagination\LengthAwarePaginator;
/*
 * PHP Pagination Class
 *
 * @author David Carr - dave@daveismyname.com - http://www.daveismyname.com
 * @version 1.0
 * @date October 20, 2012
 http://laravel.io/forum/11-13-2014-laravel-5-pagination
 */
class PaginatorController { 
        
public static function makeLengthAware($collection, $total, $perPage, $appends = array())
{
    $paginator = new LengthAwarePaginator(
    $collection, 
    $total, 
    $perPage, 
    Paginator::resolveCurrentPage(), 
    ['path' => Paginator::resolveCurrentPath()]
  );

  if($appends) $paginator->appends($appends);

  return str_replace('/?', '?', $paginator->render());
}
}