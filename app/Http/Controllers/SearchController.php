<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use View;
use App\Profile;
use App\Catagory;
use Input;
use App\Http\Controllers\PaginatorController;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    } 
     public function doctorSearchByProcedures($searchKey)
    { 
      $query_string             = array();
      $input                    = Input::all();  
      $specialities_ids         = @$input['specialities'];      
      $query_string_location    = @$input['location'];
      $city                     = @$input['state_citie'];
      $collection['doctors_names_autocomplete'] = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`, `wholename`'))
      ->orderBy('wholename')->groupBy('id')->get(); 
      $query_variable   = ' procedure_id = '.$searchKey.' ';
      $collection['catagories'] = Catagory::where('parent_id', 0)->orderBy('name')->get();
      $collection['locations']  = DB::table('state')->orderBy('state_name')->get();
      
        $sort_by          = @Input::get('sort_by');
      //FIRST SELECT ALL USERS IN LISTING PROCEDURES.
        $user_ids =  DB::table('user_procedures')
                    ->select('user_id')
                    ->where('procedure_id',$searchKey)
                    ->orderBy('user_id')
                    ->lists('user_id','user_id');

        if(isset($query_string_location) && $query_string_location != 0)
        {
           $collection['state_cities']      =  DB::table('city')
          ->where('state_id',$query_string_location)->orderBy('city_name')->get();
           if(isset($city) && $city != 0)
           {
             $query_variable            .= ' AND city_id='.$city.'';
             $query_string['state_citie'] = $city;
           }else {          
             $query_variable            .= ' AND state_id='.$query_string_location.'';
           }
            $query_string['location'] = $query_string_location;
        }
      //FILTER CATEGORY
      if (isset($specialities_ids) && $specialities_ids !='') {
        $query_variable .=' AND specialities_id IN ('.$specialities_ids.') ';
        $query_string['specialities'] = $specialities_ids; 
       } 

       
       $order = 'ASC';
       if(isset($sort_by)){

           switch($sort_by){
           case 'rating':
           $query_string['sort_by'] = $sort_by;
            $order = 'DESC';
           break;
           case 'wholename':
           $query_string['sort_by'] = $sort_by;
           $order = 'ASC';
           break;

         }

        }else $sort_by = 'id'; 

        
        $collection['users'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,`image_of_doctor`,`education`,`contact_dr_phone_number`,`parent_id`, `wholename`,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))->whereRaw($query_variable)->groupBy('id')->orderBy($sort_by, $order)->paginate(6); 
          $collection['users']->appends($query_string);
          //query builder ENDS

        $collection['doctors'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get(); 
            $collection['news'] = DB::table('news')
              ->join('news_translations', 'news.id', '=','news_translations.news_id')
              ->select('news.*', 'news_translations.title', 'news_translations.content','news_translations.language')
              ->paginate(3);  
  
      return View::make('searchbyprocedure', compact('collection'));
    }
    public function doctorSearchByCategory($searchKey)
    { 
      $query_string             = array();
      $input                    = Input::all();  
      $procedures_ids           = @$input['procedures']; 
      $specialities_ids         = @$input['specialities'];      
      $query_string_location    = @$input['location'];
      $city                     = @$input['state_citie'];
      $query_variable           = ' 1 '; 
      $collection['doctors_names_autocomplete'] = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`, `wholename`'))
      ->orderBy('wholename')->groupBy('id')->get();
      $collection['catagories'] = Catagory::where('parent_id', 0)->orderBy('name')->get();
      $collection['locations']  = DB::table('state')->orderBy('state_name')->get();
       
        $sort_by          = @Input::get('sort_by'); 
        //FIRST SELECT ALL USERS IN LISTING CATEGORY.
        $user_ids =  DB::table('users_specialities')
                    ->select('users_id')
                    ->where('specialities_id',$searchKey)
                    ->orderBy('users_id')
                    ->lists('users_id', 'users_id');



       if (isset($specialities_ids) && $specialities_ids !='') {
        $query_variable .=' AND specialities_id IN ('.$specialities_ids.') ';
        $query_string['specialities'] = $specialities_ids;
         // users in specific category. 
        }else {
          
            $query_variable           = '  specialities_id='.$searchKey;
        }  


        if(isset($query_string_location) && $query_string_location != 0)
        {
           $collection['state_cities']      =  DB::table('city')
          ->where('state_id',$query_string_location)->orderBy('city_name')->get();
           if(isset($city) && $city != 0)
           {
             $query_variable            .= ' AND city_id='.$city.'';
             $query_string['state_citie'] = $city;
           }else {          
             $query_variable            .= ' AND state_id='.$query_string_location.'';
           }
            $query_string['location'] = $query_string_location;
        }

      if (isset($procedures_ids) && $procedures_ids !='') {
        $query_variable .=' AND procedure_id IN ('.$procedures_ids.') ';
        $query_string['procedures'] = $procedures_ids;
 
       }
        
       $order = 'ASC';
       if(isset($sort_by)){

           switch($sort_by){
           case 'rating':
           $query_string['sort_by'] = $sort_by;
            $order = 'DESC';
           break;
           case 'wholename':
           $query_string['sort_by'] = $sort_by;
           $order = 'ASC';
           break;

         }

        }else $sort_by = 'id'; 

           $collection['users'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,`image_of_doctor`,`education`,`contact_dr_phone_number`,`parent_id`, `wholename`,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))->whereRaw($query_variable)->groupBy('id')->orderBy($sort_by, $order)->paginate(6); 
          $collection['users']->appends($query_string);
            //query builder ENDS 

        $collection['procedures'] = DB::table('procedures') 
                   ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                     ->select('procedures.*')
                     ->whereIn('user_procedures.user_id', $user_ids)
                     ->groupBy('procedures.id')->get();

        $collection['child_specilities'] = Catagory::where('parent_id', $searchKey)
        ->orderBy('name')->get();
        $collection['doctors']  = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get(); 
            $collection['news'] = DB::table('news')
              ->join('news_translations', 'news.id', '=','news_translations.news_id')
              ->select('news.*', 'news_translations.title', 'news_translations.content','news_translations.language')
              ->paginate(3);  
      return View::make('searchbycategory', compact('collection'));
    }


      public function doctorSearchByStates($searchKey)
    {
        $city_ids         = @Input::get('state_citie'); 
        $catagegory       = @Input::get('catagory');  
        $query_variable   = 'state_id = '.$searchKey.' ';
        $specialities_ids = @Input::get('specialities');
        $procedures_ids   = @Input::get('procedures'); 
        $query_string     =  array(); 
        $sort_by          = @Input::get('sort_by');
        $collection['doctors_names_autocomplete'] = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`, `wholename`'))
      ->orderBy('wholename')->groupBy('id')->get();

        $collection['cities'] = DB::table('city')
                          ->select('id','city_name')
                          ->orderBy('city_name')
                          ->where('state_id', $searchKey)
                          ->get();                          
        $collection['catagories'] = Catagory::where('parent_id', 0)
                                  ->orderBy('name')->get();
        
        $collection['locations']  = DB::table('state')
                                  ->orderBy('state_name')->get();

        
           
        if (isset($city_ids) && $city_ids !='' ) {
                $query_variable .=' AND city_id IN ('.$city_ids.') '; 
                $query_string['state_citie'] = $city_ids;
               } 
        if (isset($specialities_ids) && $specialities_ids !='') {
          $query_variable .=' AND specialities_id IN ('.$specialities_ids.') ';
          $query_string['specialities'] = $specialities_ids;

          $collection['procedures']     = DB::table('procedures') 
                   ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                   ->select('procedures.*')
                   ->whereIn('procedures.specialitie_id', explode(',',$specialities_ids))
                   ->groupBy('procedures.id')->get();
                   $query_string['specialities'] = $specialities_ids;
                   $collection['child_specilities'] = Catagory::where('parent_id', $catagegory)
                  ->orderBy('name')->get();

         // users in specific category. 
        } elseif (isset($catagegory) && $catagegory!=''  && $catagegory!= 0) {
            $query_string['catagory']      = $catagegory;
            $query_variable .=' AND specialities_id ='.$catagegory;

            $collection['child_specilities'] = Catagory::where('parent_id', $catagegory)
        ->orderBy('name')->get();
           }

        if (isset($procedures_ids) && $procedures_ids !='') {
        $query_variable .=' AND procedure_id IN ('.$procedures_ids.') ';
        $query_string['procedures'] = $procedures_ids;
 
       }  

        $profiles = Profile::select('id')
                  ->where('state_id', $searchKey)->lists('id','id');
        // SELECT THOSE IDS WHICH BELONGS TO SPECIFICE STATE.
        $doctors = Profile::select('user_id')->whereIn('parent_id', $profiles)->lists('user_id','user_id');
        // SELECT USER IDS AGAINST THEIR PARENT IDS.  
    $order = 'ASC';
       if(isset($sort_by)){

           switch($sort_by){
           case 'rating':
           $query_string['sort_by'] = $sort_by;
            $order = 'DESC';
           break;
           case 'wholename':
           $query_string['sort_by'] = $sort_by;
           $order = 'ASC';
           break;

         }

        }else $sort_by = 'id'; 
        $collection['users'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,`image_of_doctor`,`education`,`contact_dr_phone_number`,`parent_id`, `wholename`,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))->whereRaw($query_variable)->groupBy('id')->orderBy($sort_by, $order)->paginate(6); 
          $collection['users']->appends($query_string);
            //query builder ENDS

      $collection['doctors'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get(); 

            $collection['news'] = DB::table('news')
              ->join('news_translations', 'news.id', '=','news_translations.news_id')
              ->select('news.*', 'news_translations.title', 'news_translations.content','news_translations.language')
              ->paginate(3);  


      return View::make('search', compact('collection'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
