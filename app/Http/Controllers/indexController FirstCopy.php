<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Catagory;
use DB;
use Input;
use App\Profile;
use App\users_specialities;
use App\User;
use DOMDocument;
use DOMXPath;

class indexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $doctors=DB::table('profile')
          ->join('users', 'profile.user_id', '=', 'users.id')
          ->select('profile.*', 'users.*')
          ->where('parent_id','<>', 0)
          ->get();
          $procedures = DB::table('procedures')->orderBy('procedure_name')->get();
          $catagories	= Catagory::where('parent_id', 0)->orderBy('name')->get();
          $locations	= DB::table('state')->orderBy('state_name')->get();

          return View::make('index', compact('catagories', 'locations','doctors','procedures')); 
    }
    //Simple search for doctor

    public function simpleSearchForDoctor()
    { 
      $input           = Input::all();
      $state_id        = $input['location'];
      $search_keyword  = $input['search_keyword'];
      $specialities_id = $input['catagory'];
      $catagories      = Catagory::where('parent_id', 0)->orderBy('name')->get();
      $locations       = DB::table('state')->orderBy('state_name')->get();
      $query_variable  = 'WHERE 1 ';
      if((isset($state_id) && $state_id != '0')
      {
        echo $query_variable .= ' AND state_id = '.$state_id; 
      }
      if((isset($search_keyword) && $search_keyword != '0')
      {
        echo $query_variable .= ' AND wholename = LIKE'.'%'.$search_keyword.'%'; 
      }
      if((isset($specialities_id) && $specialities_id != '0')
      {
        echo $query_variable .= ' AND specialities_id = '.$specialities_id; 
      }

      if((isset($state_id) && $state_id != '0') &&  $search_keyword == '' && $specialities_id == '0')
      { // if only state is selected then use this condition.
         

         $profiles = Profile::select('id')->where('state_id', $state_id)->lists('id','id');
         // SELECT THOSE IDS WHICH BELONGS TO SPECIFICE STATE.

         $doctors  = Profile::select('user_id')->whereIn('parent_id', $profiles)
                   ->lists('user_id','user_id');// SELECT USER IDS AGAINST THEIR PARENT IDS.

         //PROCEDURES OF THE RELATED USERS.
         $procedures = DB::table('procedures') 
                     ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                     ->select('procedures.*')
                     ->whereIn('user_procedures.user_id', $doctors)->groupBy('procedures.id')->get();
                     
         $users    = DB::table('profile')
                     ->join('users', 'profile.user_id', '=', 'users.id')
                     ->select('profile.*', 'users.*')
                     ->whereIn('profile.user_id', $doctors) 
                     ->paginate(3);
        $users->appends(['search_keyword' => '' , 'catagory' => '0' , 'location' => $state_id ])->render();

      }// END OF OTHRE MOST IF.

      elseif((isset($search_keyword) && $search_keyword != '') &&  $state_id == '0' && $specialities_id == '0')
      {// SEARCH ONLY FOR KEY WORD.
 
          $users =  DB::table('profile')
                      ->select(DB::raw('CONCAT_WS(" ",`first_name`,`last_name`) as `wholename`, profile.* , users.*'))
                      ->join('users', 'profile.user_id', '=', 'users.id') 
                       ->having('wholename', 'LIKE','%'.$search_keyword.'%') 
                      ->simplePaginate(15); 
                       $users->appends(['search_keyword' => $search_keyword , 'catagory' => '0' , 'location' => '0'])->render();
      }

      elseif((isset($specialities_id) && $specialities_id != '0') &&  $state_id == '0' && $search_keyword == '')
      {// SEARCH ONLY FOR CATEGORY.

        //FIRST SELECT ALL USERS IN LISTING CATEGORY.
        $user_ids =  DB::table('users_specialities')->select('users_id')->where('specialities_id',$specialities_id)->orderBy('users_id')->lists('users_id', 'users_id'); // users in specific category.
 //PROCEDURES OF THE RELATED USERS.
         $procedures = DB::table('procedures') 
                     ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                     ->select('procedures.*')
                     ->whereIn('user_procedures.user_id', $user_ids)->groupBy('procedures.id')->get();
        //$user_ids = implode(',',$user_ids); 
        
        $users   =  DB::table('profile')
                      ->join('users', 'profile.user_id', '=', 'users.id')
                      ->select('profile.*', 'users.*')
                      ->whereIn('users.id', $user_ids)
                      ->paginate(10);
         $users->appends(['search_keyword' => '' , 'catagory' => $specialities_id , 'location' => '0'])->render();
      }


      elseif((isset($state_id) && $state_id != '0') &&  (isset($search_keyword) && $search_keyword != '') && $specialities_id == '0')
      {// SEARCH WHEN STATE AND KEYWORD IS SELECTED.
         

         $profiles = Profile::select('id')->where('state_id', $state_id)
         ->lists('id','id');// SELECT THOSE IDS WHICH BELONGS TO SPECIFICE STATE.
         $doctors = Profile::select('user_id')->whereIn('parent_id', $profiles)
         ->lists('user_id','user_id');// SELECT USER IDS AGAINST THEIR PARENT IDS.
         
         $procedures = DB::table('procedures') 
                     ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                     ->select('procedures.*')
                     ->whereIn('user_procedures.user_id', $doctors)->groupBy('procedures.id')->get();

          $users =  DB::table('profile')
                      ->select(DB::raw('CONCAT_WS(" ",`first_name`,`last_name`) as `wholename`, profile.* , users.*'))
                      ->join('users', 'profile.user_id', '=', 'users.id') 
                      ->whereIn('users.id', $doctors)
                       ->having('wholename', 'LIKE','%'.$search_keyword.'%') 
                      ->simplePaginate(15); 

        $users->appends(['search_keyword' => $search_keyword , 'catagory' => '0' , 'location' => $state_id])->render();

      }

      elseif((isset($specialities_id) && $specialities_id != '0') &&  (isset($search_keyword) && $search_keyword != '') && $state_id == '0')
      {// SEARCH WHEN CATEGORY AND KEYWORD IS SELECTED.
 
         
        //FIRST SELECT ALL USERS IN LISTING CATEGORY.
        $user_ids =  DB::table('users_specialities')->select('users_id')->where('specialities_id',$specialities_id)->orderBy('users_id')->lists('users_id', 'users_id'); // users in specific category.

        $procedures = DB::table('procedures') 
                     ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                     ->select('procedures.*')
                     ->whereIn('user_procedures.user_id', $user_ids)->groupBy('procedures.id')->get();

  
          $users =  DB::table('profile')
                      ->select(DB::raw('CONCAT_WS(" ",`first_name`,`last_name`) as `wholename`, profile.* , users.*'))
                      ->join('users', 'profile.user_id', '=', 'users.id') 
                      ->whereIn('users.id', $user_ids)
                       ->having('wholename', 'LIKE','%'.$search_keyword.'%') 
                      ->simplePaginate(15);
          $users->appends(['search_keyword' => $search_keyword , 'catagory' => $specialities_id , 'location' => '0'])->render(); 

      }

       elseif((isset($specialities_id) && $specialities_id != '0') &&  (isset($state_id) && $state_id != '0') && $search_keyword == '')
      { // SEARCH WHEN STATE AND CATEGORY IS SELECTED.
 
        $user_ids =  DB::table('users_specialities')->select('users_id')
        ->where('specialities_id',$specialities_id)->orderBy('users_id')->lists('users_id', 'users_id'); // users in specific category.
        
        $profiles = DB::table('profile')->select('id')
        ->where('state_id', $state_id)->lists('id','id');// PARENT USER IDS FROM PROFILE TABLE.
        
        $doctors  = DB::table('profile')->select('user_id')
        ->whereIn('parent_id', $profiles)->lists('user_id','user_id');// SELECT USER IDS AGAINST THEIR PARENT IDS. 

        $combined_array_users_ids = array_intersect_assoc($user_ids, $doctors);// COMBINE BOTH USERS IN STATES AND USERS IN CATEGORY.

         $procedures = DB::table('procedures') 
                     ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                     ->select('procedures.*')
                     ->whereIn('user_procedures.user_id', $combined_array_users_ids)->groupBy('procedures.id')->get();


          $users = DB::table('profile')
                     ->join('users', 'profile.user_id', '=', 'users.id')
                     ->select('profile.*', 'users.*')
                    ->whereIn('user_id', $combined_array_users_ids) 
                    ->paginate(50); 

          $users->appends(['search_keyword' => '' , 'catagory' => $specialities_id , 'location' => $state_id])->render();
 
      }

    elseif((isset($state_id) && $state_id != '0') &&  (isset($search_keyword) && $search_keyword != '') &&  (isset($specialities_id) && $specialities_id != '0'))
      {
        //SEARCH WHEN ALL THE CRITERIA IS SELECTED .
         $user_ids =  DB::table('users_specialities')->select('users_id')
        ->where('specialities_id',$specialities_id)->orderBy('users_id')
        ->lists('users_id', 'users_id'); // users in specific category.
        
        $profiles = DB::table('profile')->select('id')
        ->where('state_id', $state_id)->lists('id','id');// PARENT USER IDS FROM PROFILE TABLE.
        
        $doctors  = DB::table('profile')->select('user_id')
        ->whereIn('parent_id', $profiles)->lists('user_id','user_id');// SELECT USER IDS AGAINST THEIR PARENT IDS. 

        $combined_array_users_ids = array_intersect_assoc($user_ids, $doctors);// COMBINE BOTH USERS IN STATES AND USERS IN CATEGORY.

         $procedures = DB::table('procedures') 
                     ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                     ->select('procedures.*')
                     ->whereIn('user_procedures.user_id', $combined_array_users_ids)->groupBy('procedures.id')->get();

        $users =  DB::table('profile')
                      ->select(DB::raw('CONCAT_WS(" ",`first_name`,`last_name`) as `wholename`, profile.* , users.*'))
                      ->join('users', 'profile.user_id', '=', 'users.id') 
                      ->whereIn('users.id', $combined_array_users_ids)
                      ->having('wholename', 'LIKE','%'.$search_keyword.'%') 
                      ->simplePaginate(15); 
        $users->appends(['search_keyword' => $search_keyword , 'catagory' => $specialities_id , 'location' => $state_id])->render();
 
  
      }

      $doctors=DB::table('profile')
          ->join('users', 'profile.user_id', '=', 'users.id')
          ->select('profile.*', 'users.*')
          ->where('parent_id','<>', 0)
          ->get();

      return View::make('doctorListing', compact('users','doctors','catagories', 'locations','procedures'));
    } 



    /**
     * Show the form for doctorDetailPage a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doctorDetailPage($id)
    {
      //set_time_limit(0);
      //ini_set('memory_limit', '4095M');
      //error_reporting(0);






/*$url = 'http://www.healthgrades.com/specialty-directory';
$json = $this->curl_get_contents($url);
preg_match_all('/<div class="specialtyBlock">(.*?)<\/div>/s',$json,$estimates);

//print_r($estimates[1]);die;
preg_match_all('/<a (.*?)>(.*?)<\/a>/s',$estimates[1][0],$estimates_1);

foreach ($estimates_1[2] as $key => $value) {
  echo $value.'<br>';
  Catagory::create(['name' => $value]);
}
die;
echo '<pre>';
print_r($estimates_1);die;*/
     $catagories = Catagory::where('parent_id', 0)->orderBy('name')->get();
      $locations  = DB::table('state')->orderBy('state_name')->get();

    $doctor=  DB::table('profile')
              ->join('users', 'profile.user_id', '=', 'users.id')
              ->select('profile.*', 'users.*')
              ->where('user_id', $id)
              ->get();

    //var_dump($doctor[0]->id);die;
    $business_profile=Profile::find($doctor[0]->parent_id);
    //var_dump($profile->id);die;
    $related_doctors =  DB::table('profile')
                        ->join('users', 'profile.user_id', '=', 'users.id')
                        ->select('profile.*', 'users.*')
                        ->where('parent_id', $business_profile->id)
                        //->where('id', '!=', $doctor[0]->id)
                        ->get();
                        $doctors=DB::table('profile')
                            ->join('users', 'profile.user_id', '=', 'users.id')
                            ->select('profile.*', 'users.*')
                            ->where('parent_id','<>', 0)
                            ->get();

        return View::make('doctorDetail', compact('doctor','business_profile','related_doctors','doctors','catagories','locations'));
    }

private function curl_get_contents($url)
{
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
