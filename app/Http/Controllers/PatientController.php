<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use DB;
use View;
use Input;
use App\Profile;
use Redirect;
class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
      $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first();  
      $collection['first_name']     = Sentinel::getUser()->first_name;
      $collection['last_name']      = Sentinel::getUser()->last_name;
      $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
      $collection['created_at']     = Sentinel::getUser()->created_at;

      $collection['allMyComments']  = DB::table('view_rating_doctors')->where('user_id', Sentinel::getUser()
        ->id)->get(); 
      return View::make('patients.membermanagment',compact('collection'));

    }
    public function appointments()
    {
            $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first();  
      $collection['first_name']     = Sentinel::getUser()->first_name;
      $collection['last_name']      = Sentinel::getUser()->last_name;
      $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
      $collection['created_at']     = Sentinel::getUser()->created_at;

      $collection['allAppointments']  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.id', '=', 'schedule_profile_to_patient.profile_doctor_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'profile.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.schedule_time','schedule_profile_to_patient.status')
            ->where('user_patient_id',Sentinel::getUser()->id)
            ->get(); 

      return View::make('patients.appointments',compact('collection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    //getRegister method for Memeber.
    public function getRegister(){
        return View::make('patients.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function postRegister(Request $request)
    { 
        $inputs = $request->all();
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        //@todo: this should be move to config
        $autoActivate = false;

        if( $user = Sentinel::register($request->all(), $autoActivate ) )
        {

            //Sentinel::getRoleRepository()->findById($inputs['role']); 
            //$memberRole = Sentinel::findRoleByName('member');
            $memberRole = Sentinel::findRoleById($inputs['role']);
            $memberRole->users()->attach($user); 
            DB::table('profile')->insert(['user_id' => $user->getUserId()]);// INSERT AN ID INTO THE PROFILE TABLE ALSO
            if( false === $autoActivate )
            {
                $activation = Activation::create($user);
                $code = $activation->code;
                $sent = \Mail::send('sentinel.emails.activate', compact('user', 'code'), function($m) use ($user)
                {
                    $m->from('junaid@pkteam.com', 'Nation Wide Physicians');
                    $m->to($user->email)->subject('Activate Your Account');
                });
                if ($sent === 0)
                {
                    return redirect()->back()->withInput()
                    ->withErrors('Failed to send activation email.');
                }
                return redirect('auth/login')
                    ->withSuccess('Your account was successfully created. You might login now.')
                    ->with('userId', $user->getUserId());
            }else{
                $credentials = $this->getCredentials($request);
                Sentinel::authenticate($credentials);

                return redirect($this->redirectPath());
            }
        }
        return redirect()->back()->withInput()
        ->withErrors('An error occured while registering.');
    }



public function ajaxUserRegister(Request $request)
    { 
        $inputs    = $request->all();
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        //@todo: this should be move to config
        $autoActivate = false;

        if( $user = Sentinel::register($request->all(), $autoActivate ) )
        {

            //Sentinel::getRoleRepository()->findById($inputs['role']); 
            //$memberRole = Sentinel::findRoleByName('member');
            $memberRole = Sentinel::findRoleById($inputs['role']);
            $memberRole->users()->attach($user); 
            DB::table('profile')->insert(['user_id' => $user->getUserId()]);// INSERT AN ID INTO THE PROFILE TABLE ALSO
            if( false === $autoActivate )
            {
                $activation = Activation::create($user);
                $code       = $activation->code;
                $sent       = \Mail::send('sentinel.emails.activate', compact('user', 'code'), function($m) use ($user)
                {
                    $m->from('junaid@pkteam.com', 'Nation Wide Physicians');
                    $m->to($user->email)->subject('Activate Your Account');
                });
                if ($sent === 0)
                {
                    return redirect()->back()->withInput()
                    ->withErrors('Failed to send activation email.');
                }
                return redirect('auth/login')
                    ->withSuccess('Your account was successfully created. You might login now.')
                    ->with('userId', $user->getUserId());
            }else{
                $credentials = $this->getCredentials($request);
                Sentinel::authenticate($credentials);

                return redirect($this->redirectPath());
            }
        }
        return redirect()->back()->withInput()
        ->withErrors('An error occured while registering.');
    }





    public function profile ()
        {
         $collection['countries']      = DB::table('country')->get();
         $collection['states']         = DB::table('state')->get();
         $collection['cities']         = DB::table('city')->get();
         $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
         $collection['first_name']     = Sentinel::getUser()->first_name;
         $collection['last_name']      = Sentinel::getUser()->last_name;
         $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
         $collection['created_at']     = Sentinel::getUser()->created_at;
         return View::make('patients.editMemberProfile',compact('Profile','collection'));

        } 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    { 
        $input = $request->all(); 
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required'
        ];


        $messages = $this->validatorGeneral($request->all() , $rules); 
        $Profile         = Profile::find($input['profile_id']); 
            if ($messages->isEmpty())
            {
                if (@$input['avatar'] != '' && !empty($input['avatar']) && isset($input['avatar'])) {

                
               \File::delete('images/catalog/'.$Profile->avatar);                
                $imageName           = Input::file('avatar')->getClientOriginalName();
                Input::file('avatar')->move('images/catalog', $imageName);
                $input['avatar']     = $imageName;
              }
              else
              {
                 $input['avatar'] = $Profile->avatar;
 
              }

            User::where('id', $input['user_id'])
            ->update(['first_name' => $input['first_name'] ,'last_name'=>$input['last_name']]);
                $Profile->fill($input);
                $Profile->save();
            } 

        if ($messages->isEmpty())
        {
            return Redirect::to('/visitors/profile');
        }

        return Redirect::back()->withInput()->withErrors($messages);

    }

public function settings ()
{

  $collection['Profile']               = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
         $collection['first_name']     = Sentinel::getUser()->first_name;
         $collection['last_name']      = Sentinel::getUser()->last_name;
         $collection['users_fullname'] = $collection['first_name'].' '.$collection['last_name'];
         $collection['created_at']     = Sentinel::getUser()->created_at;
 return View::make('patients.changepassword',compact('Profile','collection'));

}
  public function changepassword(Request $request)
    {            
      $input = Input::except('_token'); 
       
      $rules = array(       
          'password'              => 'required|min:6|confirmed',
          'password_confirmation' => 'required|min:6'         
          // required and has to match the password field
      );
       
     $messages = $this->validatorGeneral($input, $rules);

      if ($messages->isEmpty())
      {
        user::where('id', Sentinel::getUser()->id)
        ->update(array('password' => bcrypt($input['password']) 
                            ));  
        return Redirect::to('visitors/profile/settings')
        ->withSuccess('Password changed successfully.');
      }
      else
      {  
       return redirect()->back()->withErrors($messages);
          
      }
    
    
    }
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {

        return Validator::make($data, [
            'first_name' => 'max:255',
            'last_name' => 'max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    protected function validatorGeneral($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    } 





}
