<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request; 
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\association; 
use Input;
use DB;
use View; 
use Validator;
use Redirect;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
class AssociationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $collection['allAssociations'] = association::paginate(5);  
        return View::make('associations.index',compact('collection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //var_dump('there i go in the CREATE of associationController');die;
        return $this->showForm('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       return $this->processForm('create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $association_check = association::find($id);
        $image_path =  base_path('images/associations/');
        if ($association_check)
        {
            if($association_check->assoc_logo !=''){
                unlink($image_path.$association_check->assoc_logo);
            }
            $association_check->delete();

            return Redirect::to('associations');
        }

        return Redirect::to('associations');
    }





     /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        $association = null;
        if ($id)
        {
            if ( ! $association = association::find($id))
            {
                return Redirect::to('associations');
            }
        } 
        return View::make('associations.form', compact('mode', 'association'));
    }
    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = Input::all();
        $rules = ['name' => 'required|unique:association|max:200'];
        if ($id)
        {
            
            $rules = ['name' => 'required|max:200'];
            $association_check = association::find($id); 
            $image_path        =  base_path('images/associations/'); 
            $messages = $this->validateGeneral($input, $rules);

            if ($messages->isEmpty())
            {

              if(!empty(Input::file('assoc_logo')) && Input::file('assoc_logo') != ''){
                   if($association_check->assoc_logo !=''){
                    unlink($image_path.$association_check->assoc_logo);
                }

                 $extension = Input::file('assoc_logo')->getClientOriginalExtension(); // getting image extension
                $imageName = date("Y-m-d-h-i-s").'.'.$extension; // renameing image

                  //$imageName = Input::file('assoc_logo')->getClientOriginalName();
                  Input::file('assoc_logo')->move('images/associations', $imageName);
                  $input['assoc_logo'] = $imageName;
              }
              else{
                $input['assoc_logo'] = $association_check->assoc_logo;
              } 


                $association_check->fill($input);
                $association_check->save();
            }
        }
        else
        {
            $messages = $this->validateGeneral($input, $rules);

            if ($messages->isEmpty())
            {
                    $imageName = Input::file('assoc_logo')->getClientOriginalName();
                    Input::file('assoc_logo')->move('images/associations', $imageName);
                    $input['assoc_logo'] = $imageName;
                     $input['created_at'] = date("Y-m-d H:i:s"); 
                    $package = association::create($input);

            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::to('associations')->withSuccess('Record Saved Successfully.');
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }
    /**
     * Validates a package.
     *
     * @param  array  $data
     * @param  mixed  $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateGeneral($data, $packages)
    {
        $validator = Validator::make($data, $packages);

        $validator->passes();

        return $validator->errors();
    }

}
