<?php  

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\Profile;
use App\Procedure;
use Validator;
use View;
use Redirect;
use DB;
use App\Profileschedule;
use App\User;
use App\Catagory;
use App\Insurance;
use App\users_specialities;
use App\association;
use App\creditcard;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\Reviews; 
use Carbon\Carbon;
use App\Models\Conditions;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function __construct()
          {
            $this->middleware('auth');
          }

    public function index()
    { 
    $role    = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 
    
    $Profile = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
    $doctors = Profile::where('parent_id',$Profile->id)->get();
    switch ($role->slug) {
      case 'doctor':        
      return View::make('patients.membermanagment', compact('Profile'));
      break;

      case 'patient':       
      return View::make('patients.membermanagment', compact('Profile', 'doctors'));
      break;
      
      case 'member':         
      return View::make('members.membermanagment', compact('Profile', 'doctors'));
      break;      
      
      default:  
      return View::make('members.membermanagment', compact('Profile', 'doctors'));
      break;
    }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request , $id)
    { 
        $user = \Sentinel::findById($id); // set user permission
        if(!isset($request->create)) $request->create = 'false';
        if(!isset($request->update)) $request->update = 'false';
        if(!isset($request->view))   $request->view   = 'false';
        if(!isset($request->delete)) $request->delete = 'false';
    $user->permissions = [
        'user_create' => $request->create,
        'user_update' => $request->update,
        'user_delete' => $request->delete,
        'user_view'   => $request->view,
        'user_active' => $request->active
    ];

    $user->save();
    return Redirect::back()->withSuccess('Record saved successfully.');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      return $this->processForm('create');
    }

    public function doctorProfileTransfer(Request $request)
    {
        $input = $request->all(); 
        $rules = ['practice_code' => 'required'];
        $messages = $this->validateProfile($request->all() , $rules);
        $data     = Profile::select('id','user_id')
          ->where('user_id', Sentinel::getUser()->id)->first();

          $checkRequest = Profile::select('transfer_request_to_parent')->where('Practice_id', trim($input['practice_code']))
          ->first(); 
       if($checkRequest->transfer_request_to_parent == 0 ) { 

            if ($messages->isEmpty())
            {
             Profile::where('Practice_id', $input['practice_code'])
                 ->update(['transfer_request_to_parent' => $data->id]);
                return Redirect::to('/profiles/completesingleProfile/'.$data->id)
                ->withSuccess('Request Sent Successfully.');
            }
        
    } else return Redirect::back()->withInput()->withErrors('Your Request Is Pending For This Doctor');
        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function conditionsTreated($id)
    {

    $role           = Sentinel::findById(Sentinel::getUser()
                    ->id)->roles->first(); 
    $Profile        = Profile::where('user_id', Sentinel::getUser()
                    ->id)->first(); 
    $doctors        = Profile::where('parent_id',$Profile->id)
                    ->get();
    $collection['user_conditions'] = array();
    $collection['allCondtions']    = Conditions::get();
    $user_conditions =  DB::table('user_conditions')
                    ->where('user_id',$id)->count();
    if($user_conditions != 0){
    $collection['user_conditions'] =  DB::table('user_conditions')
                                  ->where('user_id',$id)->first(); 
    $collection['user_conditions'] = json_decode($collection['user_conditions']->condition_id); 
  }
   // $access      = json_decode($permissions->permissions);
    
    return View::make('members.conditions_treated',
      compact('role','Profile','doctors','collection'));

    }
     public function createConditions(Request $request , $id)
    { 
      $input_condition_id = '';
      $input              = $request->all();
      if(isset($input['condition_id']))
      {
       $input_condition_id = json_encode($input['condition_id']); 
      }
      $check_conditions   = DB::table('user_conditions')->where('user_id',$id)->count();
     //echo $check_conditions;exit;
     if($check_conditions == 0)
      { 
        DB::table('user_conditions')->insert(['user_id'=>$id, 'condition_id'=>$input_condition_id]);
         return Redirect::back()->withSuccess('Record saved successfully.');


      } else {
        
        DB::table('user_conditions')->where('user_id', $id)
                     ->update(['condition_id' => $input_condition_id]);
                      return Redirect::back()->withSuccess('Record updated successfully.');
      }
        //$user->save();
        return Redirect::back()->withSuccess('Record saved successfully.');
        //
    }


    public function show($id)
    {

    $role        = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 
    $Profile     = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
    $doctors     = Profile::where('parent_id',$Profile->id)->get();
    $permissions = User::select('permissions')->where('id', $id)->first(); 
    $access      = json_decode($permissions->permissions);
    
    return View::make('members.permissions', compact('role','Profile','doctors','access'));

    }

     public function showComments($id)
    
    {

    $role    = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 
    $Profile = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
    $doctors = Profile::where('parent_id',$Profile->id)->get();

      $collection['allMyComments']  = DB::table('view_rating_doctors')->where('doctor_id', $id)->where('reviews_type','<>','Archive')->get(); 
    
    return View::make('members.comments', compact('role','Profile','doctors','collection'));

    }

      public function CommentsAproveDisaprove()
    {   
        $input = Input::all(); 
        Reviews::where('id', $input['review_id'])
                     ->update(['approved' => $input['approve']]);

        return redirect()->back();
    }
// THE FUNCTION APPROVES OR DISAPPROVES A PROFILE REQUEST FROM DOCTOR.
    public function TransferAproveDisaprove()
    {   
        $input = Input::all();
        if($input['approve'] == 1){ 
        Profile::where('id', $input['profile_id'])
                     ->update(['transfer_request_approved' => 1]);
        } elseif($input['approve'] == 0){ 
        Profile::where('id', $input['profile_id'])
                     ->update(['transfer_request_approved' => 0]);

        }
        return redirect()->back();
    }



// SHOW THE JOINING LIST OF DOCTORS TO MEMBER PROFILE.
    public function dortorsRequestToJoin()
    {
        $role    = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 
        $Profile = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
        $doctors = Profile::where('transfer_request_to_parent',$Profile->id)->get();

      return View::make('members.request_to_join_clinic', compact('Profile', 'doctors'));
    
    } 
// CONFIRM A REQUEST THAT IS APPROVED FROM THE MEMBER OTHER MEMBER WILL CONFIRM IT.


    public function TransferAproveConfirm()
    {   
        $input = Input::all();
        if($input['approve'] == 1) {
        $new_parent_id = $input['transfer_request_to_parent'];
      //FIRST SELECT THE PARENT COUNTRY, STATE, CITY.
      $Profile = Profile::select('country_id','state_id','city_id')
              ->where('id',$new_parent_id)->first();
      //---------------END---PARENT SELECTIONS.

        Profile::where('id', $input['profile_id'])
                     ->update(
                    ['transfer_request_to_parent' => 0,
                     'transfer_request_approved'  => 0,
                     'parent_id'=>$new_parent_id,
                     'old_parent'=>$input['parent_id']
                     ]);

          Profile::where('parent_id', $new_parent_id)
                 ->update(['country_id' => $Profile->country_id ,
                 'state_id'=>$Profile->state_id,
                 'city_id'=>$Profile->city_id]);
        }
        return redirect()->back();
    }
// SHOW THE LIST OF DOCTORS THAT ARE REQUESTING FOR TRANSFERRING THEIR PROFILE.
 public function TransferRequest() {
    $role    = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 
    $Profile = Profile::where('user_id', Sentinel::getUser()->id)->first();
    $doctors = Profile::where('parent_id',$Profile->id)
    ->where('transfer_request_to_parent','<>', 0)->get();
    return View::make('members.request_to_leave_clinic', compact('Profile', 'doctors'));
    } 


 public function trasferHistory($id)
    {
     $role    = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 
     $Profile = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
     $doctors = Profile::where('parent_id',$Profile->id)->get();

     $collection['allhistory']  = DB::select('SELECT p.id userId,p.Practice_id user_practice,ptt.old_parent userOldParent,ptt.new_parent userNewParent,ptt.created_at trasferDate,pp.business_name newParentBusiness,ppp.business_name oldParentBusiness FROM profile p JOIN profile_to_transferhistory ptt ON ptt.profile_id = p.id JOIN profile pp ON ptt.new_parent=pp.id JOIN profile ppp ON ptt.old_parent=ppp.id WHERE p.id='.$id);   
    return View::make('members.trasferhistory', compact('role','Profile','doctors','collection'));

    }

    public function GetAllAppointments($id)
  { 
    $appointments  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.profile_doctor_id',$id)->get(); 
            $calendarAppointments = array(); 
            foreach($appointments as $a) {
              if($a->schedule_date < date("Y-m-d"))
              {
                if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                  
                     $backgroundColor = "#f39c12";
                     $borderColor     = "#f39c12";
                   
                }
              }
            elseif($a->schedule_date == date("Y-m-d"))
            {
               if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
              $backgroundColor   = "#dd4b39"; //red
              $borderColor       = "#dd4b39"; //red
           }
            }      

            else {

              //NEXT DAY date('Y-m-d', strtotime($a->schedule_date . ' +3 day')

              if((date("Y-m-d", time() + 86400) == $a->schedule_date) OR ((date("Y-m-d", time() + 172800) == $a->schedule_date))
                OR ((date("Y-m-d", time() + 259200) == $a->schedule_date)) OR ((date("Y-m-d", time() + 345600) == $a->schedule_date)))
                {    if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                     $backgroundColor = "#00a65a";
                     $borderColor     = "#00a65a";}
                }
                else { 
                   if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                     $backgroundColor = "#0073b7";
                     $borderColor     = "#0073b7";
                   }
                   }
               }


         
                    $event              = array(
                      'id'              => $a->id,
                      'title'           => $a->first_name.' '.$a->last_name,
                      'start'           => $a->schedule_date,
                      'end'             => $a->schedule_date,
                      'backgroundColor' => $backgroundColor, //yellow
                      'borderColor'     => $borderColor //yellow
                      );
                      array_push($calendarAppointments, $event);
   }

    return response()->json($calendarAppointments);
  }

    public function appointments($id)
    {
      $role    = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 
      $Profile = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
      $doctors = Profile::where('parent_id',$Profile->id)->get();

      $collection['allAppointments']  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.schedule_time','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.profile_doctor_id',$id)
            ->get(); 

      return View::make('members.appointments',compact('role','Profile','doctors','collection'));



    }
    public function ajaxSearchAppointments(Request $request)
    {
      $html = '';
      //$html .= $request->search_by_date;
      if($request->search_by_date == '' || !isset($request->search_by_date)):
        $searchByDate = date("Y-m-d");
        else:
          $searchByDate = \Carbon\Carbon::createFromFormat('d/m/Y', $request->search_by_date)->format('Y-m-d');
      endif;     
      $collection['Profile']        = Profile::where('user_id', Sentinel::getUser()->id)->first();
       $collection['allApts']  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.schedule_time','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.profile_doctor_id',$request->profile_id)
            ->whereDate('schedule_date', '=', $searchByDate)
            ->get();
      $html .= '<table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr><th>Sr.No</th>
                    <th>Patients Name</th> 
                    <th>Date</th>
                    <th>Time</th>
                    <th>Current Status</th>  
                    <th>Change Status</th> 
                    
                  </tr>
                </thead>
                <tbody>';                
                $counter = 1;
                foreach($collection['allApts'] as $doctor):
                   $html .= '<tr>
                  <td>'.$counter.'</td><td>'.$doctor->first_name.' '.$doctor->last_name.'</td>
                   <td>'.\Carbon\Carbon::createFromFormat('Y-m-d', $doctor->schedule_date)->format('d-m-Y').'</td>
                    <td>'.\Carbon\Carbon::createFromFormat('H:i', $doctor->schedule_time)->format('h:i A').'</td><td id="td_'.$doctor->id.'">';
                    switch ($doctor->status) {
                      case 1:
                        $html .= 'Unapproved';
                        break;
                      case 2:
                        $html .=  'Approved';
                        break;
                      case 3:
                        $html .=  'Canceled';
                        break;
                      case 4:
                        $html .=  'Pending';
                        break;
                      default:
                         $html .=  'Pending';
                        break;
                    }                   
                    $html .='</td>
                   <td>                    
                    <div title="" data-toggle="tooltip" class="box-tools" data-original-title="Status">
                    <div data-toggle="btn-toggle" class="btn-group">
                      <button class="btn btn-default btn-sm active approve" type="button" data-id="'.$doctor->id.'" data-value="2" title="Active">
                      <i class="fa fa-square text-green"></i></button>
                      <button class="btn btn-default btn-sm approve" type="button" data-id="'.$doctor->id.'" data-value="1" title="Inactive"><i class="fa fa-square text-red"></i></button>

                      <button class="btn btn-default btn-sm approve" type="button" data-id="'.$doctor->id.'" data-value="3" title="Reject"><i class="fa fa-square text-blue"></i></button>
                    </div>
                  </div>

                   </td> 
                  </tr>';
                  $counter ++;
                endforeach;
                $html .= '</tbody>
                <tfoot>
                  <tr>
                    <th>Sr.No</th>
                    <th>Patients Name</th> 
                    <th>Date</th>
                     <th>Time</th>
                     <th>Current Status</th>  
                    <th>Change Status</th> 
                  </tr>
                </tfoot>
              </table>';
              return $html;
               }

               public function checkStatusAppointment()
        {
      $dataId    =  Input::get('dataId');
      $dataValue =  Input::get('dataValue');
      DB::table('schedule_profile_to_patient')->where('id',$dataId)->update(['status'=>$dataValue]);
      switch ($dataValue) {
                      case 1:
                        echo 'Unapproved';
                        break;
                      case 2:
                        echo 'Approved';
                        break;
                      case 3:
                        echo 'Canceled';
                        break;
                      case 4:
                        echo 'Pending';
                        break;
                      default:
                         echo 'Pending';
                        break;
      }
    }



     public function rescheduleAppointments($user , $id)
  {  

    $role    = Sentinel::findById(Sentinel::getUser()->id)->roles->first(); 
    $Profile = Profile::where('user_id', Sentinel::getUser()->id)->first(); 
    $doctors = Profile::where('parent_id',$Profile->id)->get();



    $collection['allAppointments'] = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.schedule_time','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.id',$id)
            ->get(); 

      return View::make('members.rescheduleappointments',compact('role','Profile','doctors','collection'));


    }

     public function GetAvailableAppointments($id)
  { 
    $appointments  = DB::table('schedule_profile')
                   ->where('profile_id',$id)
                   ->get(); 
            $calendarAppointments = array();
            $day_count = 0; 
            for($i = 0; $i<6;$i++):
            foreach($appointments as $a) {
              $from_current_date_tonext = date('Y-m-d', strtotime("+".$day_count." days"));

            $weekday = date('l', strtotime($from_current_date_tonext));
            $open    = $this->checkDayAvailable(strtolower($weekday),$id);
              if($open == 1){              
                  $weekday         = 'Available';
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
              }
              else {
                    $weekday           = 'Not Available';                 
                    $backgroundColor   = "#dd4b39"; //red
                    $borderColor       = "#dd4b39"; //red
                }
                $event                  = array(
                      'id'              => $a->id,
                      'title'           => $weekday,
                      'start'           => $from_current_date_tonext, 
                      'backgroundColor' => $backgroundColor,  
                      'borderColor'     => $borderColor,
                      'cust_date'       => $from_current_date_tonext
                      ); 
                array_push($calendarAppointments, $event);
                $day_count ++;
              }
 endfor;

    return response()->json($calendarAppointments);
  }

private function checkDayAvailable($day,$profile_id)
{
  $appointments  = DB::table('schedule_profile')
                   ->where('days', $day)
                   ->where('profile_id',$profile_id)
                   ->first(); 
  return $appointments->open;
}



public function schedualAppointment(Request $request)
  {
    
    $message                 = '';
    $error                   = false;
    $data                    = $request->all();
    $row = DB::table('schedule_profile_to_patient')
                       ->where('id',$data['edit_id'])
                       ->first();
    $data['user_patient_id'] = $row->id;
    $data['created_at']      = \Carbon\Carbon::now();
    $data['updated_at']      = \Carbon\Carbon::now();
    $data['schedule_date']   = date_create($data['schedule_date']);
  
    $data['schedule_date']   = date_format($data['schedule_date'],"Y-m-d");
    $date                    = \Carbon\Carbon::createFromFormat("Y-m-d", $data['schedule_date']); 
    $checkScheduleOnDay = DB::table('schedule_profile')
                        ->where('days',''.strtolower($date->format("l")).'')
                        ->where('profile_id',$data['user_doctor_id'])
                        ->where('open',1)
                        ->count();
  $checkApointMentQuota = DB::table('schedule_profile_to_patient')
                       ->where('profile_doctor_id',$data['user_doctor_id'])
                       ->whereDate('schedule_date', '=', $data["schedule_date"])
                       ->count();
  $checkApointAlready = DB::table('schedule_profile_to_patient')
                     ->where('profile_doctor_id',$data['user_doctor_id'])
                     ->where('user_patient_id',21) 
                     ->whereDate('schedule_date', '=', $data["schedule_date"])->count();
    $checkApointOnthisDate = DB::table('schedule_profile_to_patient') 
                           ->where('profile_doctor_id',$data['user_doctor_id']) 
                           ->whereDate('schedule_date', '=', $data["schedule_date"])
                           ->lists('schedule_time'); 
 

  if($checkScheduleOnDay == 0):
    $message = 'No scheduale available on this date.';
    $error   = true; 
  endif; 
  if($checkApointMentQuota > 50):
    $message = 2;
    $error   = true;
    endif;
    if($checkApointAlready > 0):
    $message = 3;
    $error   = true;
    endif;
    if(date('Y-m-d') > $data["schedule_date"]):
    $message = 4;
    $error   = true;
    endif; 
    if($error == false):
      unset($data['doctors_id']);
      $getScheduleOnDay = DB::table('schedule_profile')->select('from','to','break_time','break_duration')
                     ->where('days',''.strtolower($date->format("l")).'')
                     ->where('profile_id',$data['user_doctor_id'])
                     ->where('open',1)->first();
    //DB::table('schedule_profile_to_patient')->insert($data);
    //$message = 6; 
    $duration  = 15;  // split by 30 mins                          
    $array_of_time      = array ();
    $array_ofbreak_time = array ();
    $start_time         = strtotime ($getScheduleOnDay->from); //change to strtotime
    $end_time           = strtotime ($getScheduleOnDay->to); //change to strtotime
    $add_mins           = $duration * 60;
    $gapArray           = array();
    $html               = '<tr>';
    $tdcounter          = 0;
      while ($start_time <= $end_time) // loop between time
      {
        $check_time_forbreak    = date ("H:i", $start_time); 
        if($check_time_forbreak == date ("H:i", strtotime ($getScheduleOnDay->break_time))){ 
        $bstarttime             = $getScheduleOnDay->break_time; 
        preg_match("/([0-9]{1,2}):([0-9]{1,2})/", $getScheduleOnDay->break_duration, $match);
        $hour                   = $match[1];
        $min                    = $match[2];

        $bendtime = date('H:i',strtotime('+'.$hour.' hour +'.$min.' minutes',strtotime($getScheduleOnDay->break_time)));

        $gapArray   = $this->returnBreakArray($bstarttime,$bendtime,$add_mins);
        }
        if (!in_array($check_time_forbreak, $gapArray)){
            // $array_of_time [] = date ("H:i", $start_time);
        if(count($checkApointOnthisDate) > 0){
          if (!in_array($check_time_forbreak, $checkApointOnthisDate)){
             if($tdcounter%8 == 0 ){
              $html .= '</tr><tr>';
              $tdcounter=0;
             }
             $html .= '<td><a href="'.url().'/" data-id="'.$data['user_doctor_id'].'" data-value="'.date ("H:i", $start_time).'" data-date="'.$data["schedule_date"].'" class="book-appointment">'.date ("H:i", $start_time).'</a></td>';
              $tdcounter ++;
            }// end if
          }
          else {
             if($tdcounter%8 == 0 ){
              $html .= '</tr><tr>';
              $tdcounter=0;
             }
             $html .= '<td><a href="'.url().'/" data-id="'.$data['user_doctor_id'].'" data-value="'.date ("H:i", $start_time).'" data-date="'.$data["schedule_date"].'" class="book-appointment">'.date ("H:i", $start_time).'</a></td>';
              $tdcounter ++;
          }
        }// end of if.
         $start_time += $add_mins; // to check endtime
       
      }
    $html               .= '</tr>';

    $message = $html;
    endif;

    return $message;
  }

// Get available days
    function GetAvailableDays() {
        //return response()->json(BookingDateTime::all());
        $message                 = '';
    $error                   = false;
    $data                    = Input::all();
    $data['user_patient_id'] = Sentinel::getUser()->id;
    $data['created_at']      = \Carbon\Carbon::now();
    $data['updated_at']      = \Carbon\Carbon::now();
    //$data['schedule_date']   = date_create($data['schedule_date']);

    //$data['schedule_date']   = date_format($data['schedule_date'],"Y-m-d");
   // $date                    = Carbon::createFromFormat("Y-m-d", $data['schedule_date']);   
  $checkApointAlready = DB::table('schedule_profile_to_patient')
                     ->where('profile_doctor_id',$data['profile_doctor_id'])
                     ->where('user_patient_id',$data['user_patient_id']) 
                     ->whereDate('schedule_date', '=', $data["schedule_date"])->count(); 
          if($checkApointAlready > 0):
          $message = 1;
          $error   = true;
          endif;
          if(date('Y-m-d') > $data["schedule_date"]):
          $message = 2;
          $error   = true;
          endif;
         // if($data['user_patient_id'] == $data['doctors_id']):
         // $message = 3;
         // $error   = true;
         // endif;
          if($error == false):
            DB::table('schedule_profile_to_patient')
            ->where('id', $data["patient_id"])
            ->where('profile_doctor_id', $data['profile_doctor_id'])
            ->update(
            [  'schedule_date' => $data["schedule_date"],
               'schedule_time' => $data["schedule_time"],
               'created_at'    => $data['created_at'],
               'updated_at'    => $data['updated_at']
            ]);
            $message = 3;
            endif;
         return $message;
    }
private function returnBreakArray($bstarttime = '02:00',$bendtime = '03:30',$add_mins = 30)
{ 
    $bstart_time    = strtotime ($bstarttime); //change to strtotime
    $bend_time      = strtotime ($bendtime); //change to strtotime

    while ($bstart_time <= $bend_time) // loop between time
    {
       $array_ofbreak_time[] = date ("H:i", $bstart_time);
       $bstart_time += $add_mins; // to check endtime
    }
    return $array_ofbreak_time;

}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $id = $request->id;
      return $this->processForm('update', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = Input::all();

        $rules = [
            //'education' => 'required',
            //'member_id' => 'required',
            'phone' => 'required',
            'mobile' => 'required'
            //'package' => 'required'
        ];

        //$input['slug'] = $this->slugify($input['name']);

        if ($id)
        {

            $Profile         = Profile::find($id);
            $messages        = $this->validateProfile($input, $rules);

            if ($messages->isEmpty())
            {
              if (@$input['avatar'] != '' && !empty($input['avatar']) && isset($input['avatar'])) {

                
               \File::delete('images/catalog/'.$Profile->image_of_doctor);                
                $imageName           = Input::file('avatar')->getClientOriginalName();
                Input::file('avatar')->move('images/catalog', $imageName);
                $input['avatar']     = $imageName;
              }
              else
              {
                 $input['avatar'] = $Profile->avatar;
 
              }


                User::where('id', $input['user_id'])
                     ->update(['first_name' => $input['first_name'] ,'last_name'=>$input['last_name']]);
                $Profile->fill($input);
                $Profile->save();
                DB::table('schedule_profile')->where('profile_id', '=', $id)->delete();
               


                for($ps = 0; $ps < count($input['days']); $ps++){
                        $sunday = array('profile_id'=>$id,
                                        'days' => $input['days'][$ps],
                                        'open' => $input['opening_sunday'][$ps],
                                        'from' => $input['from_sunday'][$ps],
                                        'to'   => $input['to_sunday'][$ps]);

                            $Profileschedule = Profileschedule::create($sunday);
                  }

                  Profile::where('parent_id', $Profile->id)->update(['country_id' => $Profile->country_id ,'state_id'=>$Profile->state_id, 'city_id'=>$Profile->city_id]); 


            }
        }
        else
        {

            $messages = $this->validateProfile($input, $rules);


            if ($messages->isEmpty())
            {

                  $imageName = Input::file('avatar')->getClientOriginalName();
                  Input::file('avatar')->move('images/catalog', $imageName); 

                    $input['avatar'] = $imageName;
                    $Profile = Profile::create($input);

                    //saving profile schedule
                    $profile_last_id = $Profile->id;

                    $sunday = array(
                                    'profile_id'=>$profile_last_id,
                                    'days'=>'sunday',
                                    'open'=>$input['opening_sunday'],
                                     'from'=>$input['from_sunday'],
                                    'to'=>$input['to_sunday']

                                  );

                    $Profileschedule = Profileschedule::create($sunday);

                    $monday = array(
                                    'profile_id'=>$profile_last_id,
                                    'days'=>'monday',
                                    'open'=>$input['opening_monday'],
                                     'from'=>$input['from_monday'],
                                    'to'=>$input['to_monday']

                                  );

                    $Profileschedule = Profileschedule::create($monday);

                    $tuesday = array(
                                    'profile_id'=>$profile_last_id,
                                    'days'=>'tuesday',
                                    'open'=>$input['opening_tuesday'],
                                     'from'=>$input['from_tuesday'],
                                    'to'=>$input['to_tuesday']

                                  );

                    $Profileschedule = Profileschedule::create($tuesday);

                    $wednesday = array(
                                    'profile_id'=>$profile_last_id,
                                    'days'=>'wednesday',
                                    'open'=>$input['opening_wednesday'],
                                     'from'=>$input['from_wednesday'],
                                    'to'=>$input['to_wednesday']

                                  );

                    $Profileschedule = Profileschedule::create($wednesday);

                    $thursday = array(
                                    'profile_id'=>$profile_last_id,
                                    'days'=>'thursday',
                                    'open'=>$input['opening_thursday'],
                                     'from'=>$input['from_thursday'],
                                    'to'=>$input['to_thursday']

                                  );

                    $Profileschedule = Profileschedule::create($thursday);

                    $friday = array(
                                    'profile_id'=>$profile_last_id,
                                    'days'=>'friday',
                                    'open'=>$input['opening_friday'],
                                     'from'=>$input['from_friday'],
                                    'to'=>$input['to_friday']

                                  );

                    $Profileschedule = Profileschedule::create($friday);

                    $saturday = array(
                                    'profile_id'=>$profile_last_id,
                                    'days'=>'saturday',
                                    'open'=>$input['opening_saturday'],
                                     'from'=>$input['from_saturday'],
                                    'to'=>$input['to_saturday']

                                  );

                    $Profileschedule = Profileschedule::create($saturday);


                    //updating the user table first and last names

                    $id = $input['user_id']; 

                    $user = User::find($id);

                    $user->first_name = $input['first_name'];
                    $user->last_name  = $input['last_name'];

                    $user->save();


                //}
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::back()->withSuccess('Record saved successfully.');
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a profile.
     *
     * @param  array  $data
     * @param  mixed  $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateProfile($data, $profiles)
    {

        $validator = Validator::make($data, $profiles);

        $validator->passes();

        return $validator->errors();
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {

      $countries       = DB::table('country')->get();
      $states          = DB::table('state')->get();
      $cities          = DB::table('city')->get();
      $Profile         = Profile::where('user_id', Sentinel::getUser()->id)->first();
      $doctors         = Profile::where('parent_id',$Profile->id)->get();
      $Profileschedules = Profileschedule::where('profile_id', $Profile->id)->get();
      $count_secedule =  Profileschedule::where('profile_id', $Profile->id)->count(); 
      $data      = null;
        if ($id)
        {

            if ( ! $data = Profile::find($id))
            {
                return Redirect::to('Profile');
            }
             $users = User::find($data->user_id);
             $users = array('first_name'=>$users->first_name,'last_name'=>$users->last_name); 
        }


        return View::make('editMemberProfile', compact('mode', 'data','countries','states','cities','users','Profile','doctors','Profileschedules','count_secedule'));

    }



    /**
     * populate the state against counrty
     *
     * @return \Illuminate\Http\Response
     */
    public function state()
    {
        //
        $input = Input::all();
        $input['country_id'];
        $states = DB::table('state')->where('country_id', '=',$input['country_id'])->lists('state_name', 'id');


        return  json_encode($states);

    }

    /**
     * populate the cities against counrty
     *
     * @return \Illuminate\Http\Response
     */
    public function cities()
    {
        //
        $input = Input::all();

        $input['state_id'];
        $cities = DB::table('city')->where('state_id', '=',$input['state_id'])->lists('city_name', 'id');;


        return  json_encode($cities);

    }

    //complete user profile to 100 %.
    public function completeProfile($id){

        $data              = Profile::find($id);
        $Profile           = Profile::find($id);
        $procedures        = DB::table('procedures')->get();        
        $doctors = Profile::where('parent_id',$Profile->id)->get();

      //getting catagories
      $catagories        = Catagory::all();
      $catagory          = Catagory::where('parent_id', 0)->get();
      //getting insurance
      $insurances        = Insurance::where('parent_id', 0)->get();
      $association_names = association::select('name')->get();
    
       //  return View::make('completeMemberProfile', compact('mode', 'data', 'catagory', 'catagories', 'insurances' , 'association_names'));
    return View::make('completeMemberProfiles', compact('mode', 'data', 'catagory', 'catagories', 'insurances' , 'association_names','Profile','doctors','procedures'));
    }

    //populate subCatagories

    public function subCatagories(){
      $input = Input::all();

      $input['catagory_id'];
      $catagories = DB::table('specialities')->where('parent_id', '=',$input['catagory_id'])->lists('name', 'id');

      return  json_encode($catagories);

    }
    //AJAX REQUEST FOR SUB CATEGORIES.

   public function subCatagoriesProcedures(){
      $input = Input::all();

      $input['catagory_id'];
      $procedures = DB::table('procedures')->where('specialitie_id', '=',$input['catagory_id'])->lists('procedure_name', 'id');

      return  json_encode($procedures);

    }

//SINGLE COMPLETED PROFILE.
    public function singleCompleteProfile(){

      $input             = Input::all();  
      $number_of_doctors = $input['number_of_doctors'];
      $state_city_zip    = Profile::select('zip_code','state_id','country_id','city_id')
                                   ->where('id',$input['profile_id'])->first();
      $users_inputs                 = array();
      $catagories_inputs            = array();
      $profile_inputs               = array();
      $schedule_availibility_inputs = array();
      $association_inputs           = array(); 
      $catagory_id                  = $input['catagory_id'];
      @$sub_catagory_1              = $input['sub_catagory_1'];//users_specialities 
      @$procedures_1                = $input['procedures_1'];//users_specialities 
      @$sub_catagory_2              = $input['sub_catagory_2'];
      @$procedures_2                = $input['procedures_2'];//users_specialities 


      $rules = ['first_name'     => 'required',
                'Practice_id'    => 'required|unique:profile',
                'catagory_id'    => 'required',
                'sub_catagory_1' => 'required', 
                'email'          => 'required|unique:users'];

      $messages = $this->validateProfile($input, $rules);

      if ($messages->isEmpty()) { 
          # code...
          $first_name                 = $input['first_name'];
          $last_name                  = $input['last_name'];
          $email                      = $input['email']; //users table
          $users_inputs['first_name'] = $first_name;
          $users_inputs['last_name']  = $last_name;
          $users_inputs['email']      = $email;
          //saving users_inputs
          $users                      = User::create($users_inputs);
          //getting last id of the user
          $user_last_id               =  $users->id;
          // INSERT PROCEDURES_________________________________________
         for($j = 1; $j<3;$j++){ 
          
         if(!empty($input['procedures_'.$j]) && isset($input['procedures_'.$j])){ 
                  $procedure_array = explode(',', $input['procedures_'.$j]);
                  foreach ($procedure_array as $procedure_names) {
                    $procedure_return_array = array();
                    # code... 
                   // echo $input['sub_catagory_'.$j];
                $procedure_return_array               = Procedure::select('id')
                ->whereRaw('UPPER(procedure_name) ="'.strtoupper(trim($procedure_names)).'"')->first();

                      if(count($procedure_return_array) > 0) {
                         $procedure_last_id = $procedure_return_array->id;
                        
                      }
                      else
                      {
                        $procedures_inputs['procedure_name'] = trim($procedure_names);
                        $procedures_inputs['specialitie_id'] = $input['sub_catagory_'.$j];
                        $procedure_return_array              = Procedure::create($procedures_inputs);
                        $procedure_last_id                   = $procedure_return_array->id;
                    //saving the composite keys in teh users_associations table
                      }
                       $users_procedures          = DB::table('user_procedures')->insert(
                      ['user_id' => $user_last_id, 'procedure_id' => $procedure_last_id]); 
                 }// END OF FOREACH.
              }// END OF OUTERE IF CONDITIONS.
    }// end of for loop.
          //--------------------------------------------------------

          $memberRole = Sentinel::findRoleByName('doctor');
          $memberRole->users()->attach($user_last_id);  

          DB::table('users_specialities')->insert([
            ['users_id' => $user_last_id, 'specialities_id' => $catagory_id],
            ['users_id' => $user_last_id, 'specialities_id' => $sub_catagory_1],
            ['users_id' => $user_last_id, 'specialities_id' => $sub_catagory_2]]);
          $education                               =  $input['education'];
          $affiliation                             =  $input['affiliation'];
          $contact_dr_phone_number                 =  $input['contact_dr_phone_number'];
          $Practice_id                             =  $input['Practice_id'];


        if(Input::file('image_of_doctor') != ''):
        $imageName                               = Input::file('image_of_doctor')->getClientOriginalName();
          $input['image_of_doctor']              = $imageName; // profile
          Input::file('image_of_doctor')->move('images/catalog', $imageName);
          else:
            $input['image_of_doctor']                  = '';
          endif; 

          $profile_inputs['education']               = $education;
          $profile_inputs['affiliation']             = $affiliation;
          $profile_inputs['contact_dr_phone_number'] = $contact_dr_phone_number;
          $profile_inputs['Practice_id']             = $Practice_id;
          $profile_inputs['image_of_doctor']         = $input['image_of_doctor'];
          $profile_inputs['parent_id']               = $input['profile_id'];
          $profile_inputs['gender']                  = $input['gender'];
          $profile_inputs['user_dob']                = $input['user_dob'];
          $profile_inputs['user_id']                 = $user_last_id;
          $profile_inputs['country_id']              = $state_city_zip->country_id;
          $profile_inputs['state_id']                = $state_city_zip->state_id;
          $profile_inputs['city_id']                 = $state_city_zip->city_id;
          $profile_inputs['zip_code']                = $state_city_zip->zip_code;
          $Profile                                   = Profile::create($profile_inputs);
          //getting last id of the profile
          $profile_last_id                           = $Profile->id;
          $opening_sunday                            =  $input['opening_sunday'];
          $opening_monday                            =  $input['opening_monday'];
          $opening_tuesday                           =  $input['opening_tuesday'];
          $opening_wednesday                         =  $input['opening_wednesday'];
          $opening_thursday                          =  $input['opening_thursday'];
          $opening_friday                            =  $input['opening_friday'];
          $opening_saturday                          =  $input['opening_saturday'];// schedule_availibility

          ///
          $sunday = array(
                          'profile_id' => $profile_last_id,
                          'days'       => 'sunday',
                          'open'       => $opening_sunday,
                          'from'       => $input['from_time_sunday'],
                          'to'         => $input['to_time_sunday']

                        );

          $Profileschedule = Profileschedule::create($sunday);

          $monday = array(
                          'profile_id' => $profile_last_id,
                          'days'       => 'monday',
                          'open'       => $opening_monday,
                          'from'       => $input['from_time_monday'],
                          'to'         => $input['to_time_monday']

                        );

          $Profileschedule = Profileschedule::create($monday);

          $tuesday = array(
                          'profile_id' => $profile_last_id,
                          'days'       => 'tuesday',
                          'open'       => $opening_tuesday,
                          'from'       => $input['from_time_tuesday'],
                          'to'         => $input['to_time_tuesday']

                        );

          $Profileschedule = Profileschedule::create($tuesday);

          $wednesday = array(
                          'profile_id' => $profile_last_id,
                          'days'       => 'wednesday',
                          'open'       => $opening_wednesday,
                          'from'       => $input['from_time_wednesday'],
                          'to'         => $input['to_time_wednesday']

                        );

          $Profileschedule = Profileschedule::create($wednesday);

          $thursday = array(
                          'profile_id' => $profile_last_id,
                          'days'       => 'thursday',
                          'open'       => $opening_thursday,
                          'from'       => $input['from_time_thursday'],
                          'to'         => $input['to_time_thursday']

                        );

          $Profileschedule = Profileschedule::create($thursday);

          $friday          = array(
                                    'profile_id' => $profile_last_id,
                                    'days'       => 'friday',
                                    'open'       => $opening_friday,
                                    'from'       => $input['from_time_friday'],
                                    'to'         => $input['to_time_friday']

                                  );

          $Profileschedule = Profileschedule::create($friday);

          $saturday         = array(
                                  'profile_id'=>$profile_last_id,
                                  'days'=>'saturday',
                                  'open'=>$opening_saturday,
                                  'from'       => $input['from_time_saturday'],
                                  'to'         => $input['to_time_saturday']

                                );

          $Profileschedule            = Profileschedule::create($saturday);
          ///

        // INSERT ASSOCIATIONS........................
        $totalAssociations = count($input['association']);
        if($totalAssociations > 0):
          for($a = 0; $a < $totalAssociations; $a++):
          $association                =  $input['association'][$a]; //association
          $association_inputs['name'] = $association;   

          $associations               = association::select('id')
          ->whereRaw('UPPER(name) ="'.strtoupper($association).'"')->first();

          if(count($associations) > 0) {
            $association_last_id = $associations->id;
          }
          else
          {
             $association = association::create($association_inputs);
             $association_last_id         = $association->id;
        //saving the composite keys in teh users_associations table
          }

           $users_associations  = DB::table('users_associations')
           ->insert([
           'users_id' => $user_last_id,
           'associations_id' => $association_last_id,
          'availability'=>$input['availability'][$a],'from_time'=>$input['from_time'][$a],'to_time'=>$input['to_time'][$a],'associations_order'=>$a,'date_added'=>date("Y-m-d H:i:s")]
        ); 
          endfor;
          endif;
        $created_at = \Carbon\Carbon::now()->toDateTimeString();
        $updated_at = \Carbon\Carbon::now()->toDateTimeString();
          // ASSOCIATIONS ENDS HERE..................
    //CREDIT CARDS INSERT STARTS HERE.
      if(isset($input['credit_card']) && count($input['credit_card']) > 0):
         for($cc = 0; $cc < count($input['credit_card']); $cc++):
          DB::table('users_to_creditcards')
          ->insert([
          'user_id'        => $user_last_id,
          'credit_card_id' => $input['credit_card'][$cc],
          'created_at'   => $created_at,
          'updated_at'   => $updated_at
          ]
        ); 
      endfor;
      endif;

       //CREDIT CARDS INSERT STARTS HERE.
      if(isset($input['p_insurance']) && count($input['p_insurance']) > 0):
        
         for($pins = 0; $pins < count($input['p_insurance']); $pins++):
          DB::table('users_to_insurance')
          ->insert([
          'user_id'      => $user_last_id,
          'insurance_id' => $input['p_insurance'][$pins],
          'created_at'   => $created_at,
          'updated_at'   => $updated_at
          ]
        ); 
      endfor;


       if(isset($input['s_p_insurance']) && count($input['s_p_insurance']) > 0):
         for($pins = 0; $pins < count($input['s_p_insurance']); $pins++):
          DB::table('users_to_insurance')
          ->insert([
          'user_id'      => $user_last_id,
          'insurance_id' => $input['s_p_insurance'][$pins],
          'created_at'   => $created_at,
          'updated_at'   => $updated_at
          ]
        ); 
      endfor;


 if(isset($input['s_o_s_insurance']) && count($input['s_o_s_insurance']) > 0):
         for($pins = 0; $pins < count($input['s_o_s_insurance']); $pins++):
          DB::table('users_to_insurance')
          ->insert([
          'user_id'      => $user_last_id,
          'insurance_id' => $input['s_o_s_insurance'][$pins],
          'created_at'   => $created_at,
          'updated_at'   => $updated_at
          ]
        ); 
      endfor;
      endif;



      endif;





      endif;

//CREDIT CARDS ENDS HERE.
    if(count($input['sociallinks']) > 0):
        $imageName = ''; 
        for($i = 0; $i<count($input['sociallinks']); $i++)
        {
            $imageName = '';
        if(isset($input['sociallinks'][$i]) && $input['sociallinks'][$i] != ''):

         if(Input::file('socialicon')[$i] != ''):
             $imageName = Input::file('socialicon')[$i]->getClientOriginalName();
             Input::file('socialicon')[$i]->move('images/socials', $imageName);
         endif;

           DB::table('users_socials')->insert(
           ['user_id'    => $user_last_id,
          'link'         => $input['sociallinks'][$i],
          'icon'         => $imageName,
          'sort_order'   => $input['sort_order'][$i]]); 
     
 
        endif;
        }// endfor loop
    endif;

      }

      if ($messages->isEmpty())
      {
          return Redirect::to('/profiles')->withSuccess('Record inserted successfully.');;
      }

      return Redirect::back()->withInput()->withErrors($messages);
    }
// END COMPLETE PROFILE .
    //full complete profile

    public function fullCompleteProfile(){

      $input = Input::all();
    
     

      $number_of_doctors = $input['number_of_doctors'];
      $state_city_zip    = Profile::select('zip_code','state_id','country_id','city_id')
                                   ->where('id',$input['profile_id'])->first();
      $users_inputs                 = array();
      $catagories_inputs            = array();
      $profile_inputs               = array();
      $schedule_availibility_inputs = array();
      $association_inputs           = array(); 
      $rules = ['first_name' => 'required','last_name' => 'required',
          //'catagory_id' => 'required',
          //'sub_catagory' => 'required',
          'email'      => 'required|unique:users',
          'Practice_id' => 'required'
          //'contact_dr_phone_number' => 'required'
      ];

      $messages = $this->validateProfile($input, $rules);

      if ($messages->isEmpty()){
        //insert data
        for ($i=0; $i < $number_of_doctors ; $i++) {
          # code...
          $first_name                 = $input['first_name'][$i];
          $last_name                  = $input['last_name'][$i];
          $email                      = $input['email'][$i]; //users table
          $users_inputs['first_name'] = $first_name;
          $users_inputs['last_name']  = $last_name;
          $users_inputs['email']      = $email;
          //saving users_inputs
          $users                      = User::create($users_inputs);
          //getting last id of the user
          $user_last_id               =  $users->id;
          $catagory_id                =  $input['catagory_id'][$i];
          $sub_catagory               =  $input['sub_catagory_1'][$i];//users_specialities
          //$procedure_id               =  $input["procedure_id_".$i.""];

          $memberRole = Sentinel::findRoleByName('doctor');
          $memberRole->users()->attach($user_last_id);
           
           /*for ($j=0; $j <count($procedure_id); $j++) {
              
              DB::table('user_procedures')->insert(
              ['user_id' => $user_last_id, 'procedure_id' => $procedure_id[$j]]);
        } */
          //-----------------------------------------------------------------------------
       
      for ($j=0; $j < count($input['procedures_'.$i+1]); $j++) {
          $procedures_name               =  $input['procedures_'.$i+1][$j]; //procedures
         // $procedures_inputs['procedure_name'] = $procedures;   
        if(count($procedures_name) > 0){
          $procedures               = procedures::select('id')
          ->whereRaw('UPPER(procedure_name) ="'.strtoupper($procedures_name).'"')->first();

          if(count($procedures) > 0) {
            $procedures_last_id = $procedures->id;
          }
          else
          {
             $procedures         = procedures::insert(
          ['procedure_name' => $procedures_name, 'specialitie_id' => $input['sub_catagory'.$i+1][$i]]
        );
             $procedures_last_id  = $procedures->id;
        //saving the composite keys in teh users_associations table
          }
           $user_procedures          = DB::table('user_procedures')->insert(
          ['users_id' => $user_last_id, 'procedure_id' => $procedures_last_id]
        );
         }
       }// end for loop

        for ($j=0; $j < count($input['procedures_'.$i+2]); $j++) {
          $procedures_name               =  $input['procedures_'.$i+2][$j]; //procedures
         // $procedures_inputs['procedure_name'] = $procedures;   
        if(count($procedures_name) > 0){
          $procedures               = procedures::select('id')
          ->whereRaw('UPPER(procedure_name) ="'.strtoupper($procedures_name).'"')->first();

          if(count($procedures) > 0) {
            $procedures_last_id = $procedures->id;
          }
          else
          {
             $procedures         = procedures::insert(
          ['procedure_name' => $procedures_name, 'specialitie_id' => $input['sub_catagory'.$i+1][$i]]
        );
             $procedures_last_id  = $procedures->id;
        //saving the composite keys in teh users_associations table
          }
           $user_procedures          = DB::table('user_procedures')->insert(
          ['users_id' => $user_last_id, 'procedure_id' => $procedures_last_id]
        );
         }
       }// end for loop
      //-----------------------------------------------------------------------------

          //$catagories_inputs[] = $catagory_id;
          //$catagories_inputs[] = $sub_catagory;

          DB::table('users_specialities')->insert([
            ['users_id' => $user_last_id, 'specialities_id' => $catagory_id],
             ['users_id' => $user_last_id, 'specialities_id' => $input['sub_catagory'.$i+1][$i]],
             ['users_id' => $user_last_id, 'specialities_id' => $input['sub_catagory'.$i+2][$i]]
         ]);
          $education                               =  $input['education'][$i];
          $affiliation                             =  $input['affiliation'][$i];
          $contact_dr_phone_number                 =  $input['contact_dr_phone_number'][$i];
          $Practice_id                             =  $input['Practice_id'][$i];
       
          $imageName                               = Input::file('image_of_doctor')[$i]->getClientOriginalName();
          $input['image_of_doctor']                  = $imageName; // profile
          Input::file('image_of_doctor')[$i]->move('images/catalog', $imageName); 

          $profile_inputs['education']               = $education;
          $profile_inputs['affiliation']             = $affiliation;
          $profile_inputs['contact_dr_phone_number'] = $contact_dr_phone_number;
          $profile_inputs['Practice_id']             = $Practice_id;

          $profile_inputs['image_of_doctor']         = $input['image_of_doctor'];
          $profile_inputs['parent_id']               = $input['profile_id'];
          $profile_inputs['user_id']                 = $user_last_id;
          $profile_inputs['country_id']              = $state_city_zip->country_id;
          $profile_inputs['state_id']                = $state_city_zip->state_id;
          $profile_inputs['city_id']                 = $state_city_zip->city_id;
          $profile_inputs['zip_code']                = $state_city_zip->zip_code;

        //var_dump($profile_inputs);die;
        //saving profile_inputs
          $Profile                                   = Profile::create($profile_inputs);
          //getting last id of the profile
          $profile_last_id                           = $Profile->id;
          $opening_sunday                            =  $input['opening_sunday'][$i];
          $opening_monday                            =  $input['opening_monday'][$i];
          $opening_tuesday                           =  $input['opening_tuesday'][$i];
          $opening_wednesday                         =  $input['opening_wednesday'][$i];
          $opening_thursday                          =  $input['opening_thursday'][$i];
          $opening_friday                            =  $input['opening_friday'][$i];
          $opening_saturday                          =  $input['opening_saturday'][$i];// schedule_availibility

          ///
          $sunday = array(
                          'profile_id'=>$profile_last_id,
                          'days'=>'sunday',
                          'open'=>$opening_sunday

                        );

          $Profileschedule = Profileschedule::create($sunday);

          $monday = array(
                          'profile_id'=>$profile_last_id,
                          'days'=>'monday',
                          'open'=>$opening_monday

                        );

          $Profileschedule = Profileschedule::create($monday);

          $tuesday = array(
                          'profile_id'=>$profile_last_id,
                          'days'=>'tuesday',
                          'open'=>$opening_tuesday

                        );

          $Profileschedule = Profileschedule::create($tuesday);

          $wednesday = array(
                          'profile_id'=>$profile_last_id,
                          'days'=>'wednesday',
                          'open'=>$opening_wednesday

                        );

          $Profileschedule = Profileschedule::create($wednesday);

          $thursday = array(
                          'profile_id'=>$profile_last_id,
                          'days'=>'thursday',
                          'open'=>$opening_thursday

                        );

          $Profileschedule = Profileschedule::create($thursday);

          $friday          = array(
                                    'profile_id'=>$profile_last_id,
                                    'days'=>'friday',
                                    'open'=>$opening_friday

                                  );

          $Profileschedule = Profileschedule::create($friday);

          $saturday         = array(
                                  'profile_id'=>$profile_last_id,
                                  'days'=>'saturday',
                                  'open'=>$opening_saturday

                                );

          $Profileschedule            = Profileschedule::create($saturday);
          ///

          $association                =  $input['association'][$i]; //association
          $association_inputs['name'] = $association;   

          $associations               = association::select('id')
          ->whereRaw('UPPER(name) ="'.strtoupper($association).'"')->first();

          if(count($associations) > 0) {
            $association_last_id = $associations->id;
          }
          else
          {
             $association = association::create($association_inputs);
             $association_last_id         = $association->id;
        //saving the composite keys in teh users_associations table
          }
           $users_associations          = DB::table('users_associations')->insert(
          ['users_id' => $user_last_id, 'associations_id' => $association_last_id]
        );




        }
      }

      if ($messages->isEmpty())
      {
          return Redirect::to('/profiles')->withSuccess('Record inserted successfully.');;
      }

      return Redirect::back()->withInput()->withErrors($messages);



    }
// END COMPLETE PROFILE .

    //SINGLE MEMBER ADD
      public function completesingleProfile($id){
      $Profile           = Profile::find($id);
      $procedures        = DB::table('procedures')->get();
      $doctors           = Profile::where('parent_id',$Profile->id)->get();
      $association_names = association::select('name')->get();
      

      $data              = Profile::find($id);      
      //getting catagories
      $catagories        = Catagory::where('parent_id','<>', 0)->get();
      $catagory          = Catagory::where('parent_id', 0)->get();
      //getting insurance
      $insurances        = Insurance::where('parent_id', 0)->get();
      $creditCards       = creditcard::get();
     
    return View::make('completeSingleMemberProfiles', compact('mode', 'data', 'catagory', 'catagories', 'insurances' , 'association_names','Profile','doctors','procedures','creditCards'));
    }
    //SINGLE ENDS.
     public function getjasonDoctorPractiveId() { 
        $term        = Input::get('query');
        $profile     = Profile::select('id')->where('user_id', Sentinel::getUser()->id)->first(); 
        $practiceIds = Profile::select('Practice_id')->where('Practice_id','LIKE','%'.$term.'%')->where('parent_id','<>',$profile->id)->where('parent_id','<>',0)->get();
         $row_array = array();
        foreach($practiceIds as $cats) {
         $row_array[] = $cats->Practice_id; 
        }
      echo json_encode($row_array); 
    }

public function getjasonDoctorEmail () { 
        $term        = Input::get('query'); 
        $practiceIds = User::where('email',''.$term.'')->count();
        if($practiceIds > 0): return 'YES';
        else:  return 'NO';
        endif;
         }


    //populate subInsurances
    public function subInsurances() {
      $input      = Input::all();
      $input['p_insurance'];
      $insurances = DB::table('insurance')
      ->where('parent_id', '=',$input['p_insurance'])
      ->lists('insurance_name', 'id');
      return  json_encode($insurances);
    }
    //populate sub of sub insurance
    public function subOfSubInsurances() {
      $input      = Input::all();
//var_dump($input,'herer in the sub of sub in');die;
      $input['s_p_insurance'];
      $insurances = DB::table('insurance')
      ->where('parent_id', '=',$input['s_p_insurance'])
      ->lists('insurance_name', 'id');
      return  json_encode($insurances);
    }
   //Edit profile

  public function editCompleteProfile($id) {
    //find the doctor profile
    $doctor_profile    = Profile::where('id',$id)->first();
    //find the doctor as user
    $doctor            = Sentinel::findById($doctor_profile->user_id);
    //getting the business_Profile
    $business_Profile	 = Profile::where('user_id', Sentinel::getUser()->id)->first();

     $Profile          = Profile::where('user_id', Sentinel::getUser()->id)->first();
     $doctors          = Profile::where('parent_id',$Profile->id)->get(); 

    //getting main Catagory
    $catagory          = Catagory::where('parent_id', 0)->get();
    $catagories        = Catagory::where('parent_id', '!=', 0)->get();
    //get associations
    $user_id           = $doctor_profile->user_id;
    
    $procedures        = DB::table('procedures')->get(); 
    $association_names = association::select('name')->get();
    //$associations      = User::find($user_id)->getAssociation()->get();
    $associations      = DB::table('association')->join('users_associations', 'association.id', '=', 'users_associations.associations_id')->select('association.name','users_associations.*')
    ->where('users_id',$user_id)->get(); 

    $collection['checkSocialLinks'] = DB::table('users_socials')->where('user_id',$user_id)->orderBy('sort_order')->get();



    $specialities       = User::find($user_id)->getSpecialities()->get(); 
    $procedures_ids       = User::find($user_id)->getProcedures()->get(); 
    $procedures_array     = array();
    $procedures_combine   = array();
    $specialities_combine = array();
    foreach($procedures_ids as $procedure):      
      $procedures_combine[] = $procedure->pivot->procedure_id;
    endforeach;
    foreach($procedures as $proc):
      if(in_array($proc->id, $procedures_combine)) {
      $procedures_array[$proc->specialitie_id][$proc->id] = $proc->procedure_name;
       }
    endforeach;
    foreach($specialities as $specialitie):
      $specialities_combine[] = $specialitie->pivot->specialities_id;

    endforeach;  


    return View::make('editProfilePersonalInfomation',
                compact('doctor_profile', 'doctor', 
                       'business_Profile', 'catagory',
                       'catagories','specialities_combine',
                       'associations','Profile','doctors',
                       'procedures_array','association_names',
                       'procedures_combine','collection'));

  }
  //update Doctor personal information

  public function updateDoctorPersonalInfo(Request $request){

    $profile_id                  = $request->profile_id;
    $user_id                     = $request->user_id;
    $input                       = array();
    $input['user_id']            = $request->user_id;
        $input['profile_id']     = $request->profile_id;
        $input['parent_id']      = $request->parent_id;
        $input['Practice_id']    = $request->Practice_id;
        $input['first_name']     = $request->first_name;
        $input['last_name']      = $request->last_name;
        $input['gender']         = $request->gender;
        $input['user_dob']       = $request->user_dob;
        $input['catagory_id']    = $request->catagory_id; 
        $input['sub_catagory_1'] = $request->sub_catagory_1;
        $input['sub_catagory_2'] = $request->sub_catagory_2;
        $input['procedures_1']   = $request->procedures_1;
        $input['procedures_2']   = $request->procedures_2;
        $input['education']      = $request->education;
        $input['affiliation']    = $request->affiliation;
        $input['contact_dr_phone_number'] =$request->contact_dr_phone_number;
        $input['email']          = $request->email;
        $input['image_of_doctor'] =$request->image_of_doctor;
        $input['_token']          = $request->_token;   
    $rules = [
        'first_name' => 'required',
        'last_name'  => 'required',
        'email'      => 'required|unique:users',
        //'avatar'   => 'required',
        'contact_dr_phone_number' => 'required',
        'Practice_id' => 'required'
    ];
    $user            = User::find($user_id);
    $rules['email'] .= ",email,{$user->email},email";
    $Profile         = Profile::find($profile_id);
    $messages        = $this->validateProfile($input, $rules);

    if ($profile_id)
    {

      if ($messages->isEmpty())
      {
        //update users
        $user->fill($input);
        $user->save();
        //update the user profile
        if(isset($input['image_of_doctor']) && !empty($input['image_of_doctor']))
        {
            \File::delete('images/catalog/'.$Profile->image_of_doctor);
            $imageName = Input::file('image_of_doctor')->getClientOriginalName();
            Input::file('image_of_doctor')->move('images/catalog', $imageName);
            $input['image_of_doctor']   = $imageName;
        }else unset($input['image_of_doctor']); 
        $Profile->fill($input);
        $Profile->save();
         ///UPDATE ASSOCIATIONS 
        $data = Input::all();
        
        $errors = array_filter($data['association']);
      
        if(!empty(@$data['association'])):// If all the associations are removed then delete all

        $totalAssociations = count($data['association']);
        if($totalAssociations >= 1 && !empty($errors)): 

             DB::table('users_associations')->where('users_id', '=', $user_id)->delete();

        for($a = 0; $a < $totalAssociations; $a++):
          $association                =  $data['association'][$a]; //association
          $association_inputs['name'] = $association;   

          $associations               = association::select('id')
          ->whereRaw('UPPER(name) ="'.strtoupper($association).'"')->first();

          if(count($associations) > 0) {
             $association_last_id = $associations->id;
          }

           $users_associations  = DB::table('users_associations')
           ->insert([
           'users_id' => $user_id,
           'associations_id' => @$association_last_id,
          'availability'=>$data['availability'][$a],'from_time'=>$data['from_time'][$a],'to_time'=>$data['to_time'][$a],'associations_order'=>$a,'date_added'=>date("Y-m-d H:i:s")]
        ); 
          endfor;
          endif;

      else: 
        DB::table('users_associations')->where('users_id', '=', $user_id)->delete();
endif;
          // ASSOCIATIONS ENDS HERE..................
// UPDATE SOCIAL LINKS

      $imageName = ''; 
        for($i = 0; $i<count($data['sociallinks']); $i++)
        {
            $imageName = '';
        if(isset($data['sociallinks'][$i]) && $data['sociallinks'][$i] != ''):

          if(isset($data['link_id'][$i]) && $data['link_id'][$i] != ''):

            $social_links = DB::table('users_socials')
            ->where('id',$data['link_id'][$i])->first(); 
         
            if(Input::file('socialicon')[$i] == ''):
             $imageName = $social_links->icon;
            else:

            \File::delete('images/socials/'.$social_links->icon);
             $imageName = Input::file('socialicon')[$i]->getClientOriginalName();
             Input::file('socialicon')[$i]->move('images/socials', $imageName); 

         endif;
        DB::table('users_socials')->where('id', $data['link_id'][$i])
             ->where('user_id', $user_id)->update(
           ['link'         => $data['sociallinks'][$i],
          'icon'         => $imageName,
          'sort_order'   => $data['sort_order'][$i]]);

          else:
             if(Input::file('socialicon')[$i] != ''):
             $imageName = Input::file('socialicon')[$i]->getClientOriginalName();
             Input::file('socialicon')[$i]->move('images/socials', $imageName);
         endif;

           DB::table('users_socials')->insert(
           ['user_id'    => $user_id,
          'link'         => $data['sociallinks'][$i],
          'icon'         => $imageName,
          'sort_order'   => $data['sort_order'][$i]]); 
         endif;


        endif;
        }



        ///UPDATE SPECILITIES
         DB::table('users_specialities')->where('users_id', '=', $input['user_id'])->delete();
         DB::table('users_specialities')->insert([
            ['users_id' => $input['user_id'], 'specialities_id' => $input['catagory_id']],
            ['users_id' => $input['user_id'], 'specialities_id' => $input['sub_catagory_1']],
            ['users_id' => $input['user_id'], 'specialities_id' => $input['sub_catagory_2']]
         ]);
         //UPDATE PROCEDURES.

         //DB::table('user_procedures')->where('user_id', '=', $input['user_id'])->delete();

           
           // INSERT PROCEDURES_________________________________________
         DB::table('user_procedures')->where('user_id', '=', $user_id)->delete();
         for($j = 1; $j<3;$j++){ 

          if($input['sub_catagory_'.$j] != -1)
          {
          
         if(!empty($input['procedures_'.$j]) && isset($input['procedures_'.$j])){

                  $procedure_array = explode(',', $input['procedures_'.$j]);
                  foreach ($procedure_array as $procedure_names) {
                    $procedure_return_array = array();
                    # code... 
                   // echo $input['sub_catagory_'.$j];
                $procedure_return_array               = Procedure::select('id')
                ->whereRaw('UPPER(procedure_name) ="'.strtoupper(trim($procedure_names)).'"')->first();

                      if(count($procedure_return_array) > 0) {
                         $procedure_last_id = $procedure_return_array->id;
                        
                      }
                      else
                      {
                        $procedures_inputs['procedure_name'] = trim($procedure_names);
                        $procedures_inputs['specialitie_id'] = $input['sub_catagory_'.$j];
                        $procedure_return_array              = Procedure::create($procedures_inputs);
                        $procedure_last_id                   = $procedure_return_array->id;
                  
                      }
                       
                       $users_procedures          = DB::table('user_procedures')->insert(
                      ['user_id' => $user_id, 'procedure_id' => $procedure_last_id]); 
                 }// END OF FOREACH.
              }// END OF OUTERE IF CONDITIONS.

            }// END OF CATEGORY CHECKING IF CONDITION.
    }// end of for loop.
          //--------------------------------------------------------






      }else{
          return Redirect::back()->withInput()->withErrors($messages);
        }
    }

    if ($messages->isEmpty())
    {
        return Redirect::to('/profiles')->withSuccess('Record updated successfully.');
    }else {
      return Redirect::back()->withInput()->withErrors($messages);
    }


  }

//edit editScheduleAvailability

public function editScheduleAvailability($id){

  //getting the business_Profile
  $business_Profile	= Profile::where('user_id', Sentinel::getUser()->id)->first();

  $schedules        = Profileschedule::where('profile_id',$id)->get();
  $dr_Profile	      = Profile::where('id', $id)->first();

//get doctor name
  $doctor  = Sentinel::findById($dr_Profile->user_id);
  $Profile = Profile::where('user_id', Sentinel::getUser()->id)->first();
  $doctors = Profile::where('parent_id',$Profile->id)->get();
return View::make('editScheduleAvailability', compact('schedules', 'business_Profile', 'doctor', 'dr_Profile','Profile','doctors'));
}
//updating updateScheduleAvailability

public function updateScheduleAvailability(Request $request)
{
  $profile_id = $request->profile_id;
  $input      = Input::all(); 
  $schedules  = array(
    $input['opening_sunday'][0],
    $input['opening_monday'][0],
    $input['opening_tuesday'][0],
    $input['opening_wednesday'][0],
    $input['opening_thursday'][0],
    $input['opening_friday'][0],
    $input['opening_saturday'][0]
  );
$Profile_schedule = Profileschedule::where('profile_id', $profile_id)->get();

foreach ($Profile_schedule as $key => $val) {
  $days[] = $val->days;
}

$i = 0;
  foreach ($schedules as $key => $value) {
    $update_schedule = Profileschedule::where('profile_id', $profile_id)
                     ->where('days', $days[$i])
                     ->first();
     
     $update_schedule->from           = $input['from_time'][$i];
     $update_schedule->to             = $input['to_time'][$i];
     $update_schedule->break_time     = $input['break'][$i];
     $update_schedule->break_duration = $input['duration'][$i];
   
     $update_schedule->open = $value;
    //$update_schedule->fill($value);
    $update_schedule->save();
$i++;
  }

  return Redirect::to('profiles')->withSuccess('Record updated successfully.');



}

public function settings ()
{

  $Profile = Profile::where('user_id', Sentinel::getUser()->id)->first();
  $doctors = Profile::where('parent_id',$Profile->id)->get();
 return View::make('members.changepassword',compact('Profile','doctors'));

}
  public function changepassword(Request $request /*this is model object*/)
	{ 			 
	  $input = Input::except('_token'); 
	   
	  $rules = array(       
		  'password'              => 'required|min:6|confirmed',
          'password_confirmation' => 'required|min:6'          // required and has to match the password field
	  );
	   
	     $messages = $this->validateProfile($input, $rules);

      if ($messages->isEmpty())
      {
      	user::where('id', Sentinel::getUser()->id)
      	->update(array('password' => bcrypt($input['password']) 
							)); 
		  
		  return Redirect::to('profiles/settings')->withSuccess('Password changed successfully.');
      }
	  else
	  {  
	   return redirect()->back()->withErrors($messages);
	      
	  }
	
	
	}



     public function removeSocials($id)
    {
        
            $social_links = DB::table('users_socials')
            ->where('id',$id)->first();

            \File::delete('images/socials/'.$social_links->icon);
         DB::table('users_socials')->where('id', $id)->delete();

    }




}
