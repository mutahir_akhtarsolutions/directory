<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Catagory;
use DB;
use Input;
use App\Profile;
use App\users_specialities;
use App\User;
use DOMDocument;
use DOMXPath;
use App\Http\Controllers\PaginatorController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Reviews;
use App\Helpers\MailChimp;
//use App\Helpers\MyHelper;
class indexController extends Controller
{
    use AuthenticatesAndRegistersUsers;
    /**
     * Display a listing of the resource.By Mutahir
     *
     * @return \Illuminate\Http\Response
     */
    public $var; 
    public function __construct()
    {

    }
    public function mailtest()
    { // Testing function , This function is not in use.
      echo 'mutahir shah'; exit;
      $MailChimp = new MailChimp('14aec4c0808bded04e475813eb909996-us13');
      $list_id = '1190a20802';

$result = $MailChimp->post("lists/$list_id/members", [
                'email_address' => 'yousaf670@gmail.com',
                'status'        => 'subscribed',
                'merge_vars'    => array('FNAME'=>'Test', 'LNAME'=>'Account'),
            ]);

print_r($result);
      $result = $MailChimp->get('lists');

print_r($result);
echo $MailChimp->getLastError().'<pre>';
print_r($MailChimp->getLastResponse());
    }
    public function index()
    {
          $collection['doctors'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get(); 
          $collection['procedures'] = DB::table('procedures')->orderBy('procedure_name')->get();
          $collection['catagories']	= Catagory::where('parent_id', 0)->orderBy('name')->get();
          $collection['locations']	= DB::table('state')->orderBy('state_name')->get();

          return View::make('index', compact('collection')); 
    }
    //Simple search for doctor
    public function returnJsonStates()
    {
      $term = Input::get('term'); 
      $locations  = DB::table('state')
                                ->where('state_name', 'like', '%'.$term.'%')
                                ->orderBy('state_name')->get();
  $return_arr = array();
 
    foreach($locations as $loc) {
        $row_array['id'] = $loc->id;
        $row_array['value'] = $loc->state_name;
         
        array_push($return_arr,$row_array);
    }

      echo json_encode($return_arr);


    }// END OF JASON.

    public function returnJsonCatagory()
    {
      $term = Input::get('term'); 
     $catagories = Catagory::where('parent_id', 0)
                   ->where('name', 'like', '%'.$term.'%')
                   ->orderBy('name')->get();
  $return_arr = array();
 
    foreach($catagories as $cats) {
        $row_array['id'] = $cats->id;
        $row_array['value'] = $cats->name;
         
        array_push($return_arr,$row_array);
    }

      echo json_encode($return_arr);


    }// END OF JASON.


    public function simpleSearchForDoctor()
    {  
      $input            = Input::all();  
      $state_id         = @$input['location'];
      $search_keyword   = @$input['search_keyword'];
      $catagegory       = @$input['catagory'];
      $specialities_ids = @Input::get('specialities');
      $procedures_ids   = @$input['procedures'];
      $city_ids         = @Input::get('state_citie');
      $sort_by          = @Input::get('sort_by');

      $collection['catagories'] = Catagory::where('parent_id', 0)
                                ->orderBy('name')->get();
      $collection['locations']  = DB::table('state')
                               ->orderBy('state_name')->get();
      $query_variable  = ' 1 ';
      $query_string    =  array(); 

      if(isset($state_id) && $state_id != '0' && $state_id != '')
      {
        $collection['cities'] = DB::table('city')
                          ->select('id','city_name')
                          ->orderBy('city_name')
                          ->where('state_id', $state_id)
                          ->get();
         $query_variable .= ' AND state_id = '.$state_id; 
         $query_string['location'] = $state_id;}
      
      /*if (isset($catagegory) && $catagegory !=''  && $catagegory !=0) {

        $query_string['catagory']        = $catagegory;
        $collection['child_specilities'] = Catagory::where('parent_id', $catagegory)
        ->orderBy('name')->get();
      }*/

      if (isset($city_ids) && $city_ids !='' && $city_ids != 0 ) {
                $query_variable .=' AND city_id IN ('.$city_ids.') '; 
                $query_string['state_citie'] = $city_ids;
              }


      if (isset($specialities_ids) && $specialities_ids !='') {
          $query_variable .=' AND specialities_id IN ('.$specialities_ids.') ';
          $query_string['specialities'] = $specialities_ids;

          $collection['procedures']     = DB::table('procedures') 
                   ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                   ->select('procedures.*')
                   ->whereIn('procedures.specialitie_id', explode(',',$specialities_ids))
                   ->groupBy('procedures.id')->get();
                   $query_string['specialities'] = $specialities_ids; 


            $collection['child_specilities'] = Catagory::where('parent_id', $catagegory)
        ->orderBy('name')->get();

        if (isset($catagegory) && $catagegory!=''  && $catagegory!= 0) {
            $query_string['catagory']      = $catagegory;
          }

         // users in specific category. 
        }elseif (isset($catagegory) && $catagegory!=''  && $catagegory!= 0) {
            $query_string['catagory']      = $catagegory;
            $query_variable .=' AND specialities_id ='.$catagegory;

            $collection['child_specilities'] = Catagory::where('parent_id', $catagegory)
        ->orderBy('name')->get();
           }



        if (isset($procedures_ids) && $procedures_ids !='') {
        $query_variable .=' AND procedure_id IN ('.$procedures_ids.') ';
        $query_string['procedures'] = $procedures_ids;
 
       }

      if(isset($search_keyword) && $search_keyword != '')
      {
         $query_variable .= " AND wholename LIKE '%".$search_keyword."%'";
         $query_string['search_keyword'] = $search_keyword;
      } 
       $order = 'ASC';
       if(isset($sort_by)){

           switch($sort_by){
           case 'rating':
           $query_string['sort_by'] = $sort_by;
            $order = 'DESC';
           break;
           case 'wholename':
           $query_string['sort_by'] = $sort_by;
           $order = 'ASC';
           break;

         }

        }else $sort_by = 'id'; 
      // COMBINED SEARCH QUERY. 
      $users = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,ROUND(AVG(`rating`)) AS averageRating'))->whereRaw($query_variable)->groupBy('id')->orderBy($sort_by, $order)->paginate(6);
      // SEARCH QUERY ENDS HERE.  
      $collection['doctors'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get(); 
           
           $users->appends($query_string);// APPENT TO PAGINATION 

      return View::make('doctorListing', compact('users','locations','collection'));
    } 



    /**
     * Show the form for doctorDetailPage a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doctorDetailPage($id)
    {
       
    $collection['catagories'] = Catagory::where('parent_id', 0)
                ->orderBy('name')
                ->get();
    $collection['locations']  = DB::table('state')
                ->orderBy('state_name')->get();

    $collection['doctors']     = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,ROUND(AVG(`rating`)) AS averageRating'))->where('user_id', $id)->groupBy('id')
       ->get();
   $collection['users_procedures'] = User::find($id)->getProcedures()->get(); 
      // $collection['users_specialities'] = User::find($id)->getSpecialities()->get();
    //var_dump($collection['doctors'][0]->id);die;
    $collection['business_profile'] = Profile::find($collection['doctors'][0]->parent_id);
    //var_dump($profile->id);die;
    $collection['related_doctors']  = DB::table('profile')
                        ->join('users', 'profile.user_id', '=', 'users.id')
                        ->select('profile.*', 'users.*')
                        ->where('parent_id', $collection['business_profile']->id)
                        //->where('id', '!=', $doctor[0]->id)
                        ->get();
                        $doctors = DB::table('profile')
                            ->join('users', 'profile.user_id', '=', 'users.id')
                            ->select('profile.*', 'users.*')
                            ->where('parent_id','<>', 0)
                            ->get();
   /* if ($commenter_id = Sentinel::getUser())
    {
      $commenter_id  = $commenter_id->id;
	    $collection['loginUserReview'] = Reviews::where('doctor_id',$id)
	                                 ->where('user_id', $commenter_id)->count();
	    }
    else {
      $collection['loginUserReview'] = 0; 
    }*/
    $collection['loginUserReview'] = Reviews::where('doctor_id',$id)->count();

    $collection['userReviews']  = DB::table('view_rating_doctors')->where('doctor_id',$id)->where('approved','1')->where('reviews_type','<>','Archive')->get();
    $collection['averageUserReviews'] = Reviews::where('doctor_id',$id)->where('approved','1')->avg('rating');

    $collection['averageReviewsAll'] = DB::table('users_to_reviews')
                ->select(DB::raw('ROUND(AVG(behaviour)) as behav , COUNT(behaviour) as total_behav ,  ROUND(AVG(availability)) as available, COUNT(availability) as total_available , ROUND(AVG(enviroment)) as envi , COUNT(enviroment) as total_envi'))->whereRaw('doctor_id = ?',array($id))->first(); 
    $collection['secedualAvailability'] = DB::table('schedule_profile')
    ->where('profile_id',$collection['doctors'][0]->id)
    ->where('open','<>',0)->get();

    return View::make('doctorDetail', compact('doctors','collection'));
    }

     public function doctorReviewPage($id)
    {
      //set_time_limit(0);
      //ini_set('memory_limit', '4095M');
      //error_reporting(0);
      /*$url = 'http://www.healthgrades.com/specialty-directory';
      $json = $this->curl_get_contents($url);
      preg_match_all('/<div class="specialtyBlock">(.*?)<\/div>/s',$json,$estimates);

      //print_r($estimates[1]);die;
      preg_match_all('/<a (.*?)>(.*?)<\/a>/s',$estimates[1][0],$estimates_1);

      foreach ($estimates_1[2] as $key => $value) {
        echo $value.'<br>';
        Catagory::create(['name' => $value]);
      }
      die;
      echo '<pre>';
      print_r($estimates_1);die;*/
    $collection['catagories'] = Catagory::where('parent_id', 0)
                ->orderBy('name')
                ->get();
    $collection['locations']  = DB::table('state')
                ->orderBy('state_name')->get();

    $collection['doctor']     = DB::table('profile')
                ->join('users', 'profile.user_id', '=', 'users.id')
                ->select('profile.*', 'users.*')
                ->where('user_id', $id)
                ->get();
   $collection['users_procedures'] = User::find($id)->getProcedures()->get(); 
      // $collection['users_specialities'] = User::find($id)->getSpecialities()->get();
    //var_dump($doctor[0]->id);die;
    $collection['business_profile'] = Profile::find($collection['doctor'][0]->parent_id);
    //var_dump($profile->id);die;
    $collection['related_doctors']  = DB::table('profile')
                        ->join('users', 'profile.user_id', '=', 'users.id')
                        ->select('profile.*', 'users.*')
                        ->where('parent_id', $collection['business_profile']->id)
                        //->where('id', '!=', $doctor[0]->id)
                        ->get();
                        $doctors = DB::table('profile')
                        ->join('users', 'profile.user_id', '=', 'users.id')
                        ->select('profile.*', 'users.*')
                        ->where('parent_id','<>', 0)
                        ->get();        
    return View::make('doctorDetail', compact('doctors','collection'));
    }

private function curl_get_contents($url)
{
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function starRatting(Request $request)
    { 
          $inputs['reviews_rating'] =  $request->get('rate');
          $data                     =   Sentinel::getUser();
          $inputs['user_name']      = $data->first_name.' '.$data->last_name;
          $inputs['user_id']        = $data->id;
          $inputs['doctor_id']      = $request->get('doctor');
          $inputs['date_added']     = date("Y-m-d h:i:s");
          $inputs['reviews_status'] = 1;

          $total = DB::table('reviews')->where('doctor_id',$inputs['doctor_id'])
                              ->where('user_id',$inputs['user_id'])
                              ->count();
           if ($total == 0) { 
          DB::table('reviews')->insert($inputs);
        }
    }

    public function login(Request $request)
    {
      $throttles         = $this->isUsingThrottlesLoginsTrait(); 
      $input['email']    = $request->login_email;
      $input['password'] = $request->login_password;
      $doctor_id         = $request->doctor_id;
      try {
            $credentials = Sentinel::authenticate($input);
            if($credentials)
            {
                $commenter_id          = Sentinel::getUser();
                $commenter_id          = $commenter_id->id; 
                $checkUserComments     = Reviews::where('doctor_id',$doctor_id)
                               ->where('user_id', $commenter_id)->count();
               //return $this->handleUserWasAuthenticated($request, $throttles);
          if($checkUserComments > 0)
                {
                  echo 'COMMENTS';
                }
                else{

                  echo 'OK';
                } 
            }
            else
            {
              echo  $errors = $this->getFailedLoginMessage();
            }
          }
          catch (NotActivatedException $e)
          {
               echo 'Account is not activated!' ;
          }
    }

      public function create(Request $request)
    {


        $inputs = $request->all(); 
        $rules  = [
            'email'    => 'required|email|unique:users', 
            'password' => 'required|confirmed',
            'password_confirmation'=> 'required',
            ];

    $messages = $this->validatorGeneral($request->all() , $rules); 
      if ($messages->isEmpty())
        {
        //@todo: this should be move to config
        $autoActivate = false;

        if( $user = Sentinel::register($request->all(), $autoActivate ) )
        {

            //Sentinel::getRoleRepository()->findById($inputs['role']); 
            //$memberRole = Sentinel::findRoleByName('member');
            $memberRole = Sentinel::findRoleById($inputs['role']);
            $memberRole->users()->attach($user); 
            DB::table('profile')->insert(['user_id' => $user->getUserId()]);
            // INSERT AN ID INTO THE PROFILE TABLE ALSO
            if( false === $autoActivate )
            {
                $activation = Activation::create($user);
                $code = $activation->code;
                $sent = \Mail::send('sentinel.emails.activate', compact('user', 'code'), function($m) use ($user)
                {
                    $m->to($user->email)->subject('Activate Your Account');
                });
                if ($sent === 0)
                {
                   print ('Failed to send activation email.');
                }
                print ('Your account was successfully created. You might login now.');
            }else{
                $credentials = $this->getCredentials($request);
                Sentinel::authenticate($credentials);
                return redirect($this->redirectPath());
            }
        }
      //  print ('An error occured while registering.');
      }else
        {  
          $messages->toArray(); 
          echo $messages->first('email', ':message');
          echo $messages->first('password', ':message');
        }
    }

    public function checkLoginUser()
    {   

        $user      =   Sentinel::check();
        $doctor_id = Input::get('doctor_id'); 
         // $inputs['user_id']   = $data->id;  
         if ($user)
         {
          $commenter_id          = Sentinel::getUser();
          $commenter_id          = $commenter_id->id; 
          $checkUserComments     = Reviews::where('doctor_id',$doctor_id)
                                 ->where('user_id', $commenter_id)->count();
          if($checkUserComments > 0)
          {
            echo 'COMMENTS';
          }
          else{
            echo 'LOGIN';
          }
       }
       else echo 'NO';
    }
    protected function validatorGeneral($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    } 

}
