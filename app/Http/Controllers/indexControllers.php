<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Catagory;
use DB;
use Input;
use App\Profile;
use App\users_specialities;
use App\User;
use DOMDocument;
use DOMXPath;
use App\Http\Controllers\PaginatorController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Reviews;
use App\Helpers\MailChimp;
use Carbon\Carbon;
use App\Album;
use App\Models\Image;
use Mail;
use App\Models\News;
use App\Models\Conditions;  
//use App\Helpers\MyHelper;
use App\Models\Page;
class indexControllers extends Controller
{
    use AuthenticatesAndRegistersUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $var; 
    public function __construct()
    {

    }
    public function mailtest()
    { // Testing function , This function is not in use.
      echo 'mutahir shah'; exit;
      $MailChimp = new MailChimp('14aec4c0808bded04e475813eb909996-us13');
      $list_id = '1190a20802';

$result = $MailChimp->post("lists/$list_id/members", [
                'email_address' => 'yousaf670@gmail.com',
                'status'        => 'subscribed',
                'merge_vars'    => array('FNAME'=>'Test', 'LNAME'=>'Account'),
            ]);

      print_r($result);
            $result = $MailChimp->get('lists');

      print_r($result);
      echo $MailChimp->getLastError().'<pre>';
      print_r($MailChimp->getLastResponse());
    }
    public function index()
    {
      $collection['doctors_names_autocomplete'] = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`, `wholename`'))
      ->orderBy('wholename')->groupBy('id')->get();

          $collection['doctors'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get(); 
          $collection['procedures'] = DB::table('procedures')->orderBy('procedure_name')->get();
          $collection['catagories']	= Catagory::where('parent_id', 0)->orderBy('name')->get();
          $collection['locations']	= DB::table('state')->orderBy('state_name')->get();
 
          $collection['footerpages'] = Page::select('id','title')
          ->whereIn('id',array(2,3,4,5,6,7,8))->where('status',1)
          ->orderBy('sort_order','DESC')->get();
          $collection['homepages'] = Page::select('id','image','title','content')->where('status',1)
                     ->whereIn('id',array(9,10,11,12))
                     ->orderBy('sort_order','DESC')->get();
          $homePagesArray       = array();
          if(count($collection['homepages'])>0):
          foreach($collection['homepages'] as $homepages):
        $homePagesArray[$homepages->id] = array('id'=>$homepages->id,'title'=>$homepages->title,
                                           'content'=>$homepages->content);
          endforeach;   
          endif;
        $collection['homePagesArray'] = $homePagesArray;
        return View::make('index', compact('collection')); 
    }
    //Simple search for doctor
    public function returnJsonStates()
    {
      $term        = Input::get('term'); 
      $locations   = DB::table('state')
       ->where('state_name', 'like', '%'.$term.'%')
       ->orderBy('state_name')->get();
       $return_arr = array();
 
    foreach($locations as $loc) {
        $row_array['id'] = $loc->id;
        $row_array['value'] = $loc->state_name;
         
        array_push($return_arr,$row_array);
    }

      echo json_encode($return_arr);


    }// END OF JASON.

    public function returnJsonCatagory()
    {
      $term = Input::get('term'); 
     $catagories = Catagory::where('parent_id', 0)
                   ->where('name', 'like', '%'.$term.'%')
                   ->orderBy('name')->get();
  $return_arr = array();
 
    foreach($catagories as $cats) {
        $row_array['id'] = $cats->id;
        $row_array['value'] = $cats->name;
         
        array_push($return_arr,$row_array);
    }

      echo json_encode($return_arr);


    }// END OF JASON.


    public function simpleSearchForDoctor()
    {  
      $input            = Input::all();  
      $state_id         = @$input['location'];
      $search_keyword   = @$input['search_keyword'];
      $catagegory       = @$input['catagory'];
      $specialities_ids = @Input::get('specialities');
      $procedures_ids   = @$input['procedures'];
      $city_ids         = @Input::get('state_citie');
      $sort_by          = @Input::get('sort_by');
$collection['doctors_names_autocomplete'] = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`, `wholename`'))
      ->orderBy('wholename')->groupBy('id')->get();
      $collection['catagories'] = Catagory::where('parent_id', 0)
                                ->orderBy('name')->get();
      $collection['locations']  = DB::table('state')
                               ->orderBy('state_name')->get();
      $query_variable  = ' 1 ';
      $query_string    =  array(); 

      if(isset($state_id) && $state_id != '0' && $state_id != '')
      {
        $collection['cities'] = DB::table('city')
                          ->select('id','city_name')
                          ->orderBy('city_name')
                          ->where('state_id', $state_id)
                          ->get();
         $query_variable .= ' AND state_id = '.$state_id; 
         $query_string['location'] = $state_id;}
      
      /*if (isset($catagegory) && $catagegory !=''  && $catagegory !=0) {

        $query_string['catagory']        = $catagegory;
        $collection['child_specilities'] = Catagory::where('parent_id', $catagegory)
        ->orderBy('name')->get();
      }*/

      if (isset($city_ids) && $city_ids !='' && $city_ids != 0 ) {
                $query_variable .=' AND city_id IN ('.$city_ids.') '; 
                $query_string['state_citie'] = $city_ids;
              }


      if (isset($specialities_ids) && $specialities_ids !='') {
          $query_variable .=' AND specialities_id IN ('.$specialities_ids.') ';
          $query_string['specialities'] = $specialities_ids;

          $collection['procedures']     = DB::table('procedures') 
                   ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                   ->select('procedures.*')
                   ->whereIn('procedures.specialitie_id', explode(',',$specialities_ids))
                   ->groupBy('procedures.id')->get();
                   $query_string['specialities'] = $specialities_ids; 


            $collection['child_specilities'] = Catagory::where('parent_id', $catagegory)
        ->orderBy('name')->get();

        if (isset($catagegory) && $catagegory!=''  && $catagegory!= 0) {
            $query_string['catagory']      = $catagegory;
          }

         // users in specific category. 
        }elseif (isset($catagegory) && $catagegory!=''  && $catagegory!= 0) {
            $query_string['catagory']      = $catagegory;
            $query_variable .=' AND specialities_id ='.$catagegory;

            $collection['child_specilities'] = Catagory::where('parent_id', $catagegory)
        ->orderBy('name')->get();
           }



        if (isset($procedures_ids) && $procedures_ids !='') {
        $query_variable .=' AND procedure_id IN ('.$procedures_ids.') ';
        $query_string['procedures'] = $procedures_ids;
 
       }

      if(isset($search_keyword) && $search_keyword != '')
      {
         $query_variable .= " AND wholename LIKE '%".$search_keyword."%'";
         $query_string['search_keyword'] = $search_keyword;
      } 
       $order = 'DESC';
       if(isset($sort_by)){

           switch($sort_by) {
           case 'rating':
           $query_string['sort_by'] = $sort_by;
            $order = 'DESC';
           break;
           case 'wholename':
           $query_string['sort_by'] = $sort_by;
           $order = 'ASC';
           break;
           default:
              $sort_by = 'rating';
              $query_string['sort_by'] = 'rating';
           break;

         }
        }else $sort_by = 'rating'; 
      // COMBINED SEARCH QUERY. 
      $users = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`,`user_id`,`image_of_doctor`,`education`,`contact_dr_phone_number`,`parent_id` ,`wholename`,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))
      ->whereRaw($query_variable)
      ->groupBy('id')
      ->orderBy($sort_by, $order)
      ->paginate(6);
      // SEARCH QUERY ENDS HERE.  
      $collection['doctors'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get();           
    $users->appends($query_string);// APPENT TO PAGINATION 

    $collection['news'] = DB::table('news')
              ->join('news_translations', 'news.id', '=','news_translations.news_id')
              ->select('news.*', 'news_translations.title', 'news_translations.content','news_translations.language')
              ->paginate(3);  

      return View::make('doctorListing', compact('users','locations','collection'));
    } 

    // Listing page is below.
      public function doctorsListing()
    {  
      $input            = Input::all();  
      $state_id         = @$input['location'];
      $search_keyword   = @$input['search_keyword'];
      $catagegory       = @$input['catagory'];
      $specialities_ids = @Input::get('specialities');
      $procedures_ids   = @$input['procedures'];
      $city_ids         = @Input::get('state_citie');
      $sort_by          = @Input::get('sort_by');
$collection['doctors_names_autocomplete'] = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`, `wholename`'))
      ->orderBy('wholename')->groupBy('id')->get();
      $collection['catagories'] = Catagory::where('parent_id', 0)
                                ->orderBy('name')->get();
      $collection['locations']  = DB::table('state')
                               ->orderBy('state_name')->get(); 
      $query_string    =  array(); 

       $order = 'DESC';
       if(isset($sort_by)){

           switch($sort_by) {
           case 'rating':
           $query_string['sort_by'] = $sort_by;
            $order = 'DESC';
           break;
           case 'wholename':
           $query_string['sort_by'] = $sort_by;
           $order = 'ASC';
           break;
           default:
              $sort_by = 'rating';
              $query_string['sort_by'] = 'rating';
           break;

         }
        }else $sort_by = 'rating'; 
      // COMBINED SEARCH QUERY. 
      $users = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`,`user_id`,`image_of_doctor`,`education`,`contact_dr_phone_number`,,`parent_id` `wholename`,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))
      ->select(DB::raw('`id`,`user_id`,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,ROUND(AVG(`rating`)) AS averageRating'))
      ->groupBy('id')
      ->orderBy($sort_by, $order)
      ->paginate(6);
      // SEARCH QUERY ENDS HERE.  
      $collection['doctors'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get();           
    $users->appends($query_string);// APPENT TO PAGINATION 

      return View::make('frontpages/doctorListing', compact('users','locations','collection'));
    } 
    /**
     * Show the form for doctorDetailPage a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doctorDetailPage($id)
    {
      $collection['doctors_names_autocomplete'] = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`, `wholename`'))
      ->orderBy('wholename')->groupBy('id')->get();
       
    $collection['catagories'] = Catagory::where('parent_id', 0)
                ->orderBy('name')
                ->get();
    $collection['locations']  = DB::table('state')
                ->orderBy('state_name')->get();

    $collection['doctors']     = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`parent_id`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating'))->where('user_id', $id)->groupBy('id')
       ->get();
  $collection['users_procedures']   = User::find($id)->getProcedures()
                                    ->get(); 
  $collection['users_specialities'] = User::find($id)->getSpecialities()
                                    ->get();
  $collection['users_associations'] = User::find($id)->getAssociation()
                                    ->get();
   $conditions_count = DB::table('user_conditions')
                                    ->select('condition_id')
                                    ->where('user_id',$id)->count();
$collection['users_conditions'] = '';
  if($conditions_count > 0):   


  $users_conditions   = DB::table('user_conditions')
                                    ->select('condition_id')
                                    ->where('user_id',$id)->first();



    $conditions_names = Conditions::select('condition_name')
    ->whereIn('id', json_decode($users_conditions
    ->condition_id))->get();
    $collection['users_conditions'] =  json_encode($conditions_names); 
endif;
  //var_dump($collection['doctors'][0]->id);die;
    $collection['business_profile'] = Profile::find($collection['doctors'][0]->parent_id);
    //var_dump($profile->id);die;
    $collection['related_doctors'] = DB::table('userprofile_procedure_specility_review')
    ->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,gender,user_dob,ROUND(AVG(`rating`)) AS averageRating , approved'))
          ->where('parent_id', $collection['business_profile']->id)
          ->where('user_id', '!=', $id)->groupBy('id')
          ->orderBy('rating','DESC')  
          ->orderBy('wholename','ASC')
          ->get();

           $doctors = DB::table('profile')
           ->join('users', 'profile.user_id', '=', 'users.id')
           ->select('profile.*', 'users.*')
           ->where('parent_id','<>', 0)
           ->get();
   /* if ($commenter_id = Sentinel::getUser())
    {
      $commenter_id  = $commenter_id->id;
	    $collection['loginUserReview'] = Reviews::where('doctor_id',$id)
	                                 ->where('user_id', $commenter_id)->count();
	    }
    else {
      $collection['loginUserReview'] = 0; 
    }*/
    $collection['loginUserReview'] = Reviews::where('doctor_id',$id)->count();

    $collection['userReviews']  = DB::table('view_rating_doctors')->where('doctor_id',$id)->where('approved','1')->where('reviews_type','<>','Archive')->get();
    $collection['averageUserReviews'] = Reviews::where('doctor_id',$id)->where('approved','1')->avg('rating');

    $collection['averageReviewsAll'] = DB::table('users_to_reviews')
                ->select(DB::raw('ROUND(AVG(behaviour)) as behav , COUNT(behaviour) as total_behav ,  ROUND(AVG(availability)) as available, COUNT(availability) as total_available , ROUND(AVG(enviroment)) as envi , COUNT(enviroment) as total_envi'))->whereRaw('doctor_id = ?',array($id))->first(); 
$collection['secedualAvailability'] = DB::table('schedule_profile')
    ->where('profile_id',$collection['doctors'][0]->id)
    ->where('open','<>',0)->get();

$collection['latest_review'] = Reviews::select('comment')
                             ->where('doctor_id',$id)
                             ->where('approved',1)
                             ->orderBy('timestamps','DESC')
                             ->limit(1)->first();
$collection['offDays']       = DB::table('schedule_profile')
                             ->select('days')->where('open',0)
                             ->where('profile_id',$collection['doctors'][0]->id)->get();

    return View::make('doctorDetail', compact('doctors','collection'));
    }
  public function getExternalCategories()
  {

   $contents =  file_get_contents("https://www.healthgrades.com/specialty-directory/");
       $regex = '/<a\s[^>]*data-hgoname=\"specialty-link\"[^>]*>(.*)<\/a>/siU';
     preg_match_all($regex, $contents, $matches); 
 
     foreach ($matches[1] as $key => $value) { 
        Catagory::create(['name' => $value]);
      }
  }

     public function doctorReviewPage($id)
    {
      //set_time_limit(0);
      //ini_set('memory_limit', '4095M');
      //error_reporting(0);
      /*$url = 'http://www.healthgrades.com/specialty-directory';
      $json = $this->curl_get_contents($url);
      preg_match_all('/<div class="specialtyBlock">(.*?)<\/div>/s',$json,$estimates);

      //print_r($estimates[1]);die;
      preg_match_all('/<a (.*?)>(.*?)<\/a>/s',$estimates[1][0],$estimates_1);

      foreach ($estimates_1[2] as $key => $value) {
        echo $value.'<br>';
        Catagory::create(['name' => $value]);
      }
      die;
      echo '<pre>';
      print_r($estimates_1);die;*/
    $collection['catagories'] = Catagory::where('parent_id', 0)
                ->orderBy('name')
                ->get();
    $collection['locations']  = DB::table('state')
                ->orderBy('state_name')->get();

    $collection['doctor']     = DB::table('profile')
                ->join('users', 'profile.user_id', '=', 'users.id')
                ->select('profile.*', 'users.*')
                ->where('user_id', $id)
                ->get();
   $collection['users_procedures'] = User::find($id)->getProcedures()->get(); 
      // $collection['users_specialities'] = User::find($id)->getSpecialities()->get();
    //var_dump($doctor[0]->id);die;
    $collection['business_profile'] = Profile::find($collection['doctor'][0]->parent_id);
    //var_dump($profile->id);die;
    $collection['related_doctors']  = DB::table('profile')
                        ->join('users', 'profile.user_id', '=', 'users.id')
                        ->select('profile.*', 'users.*')
                        ->where('parent_id', $collection['business_profile']->id)
                        //->where('id', '!=', $doctor[0]->id)
                        ->get();
                        $doctors = DB::table('profile')
                        ->join('users', 'profile.user_id', '=', 'users.id')
                        ->select('profile.*', 'users.*')
                        ->where('parent_id','<>', 0)
                        ->get();        
    return View::make('doctorDetail', compact('doctors','collection'));
    }



public function myAlbums ($user_id)
    { 
      $data = User::find($user_id)
              ->getAlbums()
              ->orderBy('sort_order')
              ->get();
 $html = ''; 
 if(count($data) > 0):

  $html = '<div class="timeline-item"><div class="timeline-body">';
  foreach($data as $data_value):  
      if (\File::exists('images/gallery/album/icon_size/'.$data_value->album_image)):
        $html .= '<a href="#galleryModal" data-toggle="modal"><img class="marginalbumimages" alt="'.$data_value->album_name.'" src="'.url().'/images/gallery/album/icon_size/'.$data_value->album_image.'" data-tip="'.$data_value->album_description.'" title="'.$data_value->album_description.'" data-id="'.$data_value->id.'" data-value="'.$data_value->user_id.'" style="margin:10px"></a>';
      else: 
         $html .= '<a href="#galleryModal" data-toggle="modal"><img class="marginalbumimages" alt="'.$data_value->album_name.'" src="http://placehold.it/100x75" data-tip="'.$data_value->album_description.'" title="'.$data_value->album_description.'" data-id="'.$data_value->id.'" data-value="'.$data_value->user_id.'" style="margin:10px"></a>'; 
      endif;  
    endforeach;
    $html .= '</div></div>';
  echo $html;
else:
        echo '<p ><h3 class="text-center"> No Albums found.</h3></p>';
      endif;
       // return View::make('doctors.gallery', compact('collection'));
    }

public function myAlbumsPhotos()
{
   $album_id = Input::get('album_id');
   $user_id  = Input::get('user_id');
   $album    = Album::where('id',$album_id)
             ->where('user_id',$user_id)->first();

   $images   = Image::where('album_id',$album_id)
             ->orderBy('sort_order')->get();
   $newJsonArray[] = array(
                'filename'      => $album->album_name,
                'original_name' => $album->album_image);
                $newJsonArray = array();
   foreach($images as $image):
    $album_images = array( 
        'filename'      => $image->filename,
        'original_name' => $image->original_name
      );
      array_push($newJsonArray, $album_images);
   endforeach;
    return response()->json($newJsonArray);

  }



        private function curl_get_contents($url)
        {
          $ch = curl_init($url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          $data = curl_exec($ch);
          curl_close($ch);
          return $data;
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function starRatting(Request $request)
    { 
          $inputs['reviews_rating'] =  $request->get('rate');
          $data                     =   Sentinel::getUser();
          $inputs['user_name']      = $data->first_name.' '.$data->last_name;
          $inputs['user_id']        = $data->id;
          $inputs['doctor_id']      = $request->get('doctor');
          $inputs['date_added']     = date("Y-m-d h:i:s");
          $inputs['reviews_status'] = 1;

          $total = DB::table('reviews')->where('doctor_id',$inputs['doctor_id'])
                              ->where('user_id',$inputs['user_id'])
                              ->count();
           if ($total == 0) { 
          DB::table('reviews')->insert($inputs);
        }
    }

    public function login(Request $request)
    {
      $throttles         = $this->isUsingThrottlesLoginsTrait(); 
      $input['email']    = $request->login_email;
      $input['password'] = $request->login_password;
      $doctor_id         = $request->doctor_id;
      try {
            $credentials = Sentinel::authenticate($input);
            if($credentials)
            {
                $commenter_id          = Sentinel::getUser();
                $commenter_id          = $commenter_id->id; 
                $checkUserComments     = Reviews::where('doctor_id',$doctor_id)
                               ->where('user_id', $commenter_id)->count();
               //return $this->handleUserWasAuthenticated($request, $throttles);
          if($checkUserComments > 0)
                {
                  echo 'COMMENTS';
                }
                else{

                  echo 'OK';
                } 
            }
            else
            {
              echo  $errors = $this->getFailedLoginMessage();
            }
          }
          catch (NotActivatedException $e)
          {
               echo 'Account is not activated!' ;
          }
    }

      public function create(Request $request)
    {


        $inputs = $request->all(); 
        $rules  = [
            'email'    => 'required|email|unique:users', 
            'password' => 'required|confirmed',
            'password_confirmation'=> 'required',
            ];

    $messages = $this->validatorGeneral($request->all() , $rules); 
      if ($messages->isEmpty())
        {
        //@todo: this should be move to config
        $autoActivate = false;

        if( $user = Sentinel::register($request->all(), $autoActivate ) )
        {

            //Sentinel::getRoleRepository()->findById($inputs['role']); 
            //$memberRole = Sentinel::findRoleByName('member');
            $memberRole = Sentinel::findRoleById($inputs['role']);
            $memberRole->users()->attach($user); 
            DB::table('profile')->insert(['user_id' => $user->getUserId()]);
            // INSERT AN ID INTO THE PROFILE TABLE ALSO
            if( false === $autoActivate )
            {
                $activation = Activation::create($user);
                $code       = $activation->code;
                $sent       = Mail::send('sentinel.emails.activate', compact('user', 'code'),
                              function($m) use ($user)
                {
                    $m->to($user->email)->subject('Activate Your Account');
                    $m->from('mutahir@pkteam.com')->subject('Activate Your Account');
                });
                if ($sent === 0)
                {
                   print ('Failed to send activation email.');
                }
                print ('An email Has been sent to the email address,please active your account.');
            }else{
                $credentials = $this->getCredentials($request);
                Sentinel::authenticate($credentials);
                return redirect($this->redirectPath());
            }
        }
      //  print ('An error occured while registering.');
      }else
        {  
          $messages->toArray(); 
          echo $messages->first('email', ':message');
          echo '<br/>';
          echo $messages->first('password', ':message');
        }
    }

    public function checkLoginUser()
    {   

        $user      = Sentinel::check();
        $doctor_id = Input::get('doctor_id'); 
         // $inputs['user_id']   = $data->id;  
         if ($user)
         {

        $commenter_id          = Sentinel::getUser();
        $commenter_id          = $commenter_id->id; 
        $checkUserComments     = Reviews::where('doctor_id',$doctor_id)
                               ->where('user_id', $commenter_id)->count();
          if($checkUserComments > 0)
          {
            echo 'COMMENTS';
          }
          else{

            echo 'LOGIN';
          }

       }
       else echo 'NO';
    }

  public function schedualAppointment(Request $request)
  {
    $message                 = '';
    $error                   = false;
    $data                    = $request->all();
    $data['user_patient_id'] = Sentinel::getUser()->id;
    $data['created_at']      = Carbon::now();
    $data['updated_at']      = Carbon::now();
    $data['schedule_date']   = date_create($data['schedule_date']);

    $data['schedule_date']   = date_format($data['schedule_date'],"Y-m-d");
    $date                    = Carbon::createFromFormat("Y-m-d", $data['schedule_date']); 
    $checkScheduleOnDay = DB::table('schedule_profile')
                        ->where('days',''.strtolower($date->format("l")).'')
                        ->where('profile_id',$data['profile_doctor_id'])
                        ->where('open',1)
                        ->count();
  $checkApointMentQuota        = DB::table('schedule_profile_to_patient')
                     ->where('profile_doctor_id',$data['profile_doctor_id']) 
                     ->whereDate('schedule_date', '=', $data["schedule_date"])->count();
  $checkApointAlready = DB::table('schedule_profile_to_patient')
                     ->where('profile_doctor_id',$data['profile_doctor_id'])
                     ->where('user_patient_id',$data['user_patient_id']) 
                     ->whereDate('schedule_date', '=', $data["schedule_date"])->count();
    $checkApointOnthisDate = DB::table('schedule_profile_to_patient') 
                           ->where('profile_doctor_id',$data['profile_doctor_id'])
                           ->where('status',2) 
                           ->whereDate('schedule_date', '=', $data["schedule_date"])
                           ->lists('schedule_time'); 
 

  if($checkScheduleOnDay == 0):
    $message = 1;
    $error   = true; 
  endif; 
  if($checkApointMentQuota > 5):
    $message = 2;
    $error   = true;
    endif;
    if($checkApointAlready > 0):
    $message = 3;
    $error   = true;
    endif;
    if(date('Y-m-d') > $data["schedule_date"]):
    $message = 4;
    $error   = true;
    endif;
    if($data['user_patient_id'] == $data['doctors_id']):
    $message = 5;
    $error   = true;
    endif;
    if($error == false):
      unset($data['doctors_id']);
      $getScheduleOnDay = DB::table('schedule_profile')->select('from','to','break_time','break_duration')
                     ->where('days',''.strtolower($date->format("l")).'')
                     ->where('profile_id',$data['profile_doctor_id'])
                     ->where('open',1)->first();
    //DB::table('schedule_profile_to_patient')->insert($data);
    //$message = 6; 
    $duration  = 15;  // split by 30 mins                          
    $array_of_time      = array ();
    $array_ofbreak_time = array ();
    $start_time         = strtotime ($getScheduleOnDay->from); //change to strtotime
    $end_time           = strtotime ($getScheduleOnDay->to); //change to strtotime
    $add_mins           = $duration * 60;
    $gapArray           = array();
    $html               = '<tr>';
    $tdcounter          = 0;
      while ($start_time <= $end_time) // loop between time
      {
        $check_time_forbreak    = date ("H:i", $start_time); 
        if($check_time_forbreak == date ("H:i", strtotime ($getScheduleOnDay->break_time))){ 
        $bstarttime             = $getScheduleOnDay->break_time; 
        preg_match("/([0-9]{1,2}):([0-9]{1,2})/", $getScheduleOnDay->break_duration, $match);
        $hour                   = $match[1];
        $min                    = $match[2];

        $bendtime = date('H:i',strtotime('+'.$hour.' hour +'.$min.' minutes',strtotime($getScheduleOnDay->break_time)));

        $gapArray   = $this->returnBreakArray($bstarttime,$bendtime,$add_mins);
        }
        if (!in_array($check_time_forbreak, $gapArray)){
            // $array_of_time [] = date ("H:i", $start_time);
        if(count($checkApointOnthisDate) > 0){
          if (!in_array($check_time_forbreak, $checkApointOnthisDate)){
             if($tdcounter%8 == 0 ){
              $html .= '</tr><tr>';
              $tdcounter=0;
             }
             $html .= '<td><a href="'.url().'/" data-id="'.$data['profile_doctor_id'].'" data-value="'.date ("H:i", $start_time).'" data-date="'.$data["schedule_date"].'" class="book-appointment">'.date ("H:i", $start_time).'</a></td>';
              $tdcounter ++;
            }// end if
          }
          else {
             if($tdcounter%8 == 0 ){
              $html .= '</tr><tr>';
              $tdcounter=0;
             }
             $html .= '<td><a href="'.url().'/" data-id="'.$data['profile_doctor_id'].'" data-value="'.date ("H:i", $start_time).'" data-date="'.$data["schedule_date"].'" class="book-appointment">'.date ("H:i", $start_time).'</a></td>';
              $tdcounter ++;
          }
        }// end of if.
         $start_time += $add_mins; // to check endtime
       
      }
      $html               .= '</tr>';

     $message = $html;
    endif;

    return $message;
  }
private function returnBreakArray($bstarttime = '02:00',$bendtime = '03:30',$add_mins = 30)
{ 
    $bstart_time    = strtotime ($bstarttime); //change to strtotime
    $bend_time      = strtotime ($bendtime); //change to strtotime

    while ($bstart_time <= $bend_time) // loop between time
    {
       $array_ofbreak_time[] = date ("H:i", $bstart_time);
       $bstart_time += $add_mins; // to check endtime
    }
    return $array_ofbreak_time;

}

  // Get available days
    function GetAvailableDays() {
        //return response()->json(BookingDateTime::all());
    $message                    = '';
    $error                      = false;
    $data                       = Input::all();
    $mailContents               = array();
    $data['user_patient_id']    = Sentinel::getUser()->id;
    $mailContents['to_email']   = Sentinel::getUser()->email;
    $mailContents['first_name'] = Sentinel::getUser()->first_name;
    $mailContents['last_name']  = Sentinel::getUser()->last_name;
    $data['created_at']         = Carbon::now();
    $data['updated_at']         = Carbon::now();
    //$data['schedule_date']   = date_create($data['schedule_date']);

    //$data['schedule_date']   = date_format($data['schedule_date'],"Y-m-d");
   // $date                    = Carbon::createFromFormat("Y-m-d", $data['schedule_date']);   
  $checkApointAlready = DB::table('schedule_profile_to_patient')
                     ->where('profile_doctor_id',$data['profile_doctor_id'])
                     ->where('user_patient_id',$data['user_patient_id']) 
                     ->whereDate('schedule_date', '=', $data["schedule_date"])->count(); 
          if($checkApointAlready > 0):
          $message = 1;
          $error   = true;
          endif;
          if(date('Y-m-d') > $data["schedule_date"]):
          $message = 2;
          $error   = true;
          endif;
         // if($data['user_patient_id'] == $data['doctors_id']):
         // $message = 3;
         // $error   = true;
         // endif;
          if($error == false):
           DB::table('schedule_profile_to_patient')->insert($data);
           $siteEmail = DB::table('sitesettings')
                      ->select('site_email')
                      ->where('visible',1)->first();                      
           $mailContents['from_email'] = $siteEmail->site_email;
           $message   = 3;
          
         $send = Mail::send('emails.bookappointment',  $mailContents, function ($message) use ($mailContents) {
          $message->from('mutahir@pkteam.com', 'Nation Wide Physician');
          $message->to($mailContents['to_email'])->subject('Request For Appointment');
            });
         endif;
         return $message;
    }


      public function contactUs()
    {
        $collection['doctors_names_autocomplete'] = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`, `wholename`'))
      ->orderBy('wholename')->groupBy('id')->get();

          $collection['doctors'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get(); 
          $collection['procedures'] = DB::table('procedures')->orderBy('procedure_name')->get();
          $collection['catagories'] = Catagory::where('parent_id', 0)->orderBy('name')->get();
          $collection['locations']  = DB::table('state')->orderBy('state_name')->get(); 

$collection['footerpages'] = DB::table('pages')->select('id','title')->whereIn('id',array(2,3,4,5,6,7,8))->get();
          return View::make('frontpages/contactus', compact('collection')); 
    }

    public function contactSubmit(){
      
      $data             = Input::all();
      $msg              = '';

      $siteEmail = DB::table('sitesettings')
                 ->select('site_email')
                 ->where('visible',1)->first();          
      $data['to_email']     = $siteEmail->site_email;
      $data['to_subject']   = 'Nation Wide Physician Contact.';
      $data['from_subject'] = 'Contact Us';

      $rules  = [
            'email'     => 'required|email', 
            'comments'  => 'required'];

      $messages         = $this->validatorGeneral($data , $rules); 
      if ($messages->isEmpty())
        {          
           $response    =   $this->sendEmail($data);
           if($response): $msg = 'Thanks for contacting us, we will get back to you shortly.';
           else: $msg   = 'Message sending fail.Please try next time.'; endif;
        }else {          
          $messages->toArray(); 
          $msg          = $messages->first('email', ':message').'<br/>';
          $msg         .= $messages->first('phone', ':message');
        }
        return $msg;
    }

     protected function sendEmail($data)
      { 
     
     $mail_sent =   Mail::send('emails.contactemailtemplate', $data, function ($message) use ($data) 
     {

            $message->from($data['email'], $data['from_subject']);
            $message->to($data['to_email'], $data['first_name'].' '.$data['last_name'])->subject($data['to_subject']);
        });
      return $mail_sent;
      }



 public function checkUserIfLogin()
    {  $user      = Sentinel::check();


         if ($user)
         {
          echo '1';
          }
       else echo '0';
    }


 public function submitEnquiry()
    {  
      $user             = Sentinel::check();
      $data             = Input::all(); 
      $doctorData       = User::select('email','first_name','last_name')
                        ->where('id',$data['doctor_id'])->first(); 
      $msg              = array();
      $data['to_email'] = $doctorData->email;
         if ($user)// check if user is already loggedin
          {
            $rules  = [ 
                  'enquiry_message'  => 'required'
                  ];  

          $messages = $this->validatorGeneral($data , $rules); 
      if ($messages->isEmpty())
        {
        $data_of_loginuser     = Sentinel::getUser();
        $data['enquiry_name']  = $data_of_loginuser->first_name.' '.$data_of_loginuser->last_name;
        $data['enquiry_email'] = $data_of_loginuser->email;
        $data['enquiry_phone'] = '';
        DB::table('enquiries')->insert(
              [
              'name'       => $data['enquiry_name'],
              'email'      => $data['enquiry_email'],
              'phone'      => $data['enquiry_phone'],
              'contents'   => $data['enquiry_message'], 
              'user_id'    => $data_of_loginuser->id,         
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
               ]);
        $mail_sent = Mail::send('emails.enquiryemailtemplate', $data, function ($message) use ($data) {
          $message->from($data['enquiry_email'], 'Your Application');
          $message->to($data['to_email'], $data['enquiry_name'])
                  ->subject('Enquiry Message!');
        }); 
           if($mail_sent):            
             $msg['status'] = 'success';
             $msg['msg']    =  'Thanks for contacting us, we will get back to you shortly.';
            
           else:
            $msg['status'] = 'fail';            
            $msg['msg']    =   'Message sending fail.Please try next time.';
             endif;

      }else
        {  
          $messages->toArray();  
          echo '<br/>';
           $msg['status'] = 'fail'; 
           $msg['msg']   = $messages->first('enquiry_message', ':message');
        }


          }
       else 
       {
         $rules  = [
                  'enquiry_email'    => 'required|email', 
                  'enquiry_message'  => 'required'
                  ];  

      $messages = $this->validatorGeneral($data , $rules); 
      if ($messages->isEmpty())
        {  
            DB::table('enquiries')->insert(
              [
              'name'      => $data['enquiry_name'],
              'email'     => $data['enquiry_email'],
              'phone'     => $data['enquiry_phone'],
              'contents'  => $data['enquiry_message'],          
              'created_at'=> Carbon::now(),
              'updated_at'=> Carbon::now()
               ]);
               $mail_sent =   Mail::send('emails.enquiryemailtemplate', $data, function ($message) use ($data) {
                  $message->from($data['enquiry_email'], 'Your Application');
                  $message->to($data['to_email'], $data['enquiry_name'])
                  ->subject('Enquiry Message!');
        }); 
           if($mail_sent):  
             $msg['status'] = 'success';
             $msg['msg']    = 'Thanks for contacting us, we will get back to you shortly.';            
           else:
            $msg['status'] = 'fail'; 
            $msg['msg']    = 'Message sending fail.Please try next time.';
           endif;

      }else
        {  
          $messages->toArray(); 
            $msg['status'] = 'fail'; 
            $msg['msg']    = $messages->first('enquiry_email', ':message').'<br/>'.$messages->first('enquiry_message', ':message'); 
        } 
       }
       return json_encode($msg);
    }



    protected function validatorGeneral($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    } 

}
