<?php 

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Validator;
use Redirect;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Helpers\NonStaticHelpers;
 
class PageController extends Controller
{
    
    
public function __construct()
{
  
}
    public function index()
    {
        return view('admin.page.list', ['pages' => Page::paginate(10) ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $helper = new NonStaticHelpers;
        $icon_size_path   = 'images/cms/icon_size/';
        $full_size_path   = 'images/cms/';
        $rules = ['title' => 'required'];
        $image = $request->file('image');
        if($image ==''):
            $resizedImage = '';
        else: 
            $resizedImage   =   $helper->resize($icon_size_path,$image, 300); 
            $request->file('image')->move($full_size_path, $resizedImage);
         endif; 

        $data  = [
        	'user_id' 		=> Sentinel::getUser()->id,
        	'title' 		=> $request->input('title'),
        	'slug'  		=> str_slug($request->input('title')),
        	'content' 		=> $request->input('content'),
            'image'         => $resizedImage, 
        	'status' 		=> $request->input('status')
        ];

        $messages = $this->validateUser($data, $rules);
        if ($messages->isEmpty())
          {
           Page::create($data); 
           return Redirect::to('cms/')->withSuccess('Successfully created!');
         }
    else {
           return Redirect::back()->withInput()->withErrors($messages);
       }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.page.edit', ['page' => Page::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $helper           = new NonStaticHelpers;
        $icon_size_path   = 'images/cms/icon_size/';
        $full_size_path   = 'images/cms/';
        $rules            = ['title' => 'required'];
        $image            = $request->file('image');
        $image_row = Page::select('image')->where('id',$id)->first();
        if($image ==''):
            $resizedImage = $image_row->image;
        else:
            \File::delete($icon_size_path.$image_row->image);
            \File::delete($full_size_path.$image_row->image);

            $resizedImage   =   $helper->resize($icon_size_path,$image, 300); 
            $request->file('image')->move($full_size_path, $resizedImage);
         endif; 

        $data = [
        	'title'      => $request->input('title'),
        	'slug'       => str_slug($request->input('title')),
        	'content'    => $request->input('content'),
        	'status'     => $request->input('status'),
            'image'      => $resizedImage,   
            'sort_order' => $request->input('sort_order')
        ];
         $messages      = $this->validateUser($data, $rules);
        if ($messages->isEmpty()){
        Page::find($id)->update($data);
        return Redirect::to('cms/')->withSuccess('Successfully Updated!');
    }     
    else {
           return Redirect::back()->withInput()->withErrors($messages);
       }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image_row = Page::select('image')->where('id',$id)->first();
        if($image_row->image !=''):
            
        $icon_size_path   = 'images/cms/icon_size/';
        $full_size_path   = 'images/cms/';
            \File::delete($icon_size_path.$image_row->image);
            \File::delete($full_size_path.$image_row->image);
        endif;

        Page::find($id)->delete();

        return Redirect::to('cms/')->withSuccess('Successfully Deleted!');
    }


      protected function validateUser($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }


    

}