<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\Models\sitesetting; 
use Validator;
use View;
use Redirect;
use App\Helpers\NonStaticHelpers;
use DB;
class siteSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function __construct()
{
  
}
    public function index()
    {
       $settings =  sitesetting::paginate(10);  
       //News:: find(1)->getNewsDetails();
        return view('admin.sitesettings.list', ['settings' => $settings ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
    {
        return view('admin.sitesettings.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $helper           = new NonStaticHelpers;
        $full_size_path   = 'images/';
        $inputs           = $request->except('_token'); 
        $rules            = ['site_title' => 'required'];
        $image            = $request->file('site_facon');
        if($image ==''):
            $resizedImage = '';
        else:  
             $resizedImage = $request->file('site_facon')
                           ->getClientOriginalName(); 
            $request->file('site_facon')->move($full_size_path, $resizedImage);
         endif; 
        $messages = $helper->validatorGeneral($inputs, $rules);
        if ($messages->isEmpty())
          {
            $inputs[''] = $resizedImage;

        $id = sitesetting::create($inputs); 
           return Redirect::to('settings/')->withSuccess('Successfully Saved!');
         }
    else {
           return Redirect::back()->withInput()->withErrors($messages);
       }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings =   sitesetting::where('id',$id)->first();  
        return view('admin.sitesettings.edit', ['settings' => $settings]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $inputs = $request->except('_token'); // Request::all() is not working
         
       $helper           = new NonStaticHelpers; 
        $full_size_path   = 'images/';
        $rules            = ['site_title' => 'required'];
        $site_facon       = $request->file('site_facon');
        $image_row        =  sitesetting::select('site_facon')
        ->where('id',$id)->first();
        if($site_facon ==''):
            $resizedImage = $image_row->site_facon;
        else: 
            $resizedImage           = $request->file('site_facon')->getClientOriginalName(); 

            \File::delete($full_size_path.$image_row->site_facon); 
            $request->file('site_facon')->move($full_size_path, $resizedImage);
         endif;   
        $messages      = $helper->validatorGeneral($inputs, $rules);
        if ($messages->isEmpty()) {
            $inputs['site_facon'] = $resizedImage;
         DB::table('sitesettings')->where('id',$id)->update($inputs);          
        return Redirect::to('settings/')->withSuccess('Successfully Updated!');
    }     
    else {
           return Redirect::back()->withInput()->withErrors($messages);
       }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image_row = sitesetting::select('site_facon')->where('id',$id)->first();
        if($image_row->image !=''):
             
        $full_size_path   = 'images/';
            \File::delete($icon_size_path.$image_row->site_facon);
            \File::delete($full_size_path.$image_row->site_facon);
        endif;

        sitesetting::find($id)->delete();

        return Redirect::to('settings/')->withSuccess('Successfully Deleted!');
    }
public function activate()
{ 
    $dataId     = Input::get('dataId');
    $dataValue  = Input::get('dataValue');
    if($dataValue == 0){
       $data  = DB::table('sitesettings')
              ->update(['visible'=>0]);
       $data  = DB::table('sitesettings')
              ->where('id',$dataId)->update(['visible'=>1]);
    }
    
    
}
 
}
