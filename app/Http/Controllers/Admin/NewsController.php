<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\News;
use Validator;
use Redirect;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Helpers\NonStaticHelpers;
use DB;
use Carbon\Carbon;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function __construct()
{
  
}
    public function index()
    {
       $allNews =  DB::table('news')
              ->join('news_translations', 'news.id', '=','news_translations.news_id')
              ->select('news.*', 'news_translations.title', 'news_translations.content','news_translations.language')
              ->paginate(10);  
       //News:: find(1)->getNewsDetails();
        return view('admin.news.list', ['news' => $allNews ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
    {
        return view('admin.news.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $helper = new NonStaticHelpers;
        $icon_size_path   = 'images/news/icon_size/';
        $full_size_path   = 'images/news/';
        $rules = ['title' => 'required'];
        $image = $request->file('image');
        if($image ==''):
            $resizedImage = '';
        else: 
            $resizedImage   =   $helper->resize($icon_size_path,$image, 300); 
            $request->file('image')->move($full_size_path, $resizedImage);
         endif; 

        $data_news  = [
            'user_id'       => Sentinel::getUser()->id,
            'visible'       => $request->input('visible'),
            'image'         => $resizedImage,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ];
         

        $messages = $this->validateUser($request->all(), $rules);
        if ($messages->isEmpty())
          {

        $id = DB::table('news')->insertGetId($data_news);
        if($id)
        {

        $data_news_translation  = [ 
            'title'         => $request->input('title'),
            'slug'          => str_slug($request->input('title')),
            'content'       => $request->input('content'), 
            'news_id'       => $id,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]; 
            DB::table('news_translations')->insert($data_news_translation);
        } 
           return Redirect::to('news/')->withSuccess('Successfully Saved!');
         }
    else {
           return Redirect::back()->withInput()->withErrors($messages);
       }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allNews =  DB::table('news')->where('news.id',$id)
              ->join('news_translations', 'news.id', '=','news_translations.news_id')
              ->select('news.*', 'news_translations.title', 'news_translations.content','news_translations.language')
              ->first();  
        return view('admin.news.edit', ['news' => $allNews]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $helper           = new NonStaticHelpers;
        $icon_size_path   = 'images/news/icon_size/';
        $full_size_path   = 'images/news/';
        $rules            = ['title' => 'required'];
        $image            = $request->file('image');
        $image_row =  DB::table('news')->select('image')
        ->where('id',$id)->first();
        if($image ==''):
            $resizedImage = $image_row->image;
        else:
            \File::delete($icon_size_path.$image_row->image);
            \File::delete($full_size_path.$image_row->image);

            $resizedImage   =   $helper->resize($icon_size_path,$image, 300); 
            $request->file('image')->move($full_size_path, $resizedImage);
         endif; 

          $data_news  = [
            'user_id'       => Sentinel::getUser()->id,
            'visible'       => $request->input('visible'),
            'image'         => $resizedImage,
            'updated_at'    => Carbon::now()
        ];
         $data_news_translation  = [ 
            'title'         => $request->input('title'),
            'slug'          => str_slug($request->input('title')),
            'content'       => $request->input('content'), 
            'updated_at'    => Carbon::now()
        ]; 
        $messages      = $this->validateUser($data_news_translation, $rules);
        if ($messages->isEmpty()) {
        $id = DB::table('news')->where('id',$id)->update($data_news);         
        $data_news_translation  = [ 
            'title'         => $request->input('title'),
            'slug'          => str_slug($request->input('title')),
            'content'       => $request->input('content'), 
            'updated_at'    => Carbon::now()
        ]; 
    DB::table('news_translations')->where('news_id',$id)->update($data_news_translation);
        return Redirect::to('news/')->withSuccess('Successfully Updated!');
    }     
    else {
           return Redirect::back()->withInput()->withErrors($messages);
       }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image_row = Page::select('image')->where('id',$id)->first();
        if($image_row->image !=''):
            
        $icon_size_path   = 'images/news/icon_size/';
        $full_size_path   = 'images/news/';
            \File::delete($icon_size_path.$image_row->image);
            \File::delete($full_size_path.$image_row->image);
        endif;

        Page::find($id)->delete();

        return Redirect::to('news/')->withSuccess('Successfully Deleted!');
    }


      protected function validateUser($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }

}
