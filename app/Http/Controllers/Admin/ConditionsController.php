<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Input;
use Validator;
use Redirect;
use App\Models\Conditions;  

use Cartalyst\Sentinel\Native\Facades\Sentinel;
class ConditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

       protected $conditions;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    { 

        $this->conditions = Conditions::select('condition_name','id');
    }


    public function index()
    {
        $conditions = $this->conditions->paginate(6);
        return View::make('admin.conditions.index', compact('conditions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return $this->showForm('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->processForm('create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         return $this->showForm('update', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if ($conditions = Conditions::find($id))
        {
            $conditions->delete();

            
                return Redirect::to('conditions')->withSuccess('Record Deleteed');
        }

       
                return Redirect::to('conditions')->withSuccess('Record Not Deleted.');
    }



      protected function showForm($mode, $id = null)
    {
        if ($id)
        {
            if ( ! $condition = Conditions::find($id))
            {
                return Redirect::to('conditions');
            }
        }
        else
        {
            $condition = $this->conditions;

        }

        return View::make('admin.conditions.form', compact('mode', 'condition'));
    }




     protected function processForm($mode, $id = null)
    {
        $input = Input::all();

        $rules = [
            'condition_name' => 'required' 
        ];

        if ($id)
        {
            $condition = Conditions::find($id); 

            $messages = $this->validateRole($input, $rules);

            if ($messages->isEmpty())
            {
                $condition->fill($input);

                $condition->save();
                return Redirect::to('conditions')->withSuccess('Record Saved');
            }
        }
        else
        {
            $messages = $this->validateRole($input, $rules);

            if ($messages->isEmpty())
            {
                $condition = Conditions::create($input);
                return Redirect::to('conditions')->withSuccess('Record Saved');
            }
        } 

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a role.
     *
     * @param  array  $data
     * @param  mixed  $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateRole($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }



}
