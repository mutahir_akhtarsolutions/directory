<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/22/2015
 * Time: 10:30 PM
 */

class BaseController extends Controller {
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }
}