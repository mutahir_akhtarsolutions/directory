<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Insurance;
use View;
use Input;
use Validator;
use Redirect;
use DB;

class insuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return View('insurance.index', array( 'insurances' => Insurance::paginate(10) ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->processForm('create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if ($insurance = Insurance::find($id))
        {
          if($insurance->parent_id != 0){
            $insurance->delete();
            return Redirect::to('insurance')->withSuccess('Insurance is successfully deleted');
          }else {
            return Redirect::to('insurance')->withErrors('Insurance is not deleted. It is parent Insurance.');
          }

        }

        return Redirect::to('insurance');
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {


        if ($id)
        {


            if ( ! $catagory = Insurance::find($id))
            {

                return Redirect::to('insurance');
            }
        }else {
          //$catagory=DB::table('specialities')->where('parent_id', 0)->get();
          $catagory = Insurance::where('parent_id', 0)->get();

        $sub_parent_catagory = Insurance::where('parent_id', 0)->get();

        }

        if($id){
            $catagories =   Insurance::where('parent_id', 0)->get();

            //$p_catagories =   Insurance::where('parent_1', 0)->get();

            /*echo '<pre>';
            print_r($catagories);die;*/


        }

        return View::make('insurance.form', compact('mode', 'catagory', 'catagories', 'sub_parent_catagory'));
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {

        $input = Input::all();



        if ($id)
        {

          $rules = [
              'insurance_name' => 'required'
          ];

            $insurance = Insurance::find($id);
            $messages = $this->validateCatagory($input, $rules);

            if ($messages->isEmpty())
            {
              if($input['p_Insurance'] != 0 && $input['s_p_Insurance']!= 0 ){
                  $input['parent_id'] = $input['s_p_Insurance'];
              }elseif ($input['p_Insurance'] != 0 && $input['s_p_Insurance']== 0) {
                  $input['parent_id'] = $input['p_Insurance'];
              }else {
                  $input['parent_id'] = $input['p_Insurance'];
              }
                //$input['parent_1'] = $input['p_Insurance'];
                //$input['parent_id'] = $input['s_p_Insurance'];
                $insurance->fill($input);
                $insurance->save();
            }
        }
        else
        {

          $rules = [
              'insurance_name' => 'required|unique:insurance'
          ];

            $messages = $this->validateCatagory($input, $rules);

            if ($messages->isEmpty())
            {
              if($input['p_Insurance'] != 0 && $input['s_p_Insurance']!= 0 ){
                  $input['parent_id'] = $input['s_p_Insurance'];
              }elseif ($input['p_Insurance'] != 0 && $input['s_p_Insurance']== 0) {
                  $input['parent_id'] = $input['p_Insurance'];
              }else {
                  $input['parent_id'] = $input['p_Insurance'];
              }

              //$input['parent_id'] = $input['s_p_Insurance'];
              //$input['parent_1'] = $input['p_Insurance'];
              $insurance = Insurance::create($input);

            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::to('insurance');
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a catagory.
     *
     * @param  array  $data
     * @param  mixed  $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateCatagory($data, $packages)
    {

        $validator = Validator::make($data, $packages);

        $validator->passes();

        return $validator->errors();
    }


    //populate subInsurances

    public function subInsurrance(){

      $input = Input::all();

      $input['p_insurance'];
      $insurances = DB::table('insurance')->where('parent_id', '=',$input['p_insurance'])->lists('insurance_name', 'id');

      return  json_encode($insurances);

    }

    public function editInsurance()
    {
        $collection['insurances']      = Insurance::where('parent_id', 0)
                                        ->get();  
        return View::make('editinsurance', compact('collection'));
    }





}
