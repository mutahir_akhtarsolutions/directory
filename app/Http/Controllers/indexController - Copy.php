<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Catagory;
use DB;
use Input;
use App\Profile;
use App\users_specialities;
use App\User;
use DOMDocument;
use DOMXPath;
use Illuminate\Pagination\Paginator;
class indexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $doctors=DB::table('profile')
          ->join('users', 'profile.user_id', '=', 'users.id')
          ->select('profile.*', 'users.*')
          ->where('parent_id','<>', 0)
          ->get();
          $procedures = DB::table('procedures')->orderBy('procedure_name')->get();
          $catagories	= Catagory::where('parent_id', 0)->orderBy('name')->get();
          $locations	= DB::table('state')->orderBy('state_name')->get();

          return View::make('index', compact('catagories', 'locations','doctors','procedures')); 
    }
    //Simple search for doctor

    public function simpleSearchForDoctor()
    { 
      include 'PaginatorController.php';
      $input           = Input::all();  
      $state_id        = @$input['location'];
      $search_keyword  = @$input['search_keyword'];
      $specialities_id = @$input['catagory'];
      $procedures_ids  = @$input['procedures'];
      $catagories      = Catagory::where('parent_id', 0)->orderBy('name')->get();
      $locations       = DB::table('state')->orderBy('state_name')->get();
      $query_variable  = ' 1 ';
      $pages = new PaginatorController('5','p');
      if(isset($state_id) && $state_id != '0' && $state_id != '')
      {
         $query_variable .= ' AND state_id = '.$state_id; 
      }
      
      if(isset($specialities_id) && $specialities_id != '0' && $specialities_id != '')
      {
        $query_variable .= ' AND specialities_id = '.$specialities_id; 
      }
       if (isset($procedures_ids) && $procedures_ids !='') {
        $query_variable .=' AND user_procedures.procedure_id IN ('.$procedures_ids.') ';
       }
      if(isset($search_keyword) && $search_keyword != '')
      {
         $query_variable .= " HAVING wholename LIKE '%".$search_keyword."%'";
      }
      if (isset($procedures_ids) && $procedures_ids !='') {
        $users = Profile:: distinct()->select(DB::raw('CONCAT_WS(" ",`first_name`,`last_name`) as `wholename`, profile.* , users.first_name , users.last_name'))->join('users', 'profile.user_id', '=', 'users.id')
        ->join('users_specialities', 'profile.user_id', '=', 'users_specialities.users_id')
        ->join('user_procedures', 'profile.user_id', '=', 'user_procedures.user_id')
        ->whereRaw($query_variable)->get();
        # code...
      }
      else
      { 

       $users = Profile:: distinct()->select(DB::raw('CONCAT_WS(" ",`first_name`,`last_name`) as `wholename`, profile.* , users.first_name , users.last_name'))->join('users', 'profile.user_id', '=', 'users.id')->join('users_specialities', 'profile.user_id', '=', 'users_specialities.users_id')->whereRaw($query_variable)->get();


        
           }   
           $users = new Paginator($users, count($users), 1);

        // $users->appends(['search_keyword' => $search_keyword , 'catagory' => $specialities_id , 'location' => $state_id , 'procedures'=>$procedures_ids])->render();
     
   // print_r($users_searches);
 
       $procedures = DB::table('procedures') 
                    ->join('user_procedures','procedures.id','=','user_procedures.procedure_id')
                     ->select('procedures.*')
                    /*->whereIn('user_procedures.user_id', $combined_array_users_ids)*/->groupBy('procedures.id')->get();

      $doctors=DB::table('profile')
          ->join('users', 'profile.user_id', '=', 'users.id')
          ->select('profile.*', 'users.*')
          ->where('parent_id','<>', 0)
          ->get();

      return View::make('doctorListing', compact('users','doctors','catagories', 'locations','procedures'));
    } 



    /**
     * Show the form for doctorDetailPage a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doctorDetailPage($id)
    {
      //set_time_limit(0);
      //ini_set('memory_limit', '4095M');
      //error_reporting(0);






/*$url = 'http://www.healthgrades.com/specialty-directory';
$json = $this->curl_get_contents($url);
preg_match_all('/<div class="specialtyBlock">(.*?)<\/div>/s',$json,$estimates);

//print_r($estimates[1]);die;
preg_match_all('/<a (.*?)>(.*?)<\/a>/s',$estimates[1][0],$estimates_1);

foreach ($estimates_1[2] as $key => $value) {
  echo $value.'<br>';
  Catagory::create(['name' => $value]);
}
die;
echo '<pre>';
print_r($estimates_1);die;*/
     $catagories = Catagory::where('parent_id', 0)->orderBy('name')->get();
      $locations  = DB::table('state')->orderBy('state_name')->get();

    $doctor=  DB::table('profile')
              ->join('users', 'profile.user_id', '=', 'users.id')
              ->select('profile.*', 'users.*')
              ->where('user_id', $id)
              ->get();

    //var_dump($doctor[0]->id);die;
    $business_profile=Profile::find($doctor[0]->parent_id);
    //var_dump($profile->id);die;
    $related_doctors =  DB::table('profile')
                        ->join('users', 'profile.user_id', '=', 'users.id')
                        ->select('profile.*', 'users.*')
                        ->where('parent_id', $business_profile->id)
                        //->where('id', '!=', $doctor[0]->id)
                        ->get();
                        $doctors=DB::table('profile')
                            ->join('users', 'profile.user_id', '=', 'users.id')
                            ->select('profile.*', 'users.*')
                            ->where('parent_id','<>', 0)
                            ->get();

        return View::make('doctorDetail', compact('doctor','business_profile','related_doctors','doctors','catagories','locations'));
    }

private function curl_get_contents($url)
{
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
