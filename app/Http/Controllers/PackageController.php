<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/22/2015
 * Time: 10:30 PM
 */

 namespace App\Http\Controllers;

 use Illuminate\Http\Request;
 use App\Http\Requests;
 use App\Http\Controllers\Controller;

use App\Package;
use View;
use Input;
use Validator;
use Redirect;

use Cartalyst\Sentinel\Native\Facades\Sentinel;

class PackageController extends AuthorizedController {

  /**
   * Holds the packages.
   *
   * @var App\package
   */
   protected $roles;

   //@todo: this will be move to site configuration later.
   const MEMBER_ROLE_ID = 2;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->roles = Sentinel::getRoleRepository()->createModel();
    }

    /**
     * Display a listing of packages.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return View('package.index', array( 'packages' => Package::paginate(10) ));
    }
    /**
     * Show the form for creating new role.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new package.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        return $this->processForm('create');
    }
    /**
     * Show the form for updating package.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    /**
     * Handle posting of the form for updating package.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified package.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        if ($package = Package::find($id))
        {
            $package->role()->delete();

            return Redirect::to('package');
        }

        return Redirect::to('package');
    }





    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        $package = null;
        if ($id)
        {
            if ( ! $package = Package::find($id))
            {
                return Redirect::to('package');
            }
        }
        return View::make('package.form', compact('mode', 'package'));
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int     $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {
        $input = Input::all();

        $rules = [
            'name' => 'required',
            'slug' => 'required|unique:roles',
            'fee' => 'integer'
        ];

        $input['slug'] = $this->slugify($input['name']);

        if ($id)
        {
            $package = Package::find($id);
            $role = $package->role;
            $rules['slug'] .= ",slug,{$role->slug},slug";

            $messages = $this->validatePackage($input, $rules);

            if ($messages->isEmpty())
            {
                $role->fill($input);
                $role->save();

                $package->fill($input);
                $package->save();
            }
        }
        else
        {
            $messages = $this->validatePackage($input, $rules);

            if ($messages->isEmpty())
            {
                $role = $this->roles->create($input);

                if(!empty($role))
                {
                    $role->parent_id = self::MEMBER_ROLE_ID;
                    $role->save();
                    $input['role_id'] = $role->id;
                    $package = Package::create($input);
                }
            }
        }

        if ($messages->isEmpty())
        {
            return Redirect::to('package');
        }

        return Redirect::back()->withInput()->withErrors($messages);
    }

    /**
     * Validates a package.
     *
     * @param  array  $data
     * @param  mixed  $id
     * @return \Illuminate\Support\MessageBag
     */
    protected function validatePackage($data, $packages)
    {
        $validator = Validator::make($data, $packages);

        $validator->passes();

        return $validator->errors();
    }


    private function slugify($text)
    {
      // replace non letter or digits by -
      $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

      // trim
      $text = trim($text, '-');

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // lowercase
      $text = strtolower($text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '', $text);

      if (empty($text))
      {
        return 'n-a';
      }

      return $text;
    }

}
