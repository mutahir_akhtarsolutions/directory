<?php 
namespace App\Http\Controllers\front;

use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use View; 
use DB;
use App\Models\Newsletters;
use App\Helpers\NonStaticHelpers;
use Input;
use Mail;
class NewslettersController extends Controller
{ 
	function __construct()
	{
		
		//parent::Public_Controller();
		//$this->load->model('newsletters_model','newsletters');
		//$this->lang->load('newsletters');

	}
	
	function index()
	{ 
	}
	
	function archive($id = '')
	{
		 
	}
	
	// Public: Register for newsletter
	function subscribe()
	{
		$inputs               = array();
		$inputs['first_name'] = '';
		$inputs['last_name']  = '';
		$inputs               = Input::all(); 
        $rules                = [
                                 'email'    => 'required|email|unique:newsletter_recipients' 
                                ];
      $msg                    = array();

      $siteEmail              = DB::table('sitesettings')
                              ->select('site_email')
                              ->where('visible',1)->first();          
      $inputs['from_email']   = $siteEmail->site_email;
      $messages               = (new NonStaticHelpers)->validatorGeneral($inputs,$rules);
      
      if ($messages->isEmpty())
        {  
            $activation = Newsletters::create($inputs); 
            $sent       = Mail::send('emails.newslettersubscribe', compact('data', 'code'), function($m) use ($inputs)
               {
               	   $m->from($inputs['from_email'], 'Nation Wide Physician');
                   $m->to($inputs['email'])->subject('Newsletter Subscription');
                });
		                if ($sent === 0)
		                { 
		                
			                $msg['status'] = 0;
			                $msg['msg']    = 'Email Sending Fail.';
		                }

		               else {                	
			                $msg['status'] = 1;
			                $msg['msg']    = 'Successfully subscribed to our newsletters. You can update your name.';
		                }
            
        }  // CHECK IF NOT EMPTY.   
      else
        {  
         if(isset($inputs['first_name']) == '' && isset($inputs['last_name']) == '')
         {
		        $messages->toArray();  
		        $msg['status'] = 0;
		        $msg['msg']    = $messages->first('email', ':message');   
	    }
	   else {
	        	   DB::table('newsletter_recipients')->where('email',$inputs['email'])->update(['first_name'=>@$inputs['first_name'],'last_name'=>@$inputs['last_name']]);

			        $msg['status'] = 2;
			        $msg['msg']    = "Successfully Updated."; 
	        	
	        } 
	    }
	    return json_encode($msg);
	}	
	// Public: Register for newsletter
	function subscribed()
	{
		 
	}
	
	// Public: Unsubscribe from newsletter
	function unsubscribe($email = '')
	{
	
	}


    
}
