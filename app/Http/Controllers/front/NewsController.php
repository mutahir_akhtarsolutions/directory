<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Catagory;
use DB;
use App\Models\News;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $collection['doctors_names_autocomplete'] = DB::table('userprofile_procedure_specility_review')
      ->select(DB::raw('`id`, `wholename`'))
      ->orderBy('wholename')->groupBy('id')->get();

          $collection['doctors'] = DB::table('userprofile_procedure_specility_review')->select(DB::raw('`id`,`user_id`,parent_id,`image_of_doctor`,`education`,`contact_dr_phone_number`, `wholename`,state_id,city_id, specialities_id,ROUND(AVG(`rating`)) AS averageRating'))
          ->where('parent_id','<>', 0)->groupBy('id')
          ->get(); 
          $collection['procedures'] = DB::table('procedures')->orderBy('procedure_name')->get();
          $collection['catagories'] = Catagory::where('parent_id', 0)->orderBy('name')->get();
          $collection['locations']  = DB::table('state')->orderBy('state_name')->get();
          $collection['news'] = DB::table('news')
              ->join('news_translations', 'news.id', '=','news_translations.news_id')
              ->select('news.*', 'news_translations.title', 'news_translations.content','news_translations.language')
              ->where('news.id',$id)->first();
              
$collection['footerpages'] = DB::table('pages')->select('id','title')->whereIn('id',array(2,3,4,5,6,7,8))->get();


          return View::make('frontpages/news', compact('collection')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
