<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Models\schedule_profile;
use Input; 
use Validator;
use View;
use Redirect;
use DB;       
use Cartalyst\Sentinel\Native\Facades\Sentinel;
 
use Carbon\Carbon;
class scheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    // Get available days
    function GetAvailableDays() {
        //return response()->json(BookingDateTime::all());
    $message                 = '';
    $error                   = false;
    $data                    = Input::all();
    $data['user_patient_id'] = Sentinel::getUser()->id;
    $data['created_at']      = \Carbon\Carbon::now();
    $data['updated_at']      = \Carbon\Carbon::now();
    //$data['schedule_date']   = date_create($data['schedule_date']);

    //$data['schedule_date']   = date_format($data['schedule_date'],"Y-m-d");
   // $date                    = Carbon::createFromFormat("Y-m-d", $data['schedule_date']);   
  $checkApointAlready = DB::table('schedule_profile_to_patient')
                     ->where('profile_doctor_id',$data['profile_doctor_id'])
                     ->where('user_patient_id',$data['user_patient_id']) 
                     ->whereDate('schedule_date', '=', $data["schedule_date"])->count(); 
          if($checkApointAlready > 0):
          $message = 1;
          $error   = true;
          endif;
          if(date('Y-m-d') > $data["schedule_date"]):
          $message = 2;
          $error   = true;
          endif;
         // if($data['user_patient_id'] == $data['doctors_id']):
         // $message = 3;
         // $error   = true;
         // endif;
          if($error == false):
            DB::table('schedule_profile_to_patient')
            ->where('id', $data["patient_id"])
            ->where('profile_doctor_id', $data['profile_doctor_id'])
            ->update(
            [  'schedule_date' => $data["schedule_date"],
               'schedule_time' => $data["schedule_time"],
               'created_at'    => $data['created_at'],
               'updated_at'    => $data['updated_at']
            ]);
            $message = 3;
            endif;
         return $message;
    }


    public function singleAppointment()
    {
        $id = Input::get('id');
      $collection  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.schedule_time','schedule_profile_to_patient.status','schedule_profile_to_patient.active')
            ->where('schedule_profile_to_patient.id',$id)
            ->first(); 

      return response()->json($collection);


    }

    public function singleAppointmentWithDoctor()
    {
        $id = Input::get('id');
      $collection  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.id', '=', 'schedule_profile_to_patient.profile_doctor_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.schedule_time','schedule_profile_to_patient.status','schedule_profile_to_patient.active')
            ->where('schedule_profile_to_patient.id',$id)
            ->first(); 

      return response()->json($collection);


    }


private function returnBreakArray($bstarttime = '02:00',$bendtime = '03:30',$add_mins = 30)
{ 
    $bstart_time    = strtotime ($bstarttime); //change to strtotime
    $bend_time      = strtotime ($bendtime); //change to strtotime

    while ($bstart_time <= $bend_time) // loop between time
    {
       $array_ofbreak_time[] = date ("H:i", $bstart_time);
       $bstart_time += $add_mins; // to check endtime
    }
    return $array_ofbreak_time;

}


public function GetPatientAppointments()
  { 
    $appointments  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.user_id', '=', 'schedule_profile_to_patient.user_patient_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.user_patient_id',Sentinel::getUser()->id)->get(); 
            $calendarAppointments = array(); 
            foreach($appointments as $a) {
              if($a->schedule_date < date("Y-m-d"))
              {
                if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                  
                     $backgroundColor = "#f39c12";
                     $borderColor     = "#f39c12";
                   
                }
              }
            elseif($a->schedule_date == date("Y-m-d"))
            {
               if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
              $backgroundColor   = "#dd4b39"; //red
              $borderColor       = "#dd4b39"; //red
           }
            }      

            else {

              //NEXT DAY date('Y-m-d', strtotime($a->schedule_date . ' +3 day')

              if((date("Y-m-d", time() + 86400) == $a->schedule_date) OR ((date("Y-m-d", time() + 172800) == $a->schedule_date))
                OR ((date("Y-m-d", time() + 259200) == $a->schedule_date)) OR ((date("Y-m-d", time() + 345600) == $a->schedule_date)))
                {    if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                     $backgroundColor = "#00a65a";
                     $borderColor     = "#00a65a";}
                }
                else { 
                   if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                     $backgroundColor = "#0073b7";
                     $borderColor     = "#0073b7";
                   }
                   }
               }


         
                    $event              = array(
                      'id'              => $a->id,
                      'title'           => $a->first_name.' '.$a->last_name,
                      'start'           => $a->schedule_date,
                      'end'             => $a->schedule_date,
                      'backgroundColor' => $backgroundColor, //yellow
                      'borderColor'     => $borderColor //yellow
                      );
                      array_push($calendarAppointments, $event);
   }

    return response()->json($calendarAppointments);
  }


  public function GetPatientAppointmentsWithDoctor()
  { 
    $appointments  = DB::table('users')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->join('schedule_profile_to_patient', 'profile.id', '=', 'schedule_profile_to_patient.profile_doctor_id')
            ->select('profile.user_id','users.first_name','users.last_name', 'schedule_profile_to_patient.id', 'schedule_profile_to_patient.profile_doctor_id','schedule_profile_to_patient.user_patient_id','schedule_profile_to_patient.schedule_date','schedule_profile_to_patient.status')
            ->where('schedule_profile_to_patient.user_patient_id',Sentinel::getUser()->id)->get(); 
            $calendarAppointments = array(); 
            foreach($appointments as $a) {
              if($a->schedule_date < date("Y-m-d"))
              {
                if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                  
                     $backgroundColor = "#f39c12";
                     $borderColor     = "#f39c12";
                   
                }
              }
            elseif($a->schedule_date == date("Y-m-d"))
            {
               if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
              $backgroundColor   = "#dd4b39"; //red
              $borderColor       = "#dd4b39"; //red
           }
            }      

            else {

              //NEXT DAY date('Y-m-d', strtotime($a->schedule_date . ' +3 day')

              if((date("Y-m-d", time() + 86400) == $a->schedule_date) OR ((date("Y-m-d", time() + 172800) == $a->schedule_date))
                OR ((date("Y-m-d", time() + 259200) == $a->schedule_date)) OR ((date("Y-m-d", time() + 345600) == $a->schedule_date)))
                {    if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                     $backgroundColor = "#00a65a";
                     $borderColor     = "#00a65a";}
                }
                else { 
                   if($a->status == 3)
                {
                  $backgroundColor = "#00c0ef";
                  $borderColor     = "#00c0ef";
                }
                else {
                     $backgroundColor = "#0073b7";
                     $borderColor     = "#0073b7";
                   }
                   }
               }


         
                    $event              = array(
                      'id'              => $a->id,
                      'title'           => $a->first_name.' '.$a->last_name,
                      'start'           => $a->schedule_date,
                      'end'             => $a->schedule_date,
                      'backgroundColor' => $backgroundColor, //yellow
                      'borderColor'     => $borderColor //yellow
                      );
                      array_push($calendarAppointments, $event);
   }

    return response()->json($calendarAppointments);
  }

}
