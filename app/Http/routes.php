<?php 

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
get('/mail-config',  function() {
    return config('mail');
});
// Disable checkpoints (throttling, activation) for demo purposes
Sentinel::disableCheckpoints();

//Route::get('/', 'WelcomeController@index');

Route::get('/', function()
{

    if (Sentinel::check())
    {
        return Redirect::to('account');
    }
      return Redirect::to('index');
    //return View::make('index');
});

//member controller
Route::get('index', 'indexControllers@index');
Route::get('mailtest', 'indexControllers@mailtest');
Route::get('/member', 'MemeberController@getRegister');
Route::post('member/post', 'MemeberController@postRegister');
Route::get('member/profile', 'MemeberController@getProfile');
Route::get('/checkLoginUser', 'indexControllers@checkLoginUser');
Route::get('/checkUserIfLogin', 'indexControllers@checkUserIfLogin');
Route::post('/submitEnquiry', 'indexControllers@submitEnquiry');

Route::post('/checklogin', 'indexControllers@login');
Route::post('/checksignup', 'indexControllers@create');
Route::get('/getjasonstates', 'indexControllers@returnJsonStates');
Route::get('/getjasoncatagory', 'indexControllers@returnJsonCatagory');

Route::get('/getjasonAlbums/{id}', 'indexControllers@myAlbums');
Route::get('/getjasonAlbumsPhotos/', 'indexControllers@myAlbumsPhotos');

//search doctors
Route::post('searchDr', 'indexControllers@simpleSearchForDoctor');

Route::get('searchDr', 'indexControllers@simpleSearchForDoctor');
Route::get('doctorlisting', 'indexControllers@doctorsListing');
 
Route::get('doctorDetail/{id}', 'indexControllers@doctorDetailPage');
Route::post('schedualAppointment', 'indexControllers@schedualAppointment');
Route::get('get-available-days', 'indexControllers@GetAvailableDays');
Route::get('contactus', 'indexControllers@contactUs');
Route::post('contactus', 'indexControllers@contactSubmit');
//Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/coming', 'HomeController@comingSoon');
//MUTAHIR CONTROLLERS.
Route::group(['namespace'=>'front'],function(){
Route::get('subscribe', 'NewslettersController@subscribe');
});
Route::group(['namespace'=>'front','prefix'=>'pages'],function(){
     Route::get('/{id}','PagesController@index');
});
Route::group(['namespace'=>'front','prefix'=>'news'],function(){
     Route::get('/{id}','NewsController@index');
});
//DOCTOR CONTROLLERS.
Route::group(['middleware' => 'doctor', 'prefix' => 'doctor'], function()
{   
    Route::get('/', 'DoctorController@index');    
    Route::get('/profile', 'DoctorController@profile');
    Route::post('/profile/update', 'DoctorController@update');
    Route::get('/profile/settings', 'DoctorController@settings');
    Route::post('/profile/settings', 'DoctorController@changepassword');
    Route::get('/myreviews', 'DoctorController@index');
    Route::get('viewrecordsdetails/{id}', 'ReviewsController@show');
    Route::get('/myspeciliaty', 'DoctorController@showSpeciliaty');
    Route::get('/edit/{id}', 'DoctorController@edit');
    Route::get('/myprocedures', 'DoctorController@show');
    
    Route::get('/myassociations', 'DoctorController@showAssociations');
    
    Route::get('/addeditassociations', 'DoctorController@addEditAssociations');
    Route::post('/addeditassociations', 'DoctorController@updateAssociations');
    Route::get('/moveprofile', 'DoctorController@moveProfile');
    Route::post('/moveprofile', 'DoctorController@store');

    // TRANSFER ROUTES.
    Route::post('approvetransfer/', 'DoctorController@TransferAproveDisaprove');
    Route::get('approvetransfer/', 'DoctorController@TransferRequest');
    
    Route::get('joiningrequest/', 'DoctorController@dortorsRequestToJoin');

    
    Route::post('joiningrequest/', 'DoctorController@TransferAproveByDoctor');
    Route::get('doctorsocialslinks/', 'DoctorController@socialLinks');
    Route::post('doctorsocialslinks/', 'DoctorController@socialLinksSaved');
    Route::get('removesocials/{id}', 'DoctorController@removeSocials');
    Route::get('gallery', 'DoctorController@myAlbums');
    Route::get('addgallery', 'DoctorController@showForm');
    Route::post('addgallery', 'DoctorController@saveForm');
    Route::get('updategallery/{id}', 'DoctorController@editForm');
    Route::post('updategallery/{id}', 'DoctorController@updateAlbum');
    Route::get('trasferhistory', 'DoctorController@trasferHistory');
    Route::get('managegalleries/{id}', 'GalleryController@index');
    Route::get('creategalleries/{id}', 'GalleryController@create');
    Route::post('upload-post','GalleryController@postUpload');
    Route::post('upload/delete', 'GalleryController@deleteUpload');
    Route::get('sortimages', 'GalleryController@sortImages');
    Route::get('deleteimages', 'GalleryController@deleteImage');
    Route::get('sortalbums', 'GalleryController@sortAlbums');
    Route::get('deletealbums', 'GalleryController@deleteAlbums');

    Route::get('updategalleryimage/{id}/{code}', 'GalleryController@editForm');
    Route::post('updategalleryimage/{id}/{code}', 'GalleryController@updateImage');
    // APPOINTMENTS UROTEST STARTS HERE.

    Route::get('/myappintments', 'DoctorController@appointments');
    Route::post('/myappintments', 'DoctorController@ajaxSearchAppointments');
    Route::get('checkStatus/', 'DoctorController@checkStatusAppointment');
    Route::get('get-all-appointments/', 'DoctorController@GetAllAppointments');
    Route::get('reschedule/{id}', 'DoctorController@rescheduleAppointments');
    Route::get('getallappointments/', 'DoctorController@GetAvailableAppointments');
    Route::get('schedualAppointment/', 'DoctorController@schedualAppointment');

    Route::get('get-available-days', 'DoctorController@GetAvailableDays');
    
    Route::get('singleAppointment/', 'scheduleController@singleAppointment');
    

});
//------------------------

//VISITOR CONTROLLERS
  Route::group(['prefix' => 'visitors'], function()
{ 
    Route::get('/visitor', 'PatientController@getRegister');
    Route::post('visitor/post', 'PatientController@postRegister');
    Route::post('visitor/ajaxpost', 'PatientController@ajaxUserRegister');

});

Route::group(['middleware' => 'patient', 'prefix' => 'visitors'], function()
{       
    Route::get('editrecordsdetails/{id}', 'ReviewsController@edit');
    Route::post('/update', 'ReviewsController@update');
    Route::get('viewrecordsdetails/{id}', 'ReviewsController@show');
    Route::get('/profile', 'PatientController@profile');
    Route::post('/profile/update', 'PatientController@update');
    Route::get('/profile/settings', 'PatientController@settings');
    Route::post('/profile/settings', 'PatientController@changepassword');
    Route::get('/myreviews', 'PatientController@index');
    Route::get('/myappintments', 'PatientController@appointments');
    Route::get('/get-all-appointments', 'scheduleController@GetPatientAppointmentsWithDoctor');
    Route::get('singleAppointment/', 'scheduleController@singleAppointmentWithDoctor');

});

Route::get('getExternalCategories', 'indexControllers@getExternalCategories');
Route::get('starRatting', 'indexControllers@starRatting');
Route::get('/searchesbyprocedures/{id}', 'SearchController@doctorSearchByProcedures');
Route::get('/searchesbycategory/{id}', 'SearchController@doctorSearchByCategory');
Route::get('/searchesbystates/{id}', 'SearchController@doctorSearchByStates');
Route::post('/makereview','ReviewsController@store');

// ENDS MUTAHIR CONTROLLERS.

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => 'auth.admin', 'prefix' => 'roles'], function()
{

    Route::get('/', 'RolesController@index');
    Route::get('create', 'RolesController@create');
    Route::post('create', 'RolesController@store');
    Route::get('{id}', 'RolesController@edit');
    Route::post('{id}', 'RolesController@update');
    Route::get('{id}/delete', 'RolesController@delete');
});

//profile routes

Route::group([ 'prefix' => 'profiles'], function()
{ 
   //edit the profile
    Route::get('/', 'ProfileController@index');
    Route::get('editCompleteProfile/{id}', 'ProfileController@editCompleteProfile');
    Route::post('updateDoctorInfo', 'ProfileController@updateDoctorPersonalInfo');
    Route::get('editSchedule/{id}', 'ProfileController@editScheduleAvailability');
    Route::post('updateSchedule', 'ProfileController@updateScheduleAvailability');
    Route::get('completesingleProfile/{id}', 'ProfileController@completesingleProfile'); 
    Route::post('singleCompleteProfile', 'ProfileController@singleCompleteProfile');
    Route::get('completeProfile/{id}', 'ProfileController@completeProfile');
    Route::get('settings', 'ProfileController@settings');
    Route::get('profile', 'ProfileController@profile');
    Route::post('settings', 'ProfileController@changepassword');
    Route::get('permissions/{id}', 'ProfileController@show');
    Route::post('permissions/{id}', 'ProfileController@create');
    Route::get('conditions/{id}', 'ProfileController@conditionsTreated');
    Route::post('conditions/{id}', 'ProfileController@createConditions');
    Route::get('comments/{id}', 'ProfileController@showComments');
    Route::post('approve/', 'ProfileController@CommentsAproveDisaprove');
    Route::get('trasferhistory/{id}', 'ProfileController@trasferHistory');
    Route::post('doctorProfileTransfer/', 'ProfileController@doctorProfileTransfer');
  
    Route::post('approvetransfer/', 'ProfileController@TransferAproveDisaprove');
    Route::get('approvetransfer/', 'ProfileController@TransferRequest');

    Route::get('joiningrequest/', 'ProfileController@dortorsRequestToJoin');
    Route::post('joiningrequest/', 'ProfileController@TransferAproveConfirm');
    
    Route::get('viewrecordsdetails/{id}', 'ReviewsController@show');
    Route::get('getjasonDoctorPractiveId/', 'ProfileController@getjasonDoctorPractiveId');

    Route::get('removesocials/{id}', 'ProfileController@removeSocials');
    Route::get('appintments/{id}', 'ProfileController@appointments');
    Route::post('getappointments', 'ProfileController@ajaxSearchAppointments');
    Route::get('get-all-appointments/{id}', 'ProfileController@GetAllAppointments');
    Route::get('checkStatus/', 'ProfileController@checkStatusAppointment');
    Route::get('reschedule/{user}/{appointment}', 'ProfileController@rescheduleAppointments');

    Route::get('getallappointments/{id}', 'ProfileController@GetAvailableAppointments');

    Route::get('schedualAppointment/','ProfileController@schedualAppointment');
    Route::get('get-available-days', 'scheduleController@GetAvailableDays');
    Route::get('singleAppointment/', 'scheduleController@singleAppointment');
    Route::get('editInsurance/{id}', 'insuranceController@editInsurance');
     Route::get('getjasonDoctorEmail', 'ProfileController@getjasonDoctorEmail');
    
});


Route::post('profile', 'ProfileController@store');
Route::get('profile/{id}', 'ProfileController@edit');
Route::post('updateProfile', 'ProfileController@update');
//for ajax request that populate states and cities in the profile building form
Route::post('populateState', 'ProfileController@state');
Route::post('populateCities', 'ProfileController@cities');
Route::post('populateSubCatagories', 'ProfileController@subCatagories');
Route::post('populateProcedures', 'ProfileController@subCatagoriesProcedures');
Route::post('fullCompleteProfile', 'ProfileController@fullCompleteProfile');
Route::post('populateSubInsurances', 'ProfileController@subInsurances');
Route::post('populateSubOfSubInsurances', 'ProfileController@subOfSubInsurances');
 


//Routes for PROCEDURES. BY MUTAHIR SHAH.

Route::group(['middleware' => 'auth.admin', 'prefix' => 'procedures'], function()
{
    Route::get('/', 'ProceduresController@index');
    Route::get('create', 'ProceduresController@create');
    Route::post('create', 'ProceduresController@store');
    Route::get('{id}', 'ProceduresController@edit');
    Route::post('{id}', 'ProceduresController@update');
    Route::get('{id}/delete', 'ProceduresController@destroy');
});
// END OF MUTAHIR ROUTES.

//routes for package plan
Route::group(['middleware' => 'auth.admin', 'prefix' => 'package'], function()
{
    Route::get('/', 'PackageController@index');
    Route::get('create', 'PackageController@create');
    Route::post('create', 'PackageController@store');
    Route::get('{id}', 'PackageController@edit');
    Route::post('{id}', 'PackageController@update');
    Route::get('{id}/delete', 'PackageController@delete');
});

//routes for catagory management
Route::group(['middleware' => 'auth.admin', 'prefix' => 'catagory'], function()
{

    Route::get('/', 'catagoryController@index');
    Route::get('create', 'catagoryController@create');
    Route::post('create', 'catagoryController@store');
    Route::get('{id}', 'catagoryController@edit');
    Route::post('{id}', 'catagoryController@update');
    Route::get('{id}/delete', 'catagoryController@delete');
});

//routes for insurance management
Route::group(['middleware' => 'auth.admin', 'prefix' => 'insurance'], function()
{

    Route::get('/', 'insuranceController@index');
    Route::get('create', 'insuranceController@create');
    Route::post('create', 'insuranceController@store');
    Route::get('{id}', 'insuranceController@edit');
    Route::post('{id}', 'insuranceController@update');
    Route::get('{id}/delete', 'insuranceController@destroy');
});
// getting sub insurances
Route::post('subInsurrance', 'insuranceController@subInsurrance');


//routes for insurance management
Route::group(['middleware' => 'auth.admin', 'prefix' => 'creditcard'], function()
{

    Route::get('/', 'creditcardController@index');
    Route::get('create', 'creditcardController@create');
    Route::post('create', 'creditcardController@store');
    Route::get('{id}', 'creditcardController@edit');
    Route::post('{id}', 'creditcardController@update');
    Route::get('{id}/delete', 'creditcardController@destroy');
});


//routes for insurance management
Route::group(['middleware' => 'auth.admin', 'prefix' => 'associations'], function()
{

    Route::get('/', 'AssociationController@index');
    Route::get('create', 'AssociationController@create');
    Route::post('create', 'AssociationController@store');
    Route::get('{id}', 'AssociationController@edit');
    Route::post('{id}', 'AssociationController@update');
    Route::get('{id}/delete', 'AssociationController@destroy');
});



Route::group(['middleware' => 'auth.admin', 'prefix' => 'users'], function()
{
    Route::get('changeUserStatus/', 'UsersController@checkStatusUsers');
    Route::get('removeActivationLink/', 'UsersController@removeActivationLink');
    Route::get('manualActivateUser/', 'UsersController@manualActivateUser');
    Route::get('/', 'UsersController@index');
    Route::get('create', 'UsersController@create');
    Route::post('create', 'UsersController@store');
    Route::get('{id}', 'UsersController@edit');
    Route::post('{id}', 'UsersController@update');
    Route::get('{id}/delete', 'UsersController@delete');

    Route::get('profile/{id}', 'UsersController@editProfile');//AUTHOR MUTAHIR
    Route::post('profile/{id}', 'UsersController@updateProfile');//AUTHOR MUTAHIR
   
   //AUTHOR MUTAHIR
    
});

Route::group(['middleware' => 'auth.admin', 'prefix' => 'ratings'], function()
{
    Route::get('/', 'ReviewsController@index');
    Route::get('create', 'UsersController@create');
    Route::post('create', 'UsersController@store');
    Route::get('{id}', 'UsersController@edit');
    Route::post('{id}', 'UsersController@update');
    Route::post('/', 'ReviewsController@aproveDisaprove');
    Route::get('viewrecordsdetails/{id}', 'ReviewsController@show');
    
});
Route::group(['middleware' => 'auth.admin', 'prefix' => 'settings','namespace' => 'Admin'], function()
{
    Route::get('/', 'siteSettingController@index'); 
    Route::get('create', 'siteSettingController@create');
    Route::post('create', 'siteSettingController@store');
    Route::get('edit/{id}', 'siteSettingController@edit');
    Route::post('edit/{id}', 'siteSettingController@update');
    Route::get('{id}/delete', 'siteSettingController@destroy');
    Route::get('activate', 'siteSettingController@activate'); 
});

Route::group(['middleware' => 'auth.admin', 'prefix' => 'cms'], function()
{
    Route::get('/', 'Admin\PageController@index'); 
    Route::get('create', 'Admin\PageController@create');
    Route::post('create', 'Admin\PageController@store');
    Route::get('edit/{id}', 'Admin\PageController@edit');
    Route::post('{id}', 'Admin\PageController@update');
    Route::get('destroy/{id}', 'Admin\PageController@destroy');
    

    
});

Route::group(['middleware' => 'auth.admin', 'prefix' => 'conditions','namespace' => 'Admin'], function()
{
    Route::get('/', 'ConditionsController@index'); 
    Route::get('create', 'ConditionsController@create');
    Route::post('create', 'ConditionsController@store');
    Route::get('{id}', 'ConditionsController@edit');
    Route::post('{id}', 'ConditionsController@update');
    Route::get('{id}/delete', 'ConditionsController@destroy'); 
});


Route::group(['middleware' => 'auth.admin', 'prefix' => 'news','namespace' => 'Admin'], function()
{
    Route::get('/', 'NewsController@index'); 
    Route::get('create', 'NewsController@create');
    Route::post('create', 'NewsController@store');
    Route::get('edit/{id}', 'NewsController@edit');
    Route::post('{id}', 'NewsController@update');
    Route::get('{id}/delete', 'NewsController@destroy'); 
});





Route::get('wait', function()
{
    return View::make('sentinel.wait');
});
Route::get('activate/{id}/{code}', function($id, $code)
{
    $user = Sentinel::findById($id);
    if ( ! Activation::complete($user, $code))
    {
        return Redirect::to("auth/login")
            ->withErrors('Invalid or expired activation code.');
    }
    return Redirect::to('auth/login')
        ->withSuccess('Account activated.');
       })
       ->where('id', '\d+');

Route::get('reactivate', function()
{
    if ( ! $user = Sentinel::check())
    {
        return Redirect::to('auth/login')->withErrors('Account is not activated!');
    }
    $activation = Activation::exists($user) ?: Activation::create($user);
    // This is used for the demo, usually you would want
    // to activate the account through the link you
    // receive in the activation email
    Activation::complete($user, $activation->code);
    // $code = $activation->code;
    // $sent = Mail::send('sentinel.emails.activate', compact('user', 'code'), function($m) use ($user)
    // {
    // 	$m->to($user->email)->subject('Activate Your Account');
    // });
    // if ($sent === 0)
    // {
    // 	return Redirect::to('register')
    // 		->withErrors('Failed to send activation email.');
    // }
    return Redirect::to('account')
        ->withSuccess('Account activated.');
})->where('id', '\d+');
Route::get('deactivate', function()
{
    //@todo replace \Cartalyst\Sentinel\Native\Facades\Sentinel with Alias of Sentinel
    $user = \Cartalyst\Sentinel\Native\Facades\Sentinel::check();
    Cartalyst\Sentinel\Laravel\Facades\Activation::remove($user);
    return Redirect::back()
        ->withSuccess('Account deactivated.');
});
Route::get('reset', function()
{
    return View::make('sentinel.reset.begin');
});
Route::post('reset', function()
{
    $rules = [
        'email' => 'required|email',
    ];
    $validator = Validator::make(Input::get(), $rules);
    if ($validator->fails())
    {
        return Redirect::back()
            ->withInput()
            ->withErrors($validator);
    }
    $email = Input::get('email');
    $user = Sentinel::findByCredentials(compact('email'));
    if ( ! $user)
    {
        return Redirect::back()
            ->withInput()
            ->withErrors('No user with that email address belongs in our system.');
    }
    // $reminder = Reminder::exists($user) ?: Reminder::create($user);
    // $code = $reminder->code;
    // $sent = Mail::send('sentinel.emails.reminder', compact('user', 'code'), function($m) use ($user)
    // {
    // 	$m->to($user->email)->subject('Reset your account password.');
    // });
    // if ($sent === 0)
    // {
    // 	return Redirect::to('register')
    // 		->withErrors('Failed to send reset password email.');
    // }
    return Redirect::to('wait');
});
Route::get('reset/{id}/{code}', function($id, $code)
{
    $user = Sentinel::findById($id);
    return View::make('sentinel.reset.complete');
})->where('id', '\d+');
Route::post('reset/{id}/{code}', function($id, $code)
{
    $rules = [
        'password' => 'required|confirmed',
    ];
    $validator = Validator::make(Input::get(), $rules);
    if ($validator->fails())
    {
        return Redirect::back()
            ->withInput()
            ->withErrors($validator);
    }
    $user = Sentinel::findById($id);
    if ( ! $user)
    {
        return Redirect::back()
            ->withInput()
            ->withErrors('The user no longer exists.');
    }
    if ( ! Reminder::complete($user, $code, Input::get('password')))
    {
        return Redirect::to('auth/login')
            ->withErrors('Invalid or expired reset code.');
    }
    return Redirect::to('login')
        ->withSuccess("Password Reset.");
})->where('id', '\d+');
Route::group(['prefix' => 'account', 'middleware' => 'auth'], function()
{
    Route::get('/', function()
    {
        //@todo replace \Cartalyst\Sentinel\Native\Facades\Sentinel with Alias of Sentinel
        $user = \Cartalyst\Sentinel\Native\Facades\Sentinel::getUser();
        $persistence = \Cartalyst\Sentinel\Native\Facades\Sentinel::getPersistenceRepository();
        return View::make('sentinel.account.home', compact('user', 'persistence'));
    });
    Route::get('kill', function()
    {
        //@todo replace \Cartalyst\Sentinel\Native\Facades\Sentinel with Alias of Sentinel
        $user = \Cartalyst\Sentinel\Native\Facades\Sentinel::getUser();
        \Cartalyst\Sentinel\Native\Facades\Sentinel::getPersistenceRepository()->flush($user);
        return Redirect::back();
    });
    Route::get('kill-all', function()
    {
        //@todo replace \Cartalyst\Sentinel\Native\Facades\Sentinel with Alias of Sentinel
        $user = \Cartalyst\Sentinel\Native\Facades\Sentinel::getUser();
        \Cartalyst\Sentinel\Native\Facades\Sentinel::getPersistenceRepository()->flush($user, false);
        return Redirect::back();
    });
    Route::get('kill/{code}', function($code)
    {
        \Cartalyst\Sentinel\Native\Facades\Sentinel::getPersistenceRepository()->remove($code);
        return Redirect::back();
    });
});
