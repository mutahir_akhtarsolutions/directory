<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users_specialities extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_specialities';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['users_id', 'specialities_id'];


}
