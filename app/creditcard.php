<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class creditcard extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'credit_card';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cc_name', 'cc_logo', 'is_deleted'];
}
