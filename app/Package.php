<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'subscription_types';

  public $timestamps = false;
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['fee', 'duration', 'role_id'];


  /**
     * Get the role record associated with the package.
  */
  public function role()
   {
        return $this->hasOne('Cartalyst\Sentinel\Roles\EloquentRole', 'id', 'role_id' );
       //return $this->hasOne('Cartalyst\Sentinel\Roles\EloquentRole','role_id');
   }


}
