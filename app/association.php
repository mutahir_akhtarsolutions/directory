<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class association extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'association';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','assoc_logo','created_at'];

}
