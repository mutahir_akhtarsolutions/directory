<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profile';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'avatar', 'education', 'social_media_link', 'phone', 'mobile','association','business_name','no_of_doctors','business_address','business_telephone','business_reception','business_email','business_website','zip_code','contact_dr_phone_number','image_of_doctor','country_id','state_id','city_id','title','business_address2','parent_id','Practice_id','affiliation','user_dob','gender'];


    /**
       * Get the member record associated with the profile.
    */
    public function member()
     {
          return $this->hasOne('Cartalyst\Sentinel\Users\EloquentUser', 'id', 'user_id' );

     }
}
