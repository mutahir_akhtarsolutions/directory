<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'insurance';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['insurance_name', 'parent_id',  'insurance_id', 'insurance_address', 'insurance_phone', 'insurance_email', 'insurance_url'];

  // getting the Main insurance using the parent_id

  public function getInsurance(){

    return $this->hasOne('App\Insurance', 'id', 'parent_id');
  }
  //getting the main insurance usign parent_1
  /*public function getMainInsurance(){
    return $this->hasOne('App\Insurance', 'id', 'parent_1');
  }*/
}
