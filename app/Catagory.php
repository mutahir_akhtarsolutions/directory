<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catagory extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'specialities';

  public $timestamps = false;
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'parent_id'];

// getting the Main Catagory using the parent_id

public function getCatagory() {

  return $this->hasOne('App\Catagory', 'id', 'parent_id');
  
}


}
