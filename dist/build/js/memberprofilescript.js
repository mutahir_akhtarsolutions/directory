//Mutahir SCRIPT CUSTOMIZATION.
 var Practice_id;
  $(document).ready(function () {
  // REFERENCE TAKEN https://github.com/biggora/bootstrap-ajax-typeahead
   $('input.typeahead').typeahead({
    //source: typeaheadSource
    onSelect: function(item) { 
        $('#doctorpractice_id').val(item.value);
        $("#practiceModal").modal({
            keyboard: true
          });
        $('#Practice_id').removeClass( "loading-circle" )
    },
    ajax: {
           url: siteUrl+'/profiles/getjasonDoctorPractiveId',
          //triggerLength: 2,
           loadingClass: "loading-circle"
    },
     hint: true,
     highlight: true,
     minLength: 1,
     delay:0,
     addItem:true,
     items:50,
     showHintOnFocus:true
}); 
$(document).on("blur", '#email', function(e) {
    //source: typeaheadSource 
    var email = $('#email').val();
      $('#email').next("span").remove();
           $.ajax({
                  url: siteUrl+'/profiles/getjasonDoctorEmail',
                  cache: false,
                  type: "get",
                  data: 'query='+email,
                  beforeSend: function()
                  {
                   $('#email').addClass( "loading-circle" )
                  },
                  success: function(data){
                    $('#email').removeClass( "loading-circle" );
                      if(data == 'YES'){
                      $('#email').after('<span class="help-block"> Email is already taken. </span>');
                    return false;
                    }
                    else
                    {
                      $('#email').after('<span style="color:#2b8236"> Email is available. </span>');
                    }
                    

                  }
                });
         });
 //});//DOCUMENT READY FUNCTION.
 var navListItems   = $('div.setup-panel div a'),
          allWells   = $('.setup-content'),
          allNextBtn = $('.nextBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target   = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });




    // validate the comment form when it is submitted
    $(document).on("click","#next_btn1",function(e){// NEXT BTN

      var error             = false;
      var Practice_id       = $('#Practice_id');
      var first_name        = $('#first_name');
      var email             = $('#email');
      var catagory_id_1     = $('#catagory_id_1');
      var sub_catagory_id_1 = $('#sub_catagory_1');
      var user_dob          = $('#user_dob');

      Practice_id.next("span").remove();
      first_name.next("span").remove();
      email.next("span").remove();
      catagory_id_1.next("span").remove();
      sub_catagory_id_1.next("span").remove();

      if(Practice_id.val() == "") {
        Practice_id.after('<span style="color:#DD4B39" class="help-block"> Please enter your Practice ID. </span>');
        $('.Practice_id').addClass('has-error');
       Practice_id.focus(); 
        error = true;
        return false;
        } 
      if(first_name.val() == "") {
        Practice_id.next("span").remove();
        first_name.after('<span class="help-block"> Please enter Your First Name </span>');
         $('.first_name').addClass('has-error');
        first_name.focus(); 
        error = true;
        return false;
        } 

         if(user_dob.val() == "") {
        first_name.next("span").remove();
        user_dob.after('<span class="help-block"> Please enter Your First Name </span>');
         $('.user_dob').addClass('has-error');
        user_dob.focus(); 
        error = true;
        return false;
        }


        

      if(catagory_id_1.val() == "") {        
        user_dob.next("span").remove();
        catagory_id_1.after('<span style="color:#DD4B39" class="help-block"> Please select a catagory. </span>');
         $('.catagory_id_1').addClass('has-error');
        catagory_id_1.focus(); 
        error = true;
        return false;
        } 

      if(sub_catagory_id_1.val() == "") {
        catagory_id_1.next("span").remove();
        sub_catagory_id_1.after('<span  class="help-block"> Please enter select a sub catagory. </span>');
         $('.sub_catagory_id_1').addClass('has-error');
        sub_catagory_id_1.focus(); 
        error = true;
        return false;
        } 

         if(email.val() == "") {
            sub_catagory_id_1.next("span").remove();       
            email.after('<span class="help-block"> Please enter your Email. </span>');
             $('.email').addClass('has-error');
            email.focus(); 
            error = true;
            return false;
        } else {
          email.next("span").remove();  
         $.ajax({
                  url: siteUrl+'/profiles/getjasonDoctorEmail',
                  cache: false,
                  type: "get",
                  data: 'query='+email.val(),
                  beforeSend: function()
                  {
                   $('#email').addClass( "loading-circle" )
                  },
                  success: function(data){
                    $('#email').removeClass( "loading-circle" );
                   if(data == 'YES'){
                      $('#email').after('<span class="help-block"> Email has already been taken. </span>');
                    return false; 
                    }
                    else
                    {
                      $('#email').after('<span style="color:#2b8236"> Email is available. </span>');
                    }
                    

                  }
                });
      
       }

        if(error == false)
        {
          Practice_id.next("span").remove();
          first_name.next("span").remove();
          email.next("span").remove();
          catagory_id_1.next("span").remove();
          sub_catagory_id_1.next("span").remove();
          user_dob.next("span").remove();
          var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
        
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
      }// end if IF condition for next page.
  

    });// END NEXT BTN

     $(document).on("click","#next_btn2",function(e){// NEXT BTN

  
         var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  

    });// END NEXT BTN


  $('div.setup-panel div a.btn-primary').trigger('click');
//$(function () {
                  $('.datetimepicker3').datetimepicker({
                    //format: 'LT',
                    format: 'H:mm'
                });
                  $('#user_dob').datepicker({
                      //format: 'LT',
                   dateFormat: 'yy-mm-dd', 
                    changeMonth: true,
                    changeYear:true,
                     yearRange: "-39:-23" // this is the option you're looking for
  
                });
        //getting sub Insurances
      
//
        $(document).on("change", '.pinsurance', function(e) {
                  var p_insurance    = $(this).val(); 
                  var data_id = $(this).data('id');
                  var token = $('#_token').val();

                  $.ajax({
                      type: "POST",
                      data: {p_insurance: p_insurance, _token:token},
                      url: siteUrl+'/populateSubInsurances',
                      dataType: 'json',
                      success: function(json) {
                     var $sub_ins = $("#s_p_insurance_"+data_id);
                      var $el      = $('.ddl'+data_id+' .sub_insurance');
                      if(typeof json =='object' && json !='' && p_insurance != 0){
                        $sub_ins.empty(); // remove old options
                        $el.css('display', 'block');
                          $sub_ins.append($("<option></option>")
                                  .attr("value", '').text('Please select sub insurance'));
                        $.each(json, function(value, key) {

                              $sub_ins.append($("<option></option>")
                                      .attr("value", value).text(key));

                          });

                        }else{
                          $sub_ins.empty(); // remove old options
                          $el.css('display', 'none');
                         $('.ddl'+data_id+' .sub_of_sub_insurance').css('display', 'none');
                        }

                  }
                  });

              });


              //getting sub of sub insurances
              $(document).on("change", '.spinsurance', function(e) {

                   var s_p_insurance = $(this).val();
                   var data_id = $(this).data('id');
                        var token = $('#_token').val(); 
                        $.ajax({
                            type: "POST",
                            data: {s_p_insurance: s_p_insurance, _token:token},
                            url: siteUrl+'/populateSubOfSubInsurances',
                            dataType: 'json',
                            success: function(json) {
                              // s_o_s_insurance means sub_of_sub_insurance
                            var $sub_of_sub_ins = $("#s_o_s_insurance_"+data_id);
                            var $el = $('.ddl'+data_id+' .sub_of_sub_insurance');
                            if(typeof json =='object' && json !='' && s_p_insurance != 0){
                              $sub_of_sub_ins.empty(); // remove old options
                              $el.css('display', 'block');
                                $sub_of_sub_ins.append($("<option></option>")
                                        .attr("value", '').text('Please select sub of sub insurance'));

                                  $.each(json, function(value, key) {

                                    $sub_of_sub_ins.append($("<option></option>")
                                            .attr("value", value).text(key));

                                });

                              }else{
                                $sub_of_sub_ins.empty(); // remove old options
                                $el.css('display', 'none');
                              }

                        }
                        });

                    });
    //adding more insurances and sub insurance on clicking add buttion START

                    $("#addBtn_ins").on("click", function() {

                    var total;
                      // To auto limit based on the number of options
                      // Hard code a limit
                    total = 5;
            var ctr = $("#adding_insurrance").find(".extra").length;
              if (ctr < total) {
                       var $ddl = $("#insurance_block").clone();
                   
                       $ddl.attr("class", "ddl" + ctr);
                    $ddl.attr("name", "ddl" + ctr);
                       $ddl.addClass("extra");
                       $ddl.find("[name='p_insurance[]']").attr('id','p_insurance_'+ctr);
                        $ddl.find("[name='s_p_insurance[]']").attr('id','s_p_insurance_'+ctr);
                         $ddl.find("[name='s_o_s_insurance[]']").attr('id','s_o_s_insurance_'+ctr);

                         $ddl.find("[name='p_insurance[]']").attr('data-id',ctr);
                        $ddl.find("[name='s_p_insurance[]']").attr('data-id',ctr);
                         $ddl.find("[name='s_o_s_insurance[]']").attr('data-id',ctr);

                       $("#adding_insurrance").append($ddl);
                    }

                    });
                    $("#removeBtn_ins").on("click", function() {

                    var ctr = $("#adding_insurrance").find(".extra").length;

                    var test = $( "#adding_insurrance" ).find(".extra").last().remove( );

                    });
                    //adding more insurances and sub insurance on licking add buttion END

          
  // ADD MORE ASSOCIATIONS 
    var max_fields_associations      = 5; //maximum input boxes allowed
    var wrapper_associations         = $(".input_associations_wrap"); //Fields wrapper
    var add_button_associations      = $(".add_associations_button"); //Add button ID
    var x_associations = $("#total_associations_filds").val(); //initlal text box count
    $(wrapper_associations).on("click",".add_associations_button", function(e){
         //on add input button click
        e.preventDefault();
        if(x_associations < max_fields_associations){ //max input box allowed
            x_associations++; //text box increment
            $(wrapper_associations).append('<div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important"><div class="col-sm-4" style="padding-left: 0px!important">          <input  maxlength="100" type="text" name="association[]" value="" class="form-control" placeholder="Enter dr association" list="datalist1"/>          </div>          <div class="col-sm-4">          <select name="availability[]" class="form-control">          <option value="0">Select A day</option>          <option value="Saturday">Saturday</option>          <option value="Sunday">Sunday</option>          <option value="Monday">Monday</option>          <option value="Tuesday">Tuesday</option>           <option value="Wedensday">Wedensday</option>           <option value="Thursday">Thursday</option>          <option value="Friday">Friday</option>           </select>          </div>          <div class="col-sm-4"><button class="remove_associations_field btn btn-primary"  data-id="" >-</button>          <button class="add_associations_button btn btn-primary">+</button>          </div>          <div class="col-sm-12">          <div class="col-sm-4">          <label class="control-label">Time From :</label><div class="input-group date datetimepicker3" id="datetimepicker3">                  <input type="text" class="form-control" id="from_time" name="from_time[]" />                    <span class="input-group-addon">                        <span class="glyphicon glyphicon-time"></span>                    </span>                </div> </div>          <div class="col-sm-4">          <label class="control-label">Time To:</label><div class="input-group date datetimepicker3" id="datetimepicker3">                    <input type="text" class="form-control"  id="to_time" name="to_time[]" />                    <span class="input-group-addon">                        <span class="glyphicon glyphicon-time"></span>                    </span>                </div></div><div class="col-sm-2"></div></div> </div>'); //add input box

               $('.datetimepicker3').datetimepicker({
                    //format: 'LT',
                    format: 'H:mm'
                });
        }
    });
    
    $(wrapper_associations).on("click",".remove_associations_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x_associations--;
    });// END ADD MORE ASSOCIATIONS



     // ADD SOCIAL ICONS FIELDS
    var max_fields      = 5; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var x = $("#total_filds").val(); //initlal text box count

    $(wrapper).on("click",".add_field_button", function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important"><div class="col-sm-6" style="padding-left: 0px!important"><input  maxlength="200" type="text" name="sociallinks[]" value="" class="form-control" placeholder="Enter dr association" /><input name="link_id[]" id="link_id" type="hidden" value=""></input></div><div class="col-sm-2"><input  maxlength="5" type="text" name="sort_order[]" value="" class="form-control" placeholder="Sort Order"  /></div><div class="col-sm-4"><button class="remove_field btn btn-primary"  data-id="" >-</button>&nbsp;<button class="add_field_button btn btn-primary">+</button></div><div class="col-sm-12"  style="padding-top: 10px; padding-bottom: 10px"><input id="socialicon" name="socialicon[]" type="file" multiple class="file-loading"></div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); 
        var data_id = $(this).data("id");
        if(data_id != ''){ 
            $.ajax({
                  url: siteUrl+'/profiles/removesocials'+ '/' + data_id,
                  cache: false,
                  type: "get",
                  data: '',
                  beforeSend: function()
                  {
                   // $('#modal-body').html('loading...');
                  },
                  success: function(data){
                  }
                });

        }
        $(this).parent('div').parent('div').remove();
        x--;
    }) 
  //END SOCIAL ICONS FIELDS

            });