 @extends('app')

 {{-- news content --}}
 @section('head')
 @stop
 @section('footer')

  <!-- CK Editor -->
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
 <script src="{{ URL::asset('plugins/filestyle/bootstrap-filestyle.min.js')}}"></script>
    <script>
      $(function () {
        // Replace the <textarea id="content"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('content');
        //bootstrap WYSIHTML5 - text editor 
      });
    </script>

 @stop
 @section('content')
 <div class="container">
	<div class="box">
		<div class="box-header with-border">
		  <h3 class="box-title">Edit news</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
		    <form action="{{url('news').'/'. $news->id}}" role="form" class="form" method="post" enctype="multipart/form-data"> 
		    	<input type="hidden" name="_token" value="{{csrf_token()}}" />
		        <!-- text input -->
		        <div class="form-group">
		        <div class="row">
		        <div class="col-md-1">
		          <label for="title">News Title<span class="text text-danger">*</span></label></div>
		          <div class="col-md-4">
		          <input id="title" name="title" type="text" class="form-control" placeholder="news title" value="{{$news->title?$news->title:old('title')}}" /></div></div>
		        </div>
		        <div class="form-group">
		        <div class="row">
		        <div class="col-md-1">
		          <label for="content">Content</label></div>
		           <div class="col-md-8">
		          <textarea name="content" id="content" rows="7" class="form-control" placeholder="news content">{{$news->content?$news->content:old('content')}}</textarea></div>
		        </div>	 </div>	

		           <div class="form-group">
		          <div class="row">
		        <div class="col-md-1">
		        	<label for="visible">Image:</label>
		        </div>
		          <div class="col-md-3">
		        	 <input id="image" name="image" type="file" class="filestyle" />
		        	 <img height="100px" width="100px" src="{{ url() }}/images/news/icon_size/{{ $news->image }}" alt="..." class="img-circle">
 </div> </div>
		        </div>	 

		        <div class="form-group">
		        <div class="row">
		        <div class="col-md-1">
		        	<label for="visible">Status</label>
		        	</div>
		        	<div class="col-md-4">
		            <select name="visible" id="visible" class="form-control">
		            	<option @if($news->visible == 0) selected="selected" @endif value="0">Disable</option>
		            	<option @if($news->visible == 1) selected="selected" @endif value="1">Enable</option>
		            </select>                        	
		            </div></div>
		        </div>
		        <div class="row text-center">
		        <div class="col-md-4">
		        <a href="{{url('news/')}}" class="btn btn-default">Back</a>
		        <button type="submit" class="btn btn-sml btn-primary">Submit</button>
		        </div>
		        </div>
		    </form>
		</div><!-- /.box-body -->
	</div><!-- /.box -->

	</div><!-- /.container-->
@endsection