 @extends('app')

 {{-- Page content --}}
 @section('head')
 @stop
 @section('footer')
 @stop
 @section('content')
 <div class="container">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">CMS page</h3>
            <div class="col-md-offset-9" ><a href="{{url('cms/create')}}" class="btn btn-sml btn-success"><i class="fa fa-pencil"></i> Add New</a>
            </div>


               @if ($pages->count())
     Page {{ $pages->currentPage() }} of {{ $pages->lastPage() }}

     <div class="pull-right">
       {!! $pages->render() !!}
     </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
         <?php //if(!$pages->isEmpty()) ?>
                <table class="table">
                    <tr>
                      <th class="col-md-1">#</th>
                      <th class="col-md-3">Name</th>
                      <th class="col-md-3">Content</th>
                      <th class="col-md-2">Status</th>
                      <th class="col-md-3">Actions</th>
                    </tr>
                    @foreach($pages as $page)
                        <tr>
                          <td>{{$page->id}}</td>
                          <td><a href="{{url('cms/edit').'/'. $page->id}}">{{$page->title}}</a></td>
                          <td>{!!nl2br(str_limit($page->content, 100))!!}</td>
                          <td>
                          	@if($page->status == 1)
                          		<button class="btn btn-success" type="button"><i class="fa fa-check"></i></button>
                          	@else
                          		<button class="btn btn-danger" type="button"><i class="fa fa-times"></i></button>
                          	@endif                          	
                          </td>
                          <td>
                                <form action="{{url('cms/destroy').'/'. $page->id}}" method="get"> 
                                	 
                                    <a href="{{url('cms/edit').'/'. $page->id}}" class="btn btn-sml btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                    <button type="submit" class="btn btn-sml btn-danger" onClick="return confirm('Are you sure you want to delete?')"><i class="fa fa-timex"></i> Delete</button>
                                </form>
                          	</td>
                        </tr>
                    @endforeach
                </table> 


             Page {{ $pages->currentPage() }} of {{ $pages->lastPage() }}

     <div class="pull-right">
        {!! $pages->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>
    @endif
        </div><!-- /.box-body --> 
    </div><!-- /.box -->
    </div><!-- /.Container -->
@endsection