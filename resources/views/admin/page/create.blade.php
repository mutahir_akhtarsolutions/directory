 @extends('app')

 {{-- Page content --}}
 @section('head')
 @stop
 @section('footer')
    <!-- CK Editor -->
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('plugins/filestyle/bootstrap-filestyle.min.js')}}"></script>
    <script>
      $(function () {
        // Replace the <textarea id="content"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('content');
        //bootstrap WYSIHTML5 - text editor 
      });
    </script>

 @stop
 @section('content')
 <div class="container">
	<div class="box">
		<div class="box-header with-border">
		  <h3 class="box-title">Add a page</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
		    <form action="create" role="form" class="form" method="post"  enctype="multipart/form-data">
		    	<input type="hidden" name="_token" value="{{csrf_token()}}">
		        <!-- text input -->
		        <div class="form-group {{ $errors->first('title', ' has-error') }}">
		        <div class="row">
		        <div class="col-md-1">
		          <label for="title">Title<span class="text text-danger">*</span></label></div>
		          <div class="col-md-4">
		          <input id="title" name="title" type="text" class="form-control" placeholder="page title" value="{{old('title')}}" />
		           <span class="help-block">{{{ $errors->first('title', ':message') }}}</span>
		           </div></div>
		        </div>
		        <div class="form-group">
		         <div class="row">
		        <div class="col-md-1">
		          <label for="content">Content</label>
		          </div>
		          <div class="col-md-8">

		          <textarea name="content" id="content" rows="10" class="form-control" placeholder="page content">{{old('content')}}</textarea>
		        </div> </div> </div>

		        <div class="form-group">
		          <div class="row">
		        <div class="col-md-1">
		        	<label for="status">Image:</label>
		        </div>
		          <div class="col-md-4">
		        	 <input id="image" name="image" type="file" class="filestyle" />
 </div> </div>
		        </div>	

		            <div class="form-group">
		          <div class="row">
		        <div class="col-md-1">
		        	<label for="status">Sort Order</label>
		        	 </div>
		          <div class="col-md-4">
		        	 <input id="sort_order" name="sort_order" type="text" class="form-control" placeholder="0" value="{{old('sort_order')}}" size="20" style="width:10%!important" />
 </div> </div>
		        </div>		        
		        <div class="form-group">
		          <div class="row">
		        <div class="col-md-1">
		        	<label for="status">Status</label>
		        	 </div>
		          <div class="col-md-4">
		            <select name="status" id="status" class="form-control">
		            	<option value="0">Disable</option>
		            	<option value="1">Enable</option>
		            </select>                     </div> </div>    	
		        </div>
		        <div class="row text-center">
		        <div class="col-md-4">

		        <a href="{{url('cms/')}}" class="btn btn-default">Back</a>
		        <button type="submit" class="btn btn-sml btn-primary">Submit</button>
		        </div></div>
		    </form>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
	</div>
@endsection