@extends('app')

 {{-- Page content --}}
 @section('content')

<div class="container">

<div class=" col-md-offset-2 col-md-7">
     <div class="page-header">
        <h1>Conditions <span class="pull-right"><a href="{{ URL::to('conditions/create') }}" class="btn btn-warning">Create</a></span></h1>
     </div>

     @if ($conditions->count())
     Page {{ $conditions->currentPage() }} of {{ $conditions->lastPage() }}

     <div class="pull-right">
       {!! $conditions->render() !!}
     </div>

     <br><br>

     <table class="table table-bordered">
        <thead>
        <th class="col-lg-2">Srno</th> 
            <th class="col-lg-6">Name</th> 
            <th class="col-lg-4">Actions</th>
        </thead>
        <tbody>
        <?php $counter = 1;?>
            @foreach ($conditions as $condition)
            @if($condition)
            <tr>
            <td>{{$counter}}</td>
                <td>{{ $condition->condition_name }}</td> 
                <td>
                    <a class="btn btn-warning" href="{{ URL::to("conditions/{$condition->id}") }}">Edit</a>
                    <a class="btn btn-danger" href="{{ URL::to("conditions/{$condition->id}/delete") }}">Delete</a>
                </td>
            </tr>
            <?php $counter++;?>
            @endif
            @endforeach
        </tbody>
     </table>

     Page {{ $conditions->currentPage() }} of {{ $conditions->lastPage() }}

     <div class="pull-right">
        {!! $conditions->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>
    @endif
</div>
</div>
 @endsection
