@extends('app')

 {{-- Page content --}}
 @section('content')

 <div class="container">
<div class=" col-md-offset-4 col-md-4">
     <div class="page-header">

        <h1>{{ $mode == 'create' ? 'Create Condition' : 'Update Condition' }} <small>{{ $mode === 'update' ? @$condition->condition_name : null }}</small></h1>
     </div>

     <form method="post" action="">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="form-group{{ $errors->first('condition_name', ' has-error') }}">

            <label for="name">Condition Name</label>

            <input type="text" class="form-control" name="condition_name" id="condition_name" value="{{ Input::old('condition_name', @$condition->condition_name) }}" placeholder="Enter the name.">

            <span class="help-block">{{{ $errors->first('condition_name', ':message') }}}</span>

        </div> 

        <button type="submit" class="btn btn-default">Submit</button>
<button type="button" onclick="location.href = '{{url('conditions')}}';" class="btn btn-default"> Cancel </button>
     </form>
     </div>
 </div>

 @stop
