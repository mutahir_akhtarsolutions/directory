 @extends('app')

 {{-- settings content --}}
 @section('head')
 @stop
 @section('footer')

  <!-- CK Editor -->
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
 <script src="{{ URL::asset('plugins/filestyle/bootstrap-filestyle.min.js')}}"></script>
    <script>
      $(function () {
        // Replace the <textarea id="content"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('address');
        //bootstrap WYSIHTML5 - text editor 
      });
    </script>

 @stop
 @section('content')
 <div class="container">
	<div class="box">
		<div class="box-header with-border">
		  <h3 class="box-title">Edit settings</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
		    <form action="{{url('settings/edit').'/'. $settings->id}}" role="form" class="form" method="post" enctype="multipart/form-data"> 
		    	<input type="hidden" name="_token" value="{{csrf_token()}}" />
		        <!-- text input -->
		        <div class="form-group {{ $errors->first('site_title', ' has-error') }}">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Settings Title<span class="text text-danger">*</span></label></div>
		          <div class="col-md-4">
		          <input id="site_title" name="site_title" type="text" class="form-control" placeholder="site title" value="{{$settings->site_title?$settings->site_title:old('site_title')}}" />

            <span class="help-block">{{{ $errors->first('site_title', ':message') }}}</span>
		          </div></div>
		        </div>

		          <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Settings Copy Right<span class="text text-danger"></span></label></div>
		          <div class="col-md-4">
		          <input id="site_copyright" name="site_copyright" type="text" class="form-control" placeholder="site Copy Right" value="{{$settings->site_copyright?$settings->site_copyright:old('site_copyright')}}" /></div></div>
		        </div>

		          <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Site Email<span class="text text-danger"></span></label></div>
		          <div class="col-md-4">
		          <input id="site_copyright" name="site_email" type="text" class="form-control" placeholder="" value="{{$settings->site_email?$settings->site_email:old('site_email')}}" /></div></div>
		        </div>



		          <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Site Contact Email<span class="text text-danger"></span></label></div>
		          <div class="col-md-4">
		          <input id="contact_email" name="contact_email" type="text" class="form-control" placeholder="" value="{{$settings->contact_email?$settings->contact_email:old('contact_email')}}" /></div></div>
		        </div>
    <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Site General Email<span class="text text-danger"></span></label></div>
		          <div class="col-md-4">
		          <input id="general_email" name="general_email" type="text" class="form-control" placeholder="" value="{{$settings->general_email?$settings->general_email:old('general_email')}}" /></div></div>
		        </div>


   <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Site Contact Phone<span class="text text-danger"></span></label></div>
		          <div class="col-md-4">
		          <input id="contact_phone" name="contact_phone" type="text" class="form-control" placeholder="" value="{{$settings->contact_phone?$settings->contact_phone:old('contact_phone')}}" /></div></div>
		        </div> 
   <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Site Contact Cell<span class="text text-danger"></span></label></div>
		          <div class="col-md-4">
		          <input id="contact_cell" name="contact_cell" type="text" class="form-control" placeholder="" value="{{$settings->contact_cell?$settings->contact_cell:old('contact_cell')}}" /></div></div>
		        </div>

		         <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Site Url<span class="text text-danger"></span></label></div>
		          <div class="col-md-4">
		          <input id="site_url" name="site_url" type="text" class="form-control" placeholder="" value="{{$settings->site_url?$settings->site_url:old('site_url')}}" /></div></div>
		        </div>

		            <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Site Address<span class="text text-danger"></span></label></div>
		          <div class="col-md-8">
		           <textarea name="address" id="address" rows="5" class="form-control" placeholder="">{{$settings->address?$settings->address:old('address')}}</textarea>
 </div></div>
		        </div>

		             <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Site Keywords<span class="text text-danger"></span></label></div>
		          <div class="col-md-8">
		           <textarea name="site_keywords" id="site_keywords" rows="5" class="form-control" placeholder="">{{$settings->site_keywords?$settings->site_keywords:old('site_keywords')}}</textarea>
		        </div></div>
		        </div>




       <div class="form-group">
		        <div class="row">
		        <div class="col-md-2">
		          <label for="title">Site Meta Tags<span class="text text-danger"></span></label></div>
		          <div class="col-md-8"> 

 <textarea name="site_metatags" id="site_metatags" rows="5" class="form-control" placeholder="">{{$settings->site_metatags?$settings->site_metatags:old('site_metatags')}}</textarea>
		          </div></div>
		        </div>


       <div class="form-group">
		<div class="row">
		<div class="col-md-2">
		<label for="title">Site Meta Description<span class="text text-danger"></span></label></div>
		<div class="col-md-8"> 
 <textarea name="site_metadescriptions" id="site_metadescriptions" rows="5" class="form-control" placeholder="">{{$settings->site_metadescriptions?$settings->site_metadescriptions:old('site_metadescriptions')}}</textarea>

		          </div></div>
		        </div> 

		           <div class="form-group">
		          <div class="row">
		        <div class="col-md-2">
		        	<label for="visible">Fav Icon:</label>
		        </div>
		          <div class="col-md-3">
		        	 <input id="site_facon" name="site_facon" type="file" class="filestyle" />
		        	 <img height="100px" width="100px" src="{{ url() }}/images/{{ $settings->site_facon }}" alt="..." class="img-circle">
 </div> </div>
		        </div>	 
 
		        <div class="row text-center">
		        <div class="col-md-6">
		        <a href="{{url('settings/')}}" class="btn btn-default">Back</a>
		        <button type="submit" class="btn btn-sml btn-primary">Submit</button>
		        </div>
		        </div>
		    </form>
		</div><!-- /.box-body -->
	</div><!-- /.box -->

	</div><!-- /.container-->
@endsection