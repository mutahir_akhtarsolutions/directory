 @extends('app')
 {{-- settings_values content --}}
 @section('head')
 @stop
 @section('footer')

<script  type="text/javascript" charset="utf-8" >
  $(document).ready(function(){


$(".activate").click(function(e){  
    e.preventDefault();
  var dataId    = $(this).attr("data-id"); 
  var dataValue = $(this).attr("data-value");
  
$('#msg-container').html('<img src="{{ URL::asset('images/276.gif')}}">');
$.ajax({
   url: "{{url()}}/settings/activate",
   data: { dataId: dataId,
           dataValue:dataValue
         },

   error: function(data) {
      $('#msg-container').html('<div class="alert alert-danger alert-block"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button><strong>Error</strong>Oops There is an error !</div>');
       
   },

   dataType: 'text',
   success: function(data) {
    $('#msg-container').html('<div class="alert alert-success alert-block"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button><strong>Success</strong>'+data+'</div>');
     window.setTimeout(function(){location.reload()},500)
   },
   type: 'GET'
});
});// END OF DOCUMENT READY FUNCTION.


  });

</script>
 @stop
 @section('content')
 <div class="container">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">General Settings.</h3>
            <div class="col-md-offset-9" ><a href="{{url('settings/create')}}" class="btn btn-sml btn-success"><i class="fa fa-pencil"></i> Add New</a>
            </div>

             @if ($settings->count())
     Page {{ $settings->currentPage() }} of {{ $settings->lastPage() }}

     <div class="pull-right">
       {!! $settings->render() !!}
     </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding"> 
                <table class="table">
                    <tr>
                      <th class="col-md-1">#</th>
                      <th class="col-md-3">Site Title</th>
                      <th class="col-md-3">Site Email</th>
                      <th class="col-md-2">Status</th>
                      <th class="col-md-3">Actions</th>
                    </tr>
                    <?php $counter = 1;?>
                    @foreach($settings as $settings_values)
                        <tr>
                          <td>{{$counter}}</td>
                          <td><a href="{{url('settings').'/'. $settings_values->id}}">{{$settings_values->site_title}}</a></td>
                          <td>{!!nl2br(str_limit($settings_values->site_email, 100))!!}</td>
                          <td>
                          	@if($settings_values->visible == 1)
                          		<button class="btn btn-success" type="button" data-id="{{$settings_values->id}}" data-value="1"><i class="fa fa-check" ></i></button>
                          	@else
                          		<button class="btn btn-danger activate" type="button" data-id="{{$settings_values->id}}" data-value="0" id="status_{{$counter}}"><i class="fa fa-times"></i></button>
                          	@endif                          	
                          </td>
                          <td>
                                <form action="{{url('settings').'/'. $settings_values->id.'/delete'}}" method="get"> 
                                	 
                                    <a href="{{url('settings/edit').'/'. $settings_values->id.''}}" class="btn btn-sml btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                    <button type="submit" class="btn btn-sml btn-danger" onClick="return confirm('Are you sure you want to delete?')"><i class="fa fa-timex"></i> Delete</button>
                                </form>
                          	</td>
                        </tr>
                        <?php $counter++;?>
                    @endforeach
                </table> 


             Page {{ $settings->currentPage() }} of {{ $settings->lastPage() }}

     <div class="pull-right">
        {!! $settings->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>
    @endif
        </div><!-- /.box-body --> 
    </div><!-- /.box -->
    </div><!-- /.Container -->
@endsection