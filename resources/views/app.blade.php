<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> Nation Wide Physicians Admin</title>

	<link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<!-- start of CSS and other files of angular tamplates-->
	<meta name="description" content="app, web app, responsive, responsive layout, admin, admin panel, admin dashboard, flat, flat ui, ui kit, AngularJS, ui route, charts, widgets, components" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="stylesheet" href="{{ asset('assets/css') }}/libs/assets/animate.css/animate.css" type="text/css" />
	<link rel="stylesheet" href="{{ asset('assets/css') }}/libs/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="{{ asset('assets/css') }}/libs/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />

	<link rel="stylesheet" href="{{ asset('assets/css') }}/libs/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="{{ asset('assets/css') }}/src/css/font.css" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css') }}/src/css/app.css" type="text/css" />

<link href="{{ URL::asset('assets/css/custom.css') }}" type="text/css" rel="stylesheet">

@yield('head')
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!--<a class="navbar-brand" href="#">Laravel</a>-->
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/') }}">Home</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">

					@if ( \Cartalyst\Sentinel\Native\Facades\Sentinel::check() === false )
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
					    @if ( \Cartalyst\Sentinel\Native\Facades\Sentinel::hasAccess('admin') !== false )
              <li{{ Request::is('associations*') ? ' class=active' : null }}><a href="{{ URL::to('associations') }}">Associations</a></li>

<li{{ Request::is('catagory*') ? ' class=active' : null }}>
                            <a href="{{ URL::to('catagory') }}">Categories  </a></li>
                            <li{{ Request::is('conditions*') ? ' class=active' : null }}>
                            <a href="{{ URL::to('conditions') }}">Conditions</a></li>

														<li{{ Request::is('creditcard*') ? ' class=active' : null }}><a href="{{ URL::to('creditcard') }}">Credit Card Management</a></li>
                            
														<li{{ Request::is('insurance*') ? ' class=active' : null }}><a href="{{ URL::to('insurance') }}">Insurance Management</a></li> 


                            <li class="dropdown {{ (Request::is('settings*') || Request::is('cms*') || Request::is('news*')) ? 'active' : null }}"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Menu
                            <span class="caret"></span></a>

                            <ul class="dropdown-menu" role="menu">
                             <li class="{{ Request::is('news*') ? 'active' : null }}"> <a href="{{ URL::to('news') }}">News</a></li>
                            <li class="{{ Request::is('settings*') ? 'active' : null }}"> <a href="{{ URL::to('settings') }}">Site Settings</a></li>

                            <li class="{{ Request::is('cms*') ? 'active' : null }}"> <a href="{{ URL::to('cms') }}">Pages</a></li>

                            </ul> 
                            </li>

														
							              <li{{ Request::is('package*') ? ' class=active' : null }}><a href="{{ URL::to('package') }}">Package Plan</a></li> 
                                  <li{{ Request::is('procedures*') ? ' class=active' : null }}><a href="{{ URL::to('procedures') }}">Procedures</a></li>

 

                            <li class="dropdown {{ (Request::is('users*') || Request::is('ratings*') || Request::is('role*')) ? 'active' : null }}"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Users
                            <span class="caret"></span></a>

                            <ul class="dropdown-menu" role="menu">
                            <li class="{{ Request::is('users*') ? 'active' : null }}"> <a href="{{ URL::to('users') }}">Users</a></li>

                            <li class="{{ Request::is('ratings*') ? 'active' : null }}"> <a href="{{ URL::to('ratings') }}">Rattings</a></li>
                            <li{{ Request::is('roles*') ? ' class=active' : null }}><a href="{{ URL::to('roles') }}">Roles</a></li>

                            </ul> 
                            </li>
                        @endif
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ \Cartalyst\Sentinel\Native\Facades\Sentinel::getUser()->email }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
							    <li{{ Request::is('account') ? ' class=active' : null }}><a href="{{ URL::to('account') }}">Account
                                    @if ( ! Activation::completed(\Cartalyst\Sentinel\Native\Facades\Sentinel::getUser()))
                                    <span class="label label-danger">Inactive</span>
                                    @endif
                                </a></li>
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
<div class="container">
  
  <div class="page-header" id="msg-container">
    @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Error</strong>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif

    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Success</strong> {{ $message }}
    </div>
    @endif
</div>
</div> 
	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>


	<script type="text/javascript">
      	$('.tip').tooltip();
        //$('#modal').modal();

        
$(document).on('change', '.form-select', function(event) {
    event.preventDefault(); 
    $(this).closest('form').submit();
});

$(".modalInput").on("click", function(e) {
    var detailId;
      $('#modal-body').html('');
    e.preventDefault();
     
     detailId = $(this).data("detail-id");
     
    $.ajax({
          url: 'ratings/viewrecordsdetails'+ '/' + detailId,
          cache: false,
          type: "get",
          data: '',
          beforeSend: function()
          {
            $('#modal-body').html('loading...');
          },
          success: function(data){
            $('.modal-title').text(''); 
            $('#modal-body').html('');
            $('#modal-body').html(data);
            $('.box-footer').show();
          }
        });  
       
});  




         $(document).on("change", '#country_id', function(e) {
            var country_id = $(this).val();
            var token = $('input[name=_token]').val();


            $.ajax({
                type: "POST",
                data: {country_id: country_id, _token:token},
                url: '<?php echo url('populateState');?>',
                dataType: 'json',
                success: function(json) {
  //console.log(json);
                    var $el = $("#state_id");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                            .attr("value", '').text('Please Select'));

                      $.each(json, function(value, key) {

                        $el.append($("<option></option>")
                                .attr("value", value).text(key));

                    });




                }
            });

        });

        //state_id
        //send ajax request to populate the city against state dropdown
        $(document).on("change", '#state_id', function(e) {
                  var state_id = $(this).val();
                  var token = $('input[name=_token]').val();


                  $.ajax({
                      type: "POST",
                      data: {state_id: state_id, _token:token},
                      url: '<?php echo url('populateCities');?>',
                      dataType: 'json',
                      success: function(json) {
        //console.log(json);
                          var $el = $("#city_id");
                          $el.empty(); // remove old options
                          $el.append($("<option></option>")
                                  .attr("value", '').text('Please Select'));

                            $.each(json, function(value, key) {

                              $el.append($("<option></option>")
                                      .attr("value", value).text(key));

                          });




                      }
                  });

              });

    </script>
    @yield('footer')
</body>
</html>
