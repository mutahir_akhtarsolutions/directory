 @extends('layout.memberapp')
 @section('head')
  <link rel="stylesheet" href="{{ URL::asset('dist/build/css/bootstrap-datetimepicker.min.css') }}">

 @stop
 @section('footer')


<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="{{ URL::asset('dist/build/js/bootstrap-datetimepicker.min.js') }}"></script> 
   <script type="text/javascript">
            $(function () {
                $('.datetimepicker3').datetimepicker({
                    format: 'LT'
                });

  $('#user_dob').datepicker({
                    //format: 'LT',
                   dateFormat: 'yy-mm-dd', 
                    changeMonth: true,
                    changeYear:true,
                     yearRange: "-39:-23" // this is the option you're looking for
  
                });

                // ADD MORE ASSOCIATIONS 
    var max_fields_associations      = 5; //maximum input boxes allowed
    var wrapper_associations         = $(".input_associations_wrap"); //Fields wrapper
    var add_button_associations      = $(".add_associations_button"); //Add button ID
    

    var x_associations = $("#total_associations_filds").val(); //initlal text box count
    $(wrapper_associations).on("click",".add_associations_button", function(e){


     //on add input button click
        e.preventDefault();
        if(x_associations < max_fields_associations){ //max input box allowed
            x_associations++; //text box increment
            $(wrapper_associations).append('<div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important"><div class="col-sm-4" style="padding-left: 0px!important">          <input  maxlength="100" type="text" name="association[]" value="" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>          </div>          <div class="col-sm-4">          <select name="availability[]" class="form-control">          <option value="0">Select A day</option>          <option value="Saturday">Saturday</option>          <option value="Sunday">Sunday</option>          <option value="Monday">Monday</option>          <option value="Tuesday">Tuesday</option>           <option value="Wedensday">Wedensday</option>           <option value="Thursday">Thursday</option>          <option value="Friday">Friday</option>           </select>          </div>          <div class="col-sm-4"><button class="remove_associations_field btn btn-primary"  data-id="" >-</button>          <button class="add_associations_button btn btn-primary">+</button>          </div>          <div class="col-sm-12">          <div class="col-sm-4">          <label class="control-label">Time From :</label><div class="input-group date datetimepicker3" id="datetimepicker3">                  <input type="text" class="form-control" id="from_time" name="from_time[]" />                    <span class="input-group-addon">                        <span class="glyphicon glyphicon-time"></span>                    </span>                </div> </div>          <div class="col-sm-4">          <label class="control-label">Time To:</label><div class="input-group date datetimepicker3" id="datetimepicker3">                    <input type="text" class="form-control"  id="to_time" name="to_time[]" />                    <span class="input-group-addon">                        <span class="glyphicon glyphicon-time"></span>                    </span>                </div></div><div class="col-sm-2"></div></div> </div>'); //add input box

             $('.datetimepicker3').datetimepicker({
                    format: 'LT'
                });
        }
    });
    
    $(wrapper_associations).on("click",".remove_associations_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x_associations--;
    });// END ADD MORE ASSOCIATIONS



     // ADD SOCIAL ICONS FIELDS
    var max_fields      = 5; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var x = $("#total_filds").val(); //initlal text box count

    $(wrapper).on("click",".add_field_button", function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important"><div class="col-sm-6" style="padding-left: 0px!important"><input  maxlength="200" type="text" name="sociallinks[]" value="" class="form-control" placeholder="Enter dr association" /><input name="link_id[]" id="link_id" type="hidden" value=""></input></div><div class="col-sm-2"><input  maxlength="5" type="text" name="sort_order[]" value="" class="form-control" placeholder="Sort Order"  /></div><div class="col-sm-4"><button class="remove_field btn btn-primary"  data-id="" >-</button>&nbsp;<button class="add_field_button btn btn-primary">+</button></div><div class="col-sm-12"  style="padding-top: 10px; padding-bottom: 10px"><input id="socialicon" name="socialicon[]" type="file" multiple class="file-loading"></div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); 
        var data_id = $(this).data("id");
        if(data_id != ''){ 
            $.ajax({
                  url: '<?php echo url('profiles/removesocials');?>'+ '/' + data_id,
                  cache: false,
                  type: "get",
                  data: '',
                  beforeSend: function()
                  {
                   // $('#modal-body').html('loading...');
                  },
                  success: function(data){
                  }
                });

        }
        $(this).parent('div').parent('div').remove();
        x--;
    }) 
  //END SOCIAL ICONS FIELDS
  
            });
        </script>
 @stop
@section('content')
<style type="text/css"> 
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Update 
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><p> Update Dr.<?php echo $doctor->first_name.' '.$doctor->last_name; ?> </p>
</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
    <!--Comment the create or update profile upto here-->



    <div class="padder">
      <!-- form for profile builder starts -->
      <div class="container">


<!--addordinalnumber function-->
 
<!--{{ url('profile') }}-->
  <form role="form" action="<?php echo url(); ?>/profiles/updateDoctorInfo" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />

    <input type="hidden" name="user_id" value="{{ $doctor->id }}" />
    <input type="hidden" name="profile_id" value="{{ $doctor_profile->id }}" />
    <input type="hidden" name="parent_id" value="{{ $doctor_profile->parent_id }}" />

    <div class="row" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Dr.<?php echo $doctor->first_name.' '.$doctor->last_name; ?> Personal Info</h3>

          <div class="form-group">
            <label class="control-label">Practice Id </label>
            <input  maxlength="100" type="text" name="Practice_id" value="<?php echo $doctor_profile->Practice_id; ?>" required="required" class="form-control" placeholder="Enter practice Id"  />
          </div>

          <div class="form-group">
            <label class="control-label">First Name</label>
            <input  maxlength="100" type="text" name="first_name" value="<?php echo $doctor->first_name; ?>" required="required" class="form-control" placeholder="Enter first name"  />
          </div>
          <div class="form-group">
            <label class="control-label">Last Name</label>
            <input  maxlength="100" type="text" name="last_name" value="<?php echo $doctor->last_name; ?>" required="required" class="form-control" placeholder="Enter last name"  />
          </div>


             <div class="form-group {{ $errors->first('gender', ' has-error') }}">
            <label class="control-label">Select Gender </label>
            <select name="gender"  class="form-control m-b" id="gender" >
            <option value="Male" @if($doctor_profile->gender == 'Male') selected="" @endif>Male</option>
            <option value="Female" @if($doctor_profile->gender == 'Female') selected="" @endif>Female</option>
            <option value="Other" @if($doctor_profile->gender == 'Other') selected="" @endif>Other</option>
           </select>
           <span style="color:#DD4B39" class="help-block">{{{ $errors->first('gender', ':message') }}}</span>
          </div>

         <div class="form-group {{ $errors->first('user_dob', ' has-error') }} ">
            <label class="control-label">Date Of Birth</label>
            <input  maxlength="100" type="text" name="user_dob" id="user_dob" value="{{$doctor_profile->user_dob}}"   class="form-control" placeholder="Enter date"  />
            <span style="color:#DD4B39" class="help-block">{{{ $errors->first('user_dob', ':message') }}}</span>
          </div>

<!--catagory and sub catagory start-->
          <div class="form-group">
            <label class="control-label">Catagory </label>
            <select name="catagory_id"  class="form-control m-b" id="catagory_id">
              <option>Select catagory</option>


              <?php foreach($catagory as $main_cat) {  
                ?>

              <option value="<?php echo $main_cat->id; ?>" @if (in_array($main_cat->id, @$specialities_combine))  selected="selected" @endif><?php echo $main_cat->name; ?></option>
              <?php } ?>
           </select>
          </div>
          <div class="form-group" id="form_group_1">
        
            <label class="control-label">Sub catagory </label>
            <select name="sub_catagory_1" class="form-control m-b sub_catagory" id="sub_catagory_1" data-id="1">
              <option value="-1">Select sub catagory</option>

              <?php
              $stoping_condition = false; 
               foreach($catagories as $sub_cate){ ?> 

              <option value="<?php echo $sub_cate->id; ?>" @if($stoping_condition === false) 
              @if (count($specialities_combine) > 0 && in_array($sub_cate->id, $specialities_combine))  selected="selected" <?php $specialities_combine = array_diff($specialities_combine, array($sub_cate->id)); $stoping_condition  = true; if(count($procedures_array) > 0) { $vars1 = @$procedures_array[$sub_cate->id]; unset($procedures_array[$sub_cate->id]);}
              ?> @endif @endif> <?php echo $sub_cate->name; ?></option>
              <?php } ?> 

           </select> 
           <div class="form-group" id="nested"><label>Enter Procedures</label>                  <input type="text" class="form-control" id="procedures_1" name="procedures_1" value="<?php if(isset($vars1)) echo implode(',', $vars1)?>" placeholder="Type something and hit enter" />   </div>

         </div> 
           <div class="form-group" id="form_group_2">
            <label class="control-label">Sub catagory </label>
            <select name="sub_catagory_2" class="form-control m-b sub_catagory" id="sub_catagory_2" data-id="2">
              <option value="-1">Select Sub Catagory</option>

              <?php foreach($catagories as $sub_cate){ ?>

              <option value="<?php echo $sub_cate->id; ?>" @if (in_array($sub_cate->id, $specialities_combine)) <?php @$vars2 = @$procedures_array[$sub_cate->id];?>  selected="selected" @endif> <?php echo $sub_cate->name; ?></option>
              <?php 

              } ?> 
</select>
           <div class="form-group" id="nested"><label>Enter Procedures</label>                  <input type="text" class="form-control" id="procedures_2" name="procedures_2" value="<?php if(isset($vars2)) echo implode(',', $vars2)?>" placeholder="Type something and hit enter" />   </div>
         </div>  

          <div class="form-group">
            <label class="control-label">Qualification </label>
            <input  maxlength="100" type="text" name="education" value="<?php echo $doctor_profile->education; ?>" required="required" class="form-control" placeholder="Enter education"  />
          </div>

             

          <div class="form-group">
            <label class="control-label">Affiliation </label>
            <input  maxlength="100" type="text" name="affiliation" value="<?php echo $doctor_profile->affiliation; ?>" required="required" class="form-control" placeholder="Enter Affiliation "  />
          </div>

          <div class="form-group">
            <label class="control-label">Contact Number </label>
            <input  maxlength="100" type="text" name="contact_dr_phone_number" value="<?php echo $doctor_profile->contact_dr_phone_number; ?>" required="required" class="form-control" placeholder="Enter dr contact number"  />
          </div>

          <div class="form-group">
            <label class="control-label">Email </label>
            <input  maxlength="100" type="text" name="email" value="<?php echo $doctor->email; ?>" required="required" class="form-control" placeholder="Enter dr email"  />
          </div>


  <datalist id="datalist1">
             @foreach($association_names as $association_name)
            <option value="{{$association_name->name}}">{{$association_name->name}} </option>
             @endforeach
          </datalist>

        <div class="form-group input_associations_wrap"> 

          <div class="col-xs-12" style="padding-left: 0px!important">
          <label class="control-label">Update Secondary Associations </label></div>          
            @if(count($associations)<=0)
            <input type="hidden" id="total_associations_filds" value="1"></input>
          <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">

          <div class="col-sm-4" style="padding-left: 0px!important">
          <input  maxlength="100" type="text" name="association[]" value="" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>
          </div>
          <div class="col-sm-4">
          <select name="availability[]" class="form-control">
          <option value="0">Select A day</option>
          <option value="Saturday">Saturday</option> 
          <option value="Sunday">Sunday</option> 
          <option value="Monday">Monday</option> 
          <option value="Tuesday">Tuesday</option> 
          <option value="Wedensday">Wedensday</option> 
          <option value="Thursday">Thursday</option> 
          <option value="Friday">Friday</option> 
          </select> 
          </div>
          <div class="col-sm-4"> 
          <button class="add_associations_button btn btn-primary">+</button>
          </div>
          <div class="col-sm-12">
          <div class="col-sm-4">
          <label class="control-label">Time From:</label> 
            <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="from_time" name="from_time[]"  value="{{@$showAssociation->from_time}}"  />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 

                 </div>
          <div class="col-sm-4">
          <label class="control-label">Time To:</label>

              <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="to_time" name="to_time[]"  value="{{@$showAssociation->to_time}}"  />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>  
</div><div class="col-sm-2"></div></div></div>
          @else
      <?php  $counter = 1; ?>
       @foreach(@$associations as $showAssociation)
      
       @if($counter == 1)

        <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">

          <div class="col-sm-4" style="padding-left: 0px!important">
          <input  maxlength="100" type="text" name="association[]" value="{{$showAssociation->name}}" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>
           <input  maxlength="100" type="hidden" name="association_id[]" value="{{$showAssociation->associations_id}}"/>
          </div>
          <div class="col-sm-4">
          <select name="availability[]" class="form-control">
          <option value="0">Select A day</option>
          <option value="Saturday" @if($showAssociation->availability == 'Saturday') selected="" @endif >Saturday</option> 
          <option value="Sunday" @if($showAssociation->availability == 'Sunday') selected="" @endif >Sunday</option> 
          <option value="Monday" @if($showAssociation->availability == 'Monday') selected="" @endif >Monday</option> 
          <option value="Tuesday" @if($showAssociation->availability == 'Tuesday') selected="" @endif >Tuesday</option> 
          <option value="Wedensday" @if($showAssociation->availability == 'Wedensday') selected="" @endif >Wedensday</option> 
          <option value="Thursday" @if($showAssociation->availability == 'Thursday') selected="" @endif >Thursday</option> 
          <option value="Friday" @if($showAssociation->availability == 'Friday') selected="" @endif >Friday</option> 
          </select> 
          </div>
          <div class="col-sm-4"> <button class="remove_associations_field btn btn-primary"  data-id="{{$showAssociation->associations_id}}" >-</button>
          <button class="add_associations_button btn btn-primary">+</button>
          </div>
          <div class="col-sm-12">
          <div class="col-sm-4">
          <label class="control-label">Time From:</label> 

          <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="from_time" name="from_time[]" value="{{$showAssociation->from_time}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 



          </div>
          <div class="col-sm-4">
          <label class="control-label">Time To:</label>  

   <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="to_time" name="to_time[]" value="{{$showAssociation->to_time}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 


</div><div class="col-sm-2"></div></div></div>


       @else         

        <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">

          <div class="col-sm-4" style="padding-left: 0px!important">
          <input  maxlength="100" type="text" name="association[]" value="{{$showAssociation->name}}" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>
          <input  maxlength="100" type="hidden" name="association_id[]" value="{{$showAssociation->associations_id}}"/>
          </div>
          <div class="col-sm-4">
          <select name="availability[]" class="form-control">
           <option value="Saturday" @if($showAssociation->availability == 'Saturday') selected="" @endif >Saturday</option> 
          <option value="Sunday" @if($showAssociation->availability == 'Sunday') selected="" @endif >Sunday</option> 
          <option value="Monday" @if($showAssociation->availability == 'Monday') selected="" @endif >Monday</option> 
          <option value="Tuesday" @if($showAssociation->availability == 'Tuesday') selected="" @endif >Tuesday</option> 
          <option value="Wedensday" @if($showAssociation->availability == 'Wedensday') selected="" @endif >Wedensday</option> 
          <option value="Thursday" @if($showAssociation->availability == 'Thursday') selected="" @endif >Thursday</option> 
          <option value="Friday" @if($showAssociation->availability == 'Friday') selected="" @endif >Friday</option> 
          </select> 
          </div>
          <div class="col-sm-4"> <button class="remove_associations_field btn btn-primary"  data-id="{{$showAssociation->associations_id}}" >-</button>
          <button class="add_associations_button btn btn-primary">+</button>
          </div>
          <div class="col-sm-12">
          <div class="col-sm-4">
          <label class="control-label">Time From:</label> 
           <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="from_time" name="from_time[]"  value="{{$showAssociation->from_time}}"  />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>  

          </div>
          <div class="col-sm-4">
          <label class="control-label">Time To:</label> 
           <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="to_time" name="to_time[]"  value="{{$showAssociation->to_time}}"  />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 

</div><div class="col-sm-2"></div></div></div>
            @endif
            <?php $counter++;?>
            @endforeach
            <input type="hidden" id="total_associations_filds" value="{{$counter-1}}"></input>
@endif
</div>
        
<div class="form-group">
 <div class="col-xs-12">
 </div>   
</div>

 <div class="form-group input_fields_wrap">   
    <div class="col-xs-12" style="padding-left: 0px!important">
          <label class="control-label">Update Social Links </label></div>        
            @if(count($collection['checkSocialLinks'])<=0) 
            
             <input type="hidden" id="total_filds" value="1"></input>
             <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">
            <div class="col-sm-6" style="padding-left: 0px!important">
            <input  maxlength="200" type="text" name="sociallinks[]" value="" class="form-control" placeholder="Enter dr association"  />
            <input name="link_id[]" id="link_id" type="hidden" value=""></input>
            </div>

            <div class="col-sm-2">
           <input  maxlength="10" type="text" name="sort_order[]" value="" class="form-control" placeholder="Sort Order"  />
            </div>
            <div class="col-sm-4"><button class="remove_field btn btn-primary"  data-id="" >-</button>
            <button class="add_field_button btn btn-primary">+</button>
            </div>

             <div class="col-sm-12"  style="padding-top: 10px; padding-bottom: 10px">
            <input id="socialicon" name="socialicon[]" type="file"   class="file-loading">
            </div>
            </div>

       @else
      <?php  $counter = 1;?>
       @foreach($collection['checkSocialLinks'] as $socials)
      
       @if($counter == 1)
       <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">
        <div class="col-sm-6" style="padding-left: 0px!important">
            <input  maxlength="200" type="text" name="sociallinks[]" value="{{$socials->link}}" class="form-control" placeholder="Enter dr association" />
            <input name="link_id[]" id="link_id" type="hidden" value="{{$socials->id}}"></input>
            </div>

            <div class="col-sm-2">
           <input  maxlength="10" type="text" name="sort_order[]" value="{{$socials->sort_order}}" class="form-control" placeholder="Sort Order"  />
            </div>
            <div class="col-sm-4"><button class="remove_field btn btn-primary"  data-id="{{$socials->id}}" >-</button>
            <button class="add_field_button btn btn-primary" data-id="{{$socials->id}}">+</button>
            </div>

             <div class="col-sm-12">
            <input id="socialicon" name="socialicon[]" type="file" multiple class="file-loading">
            
    <img src="<?php echo url(); ?>/images/socials/<?php echo @$socials->icon; ?>" alt="doctor image" height="60" width="60">
            </div>
</div>
       @else
            <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">
            <div class="col-sm-6" style="padding-left: 0px!important"><input  maxlength="200" type="text" name="sociallinks[]" value="{{$socials->link}}" class="form-control" placeholder="Enter dr association" /><input name="link_id[]" id="link_id" type="hidden" value="{{$socials->id}}"></input>
            </div>

            <div class="col-sm-2"><input  maxlength="5" type="text" name="sort_order[]" value="{{$socials->sort_order}}" class="form-control" placeholder="Sort Order"  /></div>

            <div class="col-sm-4"><button class="remove_field btn btn-primary"  data-id="{{$socials->id}}" >-</button> <button class="add_field_button btn btn-primary" data-id="{{$socials->id}}">+</button></div>
            
            <div class="col-sm-12"><input id="socialicon" name="socialicon[]" type="file" multiple class="file-loading">
<img src="<?php echo url(); ?>/images/socials/<?php echo @$socials->icon; ?>" alt="doctor image" height="60" width="60">
            </div>
            </div>
            @endif
            <?php $counter++;?>
            @endforeach
            <input type="hidden" id="total_filds" value="{{$counter-1}}"></input>
@endif   
          </div>



          <div class="form-group">
            <label class="control-label">Doctor Image </label>
            <input ui-jq="filestyle" type="file" name="image_of_doctor" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
          </div>
          <div class="form-group">
            <img src="<?php echo url(); ?>/images/catalog/<?php echo $doctor_profile->image_of_doctor; ?>" alt="doctor image" height="60" width="60">
          </div>

          <hr>



            <button class="btn btn-success btn-lg pull-right" type="submit">Update</button>
        </div>
      </div>
    </div>

  </form>


            </div><!-- /.box-body -->
            <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 
@endsection