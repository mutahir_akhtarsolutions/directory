@extends('layout.doctorapp')
@section('content')
 <!-- Modal Dialog  FOR VIEW --> 
<div class="modal fade" id="showdetails" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Your Details</h4>
      </div>
      <div class="modal-body">  
        <p id="modal-body">Be patient contents are loading.</p>
      </div>
    </div>
  </div>
</div>

<!-- Modal Dialog  FOR EDIT COMMENTS --> 
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">

     <form action="/visitors/update" method="post" accept-charset="utf-8">
     <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit Your Details</h4>
      </div>
      <div class="modal-body loader_div" >
      </div>
          <div class="modal-body modal_body" >
            <input class="form-control" id="id" name="id" type="hidden" placeholder="" value="">
             <input class="form-control" id="rating" name="rating" type="hidden" placeholder="" value="">
               <input class="form-control" id="approved" name="approved" type="hidden" placeholder="" value="">

        <input class="form-control" id="doctor_id" name="doctor_id" type="hidden" placeholder="" value="">
        <input class="form-control" id="user_id" name="user_id" type="hidden" placeholder="" value=""> 
        <div class="form-group">
        <textarea rows="2" class="form-control" placeholder="Comments" id="comments" name="comment"></textarea></div></div>

          <div class="modal-footer ">
        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
      </div>

    </form>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>All Trasfer History<small>Managment</small> </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">  
            

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">You Have Total {{count($collection['allhistory'])}} Trasfers</h3> </div>
               
                

             @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>

                    <th>Sr.No</th>
                    <th>From</th>
                    <th>To</th>  
                    <th>Date Recorded</th>  
                  </tr>
                </thead>
                <tbody>                
                   <?php $counter = 1;?>
                    @foreach($collection['allhistory'] as $values)    
                    <?php 
                    $createdAt = Carbon\Carbon::parse($values->trasferDate);

                    ?>             
                  <tr>
                  <td>{{$counter}}</td>
                  <td>{{$values->oldParentBusiness}}</td>
                   <td>{{$values->newParentBusiness}}</td> 
                   <td>{{$createdAt->format('M d Y')}}</td>                    
                  </tr>
                  <?php $counter ++;?>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Sr.No</th>
                    <th>From</th>
                    <th>To</th>  
                    <th>Date Recorded</th>  
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
          <!-- /.box --> 
        </div>
        <!-- /.col --> 
      </div>
      <!-- /.row --> 
    </section>
    <!-- /.content --> 
  </div>
@endsection

