@include('header')
@include('headerListingDetail')
<!-- banner end here -->
<?php  //echo $_SERVER['HTTP_REFERER']; ?>

        <style>
            /****** Rating Starts *****/
            @import url(http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css); 

            .rating { 
                border: none;
                float: left;
            }

            .rating > input { display: none; } 
            .rating > label:before { 
                margin: 5px;
                font-size: 1.25em;
                font-family: FontAwesome;
                display: inline-block;
                content: "\f005";
            }

            .rating > .half:before { 
                content: "\f089";
                position: absolute;
            }

            .rating > label { 
                color: #ddd; 
                float: right; 
            }

            .rating > input:checked ~ label, 
            .rating:not(:checked) > label:hover,  
            .rating:not(:checked) > label:hover ~ label { color: #FFD700;  }

            .rating > input:checked + label:hover, 
            .rating > input:checked ~ label:hover,
            .rating > label:hover ~ input:checked ~ label, 
            .rating > input:checked ~ label:hover ~ label { color: #FFED85;  }     


            /* Downloaded from http://devzone.co.in/ */
        </style>
<!-- Doctor Detail Profile start here -->
<div class="dr-det-prof">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-12 prof-img">
        <figure> <img src="{{ url() }}/images/catalog/{{$collection['doctor'][0]->image_of_doctor}}" alt="detail dr image" class="img-responsive"> </figure>
      </div>
      <div class="col-md-9 col-sm-9 col-xs-12 prof-disc">
        <h1>{{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} , {{$collection['doctor'][0]->education}} </h1>
        <span class="abt-dr"> Specializes in : <br/>
@foreach($collection['users_procedures'] as $users_procedures)
     {{$users_procedures->procedure_name}},   
@endforeach
          <br/>
        <ul>
          <li>Male</li>
          <li>Age 40</li>
        </ul>
        </span>
        <div class="dr-det-left">
          <p>Patient Satisfaction</p>
         <!-- <ul class="pt-satisf">
            <li><a href="#" class="fa fa-star"></a></li>
            <li><a href="#" class="fa fa-star"></a></li>
            <li><a href="#" class="fa fa-star"></a></li>
            <li><a href="#" class="fa fa-star-half-o"></a></li>
            <li><a href="#" class="fa fa-star-o"></a></li>
          </ul>-->
          <input name="user_id" id="user_id" value="{{$collection['doctor'][0]->user_id}}" type="hidden"> </input>
           <fieldset id='demo3' class="rating">
                        <input class="stars" type="radio" id="star53" name="rating" value="5" />
                        <label class = "full" for="star53" title="Awesome - 5 stars"></label>
                        <input class="stars" type="radio" id="star4half3" name="rating" value="4.5" />

                        <label class="half" for="star4half3" title="Pretty good - 4.5 stars"></label>
                        <input class="stars" type="radio" id="star43" name="rating" value="4" />
                        <label class = "full" for="star43" title="Pretty good - 4 stars"></label>
                        <input class="stars" type="radio" id="star3half3" name="rating" value="3.5" />
                        <label class="half" for="star3half3" title="Meh - 3.5 stars"></label>
                        <input class="stars" type="radio" id="star33" name="rating" value="3" />
                        <label class = "full" for="star33" title="Meh - 3 stars"></label>
                        <input class="stars" type="radio" checked="checked" id="star2half3" name="rating" value="2.5" />
                        <label class="half" for="star2half3" title="Kinda bad - 2.5 stars"></label>
                        <input class="stars" type="radio" id="star23" name="rating" value="2" />
                        <label class = "full" for="star23" title="Kinda bad - 2 stars"></label>
                        <input class="stars" type="radio" id="star1half3" name="rating" value="1.5" />
                        <label class="half" for="star1half3" title="Meh - 1.5 stars"></label>
                        <input class="stars" type="radio" id="star13" name="rating" value="1" />
                        <label class = "full" for="star13" title="Sucks big time - 1 star"></label>
                        <input class="stars" type="radio"  id="starhalf3" name="rating" value="0.5" />
                        <label class="half" for="starhalf3" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>

          <span class="link-sp"><a href="#" class="dr-links">13 responses</a></span> <span class="link-sp"><a href="#" class="dr-links">Take a survey</a></span> </div>
        <div class="dr-det-right"> <span class="link-sp"><a href="#" class="dr-links">Orlin & Cohen Orthopedic Group</a></span>
          <p>{{$collection['business_profile']->business_address}}<br>
            {{$collection['business_profile']->business_address2}}</p>
          <span class="link-sp"><a href="#" class="dr-links">Phone Number & Directions</a></span> </div>
        <div class="btn-links"> <a href="#" class="btn">NEW PATIENT</a> <a href="#" class="btn">Existing PATIENT</a> <a href="#" class="btn">Other Enquiry</a> </div>
      </div>
    </div>
  </div>
</div>
<!-- Doctor Detail Profile End here -->

<!-- welcome nationwide start here -->
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 hidden-xs tab-links">
      <ul class="nav nav-tabs  dr-tabs">
        <li class="active"><a href="#tab01" data-toggle="tab"><em class="fa fa-cog"></em> Overview</a></li>
        <li class=""><a href="#tab02" data-toggle="tab"><em class="fa fa-map-marker"></em> Phone& Address</a></li>
        <li class=""><a href="#tab03" data-toggle="tab"><em class="fa fa-pencil-square-o"></em> Experience</a></li>
        <li class=""><a href="#tab04" data-toggle="tab"><em class="fa fa-h-square"></em> Hospital Quality</a></li>
        <li class=""><a href="#tab05" data-toggle="tab"><em class="fa fa-comments-o"></em> Patient Satisfaction</a></li>
      </ul>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12 left-content">



      <div class="tabbable hidden-xs">
        <div class="tab-content  dr-tab-cont">
          <div class="tab-pane active" id="tab01">
            <div class="row">
              <div class="col-xs-12 dr-title"> <span>Key things you need to know about</span>
                <h2>{{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</h2>
              </div>
              <div class="col-xs-12 dr-det-sect phone-address">
                <div class="icon-colm"> <em class="fa fa-map-marker"></em>
                  <div class="det-title">
                    <h3>Phone & Address</h3>
                    <span>Check insurance plans, locations and make an appointment</span> </div>
                </div>
                <div class="dr-det-btm phone-address-bg">
                  <address>
                  <p>{{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</p>
                  <p>Winthrop Surgical Associates</p>
                  <p>Winthrop Surgical Associates</p>
                  <p>{{$collection['business_profile']->business_address}}</p>
                  <p>{{$collection['business_profile']->business_address2}}</p>
                  <p>{{$collection['doctor'][0]->contact_dr_phone_number}}</p>
                  <a href="#" class="direct-link">Directions <em class="fa fa-angle-right"></em></a>
                  </address>
                  <div  class="gmap" data-zoom="14" data-markers="Little Lonsdale St, Melbourne, Victoria 8011 Australia; 2 Elizabeth St, Melbourne, Victoria 3000 Australia">
                  </div>


                  <div class="btn-links"> <a href="#" class="btn">NEW PATIENT</a> <a href="#" class="btn">Existing PATIENT</a> <a href="#" class="btn">Other Enquiry</a> </div>

                </div>
              </div>
              <div class="col-xs-12 dr-det-sect experience-colm">
                <div class="icon-colm"> <em class="fa fa-pencil-square-o"></em>
                  <div class="det-title">
                    <h3>experience</h3>
                    <span>Research training, expertise and qualifications</span> </div>
                </div>
                <div class="dr-det-btm">
                  <div class="dr-exp">
                  <div class="exp-links">
                    <h5>Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}'s Specialties</h5>
                    <ul>
                      <li><a href="#">General Surgery</a></li>
                    </ul>
                    <a href="#" class="view-all">View 1 specialty and 1 board certification</a>
                    </div>

                    <div class="exp-links">
                    <h5>Procedures Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} Performs</h5>
                    <ul>
                    @foreach($collection['users_procedures'] as $users_procedures)
                  <li><a href="#">  {{$users_procedures->procedure_name}} </a></li>  
                     @endforeach
                     
                    </ul>
                    <a href="#" class="view-all">See all 9 procedures</a>
                    </div>
                    <div class="exp-links">
                    <h5>Conditions Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} Treats</h5>
                    <ul>
                      <li><a href="#">Biopsy of Breast</a></li>
                      <li><a href="#">Breast Cancer Treatment</a></li>
                    </ul>
                    <a href="#" class="view-all">See all 14 procedures</a>
                    </div>

                    <div class="exp-links">
                    <h5>More About Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}'s Background </h5>
                    <ul>
                      <li><a href="#">Sanctions</a></li>
                      <li><a href="#">Malpractice</a></li>
                      <li><a href="#">Board Actions</a></li>
                      <li><a href="#">Education & Training</a></li>
                      <li><a href="#">Awards & Recognitions</a></li>
                      <li><a href="#">Languages Spoken</a></li>
                    </ul>
                  </div>
                  </div>
                  <div  class="quote-colm"> <em class="fa fa-quote-left"></em>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 dr-det-sect hospital-quality">
                <div class="icon-colm"> <em class="fa fa-h-square"></em>
                  <div class="det-title">
                    <h3>Hospital Quality</h3>
                    <span>The right hospital is as important as the right doctor</span> </div>
                </div>
                <div class="dr-det-btm hospital-quality-bg">
                  <div class="hospital-left">
                    <h4>Winthrop-University Hospital</h4>
                    <p>259 First Street <br>
                      Mineola, NY 11501</p>
                    <ul>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-1.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-2.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-3.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-4.jpg" alt="img" class="img-responsive"></li>
                    </ul>
                  </div>
                  <div class="hospital-right">
                  <ul>
                  <li><img src="{{ url() }}/front_end/assets/img/hospital-img-1.jpg" alt="img" class="img-responsive"></li>
                  <li><img src="{{ url() }}/front_end/assets/img/hospital-img-2.jpg" alt="img" class="img-responsive"> </li>
                  </ul>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 dr-det-sect patience-setisfiction">
                <div class="icon-colm"> <em class="fa fa-comments-o"></em>
                  <div class="det-title">
                    <h3>Patient Satisfaction</h3>
                    <span>Patients' feedback on their experience with Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</span> </div>
                </div>
                <div class="dr-det-btm patience-setisf-bg">
                  <div class="reviews-disc">
                    <p>Likelihood of recommending Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} to family and friends is 4.9 out of 5</p>
                  </div>
                  <div class="reviews-star">
                    <ul class="pt-satisf">
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star-half-o"></a></li>
                      <li><a href="#" class="fa fa-star-o"></a></li>
                    </ul>
                  </div>
                  <div class="survey-results">
                    <h4>See More Survey Results</h4>
                    <ul>
                      <li><a href="#">Level of Trust</a></li>
                      <li><a href="#">Helps Patients Understand Their Condition</a></li>
                      <li><a href="#">Listens and Answers Questions</a></li>
                      <li><a href="#">Time Spent with Patient</a></li>
                      <li><a href="#">Scheduling Appointments</a></li>
                    </ul>
                  </div>
                  <div class="take-survey">
                    <p>26 responses</p>
                    <a href="#">Take a survey</a> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tab02">
          <div class="row">
          <div class="col-xs-12 dr-det-sect phone-address">
                <div class="icon-colm"> <em class="fa fa-map-marker"></em>
                  <div class="det-title">
                    <h3>Phone & Address</h3>
                    <span>Check insurance plans, locations and make an appointment</span> </div>
                </div>
                <div class="dr-det-btm phone-address-bg">
                  <address>
                  <p>{{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</p>
                  <p>Winthrop Surgical Associates</p>
                  <p>Winthrop Surgical Associates</p>
                  <p>{{$collection['business_profile']->business_address}}</p>
                  <p>{{$collection['business_profile']->business_address2}}</p>
                  <p>{{$collection['doctor'][0]->contact_dr_phone_number}}</p>
                  <a href="#" class="direct-link">Directions <em class="fa fa-angle-right"></em></a>
                  </address>
                  <div  class="gmap" data-zoom="14" data-markers="Little Lonsdale St, Melbourne, Victoria 8011 Australia; 2 Elizabeth St, Melbourne, Victoria 3000 Australia">
                  </div>


                  <div class="btn-links"> <a href="#" class="btn">NEW PATIENT</a> <a href="#" class="btn">Existing PATIENT</a> <a href="#" class="btn">Other Enquiry</a> </div>

                </div>
              </div>
          </div>
          </div>
          <div class="tab-pane" id="tab03">
          <div class="row">
          <div class="col-xs-12 dr-det-sect experience-colm">
                <div class="icon-colm"> <em class="fa fa-pencil-square-o"></em>
                  <div class="det-title">
                    <h3>experience</h3>
                    <span>Research training, expertise and qualifications</span> </div>
                </div>
                <div class="dr-det-btm">
                  <div class="dr-exp">
                  <div class="exp-links">
                    <h5>Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}'s Specialties</h5>
                    <ul>
                      <li><a href="#">General Surgery</a></li>
                    </ul>
                    <a href="#" class="view-all">View 1 specialty and 1 board certification</a>
                    </div>

                    <div class="exp-links">
                    <h5>Procedures Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} Performs</h5>
                    <ul>
                       @foreach($collection['users_procedures'] as $users_procedures)
                  <li><a href="#">  {{$users_procedures->procedure_name}} </a></li>  
                     @endforeach
                   
                    </ul>
                    <a href="#" class="view-all">See all 9 procedures</a>
                    </div>
                    <div class="exp-links">
                    <h5>Conditions Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} Treats</h5>
                    <ul>
                      <li><a href="#">Biopsy of Breast</a></li>
                      <li><a href="#">Breast Cancer Treatment</a></li>
                    </ul>
                    <a href="#" class="view-all">See all 14 procedures</a>
                    </div>

                    <div class="exp-links">
                    <h5>More About Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}'s Background </h5>
                    <ul>
                      <li><a href="#">Sanctions</a></li>
                      <li><a href="#">Malpractice</a></li>
                      <li><a href="#">Board Actions</a></li>
                      <li><a href="#">Education & Training</a></li>
                      <li><a href="#">Awards & Recognitions</a></li>
                      <li><a href="#">Languages Spoken</a></li>
                    </ul>
                  </div>
                  </div>
                  <div  class="quote-colm"> <em class="fa fa-quote-left"></em>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
                  </div>
                </div>
              </div>
          </div>
          </div>
          <div class="tab-pane" id="tab04">
          <div class="row">
          <div class="col-xs-12 dr-det-sect hospital-quality">
                <div class="icon-colm"> <em class="fa fa-h-square"></em>
                  <div class="det-title">
                    <h3>Hospital Quality</h3>
                    <span>The right hospital is as important as the right doctor</span> </div>
                </div>
                <div class="dr-det-btm hospital-quality-bg">
                  <div class="hospital-left">
                    <h4>Winthrop-University Hospital</h4>
                    <p>259 First Street <br>
                      Mineola, NY 11501</p>
                    <ul>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-1.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-2.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-3.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-4.jpg" alt="img" class="img-responsive"></li>
                    </ul>
                  </div>
                  <div class="hospital-right">
                  <ul>
                  <li><img src="{{ url() }}/front_end/assets/img/hospital-img-1.jpg" alt="img" class="img-responsive"></li>
                  <li><img src="{{ url() }}/front_end/assets/img/hospital-img-2.jpg" alt="img" class="img-responsive"> </li>
                  </ul>
                  </div>
                </div>
              </div>
          </div>
          </div>
          <div class="tab-pane" id="tab05">
          <div class="row">
          <div class="col-xs-12 dr-det-sect patience-setisfiction">
                <div class="icon-colm"> <em class="fa fa-comments-o"></em>
                  <div class="det-title">
                    <h3>Patient Satisfaction</h3>
                    <span>Patients' feedback on their experience with Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</span> </div>
                </div>
                <div class="dr-det-btm patience-setisf-bg">
                  <div class="reviews-disc">
                    <p>Likelihood of recommending Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} to family and friends is 4.9 out of 5</p>
                  </div>
                  <div class="reviews-star">
                    <ul class="pt-satisf">
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star-half-o"></a></li>
                      <li><a href="#" class="fa fa-star-o"></a></li>
                    </ul>
                  </div>
                  <div class="survey-results">
                    <h4>See More Survey Results</h4>
                    <ul>
                      <li><a href="#">Level of Trust</a></li>
                      <li><a href="#">Helps Patients Understand Their Condition</a></li>
                      <li><a href="#">Listens and Answers Questions</a></li>
                      <li><a href="#">Time Spent with Patient</a></li>
                      <li><a href="#">Scheduling Appointments</a></li>
                    </ul>
                  </div>
                  <div class="take-survey">
                    <p>26 responses</p>
                    <a href="#">Take a survey</a> </div>
                </div>
              </div>
          </div>
          </div>
        </div>
      </div>

      <div class="accordion visible-xs dr-tab-cont" id="accordion2">
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                  Overview
                </a>
              </div>
              <div id="collapseOne" class="accordion-body  collapse in">
                <div class="accordion-inner">
                <div class="row">
              <div class="col-xs-12 dr-title"> <span>Key things you need to know about</span>
                <h2>{{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</h2>
              </div>
              <div class="col-xs-12 dr-det-sect phone-address">
                <div class="icon-colm"> <em class="fa fa-map-marker"></em>
                  <div class="det-title">
                    <h3>Phone & Address</h3>
                    <span>Check insurance plans, locations and make an appointment</span> </div>
                </div>
                <div class="dr-det-btm phone-address-bg">
                  <address>
                  <p>{{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</p>
                  <p>Winthrop Surgical Associates</p>
                  <p>Winthrop Surgical Associates</p>
                  <p>{{$collection['business_profile']->business_address}}</p>
                  <p>{{$collection['business_profile']->business_address2}}</p>
                  <p>{{$collection['doctor'][0]->contact_dr_phone_number}}</p>
                  <a href="#" class="direct-link">Directions <em class="fa fa-angle-right"></em></a>
                  </address>
                  <div  class="gmap" data-zoom="14" data-markers="Little Lonsdale St, Melbourne, Victoria 8011 Australia; 2 Elizabeth St, Melbourne, Victoria 3000 Australia">
                  </div>


                  <div class="btn-links"> <a href="#" class="btn">NEW PATIENT</a> <a href="#" class="btn">Existing PATIENT</a> <a href="#" class="btn">Other Enquiry</a> </div>

                </div>
              </div>
              <div class="col-xs-12 dr-det-sect experience-colm">
                <div class="icon-colm"> <em class="fa fa-pencil-square-o"></em>
                  <div class="det-title">
                    <h3>experience</h3>
                    <span>Research training, expertise and qualifications</span> </div>
                </div>
                <div class="dr-det-btm">
                  <div class="dr-exp">
                  <div class="exp-links">
                    <h5>Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}'s Specialties</h5>
                    <ul>
                      <li><a href="#">General Surgery</a></li>
                    </ul>
                    <a href="#" class="view-all">View 1 specialty and 1 board certification</a>
                    </div>

                    <div class="exp-links">
                    <h5>Procedures Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} Performs</h5>
                    <ul>
                       @foreach($collection['users_procedures'] as $users_procedures)
                  <li><a href="#">  {{$users_procedures->procedure_name}} </a></li>  
                     @endforeach
                   
                    </ul>
                    <a href="#" class="view-all">See all 9 procedures</a>
                    </div>
                    <div class="exp-links">
                    <h5>Conditions Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} Treats</h5>
                    <ul>
                      <li><a href="#">Biopsy of Breast</a></li>
                      <li><a href="#">Breast Cancer Treatment</a></li>
                    </ul>
                    <a href="#" class="view-all">See all 14 procedures</a>
                    </div>

                    <div class="exp-links">
                    <h5>More About Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}'s Background </h5>
                    <ul>
                      <li><a href="#">Sanctions</a></li>
                      <li><a href="#">Malpractice</a></li>
                      <li><a href="#">Board Actions</a></li>
                      <li><a href="#">Education & Training</a></li>
                      <li><a href="#">Awards & Recognitions</a></li>
                      <li><a href="#">Languages Spoken</a></li>
                    </ul>
                  </div>
                  </div>
                  <div  class="quote-colm"> <em class="fa fa-quote-left"></em>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 dr-det-sect hospital-quality">
                <div class="icon-colm"> <em class="fa fa-h-square"></em>
                  <div class="det-title">
                    <h3>Hospital Quality</h3>
                    <span>The right hospital is as important as the right doctor</span> </div>
                </div>
                <div class="dr-det-btm hospital-quality-bg">
                  <div class="hospital-left">
                    <h4>Winthrop-University Hospital</h4>
                    <p>259 First Street <br>
                      Mineola, NY 11501</p>
                    <ul>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-1.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-2.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-3.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-4.jpg" alt="img" class="img-responsive"></li>
                    </ul>
                  </div>
                  <div class="hospital-right">
                  <ul>
                  <li><img src="{{ url() }}/front_end/assets/img/hospital-img-1.jpg" alt="img" class="img-responsive"></li>
                  <li><img src="{{ url() }}/front_end/assets/img/hospital-img-2.jpg" alt="img" class="img-responsive"> </li>
                  </ul>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 dr-det-sect patience-setisfiction">
                <div class="icon-colm"> <em class="fa fa-comments-o"></em>
                  <div class="det-title">
                    <h3>Patient Satisfaction</h3>
                    <span>Patients' feedback on their experience with Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</span> </div>
                </div>
                <div class="dr-det-btm patience-setisf-bg">
                  <div class="reviews-disc">
                    <p>Likelihood of recommending Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} to family and friends is 4.9 out of 5</p>
                  </div>
                  <div class="reviews-star">
                    <ul class="pt-satisf">
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star-half-o"></a></li>
                      <li><a href="#" class="fa fa-star-o"></a></li>
                    </ul>
                  </div>
                  <div class="survey-results">
                    <h4>See More Survey Results</h4>
                    <ul>
                      <li><a href="#">Level of Trust</a></li>
                      <li><a href="#">Helps Patients Understand Their Condition</a></li>
                      <li><a href="#">Listens and Answers Questions</a></li>
                      <li><a href="#">Time Spent with Patient</a></li>
                      <li><a href="#">Scheduling Appointments</a></li>
                    </ul>
                  </div>
                  <div class="take-survey">
                    <p>26 responses</p>
                    <a href="#">Take a survey</a> </div>
                </div>
              </div>
            </div>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                 Phone& Address
                </a>
              </div>
              <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                <div class="row">
                <div class="col-xs-12 dr-det-sect phone-address">
                <div class="icon-colm"> <em class="fa fa-map-marker"></em>
                  <div class="det-title">
                    <h3>Phone & Address</h3>
                    <span>Check insurance plans, locations and make an appointment</span> </div>
                </div>
                <div class="dr-det-btm phone-address-bg">
                  <address>
                  <p>{{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</p>
                  <p>Winthrop Surgical Associates</p>
                  <p>Winthrop Surgical Associates</p>
                  <p>{{$collection['business_profile']->business_address}}</p>
                  <p>{{$collection['business_profile']->business_address2}}</p>
                  <p>{{$collection['doctor'][0]->contact_dr_phone_number}}</p>
                  <a href="#" class="direct-link">Directions <em class="fa fa-angle-right"></em></a>
                  </address>
                  <div  class="gmap" data-zoom="14" data-markers="Little Lonsdale St, Melbourne, Victoria 8011 Australia; 2 Elizabeth St, Melbourne, Victoria 3000 Australia">
                  </div>


                  <div class="btn-links"> <a href="#" class="btn">NEW PATIENT</a> <a href="#" class="btn">Existing PATIENT</a> <a href="#" class="btn">Other Enquiry</a> </div>

                </div>
              </div>
                </div>
              </div>
            </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                Experience
                </a>
              </div>
              <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner">
                <div class="row">
                 <div class="col-xs-12 dr-det-sect experience-colm">
                <div class="icon-colm"> <em class="fa fa-pencil-square-o"></em>
                  <div class="det-title">
                    <h3>experience</h3>
                    <span>Research training, expertise and qualifications</span> </div>
                </div>
                <div class="dr-det-btm">
                  <div class="dr-exp">
                  <div class="exp-links">
                    <h5>Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}'s Specialties</h5>
                    <ul>
                      <li><a href="#">General Surgery</a></li>
                    </ul>
                    <a href="#" class="view-all">View 1 specialty and 1 board certification</a>
                    </div>

                    <div class="exp-links">
                    <h5>Procedures Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} Performs</h5>
                    <ul>
                       @foreach($collection['users_procedures'] as $users_procedures)
                      <li><a href="#">  {{$users_procedures->procedure_name}} </a></li>  
                     @endforeach
                   
                    </ul>
                    <a href="#" class="view-all">See all 9 procedures</a>
                    </div>
                    <div class="exp-links">
                    <h5>Conditions Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} Treats</h5>
                    <ul>
                      <li><a href="#">Biopsy of Breast</a></li>
                      <li><a href="#">Breast Cancer Treatment</a></li>
                    </ul>
                    <a href="#" class="view-all">See all 14 procedures</a>
                    </div>

                    <div class="exp-links">
                    <h5>More About Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}'s Background </h5>
                    <ul>
                      <li><a href="#">Sanctions</a></li>
                      <li><a href="#">Malpractice</a></li>
                      <li><a href="#">Board Actions</a></li>
                      <li><a href="#">Education & Training</a></li>
                      <li><a href="#">Awards & Recognitions</a></li>
                      <li><a href="#">Languages Spoken</a></li>
                    </ul>
                  </div>
                  </div>
                  <div  class="quote-colm"> <em class="fa fa-quote-left"></em>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
                  </div>
                </div>
              </div>
                </div>
              </div>
            </div>
            </div>

            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                Hospital Quality
                </a>
              </div>
              <div id="collapseFour" class="accordion-body collapse" >
                <div class="accordion-inner">
                <div class="row">
                 <div class="col-xs-12 dr-det-sect hospital-quality">
                <div class="icon-colm"> <em class="fa fa-h-square"></em>
                  <div class="det-title">
                    <h3>Hospital Quality</h3>
                    <span>The right hospital is as important as the right doctor</span> </div>
                </div>
                <div class="dr-det-btm hospital-quality-bg">
                  <div class="hospital-left">
                    <h4>Winthrop-University Hospital</h4>
                    <p>259 First Street <br>
                      Mineola, NY 11501</p>
                    <ul>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-1.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-2.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-3.jpg" alt="img" class="img-responsive"></li>
                      <li><img src="{{ url() }}/front_end/assets/img/hospital-logo-4.jpg" alt="img" class="img-responsive"></li>
                    </ul>
                  </div>
                  <div class="hospital-right">
                  <ul>
                  <li><img src="{{ url() }}/front_end/assets/img/hospital-img-1.jpg" alt="img" class="img-responsive"></li>
                  <li><img src="{{ url() }}/front_end/assets/img/hospital-img-2.jpg" alt="img" class="img-responsive"> </li>
                  </ul>
                  </div>
                </div>
              </div>
                </div>
                </div>
              </div>
            </div>

            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                  Patient Satisfaction
                </a>
              </div>
              <div id="collapseFive" class="accordion-body collapse">
                <div class="accordion-inner">
                 <div class="col-xs-12 dr-det-sect patience-setisfiction">
                <div class="icon-colm"> <em class="fa fa-comments-o"></em>
                  <div class="det-title">
                    <h3>Patient Satisfaction</h3>
                    <span>Patients' feedback on their experience with Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}</span> </div>
                </div>
                <div class="dr-det-btm patience-setisf-bg">
                  <div class="reviews-disc">
                    <p>Likelihood of recommending Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} to family and friends is 4.9 out of 5</p>
                  </div>
                  <div class="reviews-star">
                    <ul class="pt-satisf">
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star-half-o"></a></li>
                      <li><a href="#" class="fa fa-star-o"></a></li>
                    </ul>
                  </div>
                  <div class="survey-results">
                    <h4>See More Survey Results</h4>
                    <ul>
                      <li><a href="#">Level of Trust</a></li>
                      <li><a href="#">Helps Patients Understand Their Condition</a></li>
                      <li><a href="#">Listens and Answers Questions</a></li>
                      <li><a href="#">Time Spent with Patient</a></li>
                      <li><a href="#">Scheduling Appointments</a></li>
                    </ul>
                  </div>
                  <div class="take-survey">
                    <p>26 responses</p>
                    <a href="#">Take a survey</a> </div>
                </div>
              </div>
                </div>
              </div>
            </div>
          </div>
    </div>
    <aside class="col-md-4 col-sm-4 col-xs-12 sidebar">
      <div class="row">
        <div class="col-xs-12 more-specialist-colm">
        <div class="more-spec-bg">
          <h3>More General Surgery Specialists like Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}} at Winthrop Surgical Associates</h3>
          <div class="show-more-provider">
            <p>Showing 5 providers who match:</p>
            <ul>
              <li><a href="#">General Surgery Specialist</a></li>
              <li><a href="#">Near Dr. {{$collection['doctor'][0]->first_name.' '.$collection['doctor'][0]->last_name}}'s Office</a> </li>
            </ul>
           </div>
             <div class="more-sergen-content">
            <?php
                  foreach ($collection['related_doctors'] as $key => $doc) {
             ?>
            <div class="more-sergen-colm">
              <figure> <a href="{{ url() }}/doctorDetail/{{$doc->user_id}}"><img src="{{ url() }}/images/catalog/{{$doc->image_of_doctor}}" alt="image" class="img-responsive"></a> </figure>
              <div class="more-sergen-disc">
                <h5><a href="{{ url() }}/doctorDetail/{{$doc->user_id}}">{{ $doc->first_name.' '.$doc->last_name }}, {{$doc->education}}</a></h5>
                <p>Winthrop Surgical Associates {{$collection['business_profile']->business_address}}</p>
                <ul class="pt-satisf">
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star-half-o"></a></li>
                  <li><a href="#" class="fa fa-star-o"></a></li>
                </ul>
              </div>
            </div>
            <?php
                }
             ?>
            <!--<div class="more-sergen-colm">
              <figure> <a href="#"><img src="{{ url() }}/front_end/assets/img/specialist-img-1.jpg" alt="image" class="img-responsive"></a> </figure>
              <div class="more-sergen-disc">
                <h5><a href="#">Dr. Collin E. Brathwaite, MD</a></h5>
                <p>Winthrop Surgical Associates 120 Mineola Blvd Suite 320Mineola, NY 11501</p>
                <ul class="pt-satisf">
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star-half-o"></a></li>
                  <li><a href="#" class="fa fa-star-o"></a></li>
                </ul>
              </div>
            </div>
            <div class="more-sergen-colm">
              <figure> <a href="#"><img src="{{ url() }}/front_end/assets/img/specialist-img-1.jpg" alt="image" class="img-responsive"></a> </figure>
              <div class="more-sergen-disc">
                <h5><a href="#">Dr. Collin E. Brathwaite, MD</a></h5>
                <p>Winthrop Surgical Associates 120 Mineola Blvd Suite 320Mineola, NY 11501</p>
                <ul class="pt-satisf">
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star-half-o"></a></li>
                  <li><a href="#" class="fa fa-star-o"></a></li>
                </ul>
              </div>
            </div>
            <div class="more-sergen-colm">
              <figure> <a href="#"><img src="{{ url() }}/front_end/assets/img/specialist-img-1.jpg" alt="image" class="img-responsive"></a> </figure>
              <div class="more-sergen-disc">
                <h5><a href="#">Dr. Collin E. Brathwaite, MD</a></h5>
                <p>Winthrop Surgical Associates 120 Mineola Blvd Suite 320Mineola, NY 11501</p>
                <ul class="pt-satisf">
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star-half-o"></a></li>
                  <li><a href="#" class="fa fa-star-o"></a></li>
                </ul>
              </div>
            </div>-->
            </div>
           </div>
          </div>
        <figure class="col-xs-12 online-directory"> <a href="#"><img src="{{ url() }}/front_end/assets/img/online-directory-img.jpg" alt="dr directory" class="img-responsive"></a> </figure>

      </div>
    </aside>
  </div>
</div>
<!-- benifit and features sections end here -->

<!-- footer ------------------------>

@include('footer')
