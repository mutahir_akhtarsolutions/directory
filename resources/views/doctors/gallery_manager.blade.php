@extends('layout.doctorapp')
@section('footer')
<script type="text/javascript" charset="utf-8">
//GALLERY SORTING REFERENCE TAKEN. 
//http://edusagar.com/articles/view/57/Sorting-table-rows-using-Jquery-UI-sortable

    $("#tabledivbody").sortable({
        items: "tr",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            sendOrderToServer();
        }
    });
   
    function sendOrderToServer() {
        var order = $("#tabledivbody").sortable("serialize");
        $.ajax({
        type: "GET",
        dataType: "json",
        url: "<?php echo url();?>/doctor/sortimages",
        data: order,
        success: function(response) {
            if (response.status == "success") {
              //  window.location.href = window.location.href;
            } else {
                alert('Some error occurred');
            }
        }
        });
    }


     $(".moveuplink").click(function() {
        $(this).parents(".sectionsid").insertBefore($(this).parents(".sectionsid").prev());
        sendOrderToServer();   
    });
   
    $(".movedownlink").click(function() {
        $(this).parents(".sectionsid").insertAfter($(this).parents(".sectionsid").next());
        sendOrderToServer();
    });
  // STARTS DELETING SCRIPT.

     $('.moveuplinks').click( function () {
      var table = $('#example1').DataTable();
      var current_row_id = $(this).data().value; 
      
        $.ajax({
       type: "GET",
       url:"<?php echo url();?>/doctor/deleteimages",
       data: {id: current_row_id },
       success : function(data){
          //delete the row
          table.row('#sectionsid_'+current_row_id).remove().draw( false );
        },
       error: function(xhr){
           //error handling
        }}); 
});
</script>
@stop
@section('content')
 <!-- Modal Dialog  FOR VIEW --> 
<div class="modal fade" id="showdetails" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Your Details</h4>
      </div>
      <div class="modal-body">  
        <p id="modal-body">Be patient contents are loading.</p>
      </div>
    </div>
  </div>
</div>

<!-- Modal Dialog  FOR EDIT COMMENTS --> 
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">

     <form action="/visitors/update" method="post" accept-charset="utf-8">
     <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"> 
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit Your Details</h4>
      </div>
      <div class="modal-body loader_div" >
      </div>
          <div class="modal-body modal_body" >
            <input class="form-control" id="id" name="id" type="hidden" placeholder="" value="">
             <input class="form-control" id="rating" name="rating" type="hidden" placeholder="" value="">
               <input class="form-control" id="approved" name="approved" type="hidden" placeholder="" value="">

        <input class="form-control" id="doctor_id" name="doctor_id" type="hidden" placeholder="" value="">
        <input class="form-control" id="user_id" name="user_id" type="hidden" placeholder="" value=""> 
        <div class="form-group">
        <textarea rows="2" class="form-control" placeholder="Comments" id="comments" name="comment"></textarea></div></div>

          <div class="modal-footer ">
        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
      </div>

    </form>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>All Galleries<small>Managment</small> </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo url(); ?>/doctor/gallery">Album</a></li>
        <li class="active">Albums Gallery</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">You Have Total {{count($collection['data'])}} Images(s)</h3> 
            <div class="col-md-12">
            <div class="col-md-6"></div><div class="col-md-6">
              <div class="col-md-8"></div> 
              <div class="col-md-4">  
                <button class="btn btn-block btn-info" onclick="window.location.href='<?php echo url(); ?>/doctor/creategalleries/<?php echo Request::segment(3);?>'">Add New </button> </div> </div></div>
                </div> 
               @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>

                    <th>Sr.No</th>
                    <th>Image Title</th> 
                    <th>Image</th>
                    <th>Sort</th> 
                    <th>Options</th>
                    
                  </tr>
                </thead>
                <tbody id="tabledivbody">
                   <?php $counter = 1;?>
                    @foreach($collection['data'] as $associations) 
                  <tr class="sectionsid" id="sectionsid_{{$associations->id}}">
                  <td>{{$counter}}</td>
                   <td>{{$associations->filename}}</td>  
                  <td>
<img height="50px" width="50px" src="{{ url() }}/images/gallery/icon_size/{{$associations->original_name}}" alt="..." class="img-circle">
                  </td>  
                   
                    <td><a href="javascript:void(0)" class="movedownlink" >
                     <span class="glyphicon glyphicon-arrow-down"></span></a> 
                 <a href="javascript:void(0)" class="moveuplink" >
                     <span class="glyphicon glyphicon-arrow-up"></span></a> 
                     </td>
                   <td>
                   <a href="<?php echo url(); ?>/doctor/updategalleryimage/{{$associations->id.'/'.Request::segment(3)}}" data-title="Edit"  data-value="{{$associations->id }}"  class="btn btn-primary btn-xs"> <span class="glyphicon glyphicon-pencil"></span></a>

                   <a href="javascript:void(0)" class="moveuplinks" data-value="{{$associations->id }}"  data-title="Remove" >
                     <span class="glyphicon glyphicon-trash btn btn-danger btn-xs" ></span></a>

                   </td>
                  </tr>
                  <?php $counter ++;?>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Sr.No</th>
                      <th>Image Title</th> 
                       <th>Image</th> 
                       <th>Sort</th>
                    <th>Options</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
          <!-- /.box --> 
        </div>
        <!-- /.col --> 
      </div>
      <!-- /.row --> 
    </section>
    <!-- /.content --> 
  </div>
@endsection

