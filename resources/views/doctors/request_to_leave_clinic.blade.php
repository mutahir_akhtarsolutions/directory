@extends('layout.memberapp')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>All Doctor's Leaving Request<small>Managment</small> </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">  
            

          <div class="box">
            <div class="box-header">
              <h3 class="box-title"> {{count($doctors)}} Doctor(s) Want to Leave Your Clinic</h3> 
                </div>
                

             @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr><th>Sr.No</th>
                    <th>Name</th>
                    <th>Qualification</th>
                    <th>Phone</th>
                    <th>Afiliation</th>
                     <th>Transfer Request</th>
                    <th>Options</th>
                    
                  </tr>
                </thead>
                <tbody>
                <?php $counter = 1;?>
                    @foreach($doctors as $doctor)
                  
                  <tr><td>{{$counter}}</td>
                    <td>{{$doctor->member->first_name}}&nbsp; {{$doctor->member->last_name}}</td>
                    <td>{{$doctor->education}}</td>
                    <td>{{$doctor->contact_dr_phone_number}}</td>
                    <td>{{$doctor->affiliation}}</td>
                     <td>
@if($doctor->transfer_request_to_parent == 0 && $doctor->transfer_request_approved == 0)
                     No
                     @else
                     <form name="approve_form_{{$doctor->id}}" method="post" action="{{url('profiles/approvetransfer/')}}" >
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> 
                <input type="hidden" name="parent_id" value="{{$doctor->parent_id}}" />
                <input type="hidden" name="transfer_request_to_parent" value="{{$doctor->transfer_request_to_parent}}" />
                <input type="hidden" name="profile_id" value="{{$doctor->id}}" />
                <select name="approve" id="approve_{{$doctor->id}}" class="form-control form-select">
                <option value="-1">Yes</option>
                  <option value="1" @if($doctor->transfer_request_approved == 1) selected="" @endif >Accept</option>
                   <option value="0"  @if($doctor->transfer_request_approved == 0) selected="" @endif >Reject</option>
                </select> 
                </form>
                     @endif </td>
                    <td><div class="btn-group">
                      <button class="btn btn-info" type="button">Action</button>
                      <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('/profiles/editCompleteProfile').'/'. $doctor->id}}">Pesonal Information</a></li>
                        <li><a href="{{ url('/profiles/editSchedule').'/'. $doctor->id }}">Schedule Availability</a></li>
                        <li><a href="#">Payment Methods</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/profiles/permissions').'/'. $doctor->user_id }}">Give Permissions</a></li>

                         <li><a href="{{ url('/profiles/comments').'/'. $doctor->user_id }}">Show Comments</a></li>
                      </ul>
                    </div></td>
                  </tr>
                  <?php $counter++;?>
                  @endforeach
                  
                  
                </tbody>
                <tfoot>
                  <tr>
                    <th>Sr.No</th>
                    <th>Name</th>
                    <th>Qualification</th>
                    <th>Phone</th>
                    <th>Afiliation</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
          <!-- /.box --> 
        </div>
        <!-- /.col --> 
      </div>
      <!-- /.row --> 
    </section>
    <!-- /.content --> 
  </div>



@endsection

