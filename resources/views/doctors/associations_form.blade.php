@extends('layout.doctorapp')
@section('head')
  <link rel="stylesheet" href="{{ URL::asset('dist/build/css/bootstrap-datetimepicker.min.css') }}"> 
@stop
@section('footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js">
  
</script>
<script src="{{ URL::asset('dist/build/js/bootstrap-datetimepicker.min.js') }}"></script> 
   <script type="text/javascript">
            $(function () {
                $('.datetimepicker3').datetimepicker({
                    format: 'LT'
                });
  
    var max_fields      = 5; //maximum input boxes allowed

// ADD MORE ASSOCIATIONS  
    var wrapper_associations         = $(".input_associations_wrap"); //Fields wrapper
    var add_button_associations      = $(".add_associations_button"); //Add button ID
    

    var x_associations = $("#total_associations_filds").val(); //initlal text box count
    $(wrapper_associations).on("click",".add_associations_button", function(e){ //on add input button click
        e.preventDefault();
        if(x_associations < max_fields){ //max input box allowed
            x_associations++; //text box increment
              $(wrapper_associations).append('<div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important"><div class="col-sm-4" style="padding-left: 0px!important">          <input  maxlength="100" type="text" name="association[]" value="" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>          </div>          <div class="col-sm-4">          <select name="availability[]" class="form-control">          <option value="0">Select A day</option>          <option value="Saturday">Saturday</option>          <option value="Sunday">Sunday</option>          <option value="Monday">Monday</option>          <option value="Tuesday">Tuesday</option>           <option value="Wedensday">Wedensday</option>           <option value="Thursday">Thursday</option>          <option value="Friday">Friday</option>           </select>          </div>          <div class="col-sm-4"><button class="remove_associations_field btn btn-primary"  data-id="" >-</button>          <button class="add_associations_button btn btn-primary">+</button>          </div>          <div class="col-sm-12">          <div class="col-sm-4">          <label class="control-label">Time From :</label><div class="input-group date datetimepicker3" id="datetimepicker3">                  <input type="text" class="form-control" id="from_time" name="from_time[]" />                    <span class="input-group-addon">                        <span class="glyphicon glyphicon-time"></span>                    </span>                </div> </div>          <div class="col-sm-4">          <label class="control-label">Time To:</label><div class="input-group date datetimepicker3" id="datetimepicker3">                    <input type="text" class="form-control"  id="to_time" name="to_time[]" />                    <span class="input-group-addon">                        <span class="glyphicon glyphicon-time"></span>                    </span>                </div></div><div class="col-sm-2"></div></div> </div>'); //add input box

             $('.datetimepicker3').datetimepicker({
                    format: 'LT'
                });
           
        } 
    });
    
    $(wrapper_associations).on("click",".remove_associations_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x_associations--;
    });// END ADD MORE ASSOCIATIONS
   });
</script>
@stop
@section('content')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Add / Update Associations
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
           
  @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Error</strong>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif
   @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->

            <div class="box-body">
    <!--Comment the create or update profile upto here--> 
    <div class="padder">
      <!-- form for profile builder starts -->
      <div class="container"> 
<!--{{ url('profile') }}-->

    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3 table-border">
        <div class="col-md-12">
          <h3> Add / Update your Associations</h3> 
           <datalist id="datalist1">
             @foreach($collection['association_names'] as $association_name)
            <option selected="selected" value="{{$association_name->name}}">{{$association_name->name}} </option>
             @endforeach
          </datalist>
          
  <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
          <div class="form-group input_associations_wrap"> 

          <div class="col-xs-12" style="padding-left: 0px!important">
          <label class="control-label">Update Secondary Associations </label></div>          
            @if(count($collection['associations'])<=0)
            <input type="hidden" id="total_associations_filds" value="1"></input>
          <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">

          <div class="col-sm-4" style="padding-left: 0px!important">
          <input  maxlength="100" type="text" name="association[]" value="" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>
          </div>
          <div class="col-sm-4">
          <select name="availability[]" class="form-control">
          <option value="0">Select A day</option>
          <option value="Saturday">Saturday</option> 
          <option value="Sunday">Sunday</option> 
          <option value="Monday">Monday</option> 
          <option value="Tuesday">Tuesday</option> 
          <option value="Wedensday">Wedensday</option> 
          <option value="Thursday">Thursday</option> 
          <option value="Friday">Friday</option> 
          </select> 
          </div>
          <div class="col-sm-4"> 
          <button class="add_associations_button btn btn-primary">+</button>
          </div>
          <div class="col-sm-12">
          <div class="col-sm-4">
          <label class="control-label">Time From:</label> 
           <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="from_time" name="from_time[]" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </div>
          <div class="col-sm-4">
          <label class="control-label">Time To:</label> 
          
 <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="to_time" name="to_time[]"  value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 

</div><div class="col-sm-2"></div></div></div>
          @else
      <?php  $counter = 1; ?>
       @foreach($collection['associations'] as $showAssociation)
      
       @if($counter == 1)

<div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">
 <div class="col-sm-4" style="padding-left: 0px!important">
          <input  maxlength="100" type="text" name="association[]" value="{{$showAssociation->name}}" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>
           <input  maxlength="100" type="hidden" name="association_id[]" value="{{$showAssociation->associations_id}}"/>
          </div>
          <div class="col-sm-4">
          <select name="availability[]" class="form-control">
          <option value="0">Select A day</option>
          <option value="Saturday" @if($showAssociation->availability == 'Saturday') selected="" @endif >Saturday</option> 
          <option value="Sunday" @if($showAssociation->availability == 'Sunday') selected="" @endif >Sunday</option> 
          <option value="Monday" @if($showAssociation->availability == 'Monday') selected="" @endif >Monday</option> 
          <option value="Tuesday" @if($showAssociation->availability == 'Tuesday') selected="" @endif >Tuesday</option> 
          <option value="Wedensday" @if($showAssociation->availability == 'Wedensday') selected="" @endif >Wedensday</option> 
          <option value="Thursday" @if($showAssociation->availability == 'Thursday') selected="" @endif >Thursday</option> 
          <option value="Friday" @if($showAssociation->availability == 'Friday') selected="" @endif >Friday</option> 
          </select> 
          </div>
          <div class="col-sm-2">
          <button class="add_associations_button btn btn-primary pull-right">+</button>
          </div>
          <div class="col-sm-12">
          <div class="col-sm-4">
          <label class="control-label">Time From:
          </label>  <div class='input-group date datetimepicker3' id='datetimepicker3'>
        <input type='text' class="form-control" id="from_time" name="from_time[]"  value="{{$showAssociation->from_time}}" />
          <span class="input-group-addon">
         <span class="glyphicon glyphicon-time"></span>
          </span>
                </div> 
                 </div>
          <div class="col-sm-4">
          <label class="control-label">Time To:</label> 

 <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="to_time" name="to_time[]"  value="{{$showAssociation->to_time}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 


</div><div class="col-sm-2"></div></div></div> 
       @else         

        <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">

          <div class="col-sm-4" style="padding-left: 0px!important">
          <input  maxlength="100" type="text" name="association[]" value="{{$showAssociation->name}}" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>
          <input  maxlength="100" type="hidden" name="association_id[]" value="{{$showAssociation->associations_id}}"/>
          </div>
          <div class="col-sm-4">
          <select name="availability[]" class="form-control">
           <option value="Saturday" @if($showAssociation->availability == 'Saturday') selected="" @endif >Saturday</option> 
          <option value="Sunday" @if($showAssociation->availability == 'Sunday') selected="" @endif >Sunday</option> 
          <option value="Monday" @if($showAssociation->availability == 'Monday') selected="" @endif >Monday</option> 
          <option value="Tuesday" @if($showAssociation->availability == 'Tuesday') selected="" @endif >Tuesday</option> 
          <option value="Wedensday" @if($showAssociation->availability == 'Wedensday') selected="" @endif >Wedensday</option> 
          <option value="Thursday" @if($showAssociation->availability == 'Thursday') selected="" @endif >Thursday</option> 
          <option value="Friday" @if($showAssociation->availability == 'Friday') selected="" @endif >Friday</option> 
          </select> 
          </div>
          <div class="col-sm-4"> <button class="remove_associations_field btn btn-primary"  data-id="{{$showAssociation->associations_id}}" >-</button>
          <button class="add_associations_button btn btn-primary">+</button>
          </div>
          <div class="col-sm-12">
          <div class="col-sm-4">
          <label class="control-label">Time From:</label> 
          <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="from_time" name="from_time[]"  value="{{$showAssociation->from_time}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </div>
          <div class="col-sm-4">
          <label class="control-label">Time To:</label> 
          <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="to_time" name="to_time[]"  value="{{$showAssociation->to_time}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 
</div><div class="col-sm-2"></div></div></div>
            @endif
            <?php $counter++;?>
            @endforeach
            <input type="hidden" id="total_associations_filds" value="{{$counter-1}}"></input>
@endif
</div>
        



                  </div><!-- /.box-body -->
                  <div class="box-footer">
                  <div class="col-md-12">
                  <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-default">Cancel</button>
                   
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                    </div>
                  </div><!-- /.box-footer -->
                </form>

        </div>
      </div>
    </div> 
          
        </div>

      </div>

    </div> 
 
          


            </div><!-- /.box-body -->
         <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
         

        </section><!-- /.content --> 
 </div><!-- /.box -->
 
@endsection