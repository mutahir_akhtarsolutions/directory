@extends('layout.doctorapp')
@section('content')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Form.
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">

  @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Error</strong>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif
   @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->

            <div class="box-body">
    <!--Comment the create or update profile upto here-->
    <div class="padder">
      <!-- form for profile builder starts -->
      <div class="container">
<!--{{ url('profile') }}-->

    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3 table-border">
        <div class="col-md-12"> 
          <h3> Update Image </h3> 
 
  <form class="form-horizontal" role="form" method="POST" action="" autocomplete = "off" enctype="multipart/form-data">
  <input name="album_id" id="album_id" value="<?php echo Request::segment(4);?>" type='hidden'> </input>
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="box-body">
                    <div class="form-group {{ $errors->first('filename', ' has-error') }}"> 
                      <label for="inputEmail3" class="col-sm-4 control-label">Entere Image Title</label>
                      <div class="col-sm-8">
                      <input name="image_id" type="hidden" value="{{$collection['images']->id}}"></input>
                        <input type="text" class="form-control" name="filename" id="filename" placeholder="Album Name" value="{{$collection['images']->filename}}"><span style="color:#DD4B39" class="help-block">{{{ $errors->first('filename', ':message') }}}</span>
                      </div> 
                    </div>
  <div class="form-group {{ $errors->first('original_name', ' has-error') }}"> 
                      <label for="inputEmail3" class="col-sm-4 control-label">Select Album Image</label>
                      <div class="col-sm-8">
           <input type="file"  name="original_name" id="original_name"><span style="color:#DD4B39" class="help-block">{{{ $errors->first('original_name', ':message') }}}</span>


           <img height="50px" width="50px" src="{{ url() }}/images/gallery/icon_size/{{$collection['images']->original_name}}" alt="..." class="img-circle">
                      </div> 
                    </div> 
              <div class="form-group"> 
                      <label for="inputEmail3" class="col-sm-4 control-label">Sort Order</label>
                      <div class="col-sm-8">   
                      <input name="sort_order" value="{{$collection['images']->sort_order}}" size="20"></input>
                      </div>  
                    </div>  
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                  <div class="col-md-12">
                  <div class="col-md-6 col-md-offset-3">
                    <button type="reset" class="btn btn-default">Cancel</button>

                    <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                    </div>
                  </div><!-- /.box-footer -->
                </form>

        </div>
      </div>
    </div>

        </div>

      </div>

    </div>




            </div><!-- /.box-body -->
         <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->


        </section><!-- /.content -->
 </div><!-- /.box -->

@endsection