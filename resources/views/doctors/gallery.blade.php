@extends('layout.doctorapp')
@section('footer')
<script type="text/javascript" charset="utf-8">
//GALLERY SORTING REFERENCE TAKEN. 
//http://edusagar.com/articles/view/57/Sorting-table-rows-using-Jquery-UI-sortable

    $("#tabledivbody").sortable({
        items: "tr",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            sendOrderToServer();
        }
    });
   
    function sendOrderToServer() {
        var order = $("#tabledivbody").sortable("serialize");
        $.ajax({
        type: "GET",
        dataType: "json",
        url: "<?php echo url();?>/doctor/sortalbums",
        data: order,
        success: function(response) {
            if (response.status == "success") {
              //  window.location.href = window.location.href;
            } else {
                alert('Some error occurred');
            }
        }
        });
    }
$('.modal-delete-cancel').on('click', function(){
   $("#confirm-delete").dialog("close"); 
});

     $(".moveuplink").click(function() {
        $(this).parents(".sectionsid").insertBefore($(this).parents(".sectionsid").prev());
        sendOrderToServer();   
    });
   
    $(".movedownlink").click(function() {
        $(this).parents(".sectionsid").insertAfter($(this).parents(".sectionsid").next());
        sendOrderToServer();
    });
  // STARTS DELETING SCRIPT.

     $('.remove-album').click( function () {
      var table = $('#example1').DataTable();
      var current_row_id = $(this).data().value;  
       $("#confirm-delete").dialog().show();
       $('.modal-delete').on('click', function(){
    //$('.confirm-delete-modal').trigger('click'); 
        $.ajax({
       type: "GET",
       url:"<?php echo url();?>/doctor/deletealbums",
       data: {id: current_row_id },
       success : function(data){
          //delete the row
          table.row('#sectionsid_'+current_row_id).remove().draw( false );
          $("#confirm-delete").dialog("close");
        },
       error: function(xhr){
           //error handling
        }}); 
      });
});
</script>
@stop
@section('content')
 
 <div class="modal modal-effect-blur" id="confirm-delete">
    <div class="modal-content">
        <h3>Are you sure you want to delete?</h3>
        <div>
            <button class="modal-delete">Delete</button>
            <button class="modal-delete-cancel">Cancel</button>
        </div>
    </div>
</div>
<div class="modal-overlay"></div>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>All My Albums<small>Managment</small> </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Albums</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">You Have Total {{count($collection['data'])}} Albums(s)</h3> 
            <div class="col-md-12">
            <div class="col-md-6"></div><div class="col-md-6">
              <div class="col-md-8"></div> 
              <div class="col-md-4">  
                <button class="btn btn-block btn-info" onclick="window.location.href='<?php echo url(); ?>/doctor/addgallery'">Add New </button> </div> </div></div>
                </div> 
             @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>

                    <th>Sr.No</th>
                    <th>Album Names</th> 
                    <th>Front Image</th>
                     <th>Sort</th> 
                    <th>Options</th>
                  </tr>
                </thead>
               <tbody id="tabledivbody">
                   <?php $counter = 1;?>
                    @foreach($collection['data'] as $associations) 
                  <tr class="sectionsid" id="sectionsid_{{$associations->id}}">
                  <td>{{$counter}}</td>
                  <td>{{$associations->album_name}}</td> 
                  <td> <img height="50px" width="50px" src="{{ url() }}/images/gallery/album/icon_size/{{$associations->album_image}}" alt="..." class="img-circle"></td>
                      <td><a href="javascript:void(0)" class="movedownlink" >
                     <span class="glyphicon glyphicon-arrow-down"></span></a> 
                 <a href="javascript:void(0)" class="moveuplink" >
                     <span class="glyphicon glyphicon-arrow-up"></span></a> 
                     </td> 
                   <td> 
                    <a href="<?php echo url(); ?>/doctor/updategallery/{{$associations->id}}" data-title="Edit"  data-value="{{$associations->id }}"  class="btn btn-primary btn-xs"> <span class="glyphicon glyphicon-pencil"></span></a>

                   <a href="javascript:void(0)" class="remove-album" data-value="{{$associations->id }}"  data-title="Remove" >
                     <span class="glyphicon glyphicon-trash btn btn-danger btn-xs" ></span></a>
<a href="<?php echo url(); ?>/doctor/managegalleries/{{$associations->id}}" data-title="Manage Images" class="btn btn-primary btn-xs" style="font-size:13px" ><span class="glyphicon glyphicon-film"></span></a>
                  

                   </td>
                  </tr>
                  <?php $counter ++;?>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Sr.No</th>
                      <th>Album Names</th> 
                       <th>Front Image</th>
                       <th>Sort</th> 
                    <th>Options</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
          <!-- /.box --> 
        </div>
        <!-- /.col --> 
      </div>
      <!-- /.row --> 
    </section>
    <!-- /.content --> 
  </div>
@endsection

