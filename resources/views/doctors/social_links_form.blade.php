@extends('layout.doctorapp')
@section('content')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Add social Links
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
           
  @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Error</strong>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif
   @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->

            <div class="box-body">
    <!--Comment the create or update profile upto here--> 
    <div class="padder">
      <!-- form for profile builder starts -->
      <div class="container"> 
<!--{{ url('profile') }}-->

    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3 table-border">
        <div class="col-md-12">
          <h3> Add your social links</h3> 
          
  <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
        <div class="form-group {{ $errors->first('clinic_name', ' has-error') }}">        
         <div class="form-group input_fields_wrap">           
            @if(count($collection['checkSocialLinks'])<=0) 
            
             <input type="hidden" id="total_filds" value="1"></input>
             <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">
            <div class="col-sm-6" style="padding-left: 0px!important">
            <input  maxlength="200" type="text" name="sociallinks[]" value="" class="form-control" placeholder="Enter dr association"  />
            <input name="link_id[]" id="link_id" type="hidden" value=""></input>
            </div>

            <div class="col-sm-2">
           <input  maxlength="10" type="text" name="sort_order[]" value="" class="form-control" placeholder="Sort Order"  />
            </div>
            <div class="col-sm-4"><button class="remove_field btn btn-primary"  data-id="" >-</button>
            <button class="add_field_button btn btn-primary">+</button>
            </div>

             <div class="col-sm-12"  style="padding-bottom: 10px; padding-top: 10px;">
            <input id="socialicon" name="socialicon[]" type="file" multiple class="file-loading">
            </div>
            </div>

       @else
      <?php  $counter = 1;?>
       @foreach($collection['checkSocialLinks'] as $socials)
      
       @if($counter == 1)
       <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">
        <div class="col-sm-6" style="padding-left: 0px!important">
            <input  maxlength="200" type="text" name="sociallinks[]" value="{{$socials->link}}" class="form-control" placeholder="Enter dr association" />
            <input name="link_id[]" id="link_id" type="hidden" value="{{$socials->id}}"></input>
            </div>

            <div class="col-sm-2">
           <input  maxlength="10" type="text" name="sort_order[]" value="{{$socials->sort_order}}" class="form-control" placeholder="Sort Order"  />
            </div>
            <div class="col-sm-4"><button class="remove_field btn btn-primary"  data-id="{{$socials->id}}" >-</button>
            <button class="add_field_button btn btn-primary" data-id="{{$socials->id}}">+</button>
            </div>

             <div class="col-sm-12" style="padding-bottom: 10px; padding-top: 10px;">
            <input id="socialicon" name="socialicon[]" type="file" multiple class="file-loading">
            
    <img src="<?php echo url(); ?>/images/socials/<?php echo @$socials->icon; ?>" alt="doctor image" height="60" width="60"  style="margin-bottom: 10px; margin-top: 10px;">
            </div>
</div>
       @else
            <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">
            <div class="col-sm-6" style="padding-left: 0px!important"><input  maxlength="200" type="text" name="sociallinks[]" value="{{$socials->link}}" class="form-control" placeholder="Enter dr association" /><input name="link_id[]" id="link_id" type="hidden" value="{{$socials->id}}"></input>
            </div>

            <div class="col-sm-2"><input  maxlength="5" type="text" name="sort_order[]" value="{{$socials->sort_order}}" class="form-control" placeholder="Sort Order"  /></div>

            <div class="col-sm-4"><button class="remove_field btn btn-primary"  data-id="{{$socials->id}}" >-</button> <button class="add_field_button btn btn-primary" data-id="{{$socials->id}}">+</button></div>
            
            <div class="col-sm-12" style="padding-bottom: 10px; padding-top: 10px;"><input id="socialicon" name="socialicon[]" type="file" multiple class="file-loading">
<img src="<?php echo url(); ?>/images/socials/<?php echo @$socials->icon; ?>" alt="doctor image" height="60" width="60"   style="margin-bottom: 10px; margin-top: 10px;">
            </div>
            </div>
            @endif
            <?php $counter++;?>
            @endforeach
            <input type="hidden" id="total_filds" value="{{$counter-1}}"></input>
@endif   
          </div>
                    </div> 
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                  <div class="col-md-12">
                  <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-default">Cancel</button>
                   
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                    </div>
                  </div><!-- /.box-footer -->
                </form>

        </div>
      </div>
    </div> 
          
        </div>

      </div>

    </div> 
 
          


            </div><!-- /.box-body -->
         <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
         

        </section><!-- /.content --> 
 </div><!-- /.box -->
 
@endsection