@extends('layout.doctorapp')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>All Doctor's Joining Request<small>Managment</small> </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section> 
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">  
            

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Joining request for a Clinic</h3> 
                </div>
                

             @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr><th>Sr.No</th>
                    <th>Name</th>
                    <th>Qualification</th>
                    <th>Phone</th>
                    <th>Afiliation</th>
                     <th>Joining Request</th>
                    <th>Options</th>
                    
                  </tr>
                </thead>
                <tbody>
                <?php $counter = 1;?>
                    @foreach($collection['doctors'] as $doctor)
                  
                  <tr><td>{{$counter}}</td>
                    <td>{{$doctor->member->first_name}}&nbsp; {{$doctor->member->last_name}}</td>
                    <td>{{$doctor->education}}</td>
                    <td>{{$doctor->contact_dr_phone_number}}</td>
                    <td>{{$doctor->affiliation}}</td>
                     <td> 
                     <form name="approve_form_{{$doctor->id}}" method="post" action="{{url('doctor/joiningrequest/')}}" >
   <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />  
    
                <input type="checkbox" value="1" name="approve" id="approve" onclick="this.form.submit();">
                </form>  </td>
                    <td><div class="btn-group">
                      <button class="btn btn-info" type="button">Action</button>
                      <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                    </div></td>
                  </tr>
                  <?php $counter++;?>
                  @endforeach
                  
                  
                </tbody>
                <tfoot>
                  <tr>
                    <th>Sr.No</th>
                    <th>Name</th>
                    <th>Qualification</th>
                    <th>Phone</th>
                    <th>Afiliation</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
          <!-- /.box --> 
        </div>
        <!-- /.col --> 
      </div>
      <!-- /.row --> 
    </section>
    <!-- /.content --> 
  </div>



@endsection

