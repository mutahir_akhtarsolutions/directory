@extends('layout.doctorapp')
@section('content')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Profile Transfer Request Form.
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
           
  @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Error</strong>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif
   @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->

            <div class="box-body">
    <!--Comment the create or update profile upto here--> 
    <div class="padder">
      <!-- form for profile builder starts -->
      <div class="container"> 
<!--{{ url('profile') }}-->

    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3 table-border">
        <div class="col-md-12">
          <h3> Enter a clinic you want to transfer your profile </h3> 
          
  <form class="form-horizontal" role="form" method="POST" action="" autocomplete = "off">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="box-body">
                    <div class="form-group {{ $errors->first('clinic_name', ' has-error') }}">
                    <input type="hidden" value="{{$collection['Profile']->id}}" name="profile_id"></input>
                      <label for="inputEmail3" class="col-sm-4 control-label">Entere Clinic Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="business_name" id="business_name" placeholder="Clinic Name" list="business_names"><span style="color:#DD4B39" class="help-block">{{{ $errors->first('clinic_name', ':message') }}}</span>
                      </div>
                       <datalist id="business_names">
                       <?php foreach($collection['data']  as $bNames): ?>
                       <option value="{{$bNames->business_name}}">
                       <?php endforeach; ?>  
                       </datalist>
                    </div> 
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                  <div class="col-md-12">
                  <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-default">Cancel</button>
                   
                    <button type="submit" class="btn btn-info pull-right">Transfer</button>
                    </div>
                    </div>
                  </div><!-- /.box-footer -->
                </form>

        </div>
      </div>
    </div> 
          
        </div>

      </div>

    </div> 
 
          


            </div><!-- /.box-body -->
         <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
         

        </section><!-- /.content --> 
 </div><!-- /.box -->
 
@endsection