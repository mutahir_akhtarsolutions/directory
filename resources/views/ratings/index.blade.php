 @extends('app')
{{-- Page content --}}
@section('content')
<div class="container">
     <div class="page-header">
        <h1>User's Rattings <span class="pull-right"><!-- <a href="{{ URL::to('users/create') }}" class="btn btn-warning">Create</a>--></span></h1>
     </div>

     @if ($collection['allMyComments']->count())
     Page {{ $collection['allMyComments']->currentPage() }} of {{ $collection['allMyComments']->lastPage() }}

     <div class="pull-right">
        {!! $collection['allMyComments']->render() !!}
     </div>

     <br> 

     <table class="table table-bordered">
        <thead>
            <th class="col-lg-2">Doctor's Name</th>
            <th class="col-lg-2">Commenter's Name</th>
            <th class="col-lg-1">Rating</th>
            <th class="col-lg-1">Type</th>
            <th class="col-lg-2">Status</th>
            <th class="col-lg-4">Actions</th>
        </thead>
        <tbody>
            @foreach ($collection['allMyComments'] as $user)
            <tr>
                <td>{{$user->doctor_name}}</td>
                <td>{{$user->visitor_name}}</td>
                <td>{{$user->rating}}</td>
                <td>{{$user->reviews_type}}</td>
                <td> 
                <form name="approve_form_{{$user->id}}" method="post" action="" >
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <input type="hidden" name="review_id" value="{{$user->id}}"></input>
                <select name="approve" id="approve_{{$user->id}}" class="form-control form-select">
                  <option @if($user->approved == 1) selected="selected" @endif value="1">Approve</option>
                   <option value="0" @if($user->approved == 0) selected="selected" @endif>Unapprove</option>
                </select> 
                </form>
                </td>
                <td>
                <a href="#" class="modalInput btn btn-warning" data-toggle="modal"   data-detail-id="{{$user->id }}" data-target="#showdetails" >View</a>
 
               <!-- <a class="btn btn-warning" href="{{ URL::to("ratings/{$user->id}") }}">Edit</a>-->
<a class="btn btn-danger" href="{{ URL::to("ratings/{$user->id}/delete") }}">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
     </table>

     Page {{ $collection['allMyComments']->currentPage() }} of {{ $collection['allMyComments']->lastPage() }}

     <div class="pull-right">
         {!! @$collection['allMyComments']->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>

</div>
@endif

@endsection
<!-- Modal Dialog --> 
<div class="modal fade" id="showdetails" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Details</h4> 
      </div>
      <div class="modal-body"> 


        <p id="modal-body">Be patient contents are loading.</p>
      </div>
    </div>
  </div>
</div>