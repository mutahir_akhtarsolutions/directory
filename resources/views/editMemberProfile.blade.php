@extends('layout.memberapp')
@section('head')

  <link rel="stylesheet" href="{{ url() }}/plugins/clutip/jquery.cluetip.css" type="text/css" />
 <style type="text/css"> 
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
.load-local
{font-size: 21px;}
</style>
@stop

@section('footer')
 <script src="{{ url() }}/plugins/clutip/jquery.cluetip.js" type="text/javascript"></script>
<script  type="text/javascript" charset="utf-8" >
$(document).ready(function(){
  $('.load-local').cluetip({local:true, cursor: 'pointer',width: '500px', showTitle: false,  closePosition: 'title', arrows: true}); 
  var navListItems   = $('div.setup-panel div a'),
          allWells   = $('.setup-content'),
          allNextBtn = $('.nextBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target   = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');

});  
</script> 


@stop
@section('content')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Update 
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{url('profiles')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Profile</a></li> 
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
<p>{{ $mode == 'create' ? 'Create Profile' : 'Update profile' }} </p>
</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>

   @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->

            <div class="box-body">
    <!--Comment the create or update profile upto here--> 
    <div class="padder">
      <!-- form for profile builder starts -->
      <div class="container">

<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Step 1</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
        <p>Step 2</p>
      </div>
      <!--<div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Step 3</p>
      </div>-->
    </div>
  </div>
<?php if($mode =="update"){
    $action = url('updateProfile');
 }else{
   $action = url('profile');
 }
 ?>
<!--addordinalnumber function-->
 
<!--{{ url('profile') }}-->
  <form role="form" action="<?php echo $action;?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />
    <?php 
        if($mode !="update"){
    ?>
    <input type="hidden" name="user_id" value="{{ $data['mem_id'] }}" />
    <?php
     }else{
    ?>
    <input type="hidden" name="user_id" value="{{ $data['user_id'] }}" />
    <input type="hidden" name="id" value="{{ $data['id'] }}" />
    <?Php
      }
    ?>
    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> General member information (Step 1)</h3>
          <div class="form-group">
            <label class="control-label">Title</label>
            <input  maxlength="100" type="text" name="title" value="{{ Input::old('title', isset($data->title) ? $data->title : '' ) }}" required="required" class="form-control" placeholder="Enter phone"  />
          </div>
          <div class="form-group">
            <label class="control-label">First Name</label>
            <input  maxlength="100" type="text" name="first_name" value="<?php if(isset($users->first_name)){ echo @$users->first_name;}else{  echo @$users['first_name']; } ?>" required="required" class="form-control" placeholder="Enter mobile"  />
          </div>
          <div class="form-group">
            <label class="control-label">Last Name</label>
            <input  maxlength="100" type="text" name="last_name" value="<?php if(isset($users->last_name)){ echo @$users->last_name;}else{  echo @$users['last_name']; } ?>" required="required" class="form-control" placeholder="Enter mobile"  />
          </div>
          <div class="form-group">
            <label class="control-label">Profile Image</label>
            <input ui-jq="filestyle" type="file" name="avatar" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
          </div>
           <div class="form-group">
            <img src="<?php echo url(); ?>/images/catalog/<?php echo @$data->avatar; ?>" alt="doctor image" height="60" width="60">
          </div>

          <hr>

          <div class="form-group">
            <label class="control-label">Contact Telephone</label>
            <input  maxlength="100" type="text" name="phone" value="{{ Input::old('phone', isset($data->phone) ? $data->phone : '' ) }}" required="required" class="form-control" placeholder="Enter Country"  />
          </div>
          <div class="form-group">
            <label class="control-label">Contact Mobile</label>
            <input  maxlength="100" type="text" name="mobile" value="{{ Input::old('mobile', isset($data->mobile) ? $data->mobile : '' ) }}" required="required" class="form-control" placeholder="Enter Country"  />
          </div>



          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-2">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> General business Iinformation (Step 2)</h3>
          <div class="form-group">
            <label class="control-label">Business Name</label>
            <input maxlength="200" type="text" name="business_name" value="{{ Input::old('business_name', isset($data->business_name) ? $data->business_name : '' ) }}" required="required" class="form-control" placeholder="Business Name" />
          </div>

          <div class="form-group">
            <label class="control-label">Number of Doctors</label>
            <select name="no_of_doctors" class="form-control m-b">
              <option>select number of doctors </option>
              <?php 
              for($i = 1; $i <= 20; $i++){  ?>
              <option value="{{$i}}" @if($i == $data->no_of_doctors) selected="selected" @endif> <?php echo $i; ?></option>
              <?php }  ?>

           </select>
          </div>

          <!--Opening hours start-->

                  <h2>Opening hours</h2>
                  <p>Please select the opening hours:</p>
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Days</th>
                        <th>Open</th>
                        <th>From</th>
                        <th>To</th>
                      </tr>
                    </thead>
                    @if($count_secedule > 0)
                    <tbody>
                   
                    @foreach($Profileschedules as $Profileschedule)
                      <tr>
                        <td>{{ucfirst ($Profileschedule->days)}}
                        <input type="hidden" name="days[]" id="days" value="{{$Profileschedule->days}}"></input>

                        </td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1" @if($Profileschedule->open == 1) selected="selected" @endif >Yes</option>
                            <option value="0" @if($Profileschedule->open == 0) selected="selected" @endif >No</option>
                         </select>
                        </div>
                      </td>
                        <td>
                          <div class="form-group">
                          <select name="from_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option @if($Profileschedule->from == $i) selected="selected" @endif> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option @if($Profileschedule->to == $i) selected="selected" @endif> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                      </tr>
                      @endforeach
                      </tbody>
                      @else
                      <tbody>
                      <tr>
                        <td>Sunday
                        <input type="hidden" name="days[]" id="days" value="sunday">

                        </td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1" >Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                      </td>
                        <td>
                          <div class="form-group">
                          <select name="from_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option > 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option > 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                      </tr>
                                            <tr>
                        <td>Monday
                        <input type="hidden" name="days[]" id="days" value="monday">

                        </td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0" >No</option>
                         </select>
                        </div>
                      </td>
                        <td>
                          <div class="form-group">
                          <select name="from_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option > 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option > 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                      </tr>
                                            <tr>
                        <td>Tuesday
                        <input type="hidden" name="days[]" id="days" value="tuesday">

                        </td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1" >Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                      </td>
                        <td>
                          <div class="form-group">
                          <select name="from_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option > 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option > 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                      </tr>
                                            <tr>
                        <td>Wednesday
                        <input type="hidden" name="days[]" id="days" value="wednesday">

                        </td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1" >Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                      </td>
                        <td>
                          <div class="form-group">
                          <select name="from_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option > 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option > 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                      </tr>
                                            <tr>
                        <td>Thursday
                        <input type="hidden" name="days[]" id="days" value="thursday">

                        </td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1" >Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                      </td>
                        <td>
                          <div class="form-group">
                          <select name="from_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option > 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option > 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                      </tr>
                                            <tr>
                        <td>Friday
                        <input type="hidden" name="days[]" id="days" value="friday">

                        </td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1" >Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                      </td>
                        <td>
                          <div class="form-group">
                          <select name="from_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option > 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option > 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                      </tr>
                                            <tr>
                        <td>Saturday
                        <input type="hidden" name="days[]" id="days" value="saturday">

                        </td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0" >No</option>
                         </select>
                        </div>
                      </td>
                        <td>
                          <div class="form-group">
                          <select name="from_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option > 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_sunday[]" class="form-control m-b">
                            <option>select opening from</option>
                                                        <option> 0</option>
                                                        <option> 1</option>
                                                        <option> 2</option>
                                                        <option> 3</option>
                                                        <option> 4</option>
                                                        <option> 5</option>
                                                        <option> 6</option>
                                                        <option> 7</option>
                                                        <option> 8</option>
                                                        <option> 9</option>
                                                        <option> 10</option>
                                                        <option> 11</option>
                                                        <option> 12</option>
                                                        <option> 13</option>
                                                        <option> 14</option>
                                                        <option> 15</option>
                                                        <option> 16</option>
                                                        <option> 17</option>
                                                        <option> 18</option>
                                                        <option> 19</option>
                                                        <option> 20</option>
                                                        <option> 21</option>
                                                        <option> 22</option>
                                                        <option> 23</option>
                            
                         </select>
                        </div>
                        </td>
                      </tr>
                                          </tbody>
                      @endif
                    
                  </table>

<!--Opening hours end-->





























<p id="loadme">(clinic name,location name,city,state,country)The address will also be used for  <code>google map</code> </p>  

          <div class="form-group">
            <label class="control-label" title="Business Address">Business Address <i class="fa fa-fw fa-question-circle load-local" href="#loadme" rel="#loadme" ></i> </label>
            <input maxlength="200" type="text" name="business_address" value="{{ Input::old('business_address', isset($data->business_address) ? $data->business_address : '' ) }}" required="required" class="form-control" placeholder="Business Address" />
            <br>
             <label class="control-label">Other Business Address</label>
            <input maxlength="200" type="text" name="business_address2" value="{{ Input::old('business_address2', isset($data->business_address2) ? $data->business_address2 : '' ) }}" required="required" class="form-control" placeholder="Enter Other Business Address" />
          </div>

          <div class="form-group">
            <label class="control-label">Country</label>
            <select name="country_id"  class="form-control m-b" id="country_id">
              <option>select country</option>


              <?php foreach(@$countries as $contry){ ?>

              <option value="<?php echo $contry->id; ?>" @if($contry->id == $data->country_id) selected="selected" @endif ><?php echo $contry->country_name; ?></option>
              <?php } ?>

           </select>
          </div>

          <div class="form-group">
            <label class="control-label">State</label>
            <select name="state_id" class="form-control m-b" id="state_id">
              <option>select state</option>

              <?php foreach($states as $stat){ ?>

              <option value="<?php echo $stat->id; ?>" @if($stat->id == $data->state_id) selected="selected" @endif ><?php echo $stat->state_name; ?></option>
              <?php } ?>



           </select>
          </div>

          <div class="form-group">
            <label class="control-label">City</label>
            <select name="city_id" class="form-control m-b" id="city_id">
              <option>select city</option>
              <?php foreach($cities as $city){ ?>

              <option value="<?php echo $city->id; ?>" @if($city->id == $data->city_id) selected="selected" @endif ><?php echo $city->city_name; ?></option>
              <?php } ?>
           </select>
          </div>

          <div class="form-group">
            <label class="control-label">Zip Code</label>
            <input maxlength="200" type="text" name="zip_code" value="{{ Input::old('zip_code', isset($data->zip_code) ? $data->zip_code : '' ) }}" required="required" class="form-control" placeholder="Enter Zip Code" />
          </div>

          <div class="form-group">
            <label class="control-label">Business Telephone</label>
            <input maxlength="200" type="text" name="business_telephone" value="{{ Input::old('business_telephone', isset($data->business_telephone) ? $data->business_telephone : '' ) }}" required="required" class="form-control" placeholder="Enter Business Telephone" />
          </div>

          <div class="form-group">
            <label class="control-label">Business Reception</label>
            <input maxlength="200" type="text" name="business_reception" value="{{ Input::old('business_reception', isset($data->business_reception) ? $data->business_reception : '' ) }}" required="required" class="form-control" placeholder="Enter Business Reception" />
          </div>

          <div class="form-group">
            <label class="control-label">Business Email</label>
            <input maxlength="200" type="text" name="business_email" value="{{ Input::old('business_email', isset($data->business_email) ? $data->business_email : '' ) }}" required="required" class="form-control" placeholder="Enter Business Email" />
          </div>

          <div class="form-group">
            <label class="control-label">Business Website</label>
            <input maxlength="200" type="text" name="business_website" value="{{ Input::old('business_website', isset($data->business_website) ? $data->business_website : '' ) }}" required="required" class="form-control" placeholder="Enter Business Website" />
          </div>






          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
          <!--<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>-->
        </div>
      </div>
    </div>
    <!--<div class="row setup-content" id="step-3">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Step 3</h3>
          <div class="form-group">
            <label class="control-label">Association</label>
            <input maxlength="200" type="text" name="association" value="{{ Input::old('association', isset($data->association) ? $data->association : '' ) }}" required="required" class="form-control" placeholder="Enter Association" />
          </div>
          <div class="form-group">
            <label class="control-label">Package Plan</label>
            <select name="package" class="form-control m-b">
              <option>option 1</option>
              <option>option 2</option>
              <option>option 3</option>
              <option>option 4</option>
           </select>
          </div>
          <div class="form-group">
            <label class="control-label">Payment Method</label>
            <select name="Payment-gateway" class="form-control m-b">
              <option>option 1</option>
              <option>option 2</option>
              <option>option 3</option>
              <option>option 4</option>
           </select>
          </div>
          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
        </div>
      </div>
    </div>-->
  </form>



            </div><!-- /.box-body -->
            <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 
@endsection