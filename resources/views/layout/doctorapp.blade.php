<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title> Nation Wide Physicians </title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{ URL::asset('plugins/datatables/dataTables.bootstrap.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.css') }}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ URL::asset('dist/css/skins/_all-skins.css') }}">
     
    <link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet"> 
    <!-- Tokenfield CSS -->
    <link href="{{ URL::asset('dist/css/bootstrap-tokenfield.css') }}" type="text/css" rel="stylesheet">
     <link href="{{ URL::asset('assets/css/custom.css') }}" type="text/css" rel="stylesheet">
    <!-- Docs CSS --> 

<!-- jQuery 2.1.4 --> 
<script src="{{ URL::asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script> 
<!-- Bootstrap 3.3.5 --> 
<script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script> 
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     @yield('head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header"> 
    <!-- Logo --> 
    <a href="" class="logo"> 
    <!-- mini logo for sidebar mini 50x50 pixels --> 
    <span class="logo-mini"><img class="img-responsive" alt="logo" src="http://directory.local.com/front_end/assets/img/logo.png"></span> 
    <!-- logo for regular state and mobile devices --> 
    <span class="logo-lg"><img class="img-responsive" alt="logo" src="http://directory.local.com/front_end/assets/img/logo.png"></span> </a> 
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation"> 
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-envelope-o"></i> <span class="label label-success">4</span> </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li> 
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message --> 
                    <a href="#">
                    <div class="pull-left"> <img src="{{ URL::asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image"> </div>
                    <h4> Support Team <small><i class="fa fa-clock-o"></i> 5 mins</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                  <!-- end message -->
                  <li> <a href="#">
                    <div class="pull-left"> <img src="{{ URL::asset('dist/img/user3-128x128.jpg')}}" class="img-circle" alt="User Image"> </div>
                    <h4> AdminLTE Design Team <small><i class="fa fa-clock-o"></i> 2 hours</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                  <li> <a href="#">
                    <div class="pull-left"> <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image"> </div>
                    <h4> Developers <small><i class="fa fa-clock-o"></i> Today</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                  <li> <a href="#">
                    <div class="pull-left"> <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image"> </div>
                    <h4> Sales Department <small><i class="fa fa-clock-o"></i> Yesterday</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                  <li> <a href="#">
                    <div class="pull-left"> <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image"> </div>
                    <h4> Reviewers <small><i class="fa fa-clock-o"></i> 2 days</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <span class="label label-warning">10</span> </a>
            <ul class="dropdown-menu">
              <li class="header">You have notification(s)</li>
              <li> 
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                @if(App\Helpers\MyHelper::countTransferDoctor($collection['Profile']['user_id']) > 0) 
                  <li> <a href="{{ url('/doctor/joiningrequest') }}"> <i class="fa fa-users text-red"></i> You hav a joining request </a></li> @endif 

                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <?php
                //$doctors_to_add           = $Profile['no_of_doctors'];
               // $total_added_doctors      = count($doctors);
               // $remaining_doctors_to_add = $doctors_to_add-$total_added_doctors;

          ?>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-flag-o"></i> <span class="label label-danger"> </span> </a>
            <ul class="dropdown-menu">
              <li class="header">You can add   </li>
              <li> 
                <!-- inner menu: contains the actual data -->
                <ul class="menu">

                  <li><!-- Task item --> 
                    <a href="#">
                    <h3> Completion Percentage <small class="pull-right"> %</small> </h3>
                    <div class="progress xs">
                      <div class="progress-bar progress-bar-aqua" style=" " role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"> <span class="sr-only">  Complete</span> </div>
                    </div>
                    </a> </li>
                  <!-- end task item -->  
                </ul>
              </li>
              <li class="footer"> <a href="#">View all tasks</a> </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="{{ url() }}/images/catalog/<?php echo $collection['Profile']['avatar']; ?>" class="user-image" alt="User Image"> <span class="hidden-xs">{{$collection['users_fullname']}}</span> </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header"> <img src="{{ url() }}/images/catalog/<?php echo $collection['Profile']['avatar']; ?>" class="img-circle" alt="User Image">
                <p> {{$collection['users_fullname']}}  - 
                <?php $roles = Sentinel::findById($collection['Profile']['user_id'])->roles->first();
               echo $roles->name; ?> <small>Registered At  {{date('d-M-Y', strtotime($collection['created_at']))}}</small> </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="col-xs-4 text-center"> <a href="{{ url('/doctor/profile/settings') }}">Settings</a> </div>
                <div class="col-xs-4 text-center"> <a href="{{ url('/doctor/moveprofile/')}}">Transfer</a> </div>
                <div class="col-xs-4 text-center"> <a href="{{ url('/doctor/doctorsocialslinks/')}}">Social Links </a> </div>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left"> <a href="{{ url('/doctor/profile/')}}" class="btn btn-default btn-flat">Profile</a> </div>
                <div class="pull-right"> <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat">Sign out</a> </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li> <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image"> <img src="{{ url() }}/images/catalog/<?php echo $collection['Profile']['avatar']; ?>" class="img-circle" alt="User Image"> </div>
        <div class="pull-left info">
          <p> {{$collection['users_fullname']}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span> </div>
      </form>
      <!-- /.search form --> 
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/doctor/myreviews')}}"><i class="fa fa-circle-o"></i> Manage Reviews</a></li>

               <li class="active"><a href="{{ url('/doctor/myassociations')}}"><i class="fa fa-circle-o"></i>My Associations</a></li>
                  <li class="active"><a href="{{ url('/doctor/myprocedures')}}"><i class="fa fa-circle-o"></i>My Procedures</a></li>
          <li class="active"><a href="{{ url('/doctor/myspeciliaty')}}"><i class="fa fa-circle-o"></i>My Speciliaty</a></li>
           <li class="active"><a href="{{ url('/doctor/gallery')}}"><i class="fa fa-circle-o"></i>My Gallery/Albums</a></li>
 
 <li class="active"><a href="{{ url('/doctor/myappintments')}}"><i class="fa fa-circle-o"></i>My Appointments </a></li>

   <li class="active"><a href="{{ url('/doctor/trasferhistory')}}"><i class="fa fa-circle-o"></i>My Transfer History</a></li>

            </ul>

        </li>   </li>
      </ul>
    </section>
    <!-- /.sidebar --> 
  </aside>
 

	@yield('content')

 


 <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs"> <b>Version</b> 1.0.0 </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://nationwidephysician.com">Nation Wide Physician</a>.</strong> All rights reserved. </footer>
   
</div>
<!-- ./wrapper --> 
<!-- DataTables --> 
<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script> 
<script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script> 
<!-- SlimScroll --> 
<script src="{{ URL::asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script> 

  
<!-- FastClick --> 
<script src="{{ URL::asset('plugins/fastclick/fastclick.min.js') }}"></script> 
<!-- AdminLTE App --> 
<script src="{{ URL::asset('dist/js/app.min.js') }}"></script> 
<!-- AdminLTE for demo purposes --> 
<script src="{{ URL::asset('dist/js/demo.js') }}"></script> 
 <!-- ChartJS 1.0.1 -->
    <script src="{{ URL::asset('plugins/chartjs/Chart.min.js') }}"></script>
   
   


    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ URL::asset('dist/bootstrap-tokenfield.js') }}" charset="UTF-8"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/scrollspy.js') }}" charset="UTF-8"></script> 
    <script type="text/javascript" src="{{ URL::asset('assets/js/typeahead.bundle.min.js') }}" charset="UTF-8"></script> 

<script>
    $(function () {
      // ADD SOCIAL ICONS FIELDS
    var max_fields      = 5; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var x = $("#total_filds").val(); //initlal text box count

    $(wrapper).on("click",".add_field_button", function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important"><div class="col-sm-6" style="padding-left: 0px!important"><input  maxlength="200" type="text" name="sociallinks[]" value="" class="form-control" placeholder="Enter dr association" /><input name="link_id[]" id="link_id" type="hidden" value=""></input></div><div class="col-sm-2"><input  maxlength="5" type="text" name="sort_order[]" value="" class="form-control" placeholder="Sort Order"  /></div><div class="col-sm-4"><button class="remove_field btn btn-primary"  data-id="" >-</button>&nbsp;<button class="add_field_button btn btn-primary">+</button></div><div class="col-sm-12" style="padding-bottom: 10px; padding-top: 10px;"><input id="socialicon" name="socialicon[]" type="file" multiple class="file-loading"></div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); 
        var data_id = $(this).data("id");
        if(data_id != ''){

            $.ajax({
                  url: 'removesocials'+ '/' + data_id,
                  cache: false,
                  type: "get",
                  data: '',
                  beforeSend: function()
                  {
                   // $('#modal-body').html('loading...');
                  },
                  success: function(data){
                  }
                });

        }
        $(this).parent('div').parent('div').remove();
        x--;
    }) 
  //END SOCIAL ICONS FIELDS
         $("#example1").DataTable();
         $(document).on("change", '#country_id', function(e) {
            var country_id = $(this).val();
            var token = $('#_token').val();
             $.ajax({
                type: "POST",
                data: {country_id: country_id, _token:token},
                url: '<?php echo url('populateState');?>',
                dataType: 'json',
                success: function(json) {
                //console.log(json);
                    var $el = $("#state_id");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                            .attr("value", '').text('Please Select'));

                      $.each(json, function(value, key) {

                        $el.append($("<option></option>").attr("value", value).text(key));

                    });
                }
            });

        });

        //state_id
        //send ajax request to populate the city against state dropdown
        $(document).on("change", '#state_id', function(e) {
                  var state_id = $(this).val();
                  var token = $('#_token').val();


                  $.ajax({
                      type: "POST",
                      data: {state_id: state_id, _token:token},
                      url: '<?php echo url('populateCities');?>',
                      dataType: 'json',
                      success: function(json) {
        //console.log(json);
                          var $el = $("#city_id");
                          $el.empty(); // remove old options
                          $el.append($("<option></option>")
                                  .attr("value", '').text('Please Select'));

                            $.each(json, function(value, key) {

                              $el.append($("<option></option>")
                                      .attr("value", value).text(key));

                          });




                      }
                  });

              });

//-------------------- SHOW DOCTOR"S COMMENTS------------------------
$(document).on("click",".modalInput", function(e) {
    var detailId;
      $('#modal-body').html('');
    e.preventDefault();
     
     detailId = $(this).data("detail-id");
     
    $.ajax({
          url: '{{ URL::to("doctor/viewrecordsdetails") }}'+ '/' + detailId,
          cache: false,
          type: "get",
          data: '',
          beforeSend: function()
          {
            $('#modal-body').html('loading...');
          },
          success: function(data){ 
            $('#modal-body').html('');
            $('#modal-body').html(data);
            $('.box-footer').show();
          }
        });  
       
});


$(document).on("click",".modalEdit", function(e) {
    var detailId;
      //$('#modal-body').html('');
    e.preventDefault();
     
     detailId = $(this).data("detail-id");

    $.ajax({
          url: '{{ URL::to("visitors/editrecordsdetails") }}'+ '/' + detailId,
          cache: false,
          type: "get",
           dataType:'json',
          data: '',
          beforeSend: function()
          {
            $('.modal_body').hide();
            $('.loader_div').html('loading...');
            
          },
          success: function(data){  
            $('.modal_body').show();
            $('.loader_div').hide();
            $('#id').val(data.id);
            $('#doctor_id').val(data.doctor_id);
            $('#user_id').val(data.user_id);
            $('#comments').val(data.comment);
            $('#rating').val(data.rating);
            $('#approved').val(data.approved);
          }
        }); 
 
     });

//---------------------------------------------------------------
        
      });
    </script>
</body>
 @yield('footer')
</html>


 

