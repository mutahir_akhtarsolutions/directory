<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Nation Wide Physicians</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{ URL::asset('plugins/datatables/dataTables.bootstrap.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.css') }}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
 folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ URL::asset('dist/css/skins/_all-skins.css') }}">
<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet"> 
    <!-- Tokenfield CSS -->
<link href="{{ URL::asset('dist/css/bootstrap-tokenfield.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('assets/css/custom.css') }}" type="text/css" rel="stylesheet">
    <!-- Docs CSS -->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      

<!-- jQuery 2.1.4 --> 
<script src="{{ URL::asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script> 
<!-- Bootstrap 3.3.5 --> 
<script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script> 

@yield('head')
</head> 
<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper" >
  <header class="main-header"> 
    <!-- Logo --> 
    <a href="{{ url('/') }}" class="logo"> 
    <!-- mini logo for sidebar mini 50x50 pixels --> 
    <span class="logo-mini"><img class="img-responsive" alt="logo" src="http://directory.local.com/front_end/assets/img/logo.png"></span> 
    <!-- logo for regular state and mobile devices --> 
    <span class="logo-lg"><img class="img-responsive" alt="logo" src="http://directory.local.com/front_end/assets/img/logo.png"></span> </a> 
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation"> 
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-envelope-o"></i> <span class="label label-success">4</span> </a>
            <ul class="dropdown-menu"> 
              <li> 
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message --> 
                    <a href="#">
                    <div class="pull-left"> <img src="{{ URL::asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image"> </div>
                  
                    </a> </li>
                  <!-- end message -->
                  <li> <a href="#">
                    <div class="pull-left"> <img src="{{ URL::asset('dist/img/user3-128x128.jpg')}}" class="img-circle" alt="User Image"> </div>  
                    </a> </li>  
                  
                </ul>
              </li> 
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <span class="label label-warning">10</span> </a>
            <ul class="dropdown-menu"> 
              <li> 
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li> <a href="#"> <i class="fa fa-comments-o text-aqua"></i> {{App\Helpers\MyHelper::unApprovedComments($Profile['id'])}} Comment(s) Pending Approved  </a> </li>
                   <li> <a href="{{ url('/profiles/approvetransfer') }}"> <i class="fa fa-users text-red"></i>{{App\Helpers\MyHelper::countTransferRequests($Profile['id'])}} Doctor(s) Wants To Leave </a> </li>

                  <li> <a href="{{ url('/profiles/joiningrequest') }}"> <i class="fa fa-users text-red"></i> {{App\Helpers\MyHelper::countJoinRequests($Profile['id'])}} Doctor(s) Wants To Join</a> </li>
                  <li> <a href="#"> <i class="fa fa-shopping-cart text-green"></i> 25 sales made </a> </li>
                  <li> <a href="#"> <i class="fa fa-user text-red"></i> You changed your username </a> </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <?php
              $doctors_to_add           = $Profile['no_of_doctors'];
              $total_added_doctors      = count($doctors);
              $remaining_doctors_to_add = $doctors_to_add-$total_added_doctors;?>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-flag-o"></i> <span class="label label-danger">{{$remaining_doctors_to_add}}</span> </a>
            <ul class="dropdown-menu">
              <li class="header">You can add {{$remaining_doctors_to_add}} more doctor(s) 
              

              </li>
              <li> 
                <!-- inner menu: contains the actual data -->
                <ul class="menu">

                  <li><!-- Task item --> 
                    <a href="#">
                    <h3> Completion Percentage <small class="pull-right">{{round(($total_added_doctors/$doctors_to_add)*100)}}%</small> </h3>
                    <div class="progress xs">
                      <div class="progress-bar progress-bar-aqua" style="width: {{round((count($doctors)/$Profile['no_of_doctors'])*100)}}%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"> <span class="sr-only">{{round(($total_added_doctors/$doctors_to_add)*100)}}% Complete</span> </div>
                    </div>
                    </a> </li>
                  <!-- end task item -->  
                </ul>
              </li>
              <li class="footer"> <a href="#">View all tasks</a> </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="{{ url() }}/images/catalog/<?php echo $Profile['avatar']; ?>" class="user-image" alt="User Image"> <span class="hidden-xs">{{$Profile->member->first_name. " ".$Profile->member->last_name}}</span> </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header"> <img src="{{ url() }}/images/catalog/<?php echo $Profile['avatar']; ?>" class="img-circle" alt="User Image">
                <p> {{$Profile->member->first_name. " ".$Profile->member->last_name}} - <?php $roles = Sentinel::findById($Profile['user_id'])->roles->first();
               echo $roles->name; ?> <small>Registered At {{ date('d-M-Y', strtotime($Profile->member->created_at))}}</small> </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="col-xs-4 text-center"> <a href="{{ url('/profiles/settings') }}">Settings</a> </div>
                <div class="col-xs-4 text-center"> <a href="#">Sales</a> </div>
                <div class="col-xs-4 text-center"> <a href="#">Friends</a> </div>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left"> <a href="{{ url('/profile/').'/'.$Profile['id'] }}" class="btn btn-default btn-flat">Profile</a> </div>
                <div class="pull-right"> <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat">Sign out</a> </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li> <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image"> <img src="{{ url() }}/images/catalog/<?php echo $Profile['avatar']; ?>" class="img-circle" alt="User Image"> </div>
        <div class="pull-left info">
          <p>{{$Profile->member->first_name. " ".$Profile->member->last_name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span> </div>
      </form>
      <!-- /.search form --> 
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/profiles')}}"><i class="fa fa-circle-o"></i> Manage Profiles</a></li>
<!--<li class="active"><a href="{{ url('profiles/completeProfile').'/'.$Profile['id']}}"><i class="fa fa-circle-o"></i> Complete Profiles</a></li> -->
        </ul>

        </li>   
       
       
      </ul>
    </section>
    <!-- /.sidebar --> 
  </aside>



	@yield('content')

 


 <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs"> <b>Version</b> 2.3.0 </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved. </footer>
  
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark"> 
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content"> 
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab"> 
        <ul class="control-sidebar-menu">
          <li> <a href="javascript::;"> <i class="menu-icon fa fa-birthday-cake bg-red"></i>
            <div class="menu-info">  
            </div>
            </a> </li>
          <li> <a href="javascript::;"> <i class="menu-icon fa fa-user bg-yellow"></i>
            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4> 
            </div>
            </a> </li>
          <li> <a href="javascript::;"> <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
            <div class="menu-info"> 
            </div>
            </a> </li>
          <li> <a href="javascript::;"> <i class="menu-icon fa fa-file-code-o bg-green"></i>
            <div class="menu-info">  
            </div>
            </a> </li>
        </ul>
        <!-- /.control-sidebar-menu -->
         
        <ul class="control-sidebar-menu"> 
          <li> <a href="javascript::;">
            <h4 class="control-sidebar-subheading"> Update Resume <span class="label label-success pull-right">95%</span> </h4>
            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-success" style="width: 95%"></div>
            </div>
            </a> </li>
          <li> <a href="javascript::;">
            <h4 class="control-sidebar-subheading"> Laravel Integration <span class="label label-warning pull-right">50%</span> </h4>
            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
            </div>
            </a> </li>
          <li> <a href="javascript::;">
            <h4 class="control-sidebar-subheading"> Back End Framework <span class="label label-primary pull-right">68%</span> </h4>
            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
            </div>
            </a> </li>
        </ul>
        <!-- /.control-sidebar-menu --> 
        
      </div>
      <!-- /.tab-pane --> 
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane --> 
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>
          <div class="form-group">
            <label class="control-sidebar-subheading"> Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>
            <p> Some information about this general settings option </p>
          </div>
          <!-- /.form-group -->
          
          <div class="form-group">
            <label class="control-sidebar-subheading"> Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>
            <p> Other sets of options are available </p>
          </div>
          <!-- /.form-group -->
          
          <div class="form-group">
            <label class="control-sidebar-subheading"> Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>
            <p> Allow the user to show his name in blog posts </p>
          </div>
          <!-- /.form-group -->
          
          <h3 class="control-sidebar-heading">Chat Settings</h3>
          <div class="form-group">
            <label class="control-sidebar-subheading"> Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->
          
          <div class="form-group">
            <label class="control-sidebar-subheading"> Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->
          
          <div class="form-group">
            <label class="control-sidebar-subheading"> Delete chat history <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a> </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane --> 
    </div>
  </aside>
  <!-- /.control-sidebar --> 
  <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper --> 
<!-- DataTables --> 
<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script> 
<script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script> 
<!-- SlimScroll --> 
<script src="{{ URL::asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script> 

  
<!-- FastClick --> 
<script src="{{ URL::asset('plugins/fastclick/fastclick.min.js') }}"></script> 
<!-- AdminLTE App --> 
<script src="{{ URL::asset('dist/js/app.min.js') }}"></script> 
<!-- AdminLTE for demo purposes --> 
<script src="{{ URL::asset('dist/js/demo.js') }}"></script> 
 <!-- ChartJS 1.0.1 -->
<script src="{{ URL::asset('plugins/chartjs/Chart.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) --> 
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="{{ URL::asset('dist/bootstrap-tokenfield.js') }}" charset="UTF-8"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/scrollspy.js') }}" charset="UTF-8"></script> 
<script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap-typeahead.js') }}" charset="UTF-8"></script>

<script>
$(document).ready(function() {
    $(document).on('change', '.form-select', function(event) {
    event.preventDefault(); 
    $(this).closest('form').submit(); 
       });
   $("#example1").DataTable(); });
    </script>
    <script type="text/javascript">
 
  $(document).ready(function () {
 
 


  //send ajax request to populate the sub catagory
  <?php
       for($counter = 1;$counter<=@$no_of_doctors;$counter++){
  ?>
  $(document).on("change", '#catagory_id_<?php echo $counter;?>', function(e) {
            var catagory_id = $(this).val();
            var token = $('#_token').val();


            $.ajax({
                type: "POST",
                data: {catagory_id: catagory_id, _token:token},
                url: '<?php echo url('populateSubCatagories');?>',
                dataType: 'json',
                success: function(json) {
               //console.log(json);
                    var $el = $("#sub_catagory_<?php echo $counter;?>");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                    .attr("value", '').text('Please Select'));
                    $.each(json, function(value, key) {
                    $el.append($("<option></option>")
                    .attr("value", value).text(key));

                    });
                  }
            });

        });
  
        //adding more catagory and subCatagories on licking add buttion START
        var total;

        // To auto limit based on the number of options
        // total = $("#nativelangdrop").find("option").length - 1;

        // Hard code a limit
        total = 3;
        $("#addBtn").on("click", function() {
        var ctr = $("#form_group_2").find(".extra").length;
        if (ctr < total) {
           var ddl = $("#sub_catagory_<?php echo $counter; ?>").clone();
            ddl.attr("id", "sub_catagory_"+ctr);
            ddl.attr("name", "sub_catagory_"+ctr);
            ddl.attr('data-id',ctr)
            ddl.addClass("extra");
            $("#form_group_2").append(ddl); 
            
        }

        });
        $("#removeBtn").on("click", function() {

        var ctr  = $("#additional").find(".extra").length;

        var test = $( "#additional" ).find(".extra").last().remove( );
        //var test = $( "#additional" ).find(".extra").last().remove( );

        });
        //adding more catagory and subCatagories on licking add buttion END

        <?php
           }// END OF FOR EACH LOOP.
        ?> 
// CODE WHEN SUB CATEGORY CHANGES.
  $(document).on("change", '.sub_catagories', function(e) {
            var catagory_id = $(this).val();
            var token       = $('#_token').val();
            var proced_next = $(this).attr('data-id'); 
            $("#form_group_"+proced_next).children('#nested').remove();
             
            var html = '<div class="form-group" id="nested"><label>Enter Procedures</label>                  <input type="text" class="form-control" id="procedures_'+proced_next+'" name="procedures_'+proced_next+'" value="" placeholder="Type something and hit enter" />   </div>';
               $("#form_group_"+proced_next).append(html);

            $.ajax({
                type: "POST",
                data: {catagory_id: catagory_id, _token:token},
                url : '<?php echo url('populateProcedures');?>',
                dataType: 'json',
                success: function(json) { 
                var arr = [];
                for (var prop in json) {
                    arr.push(json[prop]);
                }
              var $el = $("#procedures_"+proced_next); 
              $el.tokenfield({
              autocomplete: {
              source: arr,
              delay: 100
            },
              showAutocompleteOnFocus: true,
              delimiter: [',', '-', '_']
          });

               }// SUCCESS ENDS
            });// AJAX ENDS HERE.

        }); 

  // DOCTOR MEMBER EDIT PAGES.
  // CODE WHEN SUB CATEGORY CHANGES.
   <?php 
        $currentRoute = Route::current();
        $params       = $currentRoute->parameters(); 
        if((Request::is('profiles/editCompleteProfile/'.@$params['id']))) 
        { 
            if(isset($vars1)){

             foreach($vars1 as &$value){
               $value = "'$value'";
            }
            
             $comma_separated = implode(",", $vars1);
          ?>
                
                var $el = $("#procedures_1"); 
                        $el.tokenfield({
                        autocomplete: {
                        source: [<?php echo $comma_separated;?>],
                        delay: 100
                      },
                      showAutocompleteOnFocus: true,
                      delimiter: [',', '-', '_']
                    });

              <?php  
            }
              if(isset($vars2)){

                  foreach($vars2 as &$value){
                      $value = "'$value'";
                      }
                      $comma_separated = implode(",", $vars2);
          ?>
                
                var $el = $("#procedures_2"); 
                        $el.tokenfield({
                        autocomplete: {
                        source: [<?php echo $comma_separated;?>],
                        delay: 100
                      },
                      showAutocompleteOnFocus: true,
                      delimiter: [',', '-', '_']
                    });
                        <?php } ?>

  $(document).on("change", '.sub_catagory', function(e) {
            var catagory_id = $(this).val();
            var token       = $('#_token').val();
            var proced_next = $(this).attr('data-id'); 
            $("#form_group_"+proced_next).children('#nested').remove();
             
            var html = '<div class="form-group" id="nested"><label>Enter Procedures</label>                  <input type="text" class="form-control" id="procedures_'+proced_next+'" name="procedures_'+proced_next+'" value="" placeholder="Type something and hit enter" />   </div>';
               $("#form_group_"+proced_next).append(html);

            $.ajax({
                type: "POST",
                data: {catagory_id: catagory_id, _token:token},
                url : '<?php echo url('populateProcedures');?>',
                dataType: 'json',
                success: function(json) { 
                var arr = [];
                for (var prop in json) {
                    arr.push(json[prop]);
                }
              var $el = $("#procedures_"+proced_next); 
                        $el.tokenfield({
                        autocomplete: {
                        source: arr,
                        delay: 100
                      },
                      showAutocompleteOnFocus: true,
                      delimiter: [',', '-', '_']
                    });

               }// SUCCESS ENDS
            });// AJAX ENDS HERE.

        });
                    
           
<?php 
        } // END OF IF.
      
        ?> 
                  }); 
          $(document).on("change", '#catagory_id', function(e) {
            var catagory_id = $(this).val();
            var token = $('#_token').val();


            $.ajax({
                type: "POST",
                data: {catagory_id: catagory_id, _token:token},
                url: '<?php echo url('populateSubCatagories');?>',
                dataType: 'json',
                success: function(json) {
                //console.log(json);
                    var $el = $("#sub_catagory");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                            .attr("value", '').text('Please Select'));

                      $.each(json, function(value, key) {
                      $el.append($("<option></option>")
                      .attr("value", value).text(key));

                    });
                  // for second sub category.
                  var $el = $("#sub_catagory_1");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                            .attr("value", '').text('Please Select'));

                      $.each(json, function(value, key) {

                      $el.append($("<option></option>")
                      .attr("value", value).text(key));

                    });




                }
            });

        });

 $(document).on("change", '#country_id', function(e) {
            var country_id = $(this).val();
            var token = $('#_token').val();


            $.ajax({
                type: "POST",
                data: {country_id: country_id, _token:token},
                url: '<?php echo url('populateState');?>',
                dataType: 'json',
                success: function(json) {
  //console.log(json);
                    var $el = $("#state_id");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                            .attr("value", '').text('Please Select'));

                      $.each(json, function(value, key) {

                        $el.append($("<option></option>")
                                .attr("value", value).text(key));

                    });




                }
            });

        });

        //state_id
        //send ajax request to populate the city against state dropdown
        $(document).on("change", '#state_id', function(e) {
                  var state_id = $(this).val();
                  var token = $('#_token').val();


                  $.ajax({
                      type: "POST",
                      data: {state_id: state_id, _token:token},
                      url: '<?php echo url('populateCities');?>',
                      dataType: 'json',
                      success: function(json) {
        //console.log(json);
                          var $el = $("#city_id");
                          $el.empty(); // remove old options
                          $el.append($("<option></option>")
                                  .attr("value", '').text('Please Select'));

                            $.each(json, function(value, key) {

                              $el.append($("<option></option>")
                                      .attr("value", value).text(key));

                          });




                      }
                  });

              });

        //-------------------- SHOW DOCTOR"S COMMENTS------------------------
$(document).on("click",".modalInput", function(e) {
    var detailId;
      $('#modal-body').html('');
    e.preventDefault();
     
     detailId = $(this).data("detail-id");
     
    $.ajax({
          url: '{{ URL::to("profiles/viewrecordsdetails") }}'+ '/' + detailId,
          cache: false,
          type: "get",
          data: '',
          beforeSend: function()
          {
            $('#modal-body').html('loading...');
          },
          success: function(data){ 
            $('#modal-body').html('');
            $('#modal-body').html(data);
            $('.box-footer').show();
          }
        });  
       
});
       

  </script>
</body>
@yield('footer')
</html>


 

