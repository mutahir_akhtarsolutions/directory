<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<?php $hplr   = \App\Helpers\NonStaticHelpers::siteSettings();?>
<title>{{$hplr->site_title}}||@yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="{{$hplr->site_metadescriptions}} @yield('description')">
<meta name="keywords" content="{{$hplr->site_keywords}} ,@yield('keywords')">
 
<meta name="author" content="@yield('author')">
<!-- Le styles -->
<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"> 
<link href="{{ url() }}/front_end/style.css" rel="stylesheet" type="text/css">

<!--jQuery UI CSS -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css"> 

<style>
  

  #loading {
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   position: fixed;
   display: block;
   opacity: 0.7;
   background-color: #fff;
   z-index: 99;
   text-align: center;
}

#loading-image {
  position: absolute;
  top: 500px;
  left: 540px;
  z-index: 100;
}
  </style>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Enable media queries on older browsers -->
<!--[if lt IE 9]>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->

@yield('head')
</head>
<body>
<div id="url" style="display: none">{{url('')}}</div>
<!--POP UP DIALOG FOR SIGNUP AS USER.-->
<div id="signupModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sign Up Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>How do you want to signup?</p>
                <p class="text-warning"><small>As a Visitor Or As a Member</small></p>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="window.location.href='{{url()}}/member'">Member</button><button type="button" class="btn btn-primary" onclick="window.location.href='{{url()}}/visitors/visitor'">Visitor</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
            </div>
        </div>
    </div>
</div>
<!--END OF SIGN UP FORM.-->
<div id="loading">
  <img id="loading-image" src="{{ url() }}/images/loader1.gif" alt="Loading..." />
</div>
<header class="header">
  <div class="header-top">
    <div class="container">
      <div class="row">
         
        <div class="col-md-3 col-sm-4 col-xs-6 log-in">
          <ul>
              @if ( \Cartalyst\Sentinel\Native\Facades\Sentinel::check() === false )
         
            <li><a href="{{url()}}/auth/login"><em class="fa fa-sign-in"></em> Login</a></li>
           <!-- <li><a href="{{url()}}/member"><em class="fa fa-pencil"></em> Register</a></li>@author Mutahir-->
           <li><a href="#" id="popsignup"><em class="fa fa-pencil"></em> Register</a></li>
            @else
            <li><a href="{{ url('/auth/logout') }}"><em class="fa fa-sign-in"></em> Logout</a></li>
         @endif
         </ul>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 text-right pull-right social-links">
          <ul>
            <li><a href="#" class="fa fa-facebook" title="Facebook"></a></li>
            <li><a href="#" class="fa fa-twitter" title="Twitter"></a></li>
            <li><a href="#" class="fa fa-google-plus" title="Google Plus"></a></li>
            <li><a href="#" class="fa fa-pinterest" title="Pinterest"></a></li>
            <li><a href="#" class="fa fa-linkedin" title="Linkedin"></a></li>
            <li><a href="#" class="fa fa-youtube-play" title="Youtube"></a></li>
          </ul>
        </div>

        <div class="dropdown social-links">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Social Media</a>
              <ul class="dropdown-menu">
            <li><a href="#" class="fa fa-facebook" title="Facebook"></a></li>
            <li><a href="#" class="fa fa-twitter" title="Twitter"></a></li>
            <li><a href="#" class="fa fa-google-plus" title="Google Plus"></a></li>
            <li><a href="#" class="fa fa-pinterest" title="Pinterest"></a></li>
            <li><a href="#" class="fa fa-linkedin" title="Linkedin"></a></li>
            <li><a href="#" class="fa fa-youtube-play" title="Youtube"></a></li>
              </ul>
            </div>



      </div>
    </div>
  </div>
  <!-- nav ---------------------->
  <div class="container">
    <div class="row">
      <figure class="col-md-4 col-sm-4 col-xs-12 logo hidden-xs"> <a href="{{url()}}"><img src="{{ url() }}/front_end/assets/img/logo.png" alt="logo" class="img-responsive"></a> </figure>
      <nav class="col-md-8 col-sm-8 col-xs-12 navbar custom-navbar">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand visible-xs" href="index.html"> <img src="{{ url() }}/front_end/assets/img/logo.png" alt="logo" class="img-responsive"></a> </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav onhover">
            <li {{ Request::is('index') ? ' class=active' : null }}><a href="{{url()}}">Home</a></li>
            <li {{ Request::is('pages*') ? ' class=active' : null }}><a href="{{ url('/pages/1') }}" title="About">About</a></li>
            <li {{ Request::is('searchDr*') ? ' class=active' : null }}><a href="{{ url('/searchDr?') }}" title="Doctors">Doctors</a></li> 
            <li {{ Request::is('contactus*') ? ' class=active' : null }}><a href="{{ url('/contactus') }}" title="Contact us">Contact us</a></li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</header>


	@yield('content')

	<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-6 footer-colm latest-post-colm">
        <h4>Latest Forum Post</h4>
        <div class="latest-forum-post">
          <p><a href="{{ url('/pages/8') }}">There are many variations of passages of Lorem Ipsum available</a></p>
          <ul>
            <li><a href="{{ url('/pages/8') }}">24 May, 2014</a></li>
            <li><a href="{{ url('/pages/8') }}">15 Comments</a></li>
          </ul>
        </div>
        <div class="latest-forum-post">
          <p><a href="{{ url('/pages/8') }}">There are many variations of passages of Lorem Ipsum available</a></p>
          <ul>
            <li><a href="{{ url('/pages/8') }}">24 May, 2014</a></li>
            <li><a href="{{ url('/pages/8') }}">15 Comments</a></li>
          </ul>
        </div>
        <div class="latest-forum-post">
          <p><a href="{{ url('/pages/8') }}">There are many variations of passages of Lorem Ipsum available</a></p>
          <ul>
            <li><a href="{{ url('/pages/8') }}">24 May, 2014</a></li>
            <li><a href="{{ url('/pages/8') }}">15 Comments</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6 footer-colm quick-links">
        <h4>Quick Links</h4>
        <ul>
          <li><a href="{{ url('/pages/8') }}">There are many variations of pas</a></li>
          <li><a href="{{ url('/pages/8') }}">Sages Lorem Ipsum available</a></li>
          <li><a href="{{ url('/pages/8') }}">Majority have suffered alteration in</a></li>
          <li><a href="{{ url('/pages/8') }}">Injected humour, or randomised</a></li>
          <li><a href="{{ url('/pages/8') }}">Words which don't look even slightly</a></li>
          <li><a href="{{ url('/pages/8') }}">Believable. If you are</a></li>
          <li><a href="{{ url('/pages/8') }}">to use a Passage of Lorem Ipsum</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6 footer-colm get-in-touch">
        <h4>Get in Touch</h4>
        <address>
        <p>{!!$hplr->address!!}</p>
        <p><em class="fa fa-phone"></em> {{$hplr->contact_phone}}</p>
        <p><em class="fa fa-envelope-o"></em> <a href="#">{{$hplr->contact_email}}</a></p>
        <p>Web: <a href="{{$hplr->site_url}}" target="_blank">{{$hplr->site_url}}</a></p>
        </address>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6 footer-colm newsletter">
        <div class="sub-newsletter">
        <h4>Subscribe Newsletter</h4> 
        
         <form action="" method="get" accept-charset="utf-8" id="form_subscribe">
  <input type="email" name="email" id="email" class="form-control" placeholder="Email Address">
          <button type="button" class="btn" id="new_submit">Subscribe</button>
        </form>

        </div>
        <div class="social-links">
        <h4>Social Media</h4>
          <ul>
            <li><a href="#" target="_blank" class="fa fa-facebook" title="Facebook"></a></li>
            <li><a href="#" target="_blank" class="fa fa-twitter" title="Twitter"></a></li>
            <li><a href="#" target="_blank" class="fa fa-google-plus" title="Google-Plus"></a></li>
            <li><a href="#" target="_blank" class="fa fa-pinterest" title="Pinterest"></a></li>
            <li><a href="#" target="_blank" class="fa fa-linkedin" title="Linkedin"></a></li>
            <li><a href="#" target="_blank" class="fa fa-youtube-play" title="youtube"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="copyrights-sect">
  <div class="container">
    <p>{!!$hplr->site_copyright!!}</p>

  </div>
</div>
</footer>

    <script type="text/javascript" src='http://code.jquery.com/jquery-1.10.2.min.js'></script>
  <!--<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>-->
   <script src="{{ url() }}/front_end/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCZBctd1qts0q3_ycTw8RSnQaqqx_cNEek"></script>
    <script src="{{ url() }}/front_end/assets/js/jquery.gmap.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{ url() }}/front_end/assets/js/customjs.js"></script>
    
    <script src="{{ url() }}/front_end/assets/js/expanding.js"></script>
    <script src="{{ url() }}/front_end/assets/js/starrr.js"></script>
    <script src="{{ url() }}/front_end/assets/js/jquery.cookie.js"></script>
     <script type="text/javascript" charset="utf-8">
    
    $(document).ready(function() {
 // Ajax for our form to login users.
  $(document).on('click', '#new_submit' , function(event) {
          event.preventDefault();  
          $('#email').next("div").remove();
          var formData = {
            email          : $('input[name=email]').val(),
            first_name     : $('input[name=first_name]').val(),
            last_name      : $('input[name=last_name]').val()
            }  
            if($('#email').val() == '')
              {
                 //$("#new_signuperrormsg").html('<div class="callout callout-danger"><p>Please Enter Email.</p></div>');
                  $('#email').after('<div class="alert-danger">Please enter email.</div>');
                return false;
              }
       else
      {
      
       // $.ajaxSetup({
            //    headers: {
            //        'X-XSRF-Token': $('meta[id="new_token"]').attr('content')
             //   }
           // }); 
      $.ajax({
        url: '{{url()}}/subscribe',
        type: "get",
        data: formData,
        dataType: 'json',
         beforeSend: function()
          {
            $('#new_submit').html('Sending...');
          },
        success: function(data) { 
           $('#new_submit').html('Subscribe');
           if(data.status == 1)
           {

            $('#email').attr('type','hidden');
            $("#new_submit").before("<br/><input type='text' value='' id='first_name' name='first_name'class='form-control' placeholder='First Name' /><input type='text' value='' id='last_name' name='last_name'class='form-control' placeholder='Last Name' />");  
            $('#email').after('<div class="alert-danger">'+data.msg+'</div>');
           }
           else if(data.status == 2)
           {

            $('#email').attr('type','email');
            $('#email').val('');
            $("#form_subscribe input").remove("#first_name");  
            $("#form_subscribe input").remove("#last_name");  
            $('#email').after('<div class="alert-danger">'+data.msg+'</div>');
            
            $('.alert-danger').next('br').remove();
           }
         else
         {
           $('#email').after('<div class="alert-danger">'+data.msg+'</div>');
         }
          }

      }); 

          
       }
      
       });
        // END FUNCTION.

    });

  </script>
<script type="text/javascript">
  // RATING SCRIPT.  
$(function() { 
  
  
      // initialize the autosize plugin on the review text area
       $('#new-review').autosize({append: "\n"});

      var reviewBox      = $('#post-review-box');
      var newReview      = $('#new-review');
      var openReviewBtn  = $('#open-review-box');
      var closeReviewBtn = $('#close-review-box');
      var ratingsField   = $('#ratings-hidden');
      var ratingsField1  = $('#ratings-hidden1');
      var ratingsField2  = $('#ratings-hidden2');

      openReviewBtn.click(function(e)
      {
        reviewBox.slideDown(400, function()
          {
            $('#new-review').trigger('autosize.resize');
            newReview.focus();
          });
        openReviewBtn.fadeOut(100);
        closeReviewBtn.show();
      });

      closeReviewBtn.click(function(e)
      {
        e.preventDefault();
        reviewBox.slideUp(300, function()
          {
            newReview.focus();
            openReviewBtn.fadeIn(200);
          });
        closeReviewBtn.hide();
        
      });

      // If there were validation errors we need to open the comment form programmatically 
      @if($errors->first('comment') || $errors->first('rating'))
        openReviewBtn.click();
      @endif

      // Bind the change event for the star rating - store the rating value in a hidden field
      $('#starrr').on('starrr:change', function(e, value){
        ratingsField.val(value); 
      });
       // Bind the change event for the star rating - store the rating value in a hidden field
      $('#starrr1').on('starrr:change', function(e, value){ 
        ratingsField1.val(value); 
      });

 // Bind the change event for the star rating - store the rating value in a hidden field
      $('#starrr2').on('starrr:change', function(e, value){ 
        ratingsField2.val(value);
      });
  // ends here.
});
//scrip for combobox starts

//SEARCHING SCRIPT WHEN WE PERFOR SEARCH.
 
  $('input[id^="checkboxProcedure_"]').not('#checkbox_all').click(function () {
            $('#checkbox_all').prop('checked', false);
   
   if($(this).is(':checked'))
   {
    var val = [];
    $('input.procedureclass:checkbox:checked').each(function(i){
      val[i] = $(this).val();});
    $('#procedures').val(val);
    $("#formnameprocedures").submit();
     
     }else{
     var val = [];
        $('input.procedureclass:checkbox:checked').each(function(i){
          val[i] = $(this).val();

        }); 
     $('#procedures').val(val);
      $("#formnameprocedures").submit();
     }
       

});
// BELOW CODE IS USED FOR SEARCH PAGE WHEN SELECTION CITIES.
   $('input[id^="city_"]').not('#checkbox_all').click(function () {
            $('#checkbox_all').prop('checked', false);

           
   if($(this).is(':checked'))
   {
      var val = [];
      $(':checkbox:checked').each(function(i){
      val[i] = $(this).val();

    });
      $('#cities').val(val);
      $("#formnamecities").submit();
     
     }else{
           var val = [];
           $(':checkbox:checked').each(function(i){
           val[i] = $(this).val();

        }); 
           $('#cities').val(val);
           $("#formnamecities").submit();
     }

    });


  // BELOW CODE IS USED FOR SEARCH PAGE WHEN SELECTION MULTIPLE CATEGORIES i.e SPECILITIES.
   $('input[id^="specialitie_"]').not('#checkbox_all').click(function () {
            $('#checkbox_all').prop('checked', false);           
   if($(this).is(':checked'))
   {
      var val = [];
      $('input.specialitieclass:checked').each(function(i){
      val[i] = $(this).val();

    });
      $('#specialities').val(val);
      $("#formnamespecialities").submit();
     
     }else{
           var val = [];
           $('input.specialitieclass:checked').each(function(i){
           val[i] = $(this).val();

        }); 
           $('#specialities').val(val);
           $("#formnamespecialities").submit();
     }

    });
// FOR SEARCH PROCEDURE PAGE SEARCHING.
$(function() {
  $('#locations').on('change', function(e) {
    $(this).closest('form').trigger('submit')
  })

   $('#state_citie').on('change', function(e) {
    $(this).closest('form').trigger('submit')
  })
    $('#state_catagories').on('change', function(e) {
    $(this).closest('form').trigger('submit')
  })
})

   $(window).load(function() {
     $('#loading').hide();
  }); 


  $(document).ready(function(){
// SEARCH FORM JQUERY STARTS HERE. 
 
  $("#remember").submit(function() {
            if($("#location").val() =="") {
                    $("#location").removeAttr("name");
            }
            if($("#catagory").val() =="") {
                    $("#catagory").removeAttr("name");
            }
            if($("#search_keyword").val() =="") {
                    $("#search_keyword").removeAttr("name");
            }
        });
//SEARCH AUTO COMPLETE 
   $('#state').val($.cookie('location_cookie'));
   $('#location').val($.cookie('location_id_cookie'));
   $('#specility').val($.cookie('category_cookie'));
   $('#catagory').val($.cookie('category_id_cookie'));

   $("#state").focusin(function() { 
   $('#state').val('');
   $('#location').val('');
   $.removeCookie('location_cookie');
   $.removeCookie('location_id_cookie');
   });
   
   $("#specility").focusin(function() { 
   $('#specility').val('');
   $('#catagory').val('');
   $.removeCookie('category_cookie');
   $.removeCookie('category_id_cookie');
   });

   var availableTags = new Array();
  <?php foreach($collection['doctors_names_autocomplete'] as $key => $doctor){ ?>
        availableTags.push('<?php echo $doctor->wholename; ?>');
    <?php } ?>

  $( "#search_keyword" ).autocomplete({
    source: availableTags,
    minLength: 3
  });

             $("#state").autocomplete({
                source: '{{url('/getjasonstates')}}',
                minLength: 3,
                select: function(event, ui) {
                    $('#location').val(ui.item.id);
                    $.cookie("location_cookie", ui.item.value, { expires : 2 });
                    $.cookie("location_id_cookie", ui.item.id, { expires : 2 });
                    $('#state').val($.cookie('location_cookie'));
                    $('#location').val($.cookie('location_id_cookie'));
                
                }
            });
  //CATEGORY AUTO COMPLETE
              $("#specility").autocomplete({
                source: '{{url('/getjasoncatagory')}}',
                minLength: 3,
                select: function(event, ui) {
                    $('#catagory').val(ui.item.id);
                    $.cookie("category_cookie", ui.item.value, { expires : 2 });
                    $.cookie("category_id_cookie", ui.item.id, { expires : 2 });
                     // TO UNSET COOKIE. $.cookie('category_cookie', null);
                }

            });
      


// SEARCH FORM JQUERY ENDS HERE.
 

 $('#popsignup').click(function(){
    $("#signupModal").modal('show');
  });
   }); 
</script>
</body>
@yield('footer')
</html>

