<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Nation Wide Physicians</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{ URL::asset('plugins/datatables/dataTables.bootstrap.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.css') }}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ URL::asset('dist/css/skins/_all-skins.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ URL::asset('plugins/cloudtags/tx3-tag-cloud.css') }}">
 

<!-- jQuery 2.1.4 --> 
<script src="{{ URL::asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script> 
<!-- Bootstrap 3.3.5 --> 
<script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script> 
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header"> 
    <!-- Logo --> 
    <a href="index2.html" class="logo"> 
    <!-- mini logo for sidebar mini 50x50 pixels --> 
    <span class="logo-mini"><b>A</b>LT</span> 
    <!-- logo for regular state and mobile devices --> 
    <span class="logo-lg"><b>Admin</b>LTE</span> </a> 
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation"> 
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-envelope-o"></i> <span class="label label-success">4</span> </a>
            <ul class="dropdown-menu">
              <li class="header">You have 40 messages</li>
              <li> 
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message --> 
                    <a href="#">
                    <div class="pull-left"> <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> </div>
                    <h4> Support Team <small><i class="fa fa-clock-o"></i> 5 mins</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                  <!-- end message -->
                  <li> <a href="#">
                    <div class="pull-left"> <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image"> </div>
                    <h4> AdminLTE Design Team <small><i class="fa fa-clock-o"></i> 2 hours</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                  <li> <a href="#">
                    <div class="pull-left"> <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image"> </div>
                    <h4> Developers <small><i class="fa fa-clock-o"></i> Today</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                  <li> <a href="#">
                    <div class="pull-left"> <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image"> </div>
                    <h4> Sales Department <small><i class="fa fa-clock-o"></i> Yesterday</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                  <li> <a href="#">
                    <div class="pull-left"> <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image"> </div>
                    <h4> Reviewers <small><i class="fa fa-clock-o"></i> 2 days</small> </h4>
                    <p>Why not buy a new awesome theme?</p>
                    </a> </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <span class="label label-warning">10</span> </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li> 
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li> <a href="#"> <i class="fa fa-users text-aqua"></i> 5 new members joined today </a> </li>
                  <li> <a href="#"> <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems </a> </li>
                  <li> <a href="#"> <i class="fa fa-users text-red"></i> 5 new members joined </a> </li>
                  <li> <a href="#"> <i class="fa fa-shopping-cart text-green"></i> 25 sales made </a> </li>
                  <li> <a href="#"> <i class="fa fa-user text-red"></i> You changed your username </a> </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li> 
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-flag-o"></i> <span class="label label-danger">0</span> </a>
            <ul class="dropdown-menu">
              <li class="header">You can add 0 more doctors </li>
              <li> 
                <!-- inner menu: contains the actual data -->
                <ul class="menu">

                  <li><!-- Task item --> 
                    <a href="#">
                    <h3> Completion Percentage <small class="pull-right">0%</small> </h3>
                    <div class="progress xs">
                      <div class="progress-bar progress-bar-aqua" style="width: 0%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"> <span class="sr-only">0% Complete</span> </div>
                    </div>
                    </a> </li>
                  <!-- end task item -->  
                </ul>
              </li>
              <li class="footer"> <a href="#">View all tasks</a> </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img  src="{{ asset('assets') }}/img/a0.jpg" class="user-image" alt="User Image"> <span class="hidden-xs">{{$data['first_name'] .' '.$data['last_name']}}</span> </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header"> <img src="{{ asset('assets') }}/img/a0.jpg" class="img-circle" alt="User Image">
                <p> {{$data['first_name'] .' '.$data['last_name']}} -
                <?php $roles = Sentinel::findById($collection['Profile']['user_id'])->roles->first();
               echo $roles->name; ?> <small>Registered At <small>Member   </small> </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="col-xs-4 text-center"> <a href="#">Followers</a> </div>
                <div class="col-xs-4 text-center"> <a href="#">Sales</a> </div>
                <div class="col-xs-4 text-center"> <a href="#">Friends</a> </div>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left"> <a href="{{ url('/profile/').'/' }}" class="btn btn-default btn-flat">Profile</a> </div>
                <div class="pull-right"> <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat">Sign out</a> </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li> <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image"> <img src="{{ asset('assets') }}/img/a0.jpg" class="img-circle" alt="User Image"> </div>
        <div class="pull-left info">
          <p>{{$data['first_name'] .' '.$data['last_name']}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span> </div>
      </form>
      <!-- /.search form --> 
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/profiles')}}"><i class="fa fa-circle-o"></i> Manage Profiles</a></li>
<!--<li class="active"><a href="{{ url('profiles/completeProfile').'/'}}"><i class="fa fa-circle-o"></i> Complete Profiles</a></li> -->

          </ul>

        </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-files-o"></i> <span>Layout Options</span> <span class="label label-primary pull-right">4</span> </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>
        <li> <a href="pages/widgets.html"> <i class="fa fa-th"></i> <span>Widgets</span> <small class="label pull-right bg-green">new</small> </a> </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-pie-chart"></i> <span>Charts</span> <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-laptop"></i> <span>UI Elements</span> <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
            <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
            <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
            <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
            <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
            <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
          </ul>
        </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-edit"></i> <span>Forms</span> <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
          </ul>
        </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-table"></i> <span>Tables</span> <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li>
        <li> <a href="pages/calendar.html"> <i class="fa fa-calendar"></i> <span>Calendar</span> <small class="label pull-right bg-red">3</small> </a> </li>
        <li> <a href="pages/mailbox/mailbox.html"> <i class="fa fa-envelope"></i> <span>Mailbox</span> <small class="label pull-right bg-yellow">12</small> </a> </li>
        <li class="treeview"> <a href="#"> <i class="fa fa-folder"></i> <span>Examples</span> <i class="fa fa-angle-left pull-right"></i> </a>
          <ul class="treeview-menu">
            <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
            <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
            <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
            <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
            <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
          </ul>
        </li> 
        <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar --> 
  </aside>



	@yield('content')

 


 <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs"> <b>Version</b> 2.3.0 </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved. </footer>
  
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark"> 
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content"> 
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li> <a href="javascript::;"> <i class="menu-icon fa fa-birthday-cake bg-red"></i>
            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
              <p>Will be 23 on April 24th</p>
            </div>
            </a> </li>
          <li> <a href="javascript::;"> <i class="menu-icon fa fa-user bg-yellow"></i>
            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
              <p>New phone +1(800)555-1234</p>
            </div>
            </a> </li>
          <li> <a href="javascript::;"> <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
              <p>nora@example.com</p>
            </div>
            </a> </li>
          <li> <a href="javascript::;"> <i class="menu-icon fa fa-file-code-o bg-green"></i>
            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
              <p>Execution time 5 seconds</p>
            </div>
            </a> </li>
        </ul>
        <!-- /.control-sidebar-menu -->
        
        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li> <a href="javascript::;">
            <h4 class="control-sidebar-subheading"> Custom Template Design <span class="label label-danger pull-right">70%</span> </h4>
            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
            </div>
            </a> </li>
          <li> <a href="javascript::;">
            <h4 class="control-sidebar-subheading"> Update Resume <span class="label label-success pull-right">95%</span> </h4>
            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-success" style="width: 95%"></div>
            </div>
            </a> </li>
          <li> <a href="javascript::;">
            <h4 class="control-sidebar-subheading"> Laravel Integration <span class="label label-warning pull-right">50%</span> </h4>
            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
            </div>
            </a> </li>
          <li> <a href="javascript::;">
            <h4 class="control-sidebar-subheading"> Back End Framework <span class="label label-primary pull-right">68%</span> </h4>
            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
            </div>
            </a> </li>
        </ul>
        <!-- /.control-sidebar-menu --> 
        
      </div>
      <!-- /.tab-pane --> 
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane --> 
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>
          <div class="form-group">
            <label class="control-sidebar-subheading"> Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>
            <p> Some information about this general settings option </p>
          </div>
          <!-- /.form-group -->
          
          <div class="form-group">
            <label class="control-sidebar-subheading"> Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>
            <p> Other sets of options are available </p>
          </div>
          <!-- /.form-group -->
          
          <div class="form-group">
            <label class="control-sidebar-subheading"> Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>
            <p> Allow the user to show his name in blog posts </p>
          </div>
          <!-- /.form-group -->
          
          <h3 class="control-sidebar-heading">Chat Settings</h3>
          <div class="form-group">
            <label class="control-sidebar-subheading"> Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->
          
          <div class="form-group">
            <label class="control-sidebar-subheading"> Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->
          
          <div class="form-group">
            <label class="control-sidebar-subheading"> Delete chat history <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a> </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane --> 
    </div>
  </aside>
  <!-- /.control-sidebar --> 
  <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper --> 
<!-- DataTables --> 
<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script> 
<script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script> 
<!-- SlimScroll --> 
<script src="{{ URL::asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script> 


<!-- FastClick --> 
<script src="{{ URL::asset('plugins/fastclick/fastclick.min.js') }}"></script> 
<!-- AdminLTE App --> 
<script src="{{ URL::asset('dist/js/app.min.js') }}"></script> 
<!-- AdminLTE for demo purposes --> 
<script src="{{ URL::asset('dist/js/demo.js') }}"></script> 
 <!-- ChartJS 1.0.1 -->
    <script src="{{ URL::asset('plugins/chartjs/Chart.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) --> 
     <script type="text/javascript" src="{{ URL::asset('plugins/cloudtags/jquery.tx3-tag-cloud.js') }}" ></script>  
<script>
      $(function () {
        $("#example1").DataTable();
        
      });
    </script>



    <script type="text/javascript">
  $(document).ready(function () {
  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');


  //send ajax request to populate the sub catagory
  <?php
       for($counter = 1;$counter<=@$no_of_doctors;$counter++){
  ?>
  $(document).on("change", '#catagory_id_<?php echo $counter;?>', function(e) {
            var catagory_id = $(this).val();
            var token = $('#_token').val();


            $.ajax({
                type: "POST",
                data: {catagory_id: catagory_id, _token:token},
                url: '<?php echo url('populateSubCatagories');?>',
                dataType: 'json',
                success: function(json) {
  //console.log(json);
                    var $el = $("#sub_catagory_<?php echo $counter;?>");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                            .attr("value", '').text('Please Select'));

                      $.each(json, function(value, key) {

                        $el.append($("<option></option>")
                                .attr("value", value).text(key));

                    });




                }
            });

        });

        //adding more catagory and subCatagories on licking add buttion START
        var total;

        // To auto limit based on the number of options
        // total = $("#nativelangdrop").find("option").length - 1;

        // Hard code a limit
        total = 5;


        $("#addBtn").on("click", function() {
        var ctr = $("#additional").find(".extra").length;
        if (ctr < total) {
           var $ddl = $("#catagory_id_<?php echo $counter; ?>").clone();
           $ddl.attr("id", "ddl" + ctr);
        $ddl.attr("name", "ddl" + ctr);
           $ddl.addClass("extra");
           $("#additional").append($ddl);
        }

        });
        $("#removeBtn").on("click", function() {

        var ctr = $("#additional").find(".extra").length;

        var test = $( "#additional" ).find(".extra").last().remove( );
        //var test = $( "#additional" ).find(".extra").last().remove( );

        });
        //adding more catagory and subCatagories on licking add buttion END

        <?php
           }
        ?>


        //getting sub Insurances

        $(document).on("change", '#p_insurance', function(e) {
                  var p_insurance = $(this).val();
                  var token = $('#_token').val();

                  $.ajax({
                      type: "POST",
                      data: {p_insurance: p_insurance, _token:token},
                      url: '<?php echo url('populateSubInsurances');?>',
                      dataType: 'json',
                      success: function(json) {

                      var $sub_ins = $("#s_p_insurance");
                      var $el = $("#sub_insurance");
                      if(typeof json =='object' && json !='' && p_insurance != 0){
                        $sub_ins.empty(); // remove old options
                        $el.css('display', 'block');
                          $sub_ins.append($("<option></option>")
                                  .attr("value", '').text('Please select sub insurance'));

                            $.each(json, function(value, key) {

                              $sub_ins.append($("<option></option>")
                                      .attr("value", value).text(key));

                          });

                        }else{
                          $sub_ins.empty(); // remove old options
                          $el.css('display', 'none');
                        }

                  }
                  });

              });


              //getting sub of sub insurances
              $(document).on("change", '#s_p_insurance', function(e) {

                        var s_p_insurance = $(this).val();
                        var token = $('#_token').val();

                        $.ajax({
                            type: "POST",
                            data: {s_p_insurance: s_p_insurance, _token:token},
                            url: '<?php echo url('populateSubOfSubInsurances');?>',
                            dataType: 'json',
                            success: function(json) {
                              // s_o_s_insurance means sub_of_sub_insurance
                            var $sub_of_sub_ins = $("#s_o_s_insurance");
                            var $el = $("#sub_of_sub_insurance");
                            if(typeof json =='object' && json !='' && s_p_insurance != 0){
                              $sub_of_sub_ins.empty(); // remove old options
                              $el.css('display', 'block');
                                $sub_of_sub_ins.append($("<option></option>")
                                        .attr("value", '').text('Please select sub of sub insurance'));

                                  $.each(json, function(value, key) {

                                    $sub_of_sub_ins.append($("<option></option>")
                                            .attr("value", value).text(key));

                                });

                              }else{
                                $sub_of_sub_ins.empty(); // remove old options
                                $el.css('display', 'none');
                              }

                        }
                        });

                    });



                    //adding more insurances and sub insurance on clicking add buttion START

                    $("#addBtn_ins").on("click", function() {

                      var total;
                      // To auto limit based on the number of options
                      // Hard code a limit
                      total = 5;
                    var ctr = $("#adding_insurrance").find(".extra").length;
                    if (ctr < total) {
                       var $ddl = $("#insurance_block").clone();
                       console.log($ddl);
                       $ddl.attr("id", "ddl" + ctr);
                    $ddl.attr("name", "ddl" + ctr);
                       $ddl.addClass("extra");
                       $("#adding_insurrance").append($ddl);
                    }

                    });
                    $("#removeBtn_ins").on("click", function() {

                    var ctr = $("#adding_insurrance").find(".extra").length;

                    var test = $( "#adding_insurrance" ).find(".extra").last().remove( );

                    });
                    //adding more insurances and sub insurance on licking add buttion END
                  });




$(document).on("change", '#catagory_id', function(e) {
            var catagory_id = $(this).val();
            var token = $('#_token').val();


            $.ajax({
                type: "POST",
                data: {catagory_id: catagory_id, _token:token},
                url: '<?php echo url('populateSubCatagories');?>',
                dataType: 'json',
                success: function(json) {
  //console.log(json);
                    var $el = $("#sub_catagory");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                            .attr("value", '').text('Please Select'));

                      $.each(json, function(value, key) {

                        $el.append($("<option></option>")
                                .attr("value", value).text(key));

                    });




                }
            });

        });

 $(document).on("change", '#country_id', function(e) {
            var country_id = $(this).val();
            var token = $('#_token').val();


            $.ajax({
                type: "POST",
                data: {country_id: country_id, _token:token},
                url: '<?php echo url('populateState');?>',
                dataType: 'json',
                success: function(json) {
  //console.log(json);
                    var $el = $("#state_id");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                            .attr("value", '').text('Please Select'));

                      $.each(json, function(value, key) {

                        $el.append($("<option></option>")
                                .attr("value", value).text(key));

                    });




                }
            });

        });

        //state_id
        //send ajax request to populate the city against state dropdown
        $(document).on("change", '#state_id', function(e) {
                  var state_id = $(this).val();
                  var token = $('#_token').val();


                  $.ajax({
                      type: "POST",
                      data: {state_id: state_id, _token:token},
                      url: '<?php echo url('populateCities');?>',
                      dataType: 'json',
                      success: function(json) {
        //console.log(json);
                          var $el = $("#city_id");
                          $el.empty(); // remove old options
                          $el.append($("<option></option>")
                                  .attr("value", '').text('Please Select'));

                            $.each(json, function(value, key) {

                              $el.append($("<option></option>")
                                      .attr("value", value).text(key));

                          });




                      }
                  });

              });

  </script>
</body>
</html>


 

