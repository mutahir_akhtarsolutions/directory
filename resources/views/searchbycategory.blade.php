@extends('layout.app')
 @section('head')
 @stop
 @section('footer')
 <script  type="text/javascript" charset="utf-8" >
     $(document).ready(function(){
// SEARCH FORM JQUERY STARTS HERE. 
   $('html, body').animate({scrollTop: $("#sortby-colm").offset().top
}, 1000); 

 });

 </script>
 @stop
@section('content') 
@include('headerListingDetail')
<!-- banner end here -->
<?php 
  $procedures_ids        = @$_GET['procedures']; 
  $specialities_ids      = @$_GET['specialities']; 
  $query_string_location = @$_GET['location']; 
  $city                  = @$_GET['state_citie'];
  $category              = Request::segment(2);
  $city                  = ($city == '0') ? '' : $city; 
  $message_variable      = '';
  $sort_by               = @$_GET['sort_by'];
  $city_nav              = '';
  $states_nav            = '';
      function stringContains($string, $needle) {
          return in_array($needle, explode(',', $string));
      }

      if (isset($query_string_location) && $query_string_location !='') {
         $state_name = App\Helpers\MyHelper::getFieldName('state' ,$query_string_location , 'id' , 'state_name'); 
          if (isset($city) && $city !='') {
         $city_name = App\Helpers\MyHelper::getFieldName('city' ,$city , 'id' , 'city_name');
         $city_name = ' , '.$city_name; 
         if(\App\Helpers\MyHelper::getFieldName('city', $city,'id','city_name')!= '0'): 
          $city_nav = '<li><a href="'.url('searchesbycategory').'/'.$category.'?location='.$query_string_location.'&state_citie='.$city.'&sort_by=id">'.\App\Helpers\MyHelper::getFieldName('city', $city,'id','city_name').'</a> <span class="divider">»</span></li>'; 
        endif;

      }

       if(\App\Helpers\MyHelper::getFieldName('state', $query_string_location,'id','state_name')!= '0'): 

       $states_nav = '<li><a href="'.url('searchesbycategory').'/'.$category.'?location='.$query_string_location.'&sort_by=id">'.\App\Helpers\MyHelper::getFieldName('state', $query_string_location,'id','state_name').'</a> <span class="divider">»</span></li>';
endif;
         $message_variable .= 'near<span>'.$state_name.@$city_name.'</span>';
      }
     
      if (isset($specialities_ids) && $specialities_ids !='') {
          $speccilitesNames = App\Helpers\MyHelper::getFieldsName('specialities' ,$specialities_ids , 'id' , 'name'); 
          $message_variable .= '  who has Specilites In <span>';
          foreach($speccilitesNames as $snames):$message_variable .= $snames.' , ';
          endforeach;
            $message_variable .='</span>';
           
      }
      if (isset($procedures_ids) && $procedures_ids !='') {
          $proceduresNames = App\Helpers\MyHelper::getFieldsName('procedures' ,$procedures_ids , 'id' , 'procedure_name'); 

          $message_variable .='<strong> Expertise '; 
           foreach($proceduresNames as $pnames): 
              $message_variable .= $pnames.' , ';
            endforeach; $message_variable .='</strong>'; 
      }
    $category_name = App\Helpers\MyHelper::getFieldName('specialities' ,Route::current()->parameter('id') , 'id' , 'name'); 
      ?>

<!-- welcome nationwide start here -->
<div class="container">
    <div class="row">
    <div class="col-md-8 col-sm-8 col-xs-12 left-content">
    <div class="row">
    <ul class="breadcrumb ">
    <li><a href="{{url()}}">Home</a><span class="divider"> » </span></li>
<li><a href="{{ url('searchesbycategory').'/'.$category}}?sort_by=id">{{\App\Helpers\MyHelper::getFieldName('specialities', $category,'id','name')}}</a> <span class="divider">»</span></li>
<?php echo $states_nav;?> <?php echo $city_nav;?>
 
</ul> 
  </div> <!--BREADCURMS ENDS -->
     <div class="row">
    <div class="col-xs-12 provider-colm">
    <h2>We found  {{$collection['users']->total()}}  <span>{{$category_name}}(s)</span> <?php echo $message_variable;?>
</h2>
<form method="get" action="" name="" id="formnamespecialities">  
<?php
 if(isset($query_string_location) && $query_string_location != 0 && $query_string_location != '') {
    ?> 
<input type="hidden" value="{{$query_string_location}}" name="location" > </input>

<?php } 
 if(isset($city) && $city != 0 && $city != '') {
    ?> 
<input type="hidden" value="{{$city}}" name="state_citie" > </input>
<?php } ?> 
<input type="hidden" value="{{$specialities_ids}}" name="specialities" id="specialities"> </input>
<?php   if(isset($procedures_ids) && $procedures_ids != 0 && $procedures_ids != '') {
    ?> 
<input type="hidden" value="{{$procedures_ids}}" name="procedures"> </input>
<?php } if (isset($sort_by) && $sort_by !='') {?>
<input type="hidden" value="{{$sort_by}}" name="sort_by"> </input>

<?php } ?>
</form>

<form method="get" action="" name="" id="formnameprocedures">  
<input type="hidden" value="{{$city}}" name="state_citie" > </input>
<input type="hidden" value="{{$query_string_location}}" name="location" > </input>
<input type="hidden" value="{{$specialities_ids}}" name="specialities"> </input>

<input type="hidden" value="{{$procedures_ids}}" name="procedures" id="procedures"> </input>
<?php  if (isset($sort_by) && $sort_by !='') {?>
<input type="hidden" value="{{$sort_by}}" name="sort_by"> </input>

<?php } ?>
</form>

<!--STATE AND CITIES-->
<form method="get" action="" name="" id="locationsForm">
 <div class="checkbox-cont col-md-6"> 
<h5><span>Select A State</span></h5> 
  <select class="form-control" name="location" id="locations">
                <option value="0">Select State</option>
                <?php
                  if(count(@$collection['locations'])> 0):
                    foreach(@$collection['locations'] as $location){?>
                <option value="{{ $location->id }}"  @if(isset($query_string_location) && $query_string_location == $location->id) selected @endif >{{ ucfirst($location->state_name) }} ( {{ App\Helpers\MyHelper::countJoinRecord('userprofile_procedure_specility_review',$location->id , 'state_id' ,$category,'specialities_id')}} )</option>
                <?php
                 }
                 endif;
                ?>
              </select>   
 
    </div>
<?php if(count(@$collection['state_cities']) > 0):?>
<div class="checkbox-cont col-md-6">
<h5><span>Select Cities </span></h5>
  <select class="form-control" name="state_citie" id="state_citie">
      <option value="0">Select City</option>
        <?php 
         foreach(@$collection['state_cities'] as $state_citie){?>
           <option value="{{ $state_citie->id }}"  @if(isset($city) && $city == $state_citie->id) selected @endif >{{ ucfirst($state_citie->city_name) }} @if(App\Helpers\MyHelper::countJoinRecord('userprofile_procedure_specility_review',$state_citie->id , 'city_id' ,$category,'specialities_id', $distinctKeyWord = 'id')) ( {{App\Helpers\MyHelper::countJoinRecord('userprofile_procedure_specility_review',$state_citie->id , 'city_id' ,$category,'specialities_id', $distinctKeyWord = 'id')}} ) @endif</option>
             <?php
               }
              ?>
   </select>
   </div>
   <?php endif; 
   if(isset($specialities_ids) && $specialities_ids != 0 && $specialities_ids != '') {
    ?> 
    <input type="hidden" value="{{$specialities_ids}}" name="specialities" id="specialities"> </input>
    <?php } if(isset($procedures_ids) && $procedures_ids != 0 && $procedures_ids != '') {
        ?> 
    <input type="hidden" value="{{$procedures_ids}}" name="procedures"> </input>
    <?php } if (isset($sort_by) && $sort_by !='') {?>
    <input type="hidden" value="{{$sort_by}}" name="sort_by"> </input>

    <?php } ?>

   </form>
<!--END STATE AND CITIES-->

<div class="checkbox-cont" style="display: <?php if (isset($city) && $city !='') {
  ?>block; <?php } else?> none;"><br />
   <h5><span>Select Sub Specilities</span></h5> <br />
    @if(count(@$collection['child_specilities'])> 0)
    <?php $pro_counter = 0;?>
    <ul>
    @foreach(@$collection['child_specilities'] as $specialitie)
     <li><label><input type="checkbox" name="specialitie[{{$pro_counter}}]" value="{{$specialitie->id}}" id="specialitie_{{$pro_counter}}" <?php if (stringContains($specialities_ids, $specialitie->id) !== false): ?>  checked="checked"  <?php endif; ?>  class="specialitieclass" > {{ucfirst($specialitie->name)}}</span></label></li> 

      <?php $pro_counter ++;?>
    @endforeach
    </ul>
    @endif
 
    </div>

   <div class="checkbox-cont" style="display: <?php if (isset($specialities_ids) && $specialities_ids !='') {
  ?>block; <?php } else?> none;">
    @if(count(@$collection['procedures'])> 0)
    <?php $pro_counter = 0;?><br />
    <h5><span>Select Sub Procedures</span></h5> <br />
    <ul>
    @foreach(@$collection['procedures'] as $procedure)
     <li><label><input type="checkbox" class="procedureclass" name="procedure[{{$pro_counter}}]" value="{{$procedure->id}}" id="checkboxProcedure_{{$pro_counter}}" <?php if (stringContains($procedures_ids, $procedure->id) !== false): ?>  checked="checked"  <?php endif; ?>  > {{ucfirst($procedure->procedure_name)}}</span></label></li> 

      <?php $pro_counter ++;?>
    @endforeach
    </ul>
    @endif
 
    </div>
    </div>

    <div class="col-xs-12 sortby-colm" id="sortby-colm">
    <div class="sortby">
    <span>
    <label>sort by :</label>
    <select>
     <option>Best match</option>
     <option>Best match 1</option>
     <option>Best match 2</option>
      <option>Best match 3</option>
     </select>
    </span>

     <?php $url = url() . $_SERVER['REQUEST_URI'];?>
    <ul>
     <li><a href="<?php if(!isset($sort_by)) { echo $url.'&sort_by=rating'; } else { echo str_replace($sort_by,"rating", $url); } ?>">Patient Satisfaction</a></li>
     <li><a href="<?php if(!isset($sort_by)) { echo $url.'&sort_by=wholename'; } else { echo str_replace($sort_by,"wholename", $url); } ?>">Name</a></li>  
    </ul>
    </div>


    </div>

    <div class="col-xs-12 dr-listing">
      <?php
 if($collection['users']->count() > 0):
         foreach( $collection['users'] as $doctor){

                 if($doctor->image_of_doctor == '')
      {
        if($doctor->gender == 'Male')
        $image_url = url().'/images/m4.png';
      else $image_url = url().'/images/f1.jpg';
      }else  $image_url = url().'/images/catalog/'.$doctor->image_of_doctor;       

      ?>
	<div class="dr-listing-colm">
      <div class="dr-list-img">
      <figure>
      <img src="{{ $image_url}}" alt="dr img" class="img-responsive">
      </figure>
      </div>

      <div class="dr-list-det">
      <h4><a href="{{ url() }}/doctorDetail/{{$doctor->user_id}}"> {{ $doctor->wholename }} ,{{$doctor->education}} </a></h4>
      <span class="dr-address"><em class="fa fa-home"></em> Primary Care Services Blount 101 Lemley Dr Suite A Oneonta, AL 3512</span>

      <ul class="dr-job">
       <li><a href="#">Insurances</a></li>
       <li><a href="#">Specialties</a></li>
      </ul>

      <ul class="dr-contact">
       <li><em class="fa fa-stethoscope"></em> Internal Medicine</li>
       <li><em class="fa fa-phone"></em> {{$doctor->contact_dr_phone_number}} (Office)</li>
      </ul>

      <div class="patient-satisfication">
      <span>Patient Satisfaction</span>
      <ul class="pt-satisf">
       <li><a href="#" class="fa fa-star"></a></li>
       <li><a href="#" class="fa fa-star"></a></li>
       <li><a href="#" class="fa fa-star"></a></li>
       <li><a href="#" class="fa fa-star-half-o"></a></li>
       <li><a href="#" class="fa fa-star-o"></a></li>
      </ul>

      </div>


      </div>
    </div>
<?php
  } else: echo '<div class="dr-listing-colm"><div class="dr-list-det">
      <h4>No Result Found Matching Your Criteria.</h4></div></div>' ;
  endif;
?>


     
   <div class=" pager-row">
    {!! $collection['users']->render() !!} 
  </div>

    </div>


    </div>
    </div>

@include('frontpages/searchpage_sidebar')

  </div>
</div>
<!-- benifit and features sections end here -->

<!-- footer ------------------------>
@endsection
