<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/23/2015
 * Time: 8:17 PM
 */

 ?>


 @extends('app')

 {{-- Page content --}}
 @section('content')

 <div class="container">

     <div class="page-header">
        <h1>{{ $mode == 'create' ? 'Create Insurance' : 'Update Insurance' }} <small>{{ $mode === 'update' ? $catagory->name : null }}</small></h1>
     </div>

     <form method="post" action="">
        <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />





<div class="form-group{{ $errors->first('ParentCatagory', ' has-error') }}">
  <label for="ParentCatagory">Parent Insurance</label>

        <select class="form-control" name="p_Insurance" id="p_Insurance">
          <option value="0">{{'Select any parent Insurance'}}</option>
          <?php if($mode== 'update') {?>

            @foreach($catagories as $p_catagry)
              <option value="{{$p_catagry->id}}" <?php if($p_catagry->id == $catagory->parent_id){ ?> selected="selected" <?php }?>   >{{$p_catagry->insurance_name}}</option>
            @endforeach
            <?php }else{?>
        @foreach($catagory as $cata)
             <option value="{{$cata->id}}">{{$cata->insurance_name}}</option>
         @endforeach
         <?php }?>
        </select>
  </div>

  <div class="form-group{{ $errors->first('SubParentCatagory', ' has-error') }}">
    <label for="ParentCatagory">Sub Parent Insurance</label>

          <select class="form-control" name="s_p_Insurance" id="s_p_Insurance">
            <option value="0">{{'Select any sub parent Insurance'}}</option>
            <?php if($mode== 'update') {?>

              @foreach($catagories as $catagry)
                <option value="{{$catagry->id}}" <?php if($catagry->id == $catagory->parent_id){ ?> selected="selected" <?php }?>   >{{$catagry->insurance_name}}</option>
              @endforeach
              <?php }else{?>
          @foreach($sub_parent_catagory as $sub_cata)
               <option value="{{$sub_cata->id}}">{{$sub_cata->insurance_name}}</option>
           @endforeach
           <?php }?>
          </select>
    </div>



        <div class="form-group{{ $errors->first('insurance_name', ' has-error') }}">

            <label for="name">Insurance Name</label>

            <input type="text"  class="form-control" name="insurance_name" id="sub_catagory" value="{{ Input::old('insurance_name', isset($catagory->insurance_name) ? $catagory->insurance_name : '') }}" placeholder="Enter the insurance.">

            <span class="help-block">{{{ $errors->first('insurance_name', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('insurance_id', ' has-error') }}">

            <label for="name">Insurance Id</label>

            <input type="text"  class="form-control" name="insurance_id" id="insurance_id" value="{{ Input::old('insurance_id', isset($catagory->insurance_id) ? $catagory->insurance_id : '') }}" placeholder="Enter the insurance ID.">

            <span class="help-block">{{{ $errors->first('insurance_id', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('insurance_address', ' has-error') }}">

            <label for="name">Insurance Address</label>

            <input type="text"  class="form-control" name="insurance_address" id="insurance_address" value="{{ Input::old('insurance_address', isset($catagory->insurance_address) ? $catagory->insurance_address : '') }}" placeholder="Enter the insurance address.">

            <span class="help-block">{{{ $errors->first('insurance_address', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('insurance_phone', ' has-error') }}">

            <label for="name">Insurance phone</label>

            <input type="text"  class="form-control" name="insurance_phone" id="insurance_phone" value="{{ Input::old('insurance_phone', isset($catagory->insurance_phone) ? $catagory->insurance_phone : '') }}" placeholder="Enter the insurance phone.">

            <span class="help-block">{{{ $errors->first('insurance_phone', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('insurance_email', ' has-error') }}">

            <label for="name">Insurance email</label>

            <input type="text"  class="form-control" name="insurance_email" id="insurance_email" value="{{ Input::old('insurance_email', isset($catagory->insurance_email) ? $catagory->insurance_email : '') }}" placeholder="Enter the insurance email.">

            <span class="help-block">{{{ $errors->first('insurance_email', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('insurance_url', ' has-error') }}">

            <label for="name">Insurance url</label>

            <input type="text"  class="form-control" name="insurance_url" id="insurance_url" value="{{ Input::old('insurance_url', isset($catagory->insurance_url) ? $catagory->insurance_url : '') }}" placeholder="Enter the insurance url.">

            <span class="help-block">{{{ $errors->first('insurance_url', ':message') }}}</span>

        </div>



        <button type="submit" class="btn btn-default">Submit</button>

     </form>
 </div>
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
 <script>

 $( document ).ready(function() {

   $(document).on("change", '#p_Insurance', function(e) {

             var p_insurance = $(this).val();
             var token = $('#_token').val();

             $.ajax({
                 type: "POST",
                 data: {p_insurance: p_insurance, _token:token},
                 url: '<?php echo url('subInsurrance');?>',
                 dataType: 'json',
                 success: function(json) {

                 var $sub_ins = $("#s_p_Insurance");


                   $sub_ins.empty(); // remove old options
                     $sub_ins.append($("<option></option>")
                             .attr("value", '').text('sub insurance'));

                       $.each(json, function(value, key) {

                         $sub_ins.append($("<option></option>")
                                 .attr("value", value).text(key));

                     });


             }
             });

         });

 });
 </script>

 @stop
