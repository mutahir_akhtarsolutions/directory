<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/23/2015
 * Time: 8:15 PM
 */
 ?>

 @extends('app')

 {{-- Page content --}}
 @section('content')

<div class="container">
     <div class="page-header">
        <h1>Insurances <span class="pull-right"><a href="{{ URL::to('insurance/create') }}" class="btn btn-warning">Create</a></span></h1>
     </div>

   @if ($insurances->count())
     Page {{ $insurances->currentPage() }} of {{ $insurances->lastPage() }}

     <div class="pull-right">
       {!! $insurances->render() !!}
     </div>
     <br><br>

     <table class="table table-bordered">
        <thead>
            <th class="col-lg-2">insurance</th>
            <th class="col-lg-3">Parent insurance</th>
            <th class="col-lg-3">Main insurance</th>
            <th class="col-lg-2">Actions</th>
        </thead>
        <tbody>
            @foreach ($insurances as $insurance)
            <tr>

                <td>{{ $insurance->insurance_name }}</td>


                <?php if ($insurance->parent_id != 0){ ?>


                <td>{{ $insurance->getInsurance->insurance_name }}</td>
                <?php } else { ?>
                <td>{{ 'Null' }}</td>
                <?php } ?>


                <?php if ($insurance->parent_id != 0){ ?>


                <td>{{ $insurance->getInsurance->insurance_name }}</td>
                <?php } else { ?>
                <td>{{ 'Null' }}</td>
                <?php } ?>

                <td>
                    <a class="btn btn-warning" href="{{ URL::to("insurance/{$insurance->id}") }}">Edit</a>
                    <a class="btn btn-danger" href="{{ URL::to("insurance/{$insurance->id}/delete") }}">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
     </table>

 Page {{ $insurances->currentPage() }} of {{ $insurances->lastPage() }}

     <div class="pull-right">
        {!! $insurances->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>
    @endif

</div>

 @endsection
