 @extends('layout.login')
@section('content')
    
        <p class="login-box-msg">Sign in to your account</p>
        <form  role="form" method="POST" action="{{ url('/auth/login') }}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row"> 
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>
<br/>
        <a href="{{ url('/password/email') }}"> I forgot my password</a><br>
        <a href="{{ url('/member') }}" class="text-center">Register as new member</a><br>OR
        <br/>
<a href="{{ url('/visitors/visitor') }}" class="text-center">Register as new visitor / patient</a><br> 
 @endsection