 @extends('layout.login')
@section('content')
    
        <p class="login-box-msg">  <strong>Sign up to become member</strong></p>
        <form  role="form" method="POST" action="{{ url('/auth/register') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					 <div class="form-group has-feedback">
           <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" required>
          </div>
           <div class="form-group has-feedback">
           <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name" required>
          </div>

          <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>

           <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Password" required>
          </div>

           <div class="form-group has-feedback">
          <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
          </div> 
          <div class="row"> 
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign up</button>
            </div><!-- /.col -->
          </div>
        </form>
 <br> 
 <a href="{{ url('/auth/login') }}"> Login </a><br> 
        <a href="{{ url('/password/email') }}"> I forgot my password</a><br> 
 @endsection