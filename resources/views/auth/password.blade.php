 @extends('layout.login')
@section('content')
    
        <p class="login-box-msg">Reset Password</p>
      
 
					  <form  role="form" method="POST" action="{{ url('/password/email') }}">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div> 
          <div class="row"> 
            <div class="col-xs-8">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Send Password Reset Link</button>
            </div><!-- /.col -->
          </div>
        </form> <br>
        
 <a href="{{ url('/auth/login') }}"> Login </a><br> 
       <a href="{{ url('/member') }}" class="text-center">Register as new member</a><br/>
        OR
        <br/>

<a href="{{ url('/visitors/visitor') }}" class="text-center">Register as new visitor / patient</a><br/> <br/> 
 @endsection