<!DOCTYPE html>
<html lang="en" class="">
<head>
  <meta charset="utf-8" />
  <title>Directory system</title>
  <meta name="description" content="app, web app, responsive, responsive layout, admin, admin panel, admin dashboard, flat, flat ui, ui kit, AngularJS, ui route, charts, widgets, components" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="stylesheet" href="{{ asset('assets/css') }}/libs/assets/animate.css/animate.css" type="text/css" />
  <link rel="stylesheet" href="{{ asset('assets/css') }}/libs/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="{{ asset('assets/css') }}/libs/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />

  <link rel="stylesheet" href="{{ asset('assets/css') }}/libs/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="{{ asset('assets/css') }}/src/css/font.css" type="text/css" />
  <link rel="stylesheet" href="{{ asset('assets/css') }}/src/css/app.css" type="text/css" />


<!--CSS-->

<style type="text/css">
body {
    margin-top:40px;
}
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style>


</head>
<body>
<div class="app app-header-fixed ">


    <!-- header -->
  <header id="header" class="app-header navbar" role="menu">
      <!-- navbar header -->
      <div class="navbar-header bg-dark">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="#/" class="navbar-brand text-lt">
          <i class="fa fa-btc"></i>
          <img src="{{ asset('assets') }}/img/logo.png" alt="." class="hide">
          <span class="hidden-folded m-l-xs">DS</span>
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
        <!-- buttons -->
        <div class="nav navbar-nav hidden-xs">
          <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="app-aside-folded" target=".app">
            <i class="fa fa-dedent fa-fw text"></i>
            <i class="fa fa-indent fa-fw text-active"></i>
          </a>
          <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="icon-user fa-fw"></i>
          </a>
        </div>
        <!-- / buttons -->

        <!-- link and dropdown -->
        <ul class="nav navbar-nav hidden-sm">
          <li class="dropdown pos-stc">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
              <span>Mega</span>
              <span class="caret"></span>
            </a>
            <div class="dropdown-menu wrapper w-full bg-white">
              <div class="row">
                <div class="col-sm-4">
                  <div class="m-l-xs m-t-xs m-b-xs font-bold">Pages <span class="badge badge-sm bg-success">10</span></div>
                  <div class="row">
                    <div class="col-xs-6">
                      <ul class="list-unstyled l-h-2x">
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Profile</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Post</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Search</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Invoice</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-xs-6">
                      <ul class="list-unstyled l-h-2x">
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Price</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Lock screen</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Sign in</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Sign up</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 b-l b-light">
                  <div class="m-l-xs m-t-xs m-b-xs font-bold">UI Kits <span class="label label-sm bg-primary">12</span></div>
                  <div class="row">
                    <div class="col-xs-6">
                      <ul class="list-unstyled l-h-2x">
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Buttons</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Icons <span class="badge badge-sm bg-warning">1000+</span></a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Grid</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Widgets</a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-xs-6">
                      <ul class="list-unstyled l-h-2x">
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Bootstap</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Sortable</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Portlet</a>
                        </li>
                        <li>
                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Timeline</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 b-l b-light">
                  <div class="m-l-xs m-t-xs m-b-sm font-bold">Analysis</div>
                  <div class="text-center">
                    <div class="inline">
                      <div ui-jq="easyPieChart" ui-options="{
                          percent: 65,
                          lineWidth: 50,
                          trackColor: '#e8eff0',
                          barColor: '#23b7e5',
                          scaleColor: false,
                          size: 100,
                          rotate: 90,
                          lineCap: 'butt',
                          animate: 2000
                        }">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
              <span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#" translate="header.navbar.new.PROJECT">Projects</a></li>
              <li>
                <a href>
                  <span class="badge bg-info pull-right">5</span>
                  <span translate="header.navbar.new.TASK">Task</span>
                </a>
              </li>
              <li><a href translate="header.navbar.new.USER">User</a></li>
              <li class="divider"></li>
              <li>
                <a href>
                  <span class="badge bg-danger pull-right">4</span>
                  <span translate="header.navbar.new.EMAIL">Email</span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
        <!-- / link and dropdown -->

        <!-- search form -->
        <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" data-target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">
          <div class="form-group">
            <div class="input-group">
              <input type="text" ng-model="selected" typeahead="state for state in states | filter:$viewValue | limitTo:8" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search projects...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
        </form>
        <!-- / search form -->

        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
              <i class="icon-bell fa-fw"></i>
              <span class="visible-xs-inline">Notifications</span>
              <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
            </a>
            <!-- dropdown -->
            <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                  <strong>You have <span>2</span> notifications</strong>
                </div>
                <div class="list-group">
                  <a href class="list-group-item">
                    <span class="pull-left m-r thumb-sm">
                      <img src="{{ asset('assets') }}/img/a0.jpg" alt="..." class="img-circle">
                    </span>
                    <span class="clear block m-b-none">
                      Use awesome animate.css<br>
                      <small class="text-muted">10 minutes ago</small>
                    </span>
                  </a>
                  <a href class="list-group-item">
                    <span class="clear block m-b-none">
                      1.0 initial released<br>
                      <small class="text-muted">1 hour ago</small>
                    </span>
                  </a>
                </div>
                <div class="panel-footer text-sm">
                  <a href class="pull-right"><i class="fa fa-cog"></i></a>
                  <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                </div>
              </div>
            </div>
            <!-- / dropdown -->
          </li>
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">

                <img src="{{ url() }}/images/catalog/<?php echo $data['avatar']; ?>" >
                <i class="on md b-white bottom"></i>
              </span>
              <span class="hidden-sm hidden-md"><?php  echo $data->member->first_name. " ".$data->member->last_name;  ?> </span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">
              <li class="wrapper b-b m-b-sm bg-light m-t-n-xs">
                <div>
                  <p>300mb of 500mb used</p>
                </div>
                <div class="progress progress-xs m-b-none dker">
                  <div class="progress-bar progress-bar-info" data-toggle="tooltip" data-original-title="50%" style="width: 50%"></div>
                </div>
              </li>
              <!--<li>
                <a href>
                  <span class="badge bg-danger pull-right">30%</span>
                  <span>Settings</span>
                </a>
              </li>-->
            <!--  <li>
                <a ui-sref="app.page.profile">Profile</a>
              </li>-->
              <!--<li>
                <a ui-sref="app.docs">
                  <span class="label bg-info pull-right">new</span>
                  Help
                </a>
              </li>-->
              <li class="divider"></li>
              <li>
                <a href="{{ url('/auth/logout') }}">Logout</a>
                <!--<a ui-sref="access.signin">Logout</a>-->
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->
      </div>
      <!-- / navbar collapse -->
  </header>
  <!-- / header -->


    <!-- aside -->
  <aside id="aside" class="app-aside hidden-xs bg-dark">
      <div class="aside-wrap">
        <div class="navi-wrap">
          <!-- user -->
          <div class="clearfix hidden-xs text-center hide" id="aside-user">
            <div class="dropdown wrapper">
              <a href="app.page.profile">
                <span class="thumb-lg w-auto-folded avatar m-t-sm">

                  <img  src="{{ url() }}/images/catalog/<?php echo $data->avatar; ?> " class="img-full" alt="...">
                  <!--<img src="{{ asset('assets') }}/img/a0.jpg" class="img-full" alt="...">-->
                </span>
              </a>
              <a href="#" data-toggle="dropdown" class="dropdown-toggle hidden-folded">
                <span class="clear">
                  <span class="block m-t-sm">
                    <strong class="font-bold text-lt"><?php echo $data->member->first_name. " ".$data->member->last_name; ?></strong>
                    <b class="caret"></b>
                  </span>
                  <span class="text-muted text-xs block">Art Director</span>
                </span>
              </a>
              <!-- dropdown -->
              <ul class="dropdown-menu animated fadeInRight w hidden-folded">
                <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
                  <span class="arrow top hidden-folded arrow-info"></span>
                  <div>
                    <p>300mb of 500mb used</p>
                  </div>
                  <div class="progress progress-xs m-b-none dker">
                    <div class="progress-bar bg-white" data-toggle="tooltip" data-original-title="50%" style="width: 50%"></div>
                  </div>
                </li>
                <li>
                  <a href>Settings</a>
                </li>
                <li>
                  <a href="page_profile.html">Profile</a>
                </li>
                <li>
                  <a href>
                    <span class="badge bg-danger pull-right">3</span>
                    Notifications
                  </a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="page_signin.html">Logout</a>
                </li>
              </ul>
              <!-- / dropdown -->
            </div>
            <div class="line dk hidden-folded"></div>
          </div>
          <!-- / user -->

          <!-- nav -->
          <nav ui-nav class="navi clearfix">
            <ul class="nav">



              <li>
                <a href class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
                  <span class="font-bold">Dashboard</span>
                </a>
                <ul class="nav nav-sub dk">
                  <li class="nav-sub-header">
                    <a href>
                      <span>Dashboard</span>
                    </a>
                  </li>
                  <li>
                    <a href="index.html">
                      <span>Dashboard v1</span>
                    </a>
                  </li>
                  <li>
                    <a href="dashboard.html">
                      <b class="label bg-info pull-right">N</b>
                      <span>Dashboard v2</span>
                    </a>
                  </li>
                </ul>
              </li>


              <li class="line dk hidden-folded"></li>

              <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                <span>Your Stuff</span>
              </li>
              <!--<li>
                <a href="page_profile.html">
                  <i class="icon-user icon text-success-lter"></i>
                  <b class="badge bg-success pull-right">30%</b>
                  <span>Profile</span>
                </a>
              </li>-->




            </ul>
          </nav>
          <!-- nav -->


        </div>
      </div>
  </aside>
  <!-- / aside -->


  <!-- content -->
  <div id="content" class="app-content" role="main">
  	<div class="app-content-body ">


<div class="hbox hbox-auto-xs hbox-auto-sm">
  <div class="col">

    <!--Comment the create or update profile upto here-->



    <div class="padder">
<p> Complete profile </p>
      <!-- form for profile builder starts -->
      <div class="container">

<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Step 1</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
        <p>Step 2</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Step 3</p>
      </div>
    </div>
  </div>
<!--addordinalnumber function-->
<?php

  function addOrdinalNumberSuffix($num) {
    if (!in_array(($num % 100),array(11,12,13))){
      switch ($num % 10) {
        // Handle 1st, 2nd, 3rd
        case 1:  return $num.'st';
        case 2:  return $num.'nd';
        case 3:  return $num.'rd';
      }
    }
    return $num.'th';
  }

?>
<!--{{ url('profile') }}-->
  <form role="form" action="<?php echo url(); ?>/fullCompleteProfile" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />
 <datalist id="datalist1">
             @foreach($association_names as $association_name)
            <option value="{{$association_name->name}}">{{$association_name->name}} </option>
             @endforeach
          </datalist>
    <input type="hidden" name="user_id" value="{{ $data->user_id }}" />
    <input type="hidden" name="profile_id" value="{{ $data->id }}" />
    <input type="hidden" name="number_of_doctors" value="{{ $data->no_of_doctors }}" />

    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Specialty Info (Step 1)</h3>
          <?php
           $no_of_doctors = $data->no_of_doctors;
           for($counter = 1;$counter<=$no_of_doctors;$counter++){
           ?>
          <h4><?php echo addOrdinalNumberSuffix($counter) ?> Doctor Information</h4>
          <!--<div class="form-group">
            <label class="control-label">Doctor Name</label>
            <input  maxlength="100" type="text" name="doctor_name[]" value="" required="required" class="form-control" placeholder="Enter Doctor Name"  />
          </div>-->

          <div class="form-group">
            <label class="control-label">First Name</label>
            <input  maxlength="100" type="text" name="first_name[]" value="" required="required" class="form-control" placeholder="Enter first name"  />
          </div>
          <div class="form-group">
            <label class="control-label">Last Name</label>
            <input  maxlength="100" type="text" name="last_name[]" value="" required="required" class="form-control" placeholder="Enter last name"  />
          </div>


<!--catagory and sub catagory start-->
          <div class="form-group">
            <label class="control-label">catagory </label>
            <select name="catagory_id[]"  class="form-control m-b" id="catagory_id_<?php echo $counter; ?>">
              <option>select catagory</option>


              <?php foreach($catagory as $main_cat){ ?>

              <option value="<?php echo $main_cat->id; ?>"><?php echo $main_cat->name; ?></option>
              <?php } ?>

           </select>
          </div>

          <div class="form-group">
            <label class="control-label">Sub catagory </label>
            <select name="sub_catagory[]" class="form-control m-b" id="sub_catagory_<?php echo $counter; ?>">
              <option>select sub catagory</option>

              <?php foreach($catagories as $sub_cate){ ?>

              <option value="<?php echo $sub_cate->id; ?>"> <?php echo $sub_cate->name; ?></option>
              <?php } ?>



           </select>
         </div>
<!--adding more cate and subcat-->
<span id="additional"></span>

<input id="addBtn" type="button" value=" + " />
<input id="removeBtn" type="button" value=" - " />
<!--catagory and sub catagory end-->


          <div class="form-group">
            <label class="control-label">Qualification </label>
            <input  maxlength="100" type="text" name="education[]" value="" required="required" class="form-control" placeholder="Enter education"  />
          </div>

          <div class="form-group">
            <label class="control-label">Affiliation </label>
            <input  maxlength="100" type="text" name="affiliation[]" value="" required="required" class="form-control" placeholder="Enter Affiliation "  />
          </div>

          <div class="form-group">
            <label class="control-label">Contact Number </label>
            <input  maxlength="100" type="text" name="contact_dr_phone_number[]" value="" required="required" class="form-control" placeholder="Enter dr contact number"  />
          </div>

          <div class="form-group">
            <label class="control-label">Email </label>
            <input  maxlength="100" type="text" name="email[]" value="" required="required" class="form-control" placeholder="Enter dr email"  />
          </div>
          <div class="form-group">
            <label class="control-label">Association </label>
            <input  maxlength="100" type="text" name="association[]" value="" required="required" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>
           

          </div>

          <div class="form-group">
            <label class="control-label">Practice_id </label>
            <input  maxlength="100" type="text" name="Practice_id[]" value="" required="required" class="form-control" placeholder="Enter practice"  />
          </div>

          <div class="form-group">
            <label class="control-label">Social profile links of doctor </label>
            <input  maxlength="100" type="text" name="social_media_link[]" value=""  class="form-control" placeholder="Enter Social profile links of doctor"  />
          </div>

          <div class="form-group">
            <label class="control-label">Doctor Image </label>
            <input ui-jq="filestyle" type="file" name="image_of_doctor[]" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
          </div>

          <hr>

<?php } ?>



          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-2">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Schedule availability (Step 2)</h3>


          <?php
           $no_of_doctors = $data->no_of_doctors;
           for($counter = 1;$counter<=$no_of_doctors;$counter++){
           ?>

          <!--Opening hours start-->

                  <h2>Opening hours for <?php echo addOrdinalNumberSuffix($counter) ?> doctor</h2>
                  <p>Please select the opening hours:</p>
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Days</th>
                        <th>Open</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Sunday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                      </td>


                      </tr>
                      <tr>
                        <td>Monday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_monday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                      <tr>
                        <td>Tuesday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_tuesday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>

                      <tr>
                        <td>Wednesday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_wednesday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                      <tr>
                        <td>Thursday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_thursday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                      <tr>
                        <td>Friday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_friday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                      <tr>
                        <td>Saturday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_saturday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                    </tbody>
                  </table>

<!--Opening hours end-->

<?php
 }
?>



          <!--<button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>-->
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-3">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> payment accepted (Step 3)</h3>

          <!--<div class="form-group">
            <label class="control-label">Insurance </label>
            <input  maxlength="100" type="text" name="insurance" value=""  class="form-control" placeholder="Enter Social profile links of doctor"  />
          </div>-->
          <div id="insurance_block">
          <div class="form-group">
            <label class="control-label">Insurance </label>
            <select name="p_insurance[]"  class="form-control m-b" id="p_insurance">
              <option value="0">select insurance</option>


              <?php foreach($insurances as $insurance){ ?>

              <option value="<?php echo $insurance->id; ?>"><?php echo $insurance->insurance_name; ?></option>
              <?php } ?>

           </select>
          </div>


          <div class="form-group" id="sub_insurance" style="display:none">
            <label class="control-label">Sub Insurance </label>
            <select name="s_p_insurance[]"  class="form-control m-b" id="s_p_insurance">
              <option value="0">select sub insurance</option>
           </select>
          </div>

          <div class="form-group" id="sub_of_sub_insurance" style="display:none">
            <label class="control-label">Sub of Sub Insurance </label>
            <select name="s_o_s_insurance[]"  class="form-control m-b" id="s_o_s_insurance">
              <option value="0">select sub of sub insurance</option>
           </select>
          </div>
        </div>

          <!--adding more cate and subcat-->
          <span id="adding_insurrance"></span>

          <input id="addBtn_ins" type="button" value=" + " />
          <input id="removeBtn_ins" type="button" value=" - " />
          <!--catagory and sub catagory end-->

          <div class="form-group">
            <label class="control-label">Cash</label>

              <div class="checkbox">
                  <input type="checkbox" name="cash" id="cash" value="">
              </div>
              </div>

              <div class="form-group">
              <label class="control-label">credit card</label>

              <div class="checkbox">
                  <input type="checkbox" name="credit_card" id="credit_card" value="">
              </div>
              </div>
              <div class="form-group">
              <label class="control-label">copay</label>

              <div class="checkbox">
                <input type="checkbox" name="copay" id="copay" value="">
              </div>

            </div>

          <div class="form-group">
            <label class="control-label">Others </label>
            <input  maxlength="100" type="text" name="other" value=""  class="form-control" placeholder="Enter Social profile links of doctor"  />
          </div>

          <!--<div class="form-group">
            <label class="control-label">Payment Method</label>
            <select name="Payment-gateway" class="form-control m-b">
              <option>option 1</option>
              <option>option 2</option>
              <option>option 3</option>
              <option>option 4</option>
           </select>
         </div>-->
          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
        </div>
      </div>
    </div>
  </form>

</div>
<!-- form for profile builder End -->



    </div>






  </div>




</div>


	</div>
  </div>
  <!-- /content -->

  <!-- footer -->
  <footer id="footer" class="app-footer" role="footer">
    <div class="wrapper b-t bg-light">
      <span class="pull-right">2.0.3 <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
      &copy; 2015 Copyright.
    </div>
  </footer>
  <!-- / footer -->



</div>

<script src="{{ asset('assets/css') }}/libs/jquery/jquery/dist/jquery.js"></script>
<script src="{{ asset('assets/css') }}/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
<script src="{{ asset('assets/js') }}/ui-load.js"></script>
<script src="{{ asset('assets/js') }}/ui-jp.config.js"></script>
<script src="{{ asset('assets/js') }}/ui-jp.js"></script>
<script src="{{ asset('assets/js') }}/ui-nav.js"></script>
<script src="{{ asset('assets/js') }}/ui-toggle.js"></script>
<script src="{{ asset('assets/js') }}/ui-client.js"></script>


<!--<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>-->
<script type="text/javascript">
  $(document).ready(function () {
  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');


  //send ajax request to populate the sub catagory
  <?php
       for($counter = 1;$counter<=$no_of_doctors;$counter++){
  ?>
  $(document).on("change", '#catagory_id_<?php echo $counter;?>', function(e) {
            var catagory_id = $(this).val();
            var token = $('#_token').val();


            $.ajax({
                type: "POST",
                data: {catagory_id: catagory_id, _token:token},
                url: '<?php echo url('populateSubCatagories');?>',
                dataType: 'json',
                success: function(json) {
  //console.log(json);
                    var $el = $("#sub_catagory_<?php echo $counter;?>");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                            .attr("value", '').text('Please Select'));

                      $.each(json, function(value, key) {

                        $el.append($("<option></option>")
                                .attr("value", value).text(key));

                    });




                }
            });

        });

        //adding more catagory and subCatagories on licking add buttion START
        var total;

        // To auto limit based on the number of options
        // total = $("#nativelangdrop").find("option").length - 1;

        // Hard code a limit
        total = 5;


        $("#addBtn").on("click", function() {
        var ctr = $("#additional").find(".extra").length;
        if (ctr < total) {
           var $ddl = $("#catagory_id_<?php echo $counter; ?>").clone();
           $ddl.attr("id", "ddl" + ctr);
        $ddl.attr("name", "ddl" + ctr);
           $ddl.addClass("extra");
           $("#additional").append($ddl);
        }

        });
        $("#removeBtn").on("click", function() {

        var ctr = $("#additional").find(".extra").length;

        var test = $( "#additional" ).find(".extra").last().remove( );
        //var test = $( "#additional" ).find(".extra").last().remove( );

        });
        //adding more catagory and subCatagories on licking add buttion END

        <?php
           }
        ?>


        //getting sub Insurances

        $(document).on("change", '#p_insurance', function(e) {
                  var p_insurance = $(this).val();
                  var token = $('#_token').val();

                  $.ajax({
                      type: "POST",
                      data: {p_insurance: p_insurance, _token:token},
                      url: '<?php echo url('populateSubInsurances');?>',
                      dataType: 'json',
                      success: function(json) {

                      var $sub_ins = $("#s_p_insurance");
                      var $el = $("#sub_insurance");
                      if(typeof json =='object' && json !='' && p_insurance != 0){
                        $sub_ins.empty(); // remove old options
                        $el.css('display', 'block');
                          $sub_ins.append($("<option></option>")
                                  .attr("value", '').text('Please select sub insurance'));

                            $.each(json, function(value, key) {

                              $sub_ins.append($("<option></option>")
                                      .attr("value", value).text(key));

                          });

                        }else{
                          $sub_ins.empty(); // remove old options
                          $el.css('display', 'none');
                        }

                  }
                  });

              });


              //getting sub of sub insurances
              $(document).on("change", '#s_p_insurance', function(e) {

                        var s_p_insurance = $(this).val();
                        var token = $('#_token').val();

                        $.ajax({
                            type: "POST",
                            data: {s_p_insurance: s_p_insurance, _token:token},
                            url: '<?php echo url('populateSubOfSubInsurances');?>',
                            dataType: 'json',
                            success: function(json) {
                              // s_o_s_insurance means sub_of_sub_insurance
                            var $sub_of_sub_ins = $("#s_o_s_insurance");
                            var $el = $("#sub_of_sub_insurance");
                            if(typeof json =='object' && json !='' && s_p_insurance != 0){
                              $sub_of_sub_ins.empty(); // remove old options
                              $el.css('display', 'block');
                                $sub_of_sub_ins.append($("<option></option>")
                                        .attr("value", '').text('Please select sub of sub insurance'));

                                  $.each(json, function(value, key) {

                                    $sub_of_sub_ins.append($("<option></option>")
                                            .attr("value", value).text(key));

                                });

                              }else{
                                $sub_of_sub_ins.empty(); // remove old options
                                $el.css('display', 'none');
                              }

                        }
                        });

                    });



                    //adding more insurances and sub insurance on clicking add buttion START

                    $("#addBtn_ins").on("click", function() {

                      var total;
                      // To auto limit based on the number of options
                      // Hard code a limit
                      total = 5;
                    var ctr = $("#adding_insurrance").find(".extra").length;
                    if (ctr < total) {
                       var $ddl = $("#insurance_block").clone();
                       console.log($ddl);
                       $ddl.attr("id", "ddl" + ctr);
                    $ddl.attr("name", "ddl" + ctr);
                       $ddl.addClass("extra");
                       $("#adding_insurrance").append($ddl);
                    }

                    });
                    $("#removeBtn_ins").on("click", function() {

                    var ctr = $("#adding_insurrance").find(".extra").length;

                    var test = $( "#adding_insurrance" ).find(".extra").last().remove( );

                    });
                    //adding more insurances and sub insurance on licking add buttion END









});
  </script>



</body>
</html>
