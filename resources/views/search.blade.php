@extends('layout.app')
<?php 
  function stringContains($string, $needle) {
          return in_array($needle, explode(',', $string));
      }
      $procedures_ids        = @$_GET['procedures']; 
      $specialities_ids      = @$_GET['specialities']; 
      $query_string_location = @$_GET['location'];
      $city                  = @$_GET['state_citie']; 
      $catagegory            = @$_GET['catagory']; 
      $sort_by               = @$_GET['sort_by'];
      $state                 = Request::segment(2);
      $message_variable      = '';
      $city_nav              = '';
      $specialities_nav      = '';
   if (isset($city) && $city !=''  && $city !=0) {

if(\App\Helpers\MyHelper::getFieldName('city', $city,'id','city_name') !='0'):
         $city_name = App\Helpers\MyHelper::getFieldName('city' ,$city , 'id' , 'city_name'); 
         $message_variable .= '  '.$city_name.'</span>';

         $city_nav = '<li><a href="'.url('searchesbystates').'/'.$collection['doctors'][0]->state_id.'?state_citie='.$city.'&sort_by=id">'.\App\Helpers\MyHelper::getFieldName('city', $city,'id','city_name').'</a> <span class="divider">»</span></li>'; 
       endif;
      } 

      if (isset($specialities_ids) && $specialities_ids !='') {
          $speccilitesNames = App\Helpers\MyHelper::getFieldsName('specialities' ,$specialities_ids , 'id' , 'name'); 
          $message_variable .= '  who has Specilites In <span>';
        foreach($speccilitesNames as $snames):$message_variable .= $snames.' , ';
          endforeach;
            $message_variable .='</span>';

          
            $specialities_nav = '<li><a href="'.url('searchesbystates').'/'.$state.'?state_citie='.$city.'&'.'catagory='.$catagegory.'&sort_by=id">'.\App\Helpers\MyHelper::getFieldName('specialities', $catagegory,'id','name').'</a> <span class="divider">»</span></li>';  
           
      }
      if (isset($procedures_ids) && $procedures_ids !='') {
          $proceduresNames = App\Helpers\MyHelper::getFieldsName('procedures' ,$procedures_ids , 'id' , 'procedure_name'); 

          $message_variable .=' Expertise <span>'; 
           foreach($proceduresNames as $pnames): 
              $message_variable .= $pnames.' , ';
            endforeach; $message_variable .='</span>'; 
      }
    $searchCriteria = App\Helpers\MyHelper::getFieldName('state' ,Route::current()->parameter('id') , 'id' , 'state_name'); 
 
 $state_breadcurm =    \App\Helpers\MyHelper::getFieldName('state', $state,'id','state_name');
?>
<!-- welcome nationwide start here -->

@section('title') {{$state_breadcurm}}@stop
@section('description') @stop
@section('keywords') @stop

@section('author')@stop

 @section('head')
 @stop
 @section('footer')
 <script  type="text/javascript" charset="utf-8" >
     $(document).ready(function(){
// SEARCH FORM JQUERY STARTS HERE. 
   $('html, body').animate({scrollTop: $("#sortby-colm").offset().top
}, 1000); 

 });

 </script>
 @stop
@section('content') 
@include('headerListingDetail')
<!-- banner end here -->

 
 
<div class="container">

  <div class="row">
    <div class="col-md-8 col-sm-8 col-xs-12 left-content">
    <div class="row">
    <ul class="breadcrumb ">
    <li><a href="{{url()}}">Home</a><span class="divider"> » </span></li>
<li><a href="{{ url('searchesbystates').'/'.$state}}?sort_by=id">{{$state_breadcurm }}</a> <span class="divider">»</span></li> <?php echo $city_nav.' '.$specialities_nav;?>
</ul> 
  </div> <!--BREADCURMS ENDS -->
     <div class="row">
    <div class="col-xs-12 provider-colm">
    <h2>We found {{$collection['users']->total()}}  <span>Providers</span> near <span>{{$searchCriteria}}, <?php echo $message_variable; ?> </h2>

<form method="get" action="" name="" id="formnamespecialities">
<input type="hidden" value="{{$city}}" name="state_citie"> </input>

<input type="hidden" value="{{$catagegory}}" name="catagory"> </input>

<input type="hidden" value="{{$specialities_ids}}" name="specialities" id="specialities"> </input>

<?php  if (isset($procedures_ids) && $procedures_ids !='') {?>
<input type="hidden" value="{{$procedures_ids}}" name="procedures"> </input>
<?php } ?>

<?php  if (isset($sort_by) && $sort_by !='') {?>
<input type="hidden" value="{{$sort_by}}" name="sort_by"> </input>

<?php } ?>
</form>

<form method="get" action="" name="" id="formnameprocedures">  
<input type="hidden" value="{{$city}}" name="state_citie" > </input>
<input type="hidden" value="{{$catagegory}}" name="catagory" > </input>
<input type="hidden" value="{{$specialities_ids}}" name="specialities"> </input>
<input type="hidden" value="{{$procedures_ids}}" name="procedures" id="procedures"> </input>
<?php  if (isset($sort_by) && $sort_by !='') {?>
<input type="hidden" value="{{$sort_by}}" name="sort_by"> </input>

<?php } ?>
</form> 
<form method="get" action="" name="" id="locationsForm">
   <div class="checkbox-cont col-md-6">
   <h5><span>Select City</span></h5>  
<?php if(count(@$collection['cities'])> 0):?>
  <select class="form-control" name="state_citie" id="state_citie">
      <option value="0">Select City</option>
        <?php 
         foreach(@$collection['cities'] as $state_citie){ ?>
           <option value="{{ $state_citie->id }}"  @if(isset($city) && $city == $state_citie->id) selected @endif >{{ $state_citie->city_name }} ( {{ App\Helpers\MyHelper::countJoinRecord('userprofile_procedure_specility_review',$state_citie->id , 'city_id' ,$state,'state_id') }} )</option>
             <?php
               }
              ?>
   </select>  
 
     <?php endif;
                ?>
<?php if (isset($catagegory) && $catagegory !='' && $catagegory !=0 ) { ?>
<input type="hidden" value="{{$catagegory}}" name="catagory" > </input>

<?php } if (isset($specialities_ids) && $specialities_ids !='' && $specialities_ids !=0 ) { ?>
<input type="hidden" value="{{$specialities_ids}}" name="specialities" id="specialities"> </input>
<?php }  if (isset($procedures_ids) && $procedures_ids !='') {?>
<input type="hidden" value="{{$procedures_ids}}" name="procedures"> </input>
<?php } ?>
<?php if (isset($sort_by) && $sort_by !='') { ?>
<input type="hidden" value="{{$sort_by}}" name="sort_by"> </input><?php } ?>
 
 </div>
 </form>
<form method="get" action="" name=""> 
<div class="checkbox-cont col-md-6"> 
  
    <?php if(isset($city) && $city != 0) {
    ?> <input type="hidden" value="{{$city}}" name="state_citie" > </input>
 
  <h5><span>Select A Speciality</span></h5> 
<?php if(count(@$collection['catagories'])> 0): ?>
  <select class="form-control" name="catagory" id="state_catagories">

      <option value="0">Select Specilities</option>
        <?php 
         foreach(@$collection['catagories'] as $state_catagories){?>
           <option value="{{ $state_catagories->id }}"  @if(isset($catagegory) && $catagegory == $state_catagories->id) selected @endif >{{ ucfirst($state_catagories->name) }}( {{ App\Helpers\MyHelper::countJoinRecord('userprofile_procedure_specility_review',$state_catagories->id , 'specialities_id' ,$state,'state_id',$city,'city_id') }} )</option>
             <?php
               }
              ?>
   </select>   
     <?php endif; ?>
     <?php } if (isset($sort_by) && $sort_by !='') { ?>
<input type="hidden" value="{{$sort_by}}" name="sort_by"> </input><?php } ?>
   
    </div> 
 </form>
  
<div class="checkbox-cont">
    @if(count(@$collection['child_specilities'])> 0)
    <br><br><br>
  <h5><span>Select Sub Specilities</span></h5> 
    <?php $pro_counter = 0;?>
    <ul>
    @foreach(@$collection['child_specilities'] as $specialitie)
     <li><label><input type="checkbox" name="specialitie[{{$pro_counter}}]" value="{{$specialitie->id}}" id="specialitie_{{$pro_counter}}" <?php if (stringContains($specialities_ids, $specialitie->id) !== false): ?>  checked="checked"  <?php endif; ?>  class="specialitieclass" > {{ucfirst($specialitie->name)}}</span></label></li><?php $pro_counter ++;?>
    @endforeach
    </ul>
    @endif
 
    </div>

      <div class="checkbox-cont" style="display: <?php if (isset($specialities_ids) && $specialities_ids !='') {
  ?>block; <?php } else?> none;">
    @if(count(@$collection['procedures'])> 0)
      <br>
  <h5><span>Select Procedures</span></h5><br/> 
    <?php $pro_counter = 0;?>
    <ul>
    @foreach(@$collection['procedures'] as $procedure)
     <li><label><input type="checkbox" class="procedureclass" name="procedure[{{$pro_counter}}]" value="{{$procedure->id}}" id="checkboxProcedure_{{$pro_counter}}" <?php if (stringContains($procedures_ids, $procedure->id) !== false): ?>  checked="checked"  <?php endif; ?>  > {{ucfirst($procedure->procedure_name)}}</span></label></li> 

      <?php $pro_counter ++;?>
    @endforeach
    </ul>
    @endif
 
    </div>
    </div>

    <div class="col-xs-12 sortby-colm" id="sortby-colm">
    <div class="sortby">
    <span>
    <label>sort by :</label>
    <select>
     <option>Best match</option>
     <option>Best match 1</option>
     <option>Best match 2</option>
     <option>Best match 3</option>
     </select>
     </span>

     <?php $url = $_SERVER['REQUEST_URI'];?>
    <ul>
     <li><a href="<?php if(!isset($sort_by)) { echo $url.'&sort_by=rating'; } else { echo str_replace($sort_by,"rating", $url); } ?>">Patient Satisfaction</a></li>
     <li><a href="<?php if(!isset($sort_by)) { echo $url.'&sort_by=wholename'; } else { echo str_replace($sort_by,"wholename", $url); } ?>">Name</a></li>  
    </ul>
    </div>


    </div>

    <div class="col-xs-12 dr-listing">
      <?php

         foreach( $collection['users'] as $doctor){

            if($doctor->image_of_doctor == '')
      {
        if($doctor->gender == 'Male')
        $image_url = url().'/images/m4.png';
      else $image_url = url().'/images/f1.jpg';
      }else  $image_url = url().'/images/catalog/'.$doctor->image_of_doctor;       

      ?>
	<div class="dr-listing-colm">
      <div class="dr-list-img">
      <figure>
      <img src="{{ $image_url}}" alt="dr img" class="img-responsive">
      </figure>
      </div>

      <div class="dr-list-det">
      <h4><a href="{{ url() }}/doctorDetail/{{$doctor->user_id}}"> {{ $doctor->wholename }} ,{{$doctor->education}} </a></h4>
      <span class="dr-address"><em class="fa fa-home"></em>{{\App\Helpers\MyHelper::getBusinessNameOfDoctor($doctor->parent_id)}}</span>

      <ul class="dr-job">
       <li><a href="#">Insurances</a></li>
       <li><a href="#">Specialties</a></li>
      </ul>

      <ul class="dr-contact">
       <li><em class="fa fa-stethoscope"></em> Internal Medicine</li>
       <li><em class="fa fa-phone"></em> {{$doctor->contact_dr_phone_number}} (Office)</li>
      </ul>

      <div class="patient-satisfication">
      <span>Patient Satisfaction</span>
       <div class="stars-listing-doctors">
      
      @for ($i=1; $i <= 5 ; $i++)
      <span class="glyphicon glyphicon-star{{ ($i <= $doctor->averageRating) ? '' : '-empty'}}"></span>
       @endfor</div>  

      </div></div></div>
      <?php }
      ?>

   <div class=" pager-row">
    {!! $collection['users']->render() !!}
  </div>

    </div>


    </div>
    </div>

   @include('frontpages/searchpage_sidebar')
  </div>
</div>
<!-- benifit and features sections end here -->

<!-- footer ------------------------>
@endsection
