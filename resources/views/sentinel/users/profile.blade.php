 @extends('app')

 {{-- Page content --}}
 @section('content')

 <div class="container">
 <div class="row">
 <div class="col-md-2">
 </div>
 <div class="col-md-6">
     <div class="page-header">
        <h1>Update User Profile</small></h1>
     </div>
    {!! Form::open(array('method' => 'post', 'autocomplete' => 'off','enctype'=>'multipart/form-data')) !!}
 <input type="hidden" name="profile_id" value="{{$collection['Profile'] ->id}}">
     <div class="form-group">
            <label class="control-label">Contact Telephone</label>
            <input  maxlength="100" type="text" name="phone" value="{{ Input::old('phone', isset($collection['Profile'] ->phone) ? $collection['Profile'] ->phone : '' ) }}" required="required" class="form-control" placeholder="Enter Telephon"  />
          </div>  

          <div class="form-group">
            <label class="control-label">Contact Mobile</label>
            <input  maxlength="100" type="text" name="mobile" value="{{ Input::old('mobile', isset($collection['Profile'] ->mobile) ? $collection['Profile'] ->mobile : '' ) }}" required="required" class="form-control" placeholder="Enter Mobile"  />
          </div> 
           <div class="form-group">
            <label class="control-label">Profile Image</label>
            <input ui-jq="filestyle" type="file" name="avatar" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
          </div>
           <div class="form-group">
            <img src="<?php echo url(); ?>/images/catalog/<?php echo @$collection['Profile'] ->avatar; ?>" alt="doctor image" height="60" width="60">
          </div> 
          <hr>  

             <div class="form-group">
            <label class="control-label">Education </label>
              <input  maxlength="100" type="text" name="education" value="{{ Input::old('education', isset($collection['Profile'] ->education) ? $collection['Profile'] ->education : '' ) }}"   class="form-control" placeholder="Enter Education"  />
            
          </div>
         <div class="form-group">
            <label class="control-label">Country</label>
            <select name="country_id"  class="form-control m-b" id="country_id">
              <option>select country</option>


              <?php foreach(@$collection['countries'] as $contry){ ?>

              <option value="<?php echo $contry->id; ?>" @if($contry->id == $collection['Profile']->country_id) selected="selected" @endif ><?php echo $contry->country_name; ?></option>
              <?php } ?>

           </select>
          </div>

          <div class="form-group">
            <label class="control-label">State</label>
            <select name="state_id" class="form-control m-b" id="state_id">
              <option>select state</option>

              <?php foreach($collection['states'] as $stat){ ?>

              <option value="<?php echo $stat->id; ?>" @if($stat->id == $collection['Profile']->state_id) selected="selected" @endif ><?php echo $stat->state_name; ?></option>
              <?php } ?>



           </select>
          </div>

          <div class="form-group">
            <label class="control-label">City</label>
            <select name="city_id" class="form-control m-b" id="city_id">
              <option>select city</option>
              <?php foreach($collection['cities'] as $city){ ?>

              <option value="<?php echo $city->id; ?>" @if($city->id == $collection['Profile']->city_id) selected="selected" @endif ><?php echo $city->city_name; ?></option>
              <?php } ?>
           </select>
          </div>

          <div class="form-group">
            <label class="control-label">Zip Code</label>
            <input maxlength="200" type="text" name="zip_code" value="{{ Input::old('zip_code', isset($collection['Profile'] ->zip_code) ? $collection['Profile'] ->zip_code : '' ) }}" required="required" class="form-control" placeholder="Enter Zip Code" />
          </div>


           <div class="form-group"> 

            <button type="submit" class="btn btn-default">Update</button>
          </div>    
     
 <div class="form-group"> 
 
          </div> 
       

     {!! Form::close() !!}
     </div><!--COL MD 6 ENDS HERE.-->
      <div class="col-md-2">
 </div>
     </div> <!--ROW ENDS HERE.-->
 </div>

 @endsection
