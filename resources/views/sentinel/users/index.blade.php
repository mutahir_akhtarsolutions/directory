 @extends('app')

 {{-- Page content --}}
 @section('head')
 @stop
 @section('footer')
 <script  type="text/javascript" charset="utf-8">
 
$(".approvestatus").click(function(e){  
    e.preventDefault();
  var dataId    = $(this).attr("data-id"); 
$('#msg-container').html('<img src="{{ URL::asset('images/276.gif')}}">');
$.ajax({
   url: "{{url()}}/users/changeUserStatus/",
   data: { dataId: dataId},
   error: function(data) {
      $('#msg-container').html('<div class="alert alert-danger alert-block"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button><strong> Error </strong> Oops There is an error !</div>');
       
   },
   dataType: 'text',
   success: function(data) {
    $('#msg-container').html('<div class="alert alert-success alert-block"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button><strong>Success </strong> '+data+'</div>');
   // $('#td_'+dataId).html(data);
   },
   type: 'GET'
});


 

});// END OF DOCUMENT READY FUNCTION.


$(".removeLink").click(function(e){  
    e.preventDefault();
  var dataId    = $(this).attr("data-id"); 
$('#msg-container').html('<img src="{{ URL::asset('images/276.gif')}}">');
$.ajax({
   url: "{{url()}}/users/removeActivationLink/",
   data: { dataId: dataId},
   error: function(data) {
      $('#msg-container').html('<div class="alert alert-danger alert-block"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button><strong>Error</strong>Oops There is an error !</div>');
       
   },
   dataType: 'text',
   success: function(data) {
    $('#msg-container').html('<div class="alert alert-success alert-block"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button><strong>Success</strong>'+data+'</div>');
   // $('#td_'+dataId).html(data);
   },
   type: 'GET'
});
});// END OF DOCUMENT READY FUNCTION.



$(".activate").click(function(e){  
    e.preventDefault();
  var dataId    = $(this).attr("data-id"); 
$('#msg-container').html('<img src="{{ URL::asset('images/276.gif')}}">');
$.ajax({
   url: "{{url()}}/users/manualActivateUser/",
   data: { dataId: dataId},
   error: function(data) {
      $('#msg-container').html('<div class="alert alert-danger alert-block"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button><strong>Error</strong>Oops There is an error !</div>');
       
   },
   dataType: 'text',
   success: function(data) {
    $('#msg-container').html('<div class="alert alert-success alert-block"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button><strong>Success</strong>'+data+'</div>');
   // $('#td_'+dataId).html(data);
   },
   type: 'GET'
});
});// END OF DOCUMENT READY FUNCTION.
</script>
 @stop
 @section('content')

<div class="container">


     <div class="page-header">
        <h1>Users <span class="pull-right"><a href="{{ URL::to('users/create') }}" class="btn btn-warning">Create</a></span></h1>
     </div>

     @if ($users->count())
     Page {{ $users->currentPage() }} of {{ $users->lastPage() }}

     <div class="pull-right">
        {!! $users->render() !!}
     </div>

     <br><br>

     <table class="table table-bordered">
        <thead>
            <th class="col-lg-2">Name</th>
            <th class="col-lg-2">Email</th>
            <th class="col-lg-1">Role</th>
            <th class="col-lg-2">Status</th>
            <th class="col-lg-4">Actions</th>
        </thead>
        <tbody> 
            @foreach ($users as $user)
            
            <tr>
                <td>
                <?php $userrole = $user->getRoles()->first();

         


            ?>   {{ $user->first_name }} {{ $user->last_name }}</pre></td>
                <td>{{ $user->email }} </td>
                <td>{{ $userrole['name'] }} </td>
                <td id="#td_{{$user->id}}">
<?php  echo App\Helpers\MyHelper::CheckingUserActivation($user->id);?></td>

                <td>
                

                    <a class="btn btn-warning" href="{{ URL::to("users/{$user->id}") }}">Edit</a>@if($userrole['id'] != '1')
                      <a class="btn btn-warning" href="{{ URL::to("users/profile/{$user->id}") }}">Profile</a>
                    <a class="btn btn-danger" href="{{ URL::to("users/{$user->id}/delete") }}">Delete</a>

                <div class="btn-group">
                      <button class="btn btn-success" type="button">Activation</button>
                      <button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul role="menu" class="dropdown-menu">
                        <li><a class="approvestatus" href="" data-id="{{$user->id}}" data-value="{{$user->id}}" title="Send Activation Code">Send Activation Link</a></li>
                        <li><a href="#" class="removeLink" data-id="{{$user->id}}" data-value="{{$user->id}}" title="Remov Activation" >Remove Activation</a></li>
                         
                        <li class="divider"></li>
                        <li><a href="#" class="activate" data-id="{{$user->id}}" data-value="{{$user->id}}" title="Activate User">Activate</a></li>
                      </ul>
                    </div>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
     </table>

     Page {{ $users->currentPage() }} of {{ $users->lastPage() }}

     <div class="pull-right">
         {!! @$users->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>

</div>
@endif

@endsection
