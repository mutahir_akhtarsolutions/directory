 @extends('app')

 {{-- Page content --}}
 @section('content')

 <div class="container">
  <div class="row">
 <div class="col-md-2">
 </div>
 <div class="col-md-6">
     <div class="page-header">
        <h1>{{ $mode == 'create' ? 'Create User' : 'Update User' }} <small>{{ $mode === 'update' ? $user->name : null }}</small></h1>
     </div>
    {!! Form::open(array('method' => 'post', 'autocomplete' => 'off')) !!}

        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="form-group{{ $errors->first('first_name', ' has-error') }}">

            <label for="first_name">First Name</label>

            <input type="text" class="form-control" name="first_name" id="first_name" value="{{ Input::old('first_name', $user->first_name) }}" placeholder="Enter the user first name.">

            <span class="help-block">{{{ $errors->first('first_name', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('last_name', ' has-error') }}">

            <label for="name">Last Name</label>

            <input type="text" class="form-control" name="last_name" id="last_name" value="{{ Input::old('last_name', $user->last_name) }}" placeholder="Enter the user last name.">

            <span class="help-block">{{{ $errors->first('last_name', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('email', ' has-error') }}">

            <label for="email">Email</label>

            <input type="text" class="form-control" name="email" id="email" value="{{ Input::old('email', $user->email) }}" placeholder="Enter the user email.">

            <span class="help-block">{{{ $errors->first('email', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('password', ' has-error') }}">

            <label for="password">Password</label>

            <input type="password" class="form-control" name="password" id="password" value="" placeholder="Enter the user password (only if you want to modify it).">

            <span class="help-block">{{{ $errors->first('password', ':message') }}}</span>

        </div>
    <?php $userrole = $user->getRoles();

 //print_r($user);
if( count( $user->roles) == 0) $current_role = 0;else $current_role = $user->roles[0]['id'];
  
 ?>


        <div class="form-group{{ $errors->first('role', ' has-error') }}">
        <label for="role">Role</label>     
           @if($current_role == 1)
    <input type="hidden" class="form-control" name="role" id="role" value="{{$current_role}}" placeholder="{{$user->roles[0]['name']}}">
         <input type="text" disabled="" class="form-control" name="admin"  value="" placeholder="{{$user->roles[0]['name']}}">
         @else
                    
            {!! Form::select('role', $roles, $current_role ,array('class'=>"form-control")) !!}
            @endif
            <span class="help-block">{{{ $errors->first('role', ':message') }}}</span>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>

     {!! Form::close() !!}

     </div><!--COL MD 6 ENDS HERE.-->
      <div class="col-md-2">
 </div>
     </div> <!--ROW ENDS HERE.-->
 </div> <!--CONTAINER ENDS HERE.-->

 @endsection
