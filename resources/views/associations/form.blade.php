 @extends('app')

 {{-- Page content --}}
 @section('content')

 <div class="container">

     <div class="page-header">
        <h1>{{ $mode == 'create' ? 'Create Association' : 'Update Association' }} <small>{{ $mode === 'update' ? $association->name : null }}</small></h1>
     </div>

     <form method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />




        <div class="form-group{{ $errors->first('name', ' has-error') }}">

            <label for="name">Association Name</label>

            <input type="text"  class="form-control" name="name" id="name" value="{{ Input::old('name', isset($association->name) ? $association->name : '') }}" placeholder="Associations name.">

            <span class="help-block">{{{ $errors->first('name', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('assoc_logo', ' has-error') }}">

            <label for="name">Logo</label> 

            <input type="file"  name="assoc_logo" id="assoc_logo"  placeholder="Logo">

            <span class="help-block">{{{ $errors->first('assoc_logo', ':message') }}}</span>
<span>@if(isset($association->assoc_logo) && $association->assoc_logo != '')<img  src="{{ url() }}/images/associations/{{ $association->assoc_logo }}" > @endif</span>
        </div>

        <button type="submit" class="btn btn-default">{{ $mode == 'create' ? 'Submit' : 'Update' }}</button>

     </form>
 </div>

 @stop
