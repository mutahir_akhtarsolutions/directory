 @extends('app')
{{-- Page content --}}
@section('content')
<div class="container">
     <div class="page-header">
        <h1>Associations Managment <span class="pull-right"><a href="{{ URL::to('associations/create') }}" class="btn btn-warning">Create</a></span></h1>
     </div>

     @if ($collection['allAssociations']->count())
     Page {{ $collection['allAssociations']->currentPage() }} of {{ $collection['allAssociations']->lastPage() }}

     <div class="pull-right">
        {!! $collection['allAssociations']->render() !!}
     </div>

     <br> 

     <table class="table table-bordered">
        <thead>
            <th class="col-lg-2">Title</th> 
            <th class="col-lg-2">Logos</th> 
            <th class="col-lg-4">Actions</th>
        </thead>
        <tbody>
            @foreach ($collection['allAssociations'] as $data)
            <tr>
                <td>{{$data->name}}</td>  
                 <td> <img height="50px" width="50px" src="{{ url() }}/images/associations/{{$data->assoc_logo}}" alt="..." class="img-circle"></td>  
                <td><a class="btn btn-warning" href="{{ URL::to("associations/{$data->id}") }}">Edit</a>
               
 
               <!-- <a href="#" class="modalInput btn btn-warning" data-toggle="modal"   data-detail-id="{{$data->id }}" data-target="#showdetails" >View</a> -->
<a class="btn btn-danger" href="{{ URL::to("associations/{$data->id}/delete") }}">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
     </table>

     Page {{ $collection['allAssociations']->currentPage() }} of {{ $collection['allAssociations']->lastPage() }}

     <div class="pull-right">
         {!! @$collection['allAssociations']->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>

</div>
@endif

@endsection
<!-- Modal Dialog --> 
<div class="modal fade" id="showdetails" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Details</h4> 
      </div>
      <div class="modal-body"> 


        <p id="modal-body">Be patient contents are loading.</p>
      </div>
    </div>
  </div>
</div>