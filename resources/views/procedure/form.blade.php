@extends('app')

 {{-- Page content --}}
 @section('content')

 <div class="container">
<div class="row">
<div class="col-md-offset-2 col-md-6">
     <div class="page-header">
        <h1>{{ $mode == 'create' ? 'Create procedures' : 'Update procedures' }}</h1>
     </div>

     <form method="post" action="">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="form-group{{ $errors->first('procedure_name', ' has-error') }}">

            <label for="procedure_name">Procedure Name</label>

            <input type="text" class="form-control" name="procedure_name" id="procedure_name" value="{{ Input::old('procedure_name', isset($procedures->procedure_name) ? $procedures->procedure_name : '' ) }}" placeholder="Enter the procedure name."> 
            <span class="help-block">{{{ $errors->first('procedure_name', ':message') }}}</span>

        </div> 
           <div class="form-group{{ $errors->first('specialitie_id', ' has-error') }}">

            <label for="procedure_name">Select A Speciality</label>
            <select name="specialitie_id" id="specialitie_id" class="form-control">
            @foreach($categories as $cats)
                <option value="{{$cats->id}}" @if($cats->id == @$procedures->specialitie_id) selected="" @endif>{{$cats->name}}</option>
            @endforeach
            </select>
 
            <span class="help-block">{{{ $errors->first('specialitie_id', ':message') }}}</span>

        </div>

        <button type="submit" class="btn btn-default">Submit</button>&nbsp;<button type="button" class="btn btn-default" onclick="window.location.assign('{{ url('/procedures') }}')"> << Cancel </button>

     </form>
     </div>
     </div> <!--ROW ENDS HERE-->
 </div>

 @stop
