<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/23/2015
 * Time: 8:17 PM
 */

 ?>


 @extends('app')

 {{-- Page content --}}
 @section('content')

 <div class="container">

     <div class="page-header">
        <h1>{{ $mode == 'create' ? 'Create Package' : 'Update Package' }} <small>{{ $mode === 'update' ? $package->role->name : null }}</small></h1>
     </div>

     <form method="post" action="">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="form-group{{ $errors->first('name', ' has-error') }}">

            <label for="name">Name</label>

            <input type="text" class="form-control" name="name" id="name" value="{{ Input::old('name', isset($package->role->name) ? $package->role->name : '' ) }}" placeholder="Enter the package name.">

            <span class="help-block">{{{ $errors->first('slug', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('fee', ' has-error') }}">

            <label for="fee">Fee</label>

            <input type="number" min="0" class="form-control" name="fee" id="fee" value="{{ Input::old('fee', isset($package->fee) ? $package->fee : '') }}" placeholder="Enter the Package fee.">

            <span class="help-block">{{{ $errors->first('fee', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('duration', ' has-error') }}">

            <label for="duration">Duration (In days)</label>

            <input type="number" min="0" class="form-control" name="duration" id="duration" value="{{ Input::old('duration', isset($package->duration) ? $package->duration : '') }}" placeholder="Enter the Package duration.">

            <span class="help-block">{{{ $errors->first('duration', ':message') }}}</span>

        </div>

        <button type="submit" class="btn btn-default">Submit</button>

     </form>
 </div>

 @stop
