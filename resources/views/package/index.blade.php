<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/23/2015
 * Time: 8:15 PM
 */
 ?>

 @extends('app')

 {{-- Page content --}}
 @section('content')

<div class="container">
     <div class="page-header">
        <h1>Package <span class="pull-right"><a href="{{ URL::to('package/create') }}" class="btn btn-warning">Create</a></span></h1>
     </div>

  @if ($packages->count())
     Page {{ $packages->currentPage() }} of {{ $packages->lastPage() }}

     <div class="pull-right">
       {!! $packages->render() !!}
     </div>

     <table class="table table-bordered">
        <thead>
            <th class="col-lg-3">Name</th>
            <th class="col-lg-3">Fee</th>
            <th class="col-lg-4">Duration</th>
            <th class="col-lg-2">Actions</th>
        </thead>
        <tbody>
            @foreach ($packages as $package)
            <tr>
                <td>{{ $package->role->name }}</td>
                <td>{{ $package->fee }}</td>
                <td>{{ $package->duration }}</td>
                <td>
                    <a class="btn btn-warning" href="{{ URL::to("package/{$package->id}") }}">Edit</a>
                    <a class="btn btn-danger" href="{{ URL::to("package/{$package->id}/delete") }}">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
     </table>

         Page {{ $packages->currentPage() }} of {{ $packages->lastPage() }}

     <div class="pull-right">
        {!! $packages->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>
    @endif

</div>

 @endsection
