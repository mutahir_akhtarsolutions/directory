<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Nation Wide Physicians</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Le styles -->
<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
<link href="{{ url() }}/front_end/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="{{ url() }}/front_end/style.css" rel="stylesheet" type="text/css">
<!--jQuery UI CSS -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<style>
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
  }
  </style>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Enable media queries on older browsers -->
<!--[if lt IE 9]>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
</head>
<body>
<header class="header">
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-6 log-in">
          <ul>
            <li><a href="{{url()}}/auth/login"><em class="fa fa-sign-in"></em> Login</a></li>
            <li><a href="{{url()}}/member"><em class="fa fa-pencil"></em> Register</a></li>
          </ul>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 text-right pull-right social-links">
          <ul>
            <li><a href="#" class="fa fa-facebook" title="Facebook"></a></li>
            <li><a href="#" class="fa fa-twitter" title="Twitter"></a></li>
            <li><a href="#" class="fa fa-google-plus" title="Google Plus"></a></li>
            <li><a href="#" class="fa fa-pinterest" title="Pinterest"></a></li>
            <li><a href="#" class="fa fa-linkedin" title="Linkedin"></a></li>
            <li><a href="#" class="fa fa-youtube-play" title="Youtube"></a></li>
          </ul>
        </div>

        <div class="dropdown social-links">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Social Media</a>
              <ul class="dropdown-menu">
            <li><a href="#" class="fa fa-facebook" title="Facebook"></a></li>
            <li><a href="#" class="fa fa-twitter" title="Twitter"></a></li>
            <li><a href="#" class="fa fa-google-plus" title="Google Plus"></a></li>
            <li><a href="#" class="fa fa-pinterest" title="Pinterest"></a></li>
            <li><a href="#" class="fa fa-linkedin" title="Linkedin"></a></li>
            <li><a href="#" class="fa fa-youtube-play" title="Youtube"></a></li>
              </ul>
            </div>



      </div>
    </div>
</div>