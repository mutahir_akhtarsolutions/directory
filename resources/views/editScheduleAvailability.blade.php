@extends('layout.memberapp')
@section('head')
<link rel="stylesheet" href="{{URL::asset('dist/build/css/bootstrap-datetimepicker.min.css') }}"> 
@stop
@section('footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js">
  
</script>
<script src="{{ URL::asset('dist/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">
            $(function () {
                $('.datetimepicker3').datetimepicker({
                    //format: 'LT',
                    format: 'H:mm'
                });
                 });
                 </script>

@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Update 
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section> 
        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><p> Update Dr.<?php echo $doctor->first_name.' '.$doctor->last_name; ?> </p>
</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">

<!--{{ url('profile') }}-->
  <form role="form" action="<?php echo url(); ?>/profiles/updateSchedule" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />

    <input type="hidden" name="profile_id" value="{{ $dr_Profile->id }}" />


    <div class="row" id="step-2">
      <div class="col-xs-8 col-md-offset-3">
        <div class="col-md-12">
          <h3> Schedule availability</h3>

          <!--Opening hours start-->

                  <table class="table">
                    <thead>
                      <tr>
                         <th class="col-xs-2">Days</th>
                        <th class="col-xs-2">Open</th>
                        <th class="col-xs-2">From Time</th>
                        <th class="col-xs-2">To Time</th>
                        <th class="col-xs-2">Break Time</th>
                        <th class="col-xs-2">Break Duration</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($schedules as $key => $schedule) { ?>


                      <tr>
                        <td><?php echo ucfirst($schedule->days);?></td>
                        <td>
                          <div class="form-group">
                          <select name="opening_<?php echo $schedule->days;?>[]" class="form-control m-b"> 
                            <option value="1" <?php if($schedule->open==1){?> selected="selected" <?php }?>>Yes</option>
                            <option value="0" <?php if($schedule->open==0){?> selected="selected" <?php }?>>No</option>
                         </select>
                        </div>
                      </td>

<td>
 <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="from_time" name="from_time[]" value="{{$schedule->from}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </td><td>
                 <div class='input-group date datetimepicker3' id='datetimepicker3'>
                    <input type='text' class="form-control" id="to_time" name="to_time[]"  value="{{$schedule->to}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div></td>
                <td>
 <div class='input-group date datetimepicker3' >
 <input type="text" name="break[]" value="{{$schedule->break_time}}" placeholder="Break Time"  class="form-control">
  <span class="input-group-addon">
  <span class="glyphicon glyphicon-time"></span>
  </span>
</div>
 </td><td>
 <div class="col-xs-12"> 
  <select name="duration[]" class="form-control m-b">
                            <option value="00:00">Hours</option> 
                            <option value="1:00" @if($schedule->break_duration == '1:00') selected="" @endif>1:00 Hour</option> 
                             <option value="1:30" @if($schedule->break_duration == '1:30') selected="" @endif>1:30 Hour</option> 
                              <option value="2:00" @if($schedule->break_duration == '2:00') selected="" @endif>2:00 Hour</option> 
                               <option value="2:30" @if($schedule->break_duration == '2:30') selected="" @endif>2:30 Hour</option> 
                            
                         </select>
</div>
 </td>
                      </tr>
                      <?php
                        }
                       ?>

                    </tbody>
                  </table>

<!--Opening hours end-->



          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>

        </div>
      </div>
    </div>

  </form>

</div>
<!-- form for profile builder End -->


 </div><!-- /.box-body -->
            <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 
@endsection
