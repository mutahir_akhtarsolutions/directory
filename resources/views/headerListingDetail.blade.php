 <?php $segment1 =  Request::segment(1);
       $segment2 =  Request::segment(2);
 ?>
<!-- banner Start here-->
<section class="banner">
      <div class="carousel-caption banner-carousel-capt">
           <div class="container">

          <form action="{{ url() }}/searchDr" method="get" id="remember">
          <div class="row">

            <div class="col-md-3 col-sm-3 col-xs-6 formfield ui-widget" > 
            <input type="text" id="state" class="form-control" placeholder="State" />
            <input type="hidden" id="location" name="location" value="{{@$_GET['location']}}" /> 
            </div>

            
            <div class="col-md-3 col-sm-3 col-xs-6 formfield ui-widget">
             <input type="text" id="specility" class="form-control" placeholder="Category"/>
            <input type="hidden" id="catagory" name="catagory" value="{{@$_GET['catagory']}}" /> 
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6 formfield ui-widget"> 
              <input type="text" name="search_keyword" id="search_keyword" class="form-control" placeholder="Keyword" value="{{@$_GET['search_keyword']}}">
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6 formbtn">
              <input type="submit" class="btn" value="search">
            </div>
            </div>
          </form>
      </div>
</section>
