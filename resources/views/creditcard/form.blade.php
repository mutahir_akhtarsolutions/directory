<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/23/2015
 * Time: 8:17 PM
 */

 ?>


 @extends('app')

 {{-- Page content --}}
 @section('content')

 <div class="container">

     <div class="page-header">
        <h1>{{ $mode == 'create' ? 'Create Credit Card' : 'Update Credit Card' }} <small>{{ $mode === 'update' ? $creditcard->cc_name : null }}</small></h1>
     </div>

     <form method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />




        <div class="form-group{{ $errors->first('cc_name', ' has-error') }}">

            <label for="name">Credit Card Name</label>

            <input type="text"  class="form-control" name="cc_name" id="cc_name" value="{{ Input::old('cc_name', isset($creditcard->cc_name) ? $creditcard->cc_name : '') }}" placeholder="Credit card name.">

            <span class="help-block">{{{ $errors->first('cc_name', ':message') }}}</span>

        </div>

        <div class="form-group{{ $errors->first('cc_logo', ' has-error') }}">

            <label for="name">Logo</label>


            <input type="file"  name="cc_logo" id="cc_logo"  placeholder="Logo">

            <span class="help-block">{{{ $errors->first('cc_logo', ':message') }}}</span>

        </div>

        <button type="submit" class="btn btn-default">Submit</button>

     </form>
 </div>

 @stop
