<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/23/2015
 * Time: 8:15 PM
 */
 ?>

 @extends('app')

 {{-- Page content --}}
 @section('content')

<div class="container">
     <div class="page-header">
        <h1>credit cards <span class="pull-right"><a href="{{ URL::to('creditcard/create') }}" class="btn btn-warning">Create</a></span></h1>
     </div>
   @if ($creditcards->count())
     Page {{ $creditcards->currentPage() }} of {{ $creditcards->lastPage() }}

     <div class="pull-right">
       {!! $creditcards->render() !!}
     </div>
     <br><br>

     <table class="table table-bordered">
        <thead>
            <th class="col-lg-4">credit card name </th>
            <th class="col-lg-4">credit card logo</th>
            <th class="col-lg-2">Actions</th>
        </thead>
        <tbody>
            @foreach ($creditcards as $c_card)
            <tr>

                <td>{{ $c_card->cc_name }}</td>

                <td><img height="50px" width="50px" src="{{ url() }}/images/catalog/{{ $c_card->cc_logo }}" alt="..." class="img-circle"></td>

                <td>
                    <a class="btn btn-warning" href="{{ URL::to("creditcard/{$c_card->id}") }}">Edit</a>
                    <a class="btn btn-danger" href="{{ URL::to("creditcard/{$c_card->id}/delete") }}">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
     </table>


             Page {{ $creditcards->currentPage() }} of {{ $creditcards->lastPage() }}

     <div class="pull-right">
        {!! $creditcards->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>
    @endif

</div>

 @endsection
