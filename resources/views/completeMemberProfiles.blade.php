 @extends('layout.memberapp')
@section('content')

<!--CSS-->

<style type="text/css"> 
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style> 
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            You can add doctors here.
            <small> </small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">@if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Error</strong>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
        <?php

                   $no_of_doctors = $data->no_of_doctors;

 if(($no_of_doctors-(count($doctors))) > 0 )
 {
    ?>
   <div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Step 1</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
        <p>Step 2</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Step 3</p>
      </div>
    </div>
  </div>
<!--addordinalnumber function-->

<?php

  function addOrdinalNumberSuffix($num) {
    if (!in_array(($num % 100),array(11,12,13))){
      switch ($num % 10) {
        // Handle 1st, 2nd, 3rd
        case 1:  return $num.'st';
        case 2:  return $num.'nd';
        case 3:  return $num.'rd';
      }
    }
    return $num.'th';
  }

?>
<!--{{ url('profile') }}-->
  <form role="form" action="<?php echo url(); ?>/fullCompleteProfile" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />
 <datalist id="datalist1">
             @foreach($association_names as $association_name)
            <option value="{{$association_name->name}}">{{$association_name->name}} </option>
             @endforeach
          </datalist>
    <input type="hidden" name="user_id" value="{{ $data->user_id }}" />
    <input type="hidden" name="profile_id" value="{{ $data->id }}" />
    <input type="hidden" name="number_of_doctors" value="{{ ($no_of_doctors-count($doctors)) }}" />

    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Specialty Info (Step 1)</h3>
          <?php
           $proceducre_counter = 0; 
           for($counter = 1;$counter<=($no_of_doctors-count($doctors));$counter++){
           ?>
          <h4><?php echo addOrdinalNumberSuffix($counter) ?> Doctor Information</h4>
          <!--<div class="form-group">
            <label class="control-label">Doctor Name</label>
            <input  maxlength="100" type="text" name="doctor_name[]" value="" required="required" class="form-control" placeholder="Enter Doctor Name"  />
          </div>-->

          <div class="form-group">
            <label class="control-label">First Name</label>
            <input  maxlength="100" type="text" name="first_name[]" value=""  class="form-control" placeholder="Enter first name"  />
          </div>
          <div class="form-group">
            <label class="control-label">Last Name</label>
            <input  maxlength="100" type="text" name="last_name[]" value=""   class="form-control" placeholder="Enter last name"  />
          </div>


<!--catagory and sub catagory start-->
          <div class="form-group">
            <label class="control-label">catagory </label>
            <select name="catagory_id[]"  class="form-control m-b" id="catagory_id_<?php echo $counter; ?>">
              <option>select catagory</option>


              <?php foreach($catagory as $main_cat){ ?>

              <option value="<?php echo $main_cat->id; ?>"><?php echo $main_cat->name; ?></option>
              <?php } ?>

           </select>
          </div>

          <div class="form-group">
            <label class="control-label">Sub catagory </label>
            <select name="sub_catagory[]" class="form-control m-b" id="sub_catagory_<?php echo $counter; ?>">
              <option>select sub catagory</option>

              <?php foreach($catagories as $sub_cate){ ?>

              <option value="<?php echo $sub_cate->id; ?>"> <?php echo $sub_cate->name; ?></option>
              <?php } ?>



           </select>
         </div>
<!--adding more cate and subcat-->
<span id="additional"></span>

<input id="addBtn" type="button" value=" + " />
<input id="removeBtn" type="button" value=" - " />
<!--catagory and sub catagory end-->


          <div class="form-group">
            <label class="control-label">Qualification </label>
            <input  maxlength="100" type="text" name="education[]" value="" required="required" class="form-control" placeholder="Enter education"  />
          </div>

           <div class="form-group">
                      <label>Select Procedures</label>
                      <select class="form-control" multiple="" name="procedure_id_{{$proceducre_counter}}[]">
                      @foreach($procedures as $procedure)
                        <option value="{{$procedure->id}}">{{$procedure->procedure_name}}</option>
                        @endforeach
                      </select>
                    </div>

          <div class="form-group">
            <label class="control-label">Affiliation </label>
            <input  maxlength="100" type="text" name="affiliation[]" value="" required="required" class="form-control" placeholder="Enter Affiliation "  />
          </div>

          <div class="form-group">
            <label class="control-label">Contact Number </label>
            <input  maxlength="100" type="text" name="contact_dr_phone_number[]" value=""   class="form-control" placeholder="Enter dr contact number"  />
          </div>

          <div class="form-group {{ $errors->first('email', 'has-error') }}">
            <label class="control-label">Email </label>
            <input  maxlength="100" type="text" name="email[]" value=""   class="form-control" placeholder="Enter dr email"  />

            <span style="color:#DD4B39" class="help-block">{{{ $errors->first('email', ':message') }}}</span>
          </div>
          <div class="form-group">
            <label class="control-label">Association </label>
            <input  maxlength="100" type="text" name="association[]" value="" required="required" class="form-control" placeholder="Enter dr association" list="datalist1" autocomplete="off"/>
           

          </div>

          <div class="form-group {{ $errors->first('Practice_id', 'has-error') }}">
            <label class="control-label">Practice ID </label>
            <input  maxlength="100" type="text" name="Practice_id[]" value=""  class="form-control" placeholder="Enter practice"  />
            <span style="color:#DD4B39" class="help-block">{{{ $errors->first('Practice_id', ':message') }}}</span>
          </div>

          <div class="form-group">
            <label class="control-label">Social profile links of doctor </label>
            <input  maxlength="100" type="text" name="social_media_link[]" value=""  class="form-control" placeholder="Enter Social profile links of doctor"  />
          </div>

          <div class="form-group">
            <label class="control-label">Doctor Image </label>
            <input ui-jq="filestyle" type="file" name="image_of_doctor[]" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
          </div>

          <hr>

<?php 
$proceducre_counter ++;
} 
 ?>



          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-2">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Schedule availability (Step 2)</h3>


          <?php
           for($counter = 1;$counter<=($no_of_doctors-count($doctors));$counter++){
           ?>

          <!--Opening hours start-->

                  <h2>Opening hours for <?php echo addOrdinalNumberSuffix($counter) ?> doctor</h2>
                  <p>Please select the opening hours:</p>
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Days</th>
                        <th>Open</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Sunday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                      </td>


                      </tr>
                      <tr>
                        <td>Monday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_monday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                      <tr>
                        <td>Tuesday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_tuesday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>

                      <tr>
                        <td>Wednesday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_wednesday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                      <tr>
                        <td>Thursday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_thursday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                      <tr>
                        <td>Friday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_friday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                      <tr>
                        <td>Saturday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_saturday[]" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>


                      </tr>
                    </tbody>
                  </table>

<!--Opening hours end-->

<?php

 } 
?>



          <!--<button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>-->
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-3">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> payment accepted (Step 3)</h3>

          <!--<div class="form-group">
            <label class="control-label">Insurance </label>
            <input  maxlength="100" type="text" name="insurance" value=""  class="form-control" placeholder="Enter Social profile links of doctor"  />
          </div>-->
          <div id="insurance_block">
          <div class="form-group">
            <label class="control-label">Insurance </label>
            <select name="p_insurance[]"  class="form-control m-b" id="p_insurance">
              <option value="0">select insurance</option>


              <?php foreach($insurances as $insurance){ ?>

              <option value="<?php echo $insurance->id; ?>">
              <?php echo $insurance->insurance_name; ?></option>
              <?php } ?>

           </select>
          </div>


          <div class="form-group" id="sub_insurance" style="display:none">
            <label class="control-label">Sub Insurance </label>
            <select name="s_p_insurance[]"  class="form-control m-b" id="s_p_insurance">
              <option value="0">select sub insurance</option>
           </select>
          </div>

          <div class="form-group" id="sub_of_sub_insurance" style="display:none">
            <label class="control-label">Sub of Sub Insurance </label>
            <select name="s_o_s_insurance[]"  class="form-control m-b" id="s_o_s_insurance">
              <option value="0">select sub of sub insurance</option>
           </select>
          </div>
        </div>

          <!--adding more cate and subcat-->
          <span id="adding_insurrance"></span>

          <input id="addBtn_ins" type="button" value=" + " />
          <input id="removeBtn_ins" type="button" value=" - " />
          <!--catagory and sub catagory end-->

          <div class="form-group">
            <label class="control-label">Cash</label>

              <div class="checkbox">
                  <input type="checkbox" name="cash" id="cash" value="">
              </div>
              </div>

              <div class="form-group">
              <label class="control-label">credit card</label>

              <div class="checkbox">
                  <input type="checkbox" name="credit_card" id="credit_card" value="">
              </div>
              </div>
              <div class="form-group">
              <label class="control-label">copay</label>

              <div class="checkbox">
                <input type="checkbox" name="copay" id="copay" value="">
              </div>

            </div>

          <div class="form-group">
            <label class="control-label">Others </label>
            <input  maxlength="100" type="text" name="other" value=""  class="form-control" placeholder="Enter Social profile links of doctor"  />
          </div>

          <!--<div class="form-group">
            <label class="control-label">Payment Method</label>
            <select name="Payment-gateway" class="form-control m-b">
              <option>option 1</option>
              <option>option 2</option>
              <option>option 3</option>
              <option>option 4</option>
           </select>
         </div>-->
          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
        </div>
      </div>
    </div>
  </form>
<?php
}
else { //echo '<div class="box-footer"><h3>You can not add more doctors your quota exceeds.</h3> </div>';
}
?>
       </div><!-- /.box-body -->
            <div class="box-footer">
               
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 
@endsection
<!--<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>-->



 
