<div class="container">
  <div class="row benefit-feature-row">
        <div class="col-md-7 col-sm-7 col-xs-12 physician-benefits">
          <h3><span>Benefits</span> of Nation Wide Physicians</h3>
          <ul>
            <li><a href="{{ url('/pages/2') }}">There are many variations of passages of Lorem Ipsum available</a></li>
            <li><a href="{{ url('/pages/3') }}">But the majority have suffered alteration in some form, </a></li>
            <li><a href="{{ url('/pages/4') }}">Injected humour, or randomised words which don't look </a></li>
            <li><a href="{{ url('/pages/5') }}">Even slightly believable. If you are going to use a passage </a></li>
            <li><a href="{{ url('/pages/6') }}">You need to be sure there isn't anything embarrassing hidden </a></li>
            <li><a href="{{ url('/pages/7') }}">All the Lorem Ipsum generators on the Internet tend to repeat</a></li>
            <li><a href="{{ url('/pages/8') }}">Predefined chunks as necessary, making this the first true generator</a></li>
          </ul>
          </div>
        <div class="col-md-5 col-sm-5 col-xs-12 features-physician">
          <h3>featured-physicians</h3>
          <div id="myCarousel" class="carousel fade">
            <div class="carousel-inner">
              <!-- Slide 1 -->
              <div class="item active">
                <div class="feat-phys-img"> <img src="{{ url() }}/front_end/assets/img/featured-physion-img.jpg" alt="featured physion" class="img-responsive"> </div>
                <div class="feat-phys-detail">
                  <h4><a href="#">Johan Peguero, LMSW</a></h4>
                  <span><a href="#" class="feat-phys-links" title="Social Work">Social Work</a></span>
                  <p>2857 Linden Blvd Suite 2A, Brooklyn, NY 11208</p>
                  <span><a href="#" class="feat-phys-links" title="Office Location">1 Office Location</a></span>
                  <span><a href="#" class="feat-phys-links" title="Patient Satisfaction">Patient Satisfaction</a></span> </div>
              </div>
              <!-- Slide 2 -->
              <div class="item">
                <div class="feat-phys-img"> <img src="{{ url() }}/front_end/assets/img/featured-physion-img.jpg" alt="featured physion" class="img-responsive"> </div>
                <div class="feat-phys-detail">
                  <h4><a href="#">Johan Peguero, LMSW</a></h4>
                  <span><a href="#" class="feat-phys-links" title="Social Work">Social Work</a></span>
                  <p>2857 Linden Blvd Suite 2A, Brooklyn, NY 11208</p>
                  <span><a href="#" class="feat-phys-links" title="Office Location">1 Office Location</a></span>
                  <span><a href="#" class="feat-phys-links" title="Patient Satisfaction">Patient Satisfaction</a></span> </div>
              </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="fa fa-chevron-left"></span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="fa fa-chevron-right"></span> </a> </div>
          <a href="#" class="more-phys">3000 Plus Doctors</a> </div>
      </div>
    </div>