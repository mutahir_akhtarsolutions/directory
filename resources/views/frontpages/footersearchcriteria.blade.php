<div class="find-specialist">
  <div class="container">
    <div class="row">

      <div class="col-xs-12 find-specialist-disc">
        <h2>Find doctors by the following critera</h2>
       <!-- <a href="#" class="chnage-location">Change Location <em class="fa fa-angle-right"></em></a>-->

        <div class="row"> <!--INTERNAL ROW STARTS HERE.-->

          <div class="col-md-4 col-sm-4 col-xs-12 find-spec-colm procedure-colm">
            <h4>Search by Procedures</h4>
              <ul class="procedures_ul"> 
              
            </ul> 
 <a data-toggle="collapse"  href="#tab_procedures" class="view-all view_tabprocedures"><em class="fa fa-plus-square-o"></em> View All Procedures</a>

                    <div id="tab_procedures" class="collapse tab_procedures"></div>

            </div>

          <div class="col-md-4 col-sm-4 col-xs-12 find-spec-colm speciality-colm">
            <h4>Search by Speciality</h4>
              <ul class="speciality_ul"> 
              
            </ul> 
 <a data-toggle="collapse"  href="#tab_speciality" class="view-all view_tabspeciality"><em class="fa fa-plus-square-o"></em> View All Speciality</a>

       <div id="tab_speciality" class="collapse tab_speciality"></div>
       </div>

           
<div class="col-md-4 col-sm-4 col-xs-12 find-spec-colm state-colm">
         <h4>Search by States</h4> 
           <ul class="states_ul"> 
             
            </ul> 
 <a data-toggle="collapse"  href="#tab_states" class="view-all view_tabstates"><em class="fa fa-plus-square-o"></em> View All States</a>

       <div id="tab_states" class="collapse tab_states"></div>
       </div>

 </div><!--INTERNAL ROW ENDS HERE.-->
        </div>

      </div>
    </div>
  </div> 


  <!-- benifit and features sections start here -->
<div class="container">
  <div class="row benefit-feature-row">
        <div class="col-md-7 col-sm-7 col-xs-12 physician-benefits">
          <h3><span>Benefits</span> of Nation Wide Physicians</h3>
          @if(count($collection['footerpages']) > 0)
          <ul>
          @foreach($collection['footerpages'] as $pages)
            <li><a href="{{ url('/pages/').'/'.$pages->id }}">{{$pages->title}}</a></li>
            @endforeach
          </ul>
          @endif
          </div>
        <div class="col-md-5 col-sm-5 col-xs-12 features-physician">
          <h3>featured-physicians</h3>
          <div id="myCarousel" class="carousel fade">
            <div class="carousel-inner">
              <!-- Slide 1 -->
              <div class="item active">
                <div class="feat-phys-img"> <img src="{{ url() }}/front_end/assets/img/featured-physion-img.jpg" alt="featured physion" class="img-responsive"> </div>
                <div class="feat-phys-detail">
                  <h4><a href="#">Johan Peguero, LMSW</a></h4>
                  <span><a href="#" class="feat-phys-links" title="Social Work">Social Work</a></span>
                  <p>2857 Linden Blvd Suite 2A, Brooklyn, NY 11208</p>
                  <span><a href="#" class="feat-phys-links" title="Office Location">1 Office Location</a></span>
                  <span><a href="#" class="feat-phys-links" title="Patient Satisfaction">Patient Satisfaction</a></span> </div>
              </div>
              <!-- Slide 2 -->
              <div class="item">
                <div class="feat-phys-img"> <img src="{{ url() }}/front_end/assets/img/featured-physion-img.jpg" alt="featured physion" class="img-responsive"> </div>
                <div class="feat-phys-detail">
                  <h4><a href="#">Johan Peguero, LMSW</a></h4>
                  <span><a href="#" class="feat-phys-links" title="Social Work">Social Work</a></span>
                  <p>2857 Linden Blvd Suite 2A, Brooklyn, NY 11208 </p>
                  <span><a href="#" class="feat-phys-links" title="Office Location">1 Office Location</a></span>
                  <span><a href="#" class="feat-phys-links" title="Patient Satisfaction">Patient Satisfaction</a></span> </div>
              </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="fa fa-chevron-left"></span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="fa fa-chevron-right"></span> </a> </div>
          <a href="#" class="more-phys">3000 Plus Doctors</a> </div>
      </div>
    </div>
<!-- benifit and features sections end here -->