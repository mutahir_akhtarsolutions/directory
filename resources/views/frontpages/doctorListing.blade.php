@extends('layout.app')
@section('content') 
@include('headerListingDetail')
<!-- banner end here --> 
<script type="text/javascript" language="javascript">
 
  //$('input[id^="checkbox_"]').not('#checkbox_all').click(function () {
        //    $('#checkbox_all').prop('checked', false);

  // alert($(this).val()); 
  // $('#formnameprocedures').submit(); 

//});
 
</script>
<?php 
      $catagory         = @$_GET['catagory'];
      $location         = @$_GET['location'];
      $search_keywork   = @$_GET['search_keyword'];
      $procedures_ids   = @$_GET['procedures']; 
      $city             = @$_GET['state_citie'];
      $specialities_ids = @$_GET['specialities']; 
      $sort_by          = @$_GET['sort_by'];
      $city_nav         = '';
      $category_nav     = '';
      $states_nav       = '';

      function stringContains($string, $needle) {
          return in_array($needle, explode(',', $string));
      }
      $message_variable      = '';
      
      
 
      
?>
<!-- welcome nationwide start here -->
<div class="container">
  <div class="row">
    <div class="col-md-8 col-sm-8 col-xs-12 left-content">

    <div class="row">
    <ul class="breadcrumb ">
    <li><a href="{{url()}}">Home</a><span class="divider"> » </span></li>
    <?php echo $states_nav.$city_nav.$category_nav.'&nbsp;';
    if($search_keywork != ''){echo $search_keywork;}

    ?> 
</ul> 
  </div> <!--BREADCURMS ENDS -->

     <div class="row">
    <div class="col-xs-12 provider-colm">
    <h2>We found   {{$users->total()}}  <span>Providers</span><?php echo $message_variable;?> </h2> 

    </div>

    <div class="col-xs-12 sortby-colm" id="sortby-colm">
    <div class="sortby">
    <span>
    <label>sort by :</label>
    <select>
     <option>Best match</option>
     <option>Best match 1</option>
     <option>Best match 2</option>
      <option>Best match 3</option>
     </select>
    </span>
    <?php $url =  $_SERVER['REQUEST_URI'];?>
    <ul>
     <li><a href="<?php if(!isset($sort_by)) { echo $url.'&sort_by=rating'; } else { echo str_replace($sort_by,"rating", $url); } ?>">Patient Satisfaction</a></li>
     <li><a href="<?php if(!isset($sort_by)) { echo $url.'&sort_by=wholename'; } else { echo str_replace($sort_by,"wholename", $url); } ?>">Name</a></li>  
    </ul>
    </div>


    </div>

    <div class="col-xs-12 dr-listing">
    <?php
        if($users->count() > 0):
         foreach($users as $doctor){
      ?>
	<div class="dr-listing-colm">
      <div class="dr-list-img">
      <figure>
      <img src="{{ url() }}/images/catalog/{{$doctor->image_of_doctor}}" alt="dr img" class="img-responsive">
      </figure>
      </div>

      <div class="dr-list-det">
      <h4><a href="{{ url() }}/doctorDetail/{{$doctor->user_id}}"> {{ $doctor->wholename }} ,{{$doctor->education}} </a></h4>
      <span class="dr-address"><em class="fa fa-home"></em> Primary Care Services Blount 101 Lemley Dr Suite A Oneonta, AL 3512</span>

      <ul class="dr-job">
       <li><a href="#">Insurances</a></li>
       <li><a href="#">Specialties</a></li>
      </ul>

      <ul class="dr-contact">
       <li><em class="fa fa-stethoscope"></em> Internal Medicine</li>
       <li><em class="fa fa-phone"></em> {{$doctor->contact_dr_phone_number}} (Office)</li>
      </ul>

      <div class="patient-satisfication">
      <span>Patient Satisfaction</span>
      <div class="stars-listing-doctors">
      
      @for ($i=1; $i <= 5 ; $i++)
      <span class="glyphicon glyphicon-star{{ ($i <= $doctor->averageRating) ? '' : '-empty'}}"></span>
       @endfor</div> 
      </div>


      </div>
    </div>
<?php
  } else: echo '<div class="dr-listing-colm"><div class="dr-list-det">
      <h4>No Result Found Matching Your Criteria.</h4></div></div>' ;
  endif;
?>
 
   <div class=" pager-row"> 
   {!! $users->render() !!}
 
  </div>

    </div>


    </div>
    </div>

    <aside class="col-md-4 col-sm-4 col-xs-12 sidebar">

    <div class="row">
    <div class="col-xs-12 popular-searches-colm">
    <div class="popular-searches">
     <h3>Popular Searches</h3>
      <ul>
        <li><a href="#">ACL (Anterior Cruciate Ligament) Surgery</a></li>
        <li><a href="#">Dermabrasion</a></li>
        <li><a href="#">EEG (Electroencephalogram)</a></li>
        <li><a href="#">Hysterectomy</a></li>
        <li><a href="#">Knee Replacement</a></li>
        <li><a href="#">Lung Removal</a></li>
        <li><a href="#">Multiple Sclerosis Therapy</a></li>
        <li><a href="#">Prostate Removal</a></li>
     </ul>
    </div>
    </div>

    <figure class="col-xs-12 online-directory">
    <a href="#"><img src="{{ url() }}/front_end/assets/img/online-directory-img.jpg" alt="dr directory" class="img-responsive"></a>
    </figure>

    <figure class="col-xs-12 doct-alert">
     <img src="{{ url() }}/front_end/assets/img/dr-alert.jpg" alt="dr directory" class="img-responsive">
     <figcaption>
       <h4>DoctorAlert!</h4>
       <p>Contrary to popular belief, Lorem Ipsum is not simply text. Latin literature from 45 BC</p>
       <a href="#" class="btn">SIGNUP</a>
     </figcaption>
    </figure>


    <div class="col-xs-12 latest-news">
    <h3>Latest News</h3>
    <div class="lt-nes-colm">
    <figure>
     <img src="{{ url() }}/front_end/assets/img/lt-news-img.jpg" alt="latest news image" class="img-responsive">
    </figure>
    <div class="lt-news-title">
    <h5><a href="#">There are many variations of of Lorem Ipsum available</a></h5>
    <span>27 Dec, 2013</span>
    </div>
    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure If you are going to use a passage of Lorem Ipsum, you need to be sure  </p>
    <a href="#" class="fa fa-angle-right"></a>
    </div>
    <div class="lt-nes-colm">
    <figure>
     <img src="{{ url() }}/front_end/assets/img/lt-news-img.jpg" alt="latest news image" class="img-responsive">
    </figure>
    <div class="lt-news-title">
    <h5><a href="#">There are many variations of of Lorem Ipsum available</a></h5>
    <span>27 Dec, 2013</span>
    </div>
    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure If you are going to use a passage of Lorem Ipsum, you need to be sure  </p>
    <a href="#" class="fa fa-angle-right"></a>
    </div>
    <div class="lt-nes-colm">
    <figure>
     <img src="{{ url() }}/front_end/assets/img/lt-news-img.jpg" alt="latest news image" class="img-responsive">
    </figure>
    <div class="lt-news-title">
    <h5><a href="#">There are many variations of of Lorem Ipsum available</a></h5>
    <span>27 Dec, 2013</span>
    </div>
    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure If you are going to use a passage of Lorem Ipsum, you need to be sure  </p>
    <a href="#" class="fa fa-angle-right"></a>
    </div>
    </div>
    </div>
    </div>




    </aside>

  </div>
</div>
<!-- benifit and features sections end here -->

<!-- footer -->
@endsection
