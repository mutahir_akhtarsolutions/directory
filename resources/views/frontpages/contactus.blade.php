@extends('layout.app')
@section('head')
<<style type="text/css" media="screen">
#success_message{ display: none;}
  
</style>
@stop

@section('footer')
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    // Ajax for our form to search Appointments of the users.. 
          $(document).on("submit", "#contact_form", function (event) {

            event.preventDefault(); 
            var formData = {
                first_name : $('input[name=first_name]').val(),
                last_name  : $('input[name=last_name]').val(),
                email      : $('input[name=email]').val(),
                phone      : $('input[name=phone]').val(),
                comments   : $('#comments').val()
            }       
                   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });    
      $.ajax({
        url: "{{url()}}/contactus",
        type: "post",
        data: formData,
        cache: false,
        beforeSend: function() {
           
          $('#success_message').html("<img src='{{url()}}/images/general.gif' />");
          $('#success_message').show();   
        },
        success: function(data){  
          $('#success_message').html(data); 
          $('#success_message').show(); 
        }
      });  
       
      });





// BELOW Procedures.
var arr = '';
arr = <?php echo (count($collection['procedures']) > 0 ? count($collection['procedures']): true);?>; 
 
if(arr > 0)
{ 
 arr = <?php echo json_encode($collection['procedures']);?>;  
    var count_procedures          = 0;
    var firstElevenProcedures     = '';
    var procedures_html           = '<ul>';
  $.each( arr, function( index, value ){
    if(count_procedures < 12){
firstElevenProcedures += '<li><a href="{{ url() }}/searchesbyprocedures/'+value.id+'?sort_by=id">'+value.procedure_name+'</a></li>';
}
    procedures_html  += '<li><a href="{{ url() }}/searchesbyprocedures/'+value.id+'?sort_by=id">'+value.procedure_name+'</a></li>';
    count_procedures ++;
   });
 procedures_html  += '</ul>';
 $(".procedures_ul").html(firstElevenProcedures);
 $('.tab_procedures').html(procedures_html);
 $(".view_tabprocedures").click(function(){
    $(".procedures_ul").hide(1000);
  });
//$('.view_tabprocedures').text('See all '+count_procedures+' conditions');
 }
// BELOW SPECILIATIES.


var arr = '';
arr = <?php echo (count($collection['catagories']) >0  ? count($collection['catagories']): true);?>; 
 
if(arr > 0)
{ 
 arr = <?php echo json_encode($collection['catagories']);?>;  
    var count_speciality          = 0;
    var firstElevenspeciality     = '';
    var speciality_html           = '<ul>';
  $.each( arr, function( index, value ){
    if(count_speciality < 12){
firstElevenspeciality += '<li><a href="{{ url() }}/searchesbycategory/'+value.id+'?sort_by=id">'+value.name+'</a></li>';
}
    speciality_html  += '<li><a href="{{ url() }}/searchesbycategory/'+value.id+'?sort_by=id">'+value.name+'</a></li>';
    count_speciality ++;
   });
 speciality_html  += '</ul>';
 $(".speciality_ul").html(firstElevenspeciality);
 $('.tab_speciality').html(speciality_html);
 $(".view_tabspeciality").click(function(){
    $(".speciality_ul").hide(1000);
  });
//$('.view_tabprocedures').text('See all '+count_procedures+' conditions');
 }


// BELOW STATES.


var arr = '';
arr = <?php echo (count($collection['locations']) >0  ? count($collection['locations']): true);?>; 
 
if(arr > 0)
{ 
 arr = <?php echo json_encode($collection['locations']);?>;  
    var count_states          = 0;
    var firstElevenstates     = '';
    var states_html           = '<ul>';
  $.each( arr, function( index, value ){
    if(count_states < 12){
firstElevenstates += '<li><a href="{{ url() }}/searchesbystates/'+value.id+'?sort_by=id">'+value.state_name+'</a></li>';
}
    states_html  += '<li><a href="{{ url() }}/searchesbystates/'+value.id+'?sort_by=id">'+value.state_name+'</a></li>';
    count_states ++;
   });
 states_html  += '</ul>';
 $(".states_ul").html(firstElevenstates);
 $('.tab_states').html(states_html);
 $(".view_tabstates").click(function(){
    $(".states_ul").hide(1000);
  });
//$('.view_tabprocedures').text('See all '+count_procedures+' conditions');
 }



       });// END OF DOCUMTNE READY FUCNTION.
  
</script>
@stop
@section('content')
@include('headerListingDetail')
<!-- welcome nationwide start here -->
<div class="container">
  <div class="row text-center">
    <div class="col-xs-12 welcome-nationwide">
      <h2>Contact Us</h2>   

      <div class="container">

    <form class="well form-horizontal" action="{{url()}}/contactus" method="post"  id="contact_form">

    <meta name="csrf-token" content="{{ csrf_token() }}" id="csrf-token">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
<fieldset>

<!-- Form Name -->
<legend>Contact Us Today!</legend>
  
<div class="form-group">
  <label class="col-md-2 control-label">First Name</label>  
  <div class="col-md-7 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input  name="first_name" id="first_name" placeholder="First Name" class="form-control"  type="text">
    </div>
  </div>
</div>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-2 control-label" >Last Name</label> 
    <div class="col-md-7 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="last_name" placeholder="Last Name" class="form-control" id="last_name"  type="text">
    </div>
  </div>
</div>

<!-- Text input-->
       <div class="form-group">
  <label class="col-md-2 control-label">E-Mail</label>  
    <div class="col-md-7 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
  <input name="email" placeholder="E-Mail Address" class="form-control"  type="text" required="" id="email">
    </div>
  </div>
</div>


<!-- Text input-->
       
<div class="form-group">
  <label class="col-md-2 control-label">Phone #</label>  
    <div class="col-md-7 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input name="phone" id="phone" placeholder="(845)555-1212" class="form-control" type="text">
    </div>
  </div>
</div> 
      
  
<div class="form-group">
  <label class="col-md-2 control-label">Message</label>
    <div class="col-md-7 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
          <textarea class="form-control" id="comments" name="comments" placeholder="Project Description" rows="6" required=""></textarea>
  </div>
  </div>
</div>
<div class="col-md-12 text-left">
<div class="col-md-7 col-md-offset-2">
<!-- Success message -->
<div class="alert alert-success " role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>
</div>
</div>
<!-- Button -->
<div class="form-group">
  <label class="col-md-2 control-label"></label>
  <div class="col-md-4">
    <button type="submit" id="submit" name="submit" class="btn btn-warning" >Send <span class="glyphicon glyphicon-send"></span></button>
  </div>
</div>

</fieldset>
</form>
</div>
    </div><!-- /.container --> 
       
    </div>
  </div>
</div>
<!-- welcome nationwide end here -->

<!-- Find Specialist start here -->

@include('frontpages/footersearchcriteria')
<!-- Find Specialist end here-->

<!-- benifit and features sections end here -->

<!-- footer ------------------------>
@endsection
