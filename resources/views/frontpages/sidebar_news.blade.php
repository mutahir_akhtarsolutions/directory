<div class="col-xs-12 latest-news">
    <h3>Latest News</h3>
<?php 
  function fileExists($path){
     $exists = @fopen($path,"r");
     if($exists){
        fclose($exists);
        return true;
     }
     return false;
  }
?>
    @if($collection['news']->count() > 0)
    @foreach($collection['news'] as $news)

    <div class="lt-nes-colm">
    <figure>
     <?php
    $url                =  url()."/images/news/icon_size/".$news->image;
    $exists             = fileExists($url);
    if($exists): $image = url()."/images/news/icon_size/".$news->image;
    else: $image        = "http://placehold.it/71x73";
    endif;?>
    <img src="{{ $image }}" alt="latest news image" class="img-responsive">
    </figure>
    <div class="lt-news-title">
    <h5><a href="{{url('/news/'.$news->id)}}">{{$news->title}}</a></h5>
    <span>{{ date("d-M-Y", strtotime($news->created_at))}} </span>
    </div>
    <p> {!! str_limit($news->content, 200, '...') !!} </p>
    <a href="{{url('/news/'.$news->id)}}" class="fa fa-angle-right"></a>
    </div>
    @endforeach
    @endif 
    </div>