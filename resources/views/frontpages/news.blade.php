@extends('layout.app')
@section('footer')
<<script   type="text/javascript" charset="utf-8" >
  $(document).ready(function(){
// BELOW Procedures.
var arr = '';
arr = <?php echo (count($collection['procedures']) > 0 ? count($collection['procedures']): true);?>; 
 
if(arr > 0)
{ 
 arr = <?php echo json_encode($collection['procedures']);?>;  
    var count_procedures          = 0;
    var firstElevenProcedures     = '';
    var procedures_html           = '<ul>';
  $.each( arr, function( index, value ){
    if(count_procedures < 12){
firstElevenProcedures += '<li><a href="{{ url() }}/searchesbyprocedures/'+value.id+'?sort_by=id">'+value.procedure_name+'</a></li>';
}
    procedures_html  += '<li><a href="{{ url() }}/searchesbyprocedures/'+value.id+'?sort_by=id">'+value.procedure_name+'</a></li>';
    count_procedures ++;
   });
 procedures_html  += '</ul>';
 $(".procedures_ul").html(firstElevenProcedures);
 $('.tab_procedures').html(procedures_html);
 $(".view_tabprocedures").click(function(){
    $(".procedures_ul").hide(1000);
  });
//$('.view_tabprocedures').text('See all '+count_procedures+' conditions');
 }
// BELOW SPECILIATIES.


var arr = '';
arr = <?php echo (count($collection['catagories']) >0  ? count($collection['catagories']): true);?>; 
 
if(arr > 0)
{ 
 arr = <?php echo json_encode($collection['catagories']);?>;  
    var count_speciality          = 0;
    var firstElevenspeciality     = '';
    var speciality_html           = '<ul>';
  $.each( arr, function( index, value ){
    if(count_speciality < 12){
firstElevenspeciality += '<li><a href="{{ url() }}/searchesbycategory/'+value.id+'?sort_by=id">'+value.name+'</a></li>';
}
    speciality_html  += '<li><a href="{{ url() }}/searchesbycategory/'+value.id+'?sort_by=id">'+value.name+'</a></li>';
    count_speciality ++;
   });
 speciality_html  += '</ul>';
 $(".speciality_ul").html(firstElevenspeciality);
 $('.tab_speciality').html(speciality_html);
 $(".view_tabspeciality").click(function(){
    $(".speciality_ul").hide(1000);
  });
//$('.view_tabprocedures').text('See all '+count_procedures+' conditions');
 }


// BELOW STATES.


var arr = '';
arr = <?php echo (count($collection['locations']) >0  ? count($collection['locations']): true);?>; 
 
if(arr > 0)
{ 
 arr = <?php echo json_encode($collection['locations']);?>;  
    var count_states          = 0;
    var firstElevenstates     = '';
    var states_html           = '<ul>';
  $.each( arr, function( index, value ){
    if(count_states < 12){
firstElevenstates += '<li><a href="{{ url() }}/searchesbystates/'+value.id+'?sort_by=id">'+value.state_name+'</a></li>';
}
    states_html  += '<li><a href="{{ url() }}/searchesbystates/'+value.id+'?sort_by=id">'+value.state_name+'</a></li>';
    count_states ++;
   });
 states_html  += '</ul>';
 $(".states_ul").html(firstElevenstates);
 $('.tab_states').html(states_html);
 $(".view_tabstates").click(function(){
    $(".states_ul").hide(1000);
  });
//$('.view_tabprocedures').text('See all '+count_procedures+' conditions');
 }

  });

</script>
@stop
@section('content')
@include('headerListingDetail')
<?php
  function fileExists($path){
     $exists = @fopen($path,"r");
     if($exists){
        fclose($exists);
        return true;
     }
     return false;
  }

$url =  url()."/images/news/".$collection['news']->image; 
    $exists = fileExists($url);
    if($exists && $collection['news']->image!='' ):
     $image = "<img src=".url()."/images/news/".$collection['news']->image." alt='latest news image' class='img-responsive'>";
    else: $image = "";
    endif; 
?>

<!-- welcome nationwide start here -->
<div class="container">
  <div class="row text-center">
    <div class="col-xs-12 welcome-nationwide">
      <h2>{{$collection['news']->title}}</h2>
      <div class="col-md-12">
<div class="col-md-4 col-md-offset-4">
 <figure> 
 {!!$image!!}
    </figure></div></div>


          {!!$collection['news']->content!!}   
       
    </div>
  </div>
</div>
<!-- welcome nationwide end here -->

<!-- Find Specialist start here -->

@include('frontpages/footersearchcriteria') 
@endsection
