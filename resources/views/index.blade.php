@extends('layout.app')

@section('title')
Home Page
@stop
@section('description')
 
@stop
@section('keywords')
 
@stop

@section('author')
 
@stop

@section('footer')
<script   type="text/javascript" charset="utf-8" >
  $(document).ready(function(){
// BELOW Procedures.
var arr = '';
arr = <?php echo (count($collection['procedures']) > 0 ? count($collection['procedures']): true);?>; 
 
if(arr > 0)
{ 
 arr = <?php echo json_encode($collection['procedures']);?>;  
    var count_procedures          = 0;
    var firstElevenProcedures     = '';
    var procedures_html           = '<ul>';
  $.each( arr, function( index, value ){
    if(count_procedures < 12){
firstElevenProcedures += '<li><a href="{{ url() }}/searchesbyprocedures/'+value.id+'?sort_by=id">'+value.procedure_name+'</a></li>';
}
    procedures_html  += '<li><a href="{{ url() }}/searchesbyprocedures/'+value.id+'?sort_by=id">'+value.procedure_name+'</a></li>';
    count_procedures ++;
   });
 procedures_html  += '</ul>';
 $(".procedures_ul").html(firstElevenProcedures);
 $('.tab_procedures').html(procedures_html);
 $(".view_tabprocedures").click(function(){
    $(".procedures_ul").hide(1000);
  });
//$('.view_tabprocedures').text('See all '+count_procedures+' conditions');
 }
// BELOW SPECILIATIES.


var arr = '';
arr = <?php echo (count($collection['catagories']) >0  ? count($collection['catagories']): true);?>; 
 
if(arr > 0)
{ 
 arr = <?php echo json_encode($collection['catagories']);?>;  
    var count_speciality          = 0;
    var firstElevenspeciality     = '';
    var speciality_html           = '<ul>';
  $.each( arr, function( index, value ){
    if(count_speciality < 12){
firstElevenspeciality += '<li><a href="{{ url() }}/searchesbycategory/'+value.id+'?sort_by=id">'+value.name+'</a></li>';
}
    speciality_html  += '<li><a href="{{ url() }}/searchesbycategory/'+value.id+'?sort_by=id">'+value.name+'</a></li>';
    count_speciality ++;
   });
 speciality_html  += '</ul>';
 $(".speciality_ul").html(firstElevenspeciality);
 $('.tab_speciality').html(speciality_html);
 $(".view_tabspeciality").click(function(){
    $(".speciality_ul").hide(1000);
  });
//$('.view_tabprocedures').text('See all '+count_procedures+' conditions');
 }


// BELOW STATES.


var arr = '';
arr = <?php echo (count($collection['locations']) >0  ? count($collection['locations']): true);?>; 
 
if(arr > 0)
{ 
 arr = <?php echo json_encode($collection['locations']);?>;  
    var count_states          = 0;
    var firstElevenstates     = '';
    var states_html           = '<ul>';
  $.each( arr, function( index, value ){
    if(count_states < 12){
firstElevenstates += '<li><a href="{{ url() }}/searchesbystates/'+value.id+'?sort_by=id">'+value.state_name+'</a></li>';
}
    states_html  += '<li><a href="{{ url() }}/searchesbystates/'+value.id+'?sort_by=id">'+value.state_name+'</a></li>';
    count_states ++;
   });
 states_html  += '</ul>';
 $(".states_ul").html(firstElevenstates);
 $('.tab_states').html(states_html);
 $(".view_tabstates").click(function(){
    $(".states_ul").hide(1000);
  });
//$('.view_tabprocedures').text('See all '+count_procedures+' conditions');
 }

  });

</script>
@stop
@section('content')
<!-- banner Start here-->
<section class="banner">
  <div class="carousel-inner">
    <div class="item active"> <img src="{{ url() }}/front_end/assets/img/banner.jpg" alt="" title="" class="img-responsive" >
      <div class="carousel-caption">
        <div class="container">

          <form action="{{ url() }}/searchDr" method="get" id="remember">
          <div class="row">
            
            <div class="col-md-3 col-sm-3 col-xs-6 formfield ui-widget" > 
            <input type="text" id="state" class="form-control" placeholder="State" />
            <input type="hidden" id="location" name="location" value="{{@$_GET['location']}}" /> 
            </div>

            
            <div class="col-md-3 col-sm-3 col-xs-6 formfield ui-widget">
             <input type="text" id="specility" class="form-control" placeholder="Category"/>
            <input type="hidden" id="catagory" name="catagory" value="{{@$_GET['catagory']}}" /> 
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6 formfield ui-widget"> 
              <input type="text" name="search_keyword" id="search_keyword" class="form-control" placeholder="Keyword" value="{{@$_GET['search_keyword']}}">
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6 formbtn">
              <input type="submit" class="btn" value="search">
            </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- banner end here -->

<!-- welcome nationwide start here -->
<div class="container">
  <div class="row text-center">
      <div class="col-xs-12 welcome-nationwide">
      <h2>{{$collection['homePagesArray'][12]['title']}}</h2>
      <p>{!!$collection['homePagesArray'][12]['content']!!}</p>
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12 nationwide-services">
       <a href="{{ url('/pages/').'/'.$collection['homePagesArray'][9]['id'] }}">
        <span><em class="fa fa-briefcase"></em></span></a>
          <h4>{{$collection['homePagesArray'][9]['title']}}</h4>
          <p>{!!str_limit($collection['homePagesArray'][9]['content'],200,'..')!!}</p>  
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 nationwide-services">

       <a href="{{ url('/pages/').'/'.$collection['homePagesArray'][10]['id'] }}">
        <span><em class="fa fa-location-arrow"></em></span></a>
       
          <h4>{{$collection['homePagesArray'][10]['title']}}</h4>
          <p>{!!str_limit($collection['homePagesArray'][10]['content'],200,'..')!!}</p>  
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 nationwide-services">
       
       <a href="{{ url('/pages/').'/'.$collection['homePagesArray'][11]['id'] }}"> <span><em class="fa fa-umbrella"></em></span></a>
       
          <h4>{{$collection['homePagesArray'][11]['title']}}</h4>
          <p>{!!str_limit($collection['homePagesArray'][11]['content'],200,'..')!!}</p>  
        </div>
      </div>
    </div>
  </div>
</div>
<!-- welcome nationwide end here -->

<!-- Find Specialist start here -->
@include('frontpages/footersearchcriteria')
<!-- Find Specialist end here-->

 

<!-- footer ------------------------>
@endsection
