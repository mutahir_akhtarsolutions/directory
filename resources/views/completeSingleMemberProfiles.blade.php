@extends('layout.memberapp')
@section('head')
<link rel="stylesheet" href="{{ URL::asset('dist/build/css/bootstrap-datetimepicker.min.css') }}"> 
    <!-- iCheck for checkboxes and radio inputs -->
    <!--CSS-->

<style type="text/css"> 
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style> 
@stop
@section('footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js">
  
</script>
  <!-- iCheck 1.0.1 -->
<script src="{{ URL::asset('dist/build/js/bootstrap-datetimepicker.min.js') }}"></script> 

 <script src="{{ URL::asset('dist/build/js/memberprofilescript.js') }}"></script>  
 
<script>
    // global app configuration object
var siteUrl = '<?php echo url();?>'; 
 
        </script>
 @stop
@section('content')
 <!-- Modal HTML -->
    <div id="practiceModal" class="modal fade"> 
    <form action="<?php echo url(); ?>/profiles/doctorProfileTransfer" method="post" accept-charset="utf-8" id="completesingleProfile">

    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">The Doctor is already Registered</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to send notification to this doctor for joining your clinic</p>
                    <p><input name="practice_code" id="doctorpractice_id" value=""></input></p>
                    <p class="text-warning"><small>If you don't save, your changes will be lost.</small></p> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send Notification</button>
                </div>
            </div>
        </div> 
    </form>
    </div> 
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Blank page
            <small>it all starts here</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">    
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>


        <div class="box-body">
          <div class="row">
          <div class="col-md-12" > 
               @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Error</strong>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
        </div>
        @endif 
        
             @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif

</div>
            </div>
   <div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Step 1</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
        <p>Step 2</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Step 3</p>
      </div>
    </div>
  </div>
<!--addordinalnumber function-->

<?php

  function addOrdinalNumberSuffix($num) {
    if (!in_array(($num % 100),array(11,12,13))){
      switch ($num % 10) {
        // Handle 1st, 2nd, 3rd
        case 1:  return $num.'st';
        case 2:  return $num.'nd';
        case 3:  return $num.'rd';
      }
    }
    return $num.'th';
  }
$data->no_of_doctors = 1;
?>
<!--{{ url('profile') }}-->
  <form role="form" action="<?php echo url(); ?>/profiles/singleCompleteProfile" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />
 <datalist id="datalist1">
             @foreach($association_names as $association_name)
            <option value="{{$association_name->name}}">{{$association_name->name}} </option>
             @endforeach
          </datalist>
    <input type="hidden" name="user_id" value="{{ $data->user_id }}" />
    <input type="hidden" name="profile_id" value="{{ $data->id }}" />
    <input type="hidden" name="number_of_doctors" value="{{ $data->no_of_doctors }}" />

    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Specialty Info (Step 1)</h3>
          <?php
               $no_of_doctors = $data->no_of_doctors;
               $proceducre_counter = 0; 
           ?>
          <h4> Doctor Information</h4> 



          <div class="form-group Practice_id {{ $errors->first('Practice_id', ' has-error') }}">
            <label class="control-label">Practice ID * </label>
            <input  maxlength="100" type="text" name="Practice_id" id="Practice_id" value="{{ Input::old('Practice_id') }}"   class="form-control typeahead" placeholder="Enter practice" />

              <span style="color:#DD4B39" class="help-block">{{{ $errors->first('Practice_id', ':message') }}}</span>
          </div>


          <div class="form-group first_name {{ $errors->first('first_name', ' has-error') }}">
            <label class="control-label">First Name *</label>
            <input  maxlength="100" type="text" name="first_name" id="first_name" value="{{ Input::old('first_name') }}" 
             class="form-control" placeholder="Enter first name" />
            <span style="color:#DD4B39" class="help-block">{{{ $errors->first('first_name', ':message') }}}</span>
          </div>
          <div class="form-group {{ $errors->first('last_name', ' has-error') }}">
            <label class="control-label">Last Name</label>
            <input  maxlength="100" type="text" name="last_name" value="{{Input::old('last_name') }}"   class="form-control" placeholder="Enter last name"  />

            <span style="color:#DD4B39" class="help-block">{{{ $errors->first('last_name', ':message') }}}</span>
          </div>

         

             <div class="form-group {{ $errors->first('gender', ' has-error') }}">
            <label class="control-label">Select Gender </label>
            <select name="gender"  class="form-control m-b" id="gender" >
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            <option value="Other">Other</option>
           </select>
           <span style="color:#DD4B39" class="help-block">{{{ $errors->first('gender', ':message') }}}</span>
          </div>

         <div class="form-group user_dob {{ $errors->first('user_dob', ' has-error') }} ">
            <label class="control-label">Date Of Birth</label>
            <input  maxlength="100" type="text" name="user_dob" id="user_dob" value="{{Input::old('user_dob') }}"   class="form-control" placeholder="Enter date"  />
            <span style="color:#DD4B39" class="help-block">{{{ $errors->first('user_dob', ':message') }}}</span>
          </div>

<!--catagory and sub catagory start-->
          <div class="form-group catagory_id_1 {{ $errors->first('catagory_id', ' has-error') }}">
            <label class="control-label">Catagory * </label>
            <select name="catagory_id"  class="form-control m-b" id="catagory_id_1" >
              <option value="">Select catagory</option>


              <?php foreach($catagory as $main_cat){ ?>

              <option value="<?php echo $main_cat->id; ?>" @if(Input::old('catagory_id') == $main_cat->id)  selected @endif><?php echo $main_cat->name; ?></option>
              <?php } ?>

           </select>
           <span style="color:#DD4B39" class="help-block">{{{ $errors->first('catagory_id', ':message') }}}</span>
          </div>

          <div class="form-group sub_catagory_1  {{ $errors->first('sub_catagory_1', ' has-error') }}" id="form_group_1">
            <label class="control-label">Sub catagory * </label>
            <select name="sub_catagory_1" class="form-control m-b sub_catagories" id="sub_catagory_1" data-id="1"  >
              <option>select sub catagory</option>

              <?php foreach($catagories as $sub_cate){ ?>

              <option value="<?php echo $sub_cate->id; ?>" > <?php echo $sub_cate->name; ?></option>
              <?php } ?> 

           </select> 

  <span style="color:#DD4B39" class="help-block">{{{ $errors->first('sub_catagory_1', ':message') }}}</span>
         </div> 
          
              <!--adding more cate and subcat-->

                <span id="additional"></span>
                <div class="form-group" id="form_group_2">
                <div class="extra"></div>
                <div class="extra"></div>

              </div>
              <input id="addBtn" type="button" value=" + " />
              <input id="removeBtn" type="button" value=" - " />
              <!--catagory and sub catagory end-->


          <div class="form-group">
            <label class="control-label">Qualification </label>
            <input  maxlength="100" type="text" name="education" value="{{ Input::old('education') }}"  class="form-control" placeholder="Enter education"  />
          </div>
 

          <div class="form-group">
            <label class="control-label">Affiliation </label>
            <input  maxlength="100" type="text" name="affiliation" value="{{ Input::old('affiliation') }}"   class="form-control" placeholder="Enter Affiliation "  />
          </div>

          <div class="form-group">
            <label class="control-label">Contact Number </label>
            <input  maxlength="100" type="text" name="contact_dr_phone_number" value="{{ Input::old('contact_dr_phone_number') }}" class="form-control" placeholder="Enter dr contact number"  />
          </div>

          <div class="form-group email {{ $errors->first('email', ' has-error') }}">
            <label class="control-label">Email * </label>
            <input  maxlength="100" type="email" id="email"  name="email" value="{{ Input::old('email') }}" class="form-control" placeholder="Enter dr email"/>
             <span style="color:#DD4B39" class="help-block">{{{ $errors->first('email', ':message') }}}</span>
          </div>




          <div class="form-group input_associations_wrap">
          <div class="col-xs-12" style="padding-left: 0px!important">
          <label class="control-label">Add Secondary Associations </label></div>

            <input type="hidden" id="total_associations_filds" value="1"></input>
          <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">

          <div class="col-sm-4" style="padding-left: 0px!important">
          <input  maxlength="100" type="text" name="association[]" value="" class="form-control" placeholder="Enter dr association" list="datalist1"/>
          </div>
          <div class="col-sm-4">
          <select name="availability[]" class="form-control">
          <option value="0">Select A day</option>
          <option value="Saturday">Saturday</option> 
          <option value="Sunday">Sunday</option> 
          <option value="Monday">Monday</option> 
          <option value="Tuesday">Tuesday</option> 
          <option value="Wedensday">Wedensday</option> 
          <option value="Thursday">Thursday</option> 
          <option value="Friday">Friday</option> 
          </select> 
          </div>
          <div class="col-sm-4"> 
          <button class="add_associations_button btn btn-primary">+</button>
          </div>
          <div class="col-sm-12">
          <div class="col-sm-4">
          <label class="control-label">Time From : </label>
          <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="from_time" name="from_time[]" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>  </div>
          <div class="col-sm-4">
          <label class="control-label">Time To : </label> 
          <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control"  id="to_time" name="to_time[]" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> 
</div><div class="col-sm-4"></div></div></div>

</div>



<div class="form-group input_fields_wrap">   
    <div class="col-xs-12" style="padding-left: 0px!important">
          <label class="control-label">Add Social Links </label></div> 
            
             <input type="hidden" id="total_filds" value="1"></input>
             <div class="col-xs-12" style="padding-left: 0px!important; padding-top:5px!important">
            <div class="col-sm-6" style="padding-left: 0px!important">
            <input  maxlength="200" type="text" name="sociallinks[]" value="" class="form-control" placeholder="Enter Profile Link"  />
            <input name="link_id[]" id="link_id" type="hidden" value=""></input>
            </div>

            <div class="col-sm-2">
           <input  maxlength="10" type="text" name="sort_order[]" value="" class="form-control" placeholder="Sort"  />
            </div>
            <div class="col-sm-4"> 
            <button class="add_field_button btn btn-primary">+</button>
            </div>

             <div class="col-sm-12" style="padding-top: 10px; padding-bottom: 10px">
            <input id="socialicon" name="socialicon[]" type="file" multiple class="file-loading">
            </div>
            </div>

            </div>



          <div class="form-group">
            <label class="control-label"> </label>
            
          </div>

          <div class="form-group">
            <label class="control-label">Doctor Image </label>
            <input ui-jq="filestyle" type="file" name="image_of_doctor" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
          </div>

          <hr> 



          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="next_btn1" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-2">
      <div class="col-xs-8 col-md-offset-3">
        <div class="box-body">
          <h3> Schedule availability (Step 2)</h3>


          <?php
           $no_of_doctors = $data->no_of_doctors; 
           ?>

          <!--Opening hours start-->

                  <h2>Opening hours for doctor</h2>
                  <p><h4>Please select the opening hours:</h4></p>
  

                  <table class="table">
                    <thead>
                      <tr>
                        <th class="col-xs-2">Days</th>
                        <th class="col-xs-2">Open</th>
                        <th class="col-xs-2">From Time</th>
                        <th class="col-xs-2">To Time</th>
                        <th class="col-xs-2">Break Time</th>
                        <th class="col-xs-2">Break Duration</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Sunday</td>
                        <td>
                          <div class="input-group .form-control, .input-group-addon, .input-group-btn">
                          <select name="opening_sunday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                      </td>
<td>
 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="from_time_sunday" name="from_time_sunday" value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </td><td>
                 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="to_time_sunday" name="to_time_sunday"  value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div></td>
<td>
 <div class='input-group date datetimepicker3' >
 <input type="text" name="break[]" value="" placeholder="Break Time"  class="form-control">
  <span class="input-group-addon">
  <span class="glyphicon glyphicon-time"></span>
  </span>
</div>
 </td><td>
 <div class="col-xs-12"> 
                            <select name="duration[]" class="form-control m-b">
                            <option value="00:00">Hours</option> 
                            <option value="1:00">1:00 Hour</option> 
                             <option>1:30 Hour</option> 
                              <option value="2:00">2:00 Hour</option> 
                               <option value="2:30">2:30 Hour</option> 
                         </select>
</div>
 </td>
       
                      </tr>
                      <tr>
                        <td>Monday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_monday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
<td>
 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="from_time_monday" name="from_time_monday" value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </td><td>
                 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="to_time_monday" name="to_time_monday"  value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div></td>
<td>
 <div class='input-group date datetimepicker3' >
 <input type="text" name="break[]" value="" placeholder="Break Time"  class="form-control">
  <span class="input-group-addon">
  <span class="glyphicon glyphicon-time"></span>
  </span>
</div>
 </td><td>
 <div class="col-xs-12"> 
  <select name="duration[]" class="form-control m-b">
                            <option value="00:00">Hours</option> 
                            <option value="1:00">1:00 Hour</option> 
                             <option>1:30 Hour</option> 
                              <option value="2:00">2:00 Hour</option> 
                               <option value="2:30">2:30 Hour</option> 
                         </select>
</div>
 </td>
      
                      </tr>
                      <tr>
                        <td>Tuesday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_tuesday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
<td>
 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="from_time_tuesday" name="from_time_tuesday" value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </td><td>
                 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="to_time_tuesday" name="to_time_tuesday"  value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div></td>
<td>
 <div class='input-group date datetimepicker3' >
 <input type="text" name="break[]" value="" placeholder="Break Time"  class="form-control">
  <span class="input-group-addon">
  <span class="glyphicon glyphicon-time"></span>
  </span>
</div>
 </td><td>
 <div class="col-xs-12"> 
  <select name="duration[]" class="form-control m-b">
                            <option value="00:00">Hours</option> 
                            <option value="1:00">1:00 Hour</option> 
                             <option>1:30 Hour</option> 
                              <option value="2:00">2:00 Hour</option> 
                               <option value="2:30">2:30 Hour</option> 
                         </select>
</div>
 </td>
      
                      </tr>

                      <tr>
                        <td>Wednesday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_wednesday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
<td>
 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="from_time_wednesday" name="from_time_wednesday" value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </td><td>
                 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="to_time_wednesday" name="to_time_wednesday"  value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div></td>
<td>
 <div class='input-group date datetimepicker3' >
 <input type="text" name="break[]" value="" placeholder="Break Time"  class="form-control">
  <span class="input-group-addon">
  <span class="glyphicon glyphicon-time"></span>
  </span>
</div>
 </td><td>
 <div class="col-xs-12"> 
  <select name="duration[]" class="form-control m-b">
                            <option value="00:00">Hours</option> 
                            <option value="1:00">1:00 Hour</option> 
                             <option>1:30 Hour</option> 
                              <option value="2:00">2:00 Hour</option> 
                               <option value="2:30">2:30 Hour</option> 
                         </select>
</div>
 </td>
      
                      </tr>
                      <tr>
                        <td>Thursday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_thursday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>

<td>
 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="from_time_thursday" name="from_time_thursday" value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </td><td>
                 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="to_time_thursday" name="to_time_thursday"  value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div></td><td>
 <div class='input-group date datetimepicker3' >
 <input type="text" name="break[]" value="" placeholder="Break Time"  class="form-control">
  <span class="input-group-addon">
  <span class="glyphicon glyphicon-time"></span>
  </span>
</div>
 </td><td>
 <div class="col-xs-12"> 
  <select name="duration[]" class="form-control m-b">
                             <option value="00:00">Hours</option> 
                            <option value="1:00">1:00 Hour</option> 
                             <option>1:30 Hour</option> 
                              <option value="2:00">2:00 Hour</option> 
                               <option value="2:30">2:30 Hour</option> 
                         </select>
</div>
 </td>
      
                      </tr>
                      <tr>
                        <td>Friday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_friday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
<td>
 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="from_time_friday" name="from_time_friday" value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </td><td>
                 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="to_time_friday" name="to_time_friday"  value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div></td>
<td>
 <div class='input-group date datetimepicker3' >
 <input type="text" name="break[]" value="" placeholder="Break Time"  class="form-control">
  <span class="input-group-addon">
  <span class="glyphicon glyphicon-time"></span>
  </span>
</div>
 </td><td>
 <div class="col-xs-12"> 
  <select name="duration[]" class="form-control m-b">
                             <option value="00:00">Hours</option> 
                            <option value="1:00">1:00 Hour</option> 
                             <option>1:30 Hour</option> 
                              <option value="2:00">2:00 Hour</option> 
                               <option value="2:30">2:30 Hour</option> 
                         </select>
</div>
 </td>
      
                      </tr>
                      <tr>
                        <td>Saturday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_saturday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>

<td>
 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="from_time_saturday" name="from_time_saturday" value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div> </td><td>
                 <div class='input-group date datetimepicker3' >
                    <input type='text' class="form-control" id="to_time_saturday" name="to_time_saturday"  value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div></td><td>
 <div class='input-group date datetimepicker3' >
 <input type="text" name="break[]" value="" placeholder="Break Time"  class="form-control">
  <span class="input-group-addon">
  <span class="glyphicon glyphicon-time"></span>
  </span>
</div>
 </td><td>
 <div class="col-xs-12"> 
  <select name="duration[]" class="form-control m-b">
                             <option value="00:00">Hours</option> 
                            <option value="1:00">1:00 Hour</option> 
                             <option>1:30 Hour</option> 
                              <option value="2:00">2:00 Hour</option> 
                               <option value="2:30">2:30 Hour</option> 
                         </select>
</div>
 </td>
      
                      </tr>
                    </tbody>
                  </table>

<!--Opening hours end--> 
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="next_btn2" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-3">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> payment accepted (Step 3)</h3>

          <!--<div class="form-group">
            <label class="control-label">Insurance </label>
            <input  maxlength="100" type="text" name="insurance" value=""  class="form-control" placeholder="Enter Social profile links of doctor"  />
          </div>-->
          <div id="insurance_block" class="ddl">
          <div class="form-group">
            <label class="control-label">Insurance </label>
            <select name="p_insurance[]"  class="form-control m-b pinsurance" id="p_insurance_" data-id=''>
              <option value="0">select insurance</option>
              <?php foreach($insurances as $insurance){ ?>
              <option value="<?php echo $insurance->id; ?>"><?php echo $insurance->insurance_name; ?></option>
              <?php } ?>

           </select>
          </div>


          <div class="form-group sub_insurance" id="sub_insurance" style="display:none">
            <label class="control-label">Sub Insurance </label>
            <select name="s_p_insurance[]"  class="form-control m-b spinsurance" id="s_p_insurance_" data-id=''>
              <option value="0">select sub insurance</option>
           </select>
          </div>

          <div class="form-group sub_of_sub_insurance" id="sub_of_sub_insurance" style="display:none">
            <label class="control-label">Sub of Sub Insurance </label>
            <select name="s_o_s_insurance[]"  class="form-control m-b sospinsurance" id="s_o_s_insurance_" data-id=''>
              <option value="0">select sub of sub insurance</option>
           </select>
          </div>
        </div>

          <!--adding more cate and subcat-->
          <div id="adding_insurrance"></div>

          <input id="addBtn_ins" type="button" value=" + " />
          <input id="removeBtn_ins" type="button" value=" - " />
          <!--catagory and sub catagory end-->

          <div class="form-group">
            <label class="control-label">&nbsp;</label>
 
              </div>
               <div class="form-group">
            <label class="control-label">Select Credit Cards</label>
 
              </div> 
              @foreach($creditCards as $showCards)

              <div class="form-group">
              <label>
                      <input type="checkbox" class="flat-red"  name="credit_card[]" id="credit_card_{{$showCards->id}}" value="{{$showCards->id}}" >
                      {{ucfirst($showCards->cc_name)}}
                    </label>  
              </div>
              @endforeach  
 
          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
        </div>
      </div>
    </div>
  </form>
       </div><!-- /.box-body -->
            <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 
@endsection
<!--<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>-->



 
