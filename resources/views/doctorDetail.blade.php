 
@extends('layout.app')
@section('title')
{{$collection['doctors'][0]->wholename}}@stop
@section('description') @stop
@section('keywords') @stop

@section('author'){{$collection['doctors'][0]->wholename}} @stop
@section('head')
<link rel="stylesheet" href="{{URL::asset('dist/build/css/bootstrap-datetimepicker.min.css') }}"> 

<link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="{{URL::asset('front_end/assets/css/bootstrap-image-gallery.css') }}"> 
<link rel="stylesheet" href="{{URL::asset('assets/css/custom.css') }}"> 

 <!-- <link href="{{URL::asset('dist/css/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    -->
@stop

<?php 
  $procedure_array          = array();
  $users_specialities_array = array();
 foreach($collection['users_procedures'] as $users_procedures):
   $procedure_array [] = $users_procedures->procedure_name;
 endforeach;
 foreach($collection['users_specialities'] as $users_specialities):
   $users_specialities_array [] = $users_specialities->name;
 endforeach; ?>

@section('footer')
<script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="{{ URL::asset('dist/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>

<script src="{{URL::asset('front_end/assets/js/bootstrap-image-gallery.js') }}"></script>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script> 
 
 Include English language 
<script src="{{URL::asset('dist/js/datepicker.min.js') }}"></script>

    
    <script src="{{URL::asset('dist/js/i18n/datepicker.en.js') }}"></script>-->
<script type="text/javascript" charset="utf-8" >
  
   
//var app = angular.module('myShoppingLists', ['']);
 
$( document ).ready(function() {
 
$('.loading').hide();
$(document).on('click','[data-load-remote]',function(e) {
  e.preventDefault();
  var $this  = $(this);
  var remote = $this.data('load-remote');
  if(remote) {
        $($this.data('remote-target')).load(remote);
    }
});

$(document).on('click','.marginalbumimages',function(e) {
    e.preventDefault();
    var $this    = $(this);
    var album_id = $this.data('id');
    var user_id  = $this.attr('data-value');
     // Load demo i 
  $.ajax({ 
    url: "{{url()}}/getjasonAlbumsPhotos",
    type: "get",
  data: {
         album_id : album_id,
         user_id  : user_id
       }

  }).done(function (result) { 
    var linksContainer = $('#links');
    linksContainer.html('');
    var baseUrlToIcons;
    var baseUrlToFullsize;
    var firsIsAlbumImage = 1;
    // Add the demo images as links with thumbnails to the page:
    if(result.length != 0){
   $.each(result, function (key, data) {
          baseUrlToIcons = "{{ url() }}/images/gallery/icon_size/"+data.original_name; 
          baseUrlToFullsize = "{{ url() }}/images/gallery/full_size/"+data.original_name;           
    $('<a/>').append($('<img>').prop('src', baseUrlToIcons).attr('class', 'gallery_margin').attr('style', 'margin:10px'))
        .prop('href', baseUrlToFullsize)
        .prop('title', data.filename)
        .attr('data-gallery', '')
        .appendTo(linksContainer)   
});
 }else linksContainer.html('<p><h1>No Images Found In This Album</h1></p>');
   $('#basicModal').modal('toggle');
  })

});
$('.datetimepicker3').datetimepicker(
     {
      format : 'DD-MM-YYYY'
     }
    );
  //DOCTOR DETAIL PAGE SCRIPT.
  // the below function is used for commenting the doctor
$('.leaveareview').click(function(e){
  e.preventDefault();
  $(".dr-tabs li").removeClass("active");
  $('#litab05').addClass('active');
  $(".tabbable .tab-content .tab-pane").removeClass("active");
  $('#tab05').addClass('active'); 
  $('html, body').animate({
    scrollTop: $("#comments-contents").offset().top
}, 1000);
  var doc_id   = $(this).data("id");
  var rattings = $(this).attr("data-value");
  $.get( "{{url()}}/checkLoginUser", { doctor_id: doc_id}, function( data ) {
  //$( ".result" ).html( data );
   
   if(data == "NO")
    {
      $("#userauthentication").show();
      $("#loginfor").val(rattings);
    }
   else if(data == "LOGIN")
   {
     $("#commentsform").show();
     $('#new-review').focus();
   }
   else
   {
     $("#commentsform").show();
     $("#commentsform").html('<div><h2>You have already rated this doctor.</h2></div>');
   }

   });
    
  });
// BELOW SCRIPT IS USED FOR SCHEDULLING FOR DOCTOR.
$('.scheduling-appointments').click(function(e) {
  e.preventDefault();
  $(".dr-tabs li").removeClass("active");
  $('#litab05').addClass('active');
  $(".tabbable .tab-content .tab-pane").removeClass("active");
  $('#tab05').addClass('active'); 
  $('html, body').animate({ scrollTop: $("#comments-contents").offset().top },
    1000);
 var doc_id          = $(this).data("id");
 var doc_appointment = $(this).attr("data-value");

  //alert(doc_appointment);
  $.get( "{{url()}}/checkLoginUser", { doctor_id: doc_id}, function( data ) {
  //$( ".result" ).html( data );
   
   if(data == "NO")
    {
      $("#userauthentication").show();
      $("#loginfor").val(doc_appointment);
    }
   else if(data == "LOGIN")
   {
    $("#scheduleform").show();
    //$("#commentsform").hide()
    //$('#new-review').focus();
   }
   else
   {
   // $("#commentsform").show();    
      $("#scheduleform").show();
   }

   });
    
  });
// Ajax for our form to SChdeual appointment.
$(document).on('click', '#scheduale_submit' , function(event) {
            event.preventDefault();
            var msg = '';
            var formData = {
                schedule_date     : $('input[name=scheduale_date]').val(),
                profile_doctor_id : $('input[name=profile_doctor_id]').val(),doctors_id : $('input[name=doctors_id]').val()
            }  
            if($('input[name=scheduale_date]').val() == '')
              { $( "#scheduale_date" ).parent( "#datetimepicker3" ).addClass( "has-error"); 
               $( "#scheduale_date" ).focus();
               $("#appointmentmsg").html('<div class="callout callout-danger"><p>Please chose a date.</p></div>');
                return false;
              }
       else
      { 
        $.ajaxSetup({
          headers: {
                    'X-XSRF-Token': $('meta[name="_token_schedule"]').attr('content')
                   }
            }); 
      $.ajax({
        url: '{{url()}}/schedualAppointment',
        type: "post",
        data: formData,
         beforeSend: function()
          {
            $('#scheduale_submit').html('Checking...');
          },
        success: function(data){
          switch (data)
                    {
                        case '1':
                            msg = 'Doctor is not available on this day.';
                             $('#scheduale_submit').html('Done');

                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                 $('#myTbody').html('');
                        break;
                        case '2':
                           msg = 'Appointments are full.Please select another day.';
                            $('#scheduale_submit').html('Done');

                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                 $('#myTbody').html('');
                        break;
                        case '3': 
                           msg = 'You have an appoinment on this day.';
                            $('#scheduale_submit').html('Done');

                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                 $('#myTbody').html('');
                        break;
                        case '4': 
                           msg = 'Please select coming date.';
                           $('#scheduale_submit').html('Done');
                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                 $('#myTbody').html('');
                        break;
                        case '5': 
                           msg = 'Not allowed.';
                           $('#scheduale_submit').html('Done');

                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                        break; 

                       default: 
                       var url = document.getElementById("url").textContent; 
                      // $('#myTbody').html('<img src="'+url+'/images/loader.gif">');
                       $("#appointmentmsg").html('');
                       $('#scheduale_submit').html('Done'); 
                       // msg = data;
                       $('#myTbody').html('<h2>Doctor Schedule.</h2><p>Chose yourdesire Time.</p><table class="table table-bordered"><tbody >'+data+'</tbody></table>');
                    }
          // else {
             

           //}
          }
      }); 
          
       }
      
       });
  //END. OF SCHEDUALAPPOINTMENT FORM.

  $(document).on('click', '.book-appointment' , function(event) {
            event.preventDefault();
            var profile_doctor_id = $(this).data('id');
            var schedule_date     = $(this).attr('data-date');
            var time              = $(this).attr('data-value');
              $(this).closest("table").find('td').removeClass("td_select"); 
              $(this).parents('td').toggleClass("td_select");
            var formData = {
                schedule_date     : schedule_date,
                profile_doctor_id : profile_doctor_id,
                schedule_time : time
            }
            // AJAX FUNCTION STARTS.
             $.ajax({
              url: '{{url()}}/get-available-days',
              type: "get",
              data: formData,
               beforeSend: function()
                {
                $('#scheduale_submit').html('Checking...');
                var url = document.getElementById("url").textContent; 
                $('#appointmentModal').modal('toggle');
                $('#appointmentModal-modal-body').html('<img  src="'+url+'/images/loader1.gif">');
              
                },
              success: function(data){
                $('#scheduale_submit').html('Done'); 
                switch (data)
                    {
                        case '1':
                            msg = 'You already have an appoinment on this day.';
                             $('#appointmentModal-modal-body').html('<p><h3>'+msg+'</h3></p>');
                           //  $('#appointmentModal').modal('toggle');
                        break;
                  case '2': 
                           msg = 'Please select coming date.';
                            $('#appointmentModal-modal-body').html('<p><h3>'+msg+'</h3></p>');
                           // $('#appointmentModal').modal('toggle');
                        break;
                  default: 
                   $('#appointmentModal-modal-body').html('<p><h3>Success , you will be informed about the status of your appointment.</h3></p>');
                   //$('#appointmentModal').modal('toggle');
                  break;
                }// end switch.
              }
            });

            //END AJAX FUNCTION.  
          });
// Ajax for our form to login users.
  $(document).on('click', '#login_submit' , function(event) {
            event.preventDefault();
            var loginfor    = $('input[name=loginfor]').val(); 
            var formData    = {
                login_email    : $('input[name=login_email]').val(),
                login_password : $('input[name=login_password]').val(),
                doctor_id      : $('input[name=doctor_id]').val()
            }  
            if($('#login_email').val() == '')
              { $( "#login_email" ).parent( ".form-group" ).addClass( "has-error"); 
            $("#signinmsg").html('<div class="callout callout-danger"><p>Please enter email.</p></div>');
                return false;
              }
       else
      {
      
        $.ajaxSetup({
                headers: {
                    'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                }
            }); 
      $.ajax({
        url: '{{url()}}/checklogin',
        type: "post",
        data: formData,
         beforeSend: function()
          {
            $('#login_submit').html('sending...');
          },
        success: function(data){  
          if(data == 'OK' && loginfor=='rattings'){
            $("#userauthentication").hide();
            $("#commentsform").show();
            
           }
           else if(data == 'COMMENTS' && loginfor=='rattings')
           {
            $("#userauthentication").hide();

            $("#commentsform").show();
            $("#commentsform").html('<div><h2>You have already rated this doctor.</h2></div>');
           }
            else if(data == 'OK' && loginfor=='appointment')
           {
            $("#userauthentication").hide(); 
             $("#scheduleform").show();
           }

            else if(data == 'COMMENTS' && loginfor=='appointment')
           {
            $("#userauthentication").hide();
             $("#scheduleform").show(); 
           }

           else {
               $("#signinmsg").html('<div class="callout callout-danger"><p>'+data+'</p></div>');
               $('#login_submit').html('login');

           }
          }
      }); 
          
       }
      
       });
        // END FUNCTION.

         $(document).on('click', '#newlogin_submit' , function(event) {
            event.preventDefault(); 
            var formData    = {
                login_email    : $('input[name=newlogin_email]').val(),
                login_password : $('input[name=newlogin_password]').val(),
                doctor_id      : $('input[name=doctor_id]').val()
            }  
            if($('#newlogin_email').val() == '')

              { $( "#newlogin_email" ).parent( ".form-group" ).addClass( "has-error"); 
            $("#newlogin_signuperrormsg").html('<div class="callout callout-danger"><p>Please Enter Email.</p></div>');
                return false;
              }
       else
      {
      
        $.ajaxSetup({
                headers: {
                    'X-XSRF-Token': $('meta[id="newlogin_token"]').attr('content')
                }
            }); 
      $.ajax({
        url: '{{url()}}/checklogin',
        type: "post",
        data: formData,
         beforeSend: function()
          {
            $('#newlogin_submit').html('sending...');
          },
        success: function(data){  
          if(data == 'OK' || data == 'COMMENTS'){
             
            location.reload();
           }
           else {
               $("#newlogin_signuperrormsg").html('<div class="callout callout-danger"><p>'+data+'</p></div>');
               $('#newlogin_submit').html('login');

           }
          }
      }); 
          
       }
      
       });
        // END FUNCTION.
  // Ajax for our form to login users.
       $(document).on('click', '#signupbtn' , function(event) {
          event.preventDefault(); 
          var formData = {
            email          : $('input[name=signup_email]').val(),
            password       : $('input[name=signup_password]').val(),
            password_confirmation : $('input[name=confirm_password]').val(),
            role           : 12
            }  
            if($('#signup_email').val() == '')
              {
                $("#signuperrormsg").html('<div class="callout callout-danger"><p>Please Enter Email.</p></div>');
                return false;
              }
       else
      {
      
        $.ajaxSetup({
                headers: {
                    'X-XSRF-Token': $('meta[id="signup_token"]').attr('content')
                }
            }); 
      $.ajax({
        url: '{{url()}}/checksignup',
        type: "post",
        data: formData,
         beforeSend: function()
          {
            $('#signupbtn').html('sending...');
          },
        success: function(data){   
          if(data == 'OK'){
            $("#userauthentication").hide();
             $("#commentsform").show();
            
           }else { 

            $("#signuperrormsg").html('<div class="callout callout-danger"><p>'+data+'</p></div>');
             $('#signupbtn').html('sign up');
           }
          }
      }); 
          
       }
      
       });
        // END FUNCTION.

  // Ajax for our form to login users.
       $(document).on('click', '#new_submit' , function(event) {
          event.preventDefault(); 
          var formData = {
            email          : $('input[name=new_email]').val(),
            password       : $('input[name=new_password]').val(),
            password_confirmation : $('input[name=new_password_confirmation]').val(),
            role           : 12
            }  
            if($('#new_email').val() == '')
              {
                 $("#new_signuperrormsg").html('<div class="callout callout-danger"><p>Please Enter Email.</p></div>');
                return false;
              }
       else
      {
      
        $.ajaxSetup({
                headers: {
                    'X-XSRF-Token': $('meta[id="new_token"]').attr('content')
                }
            }); 
      $.ajax({
        url: '{{url()}}/checksignup',
        type: "post",
        data: formData,
         beforeSend: function()
          {
            $('#new_submit').html('sending...');
          },
        success: function(data){   
          if(data == 'OK'){
          //  $("#userauthentication").hide();
           //  $("#commentsform").show();
            
           }else { 

            $("#new_signuperrormsg").html('<div class="callout callout-danger"><p>'+data+'</p></div>');
             $('#new_submit').html('sign up');
           }
          }
      }); 
          
       }
      
       });
        // END FUNCTION.

        $('.newlogin_closebtn').click(function() {
          location.reload();
      });

//*********************TOGEL SCRIPT *********************************


//var count_allprocedures     = <?php //echo count($collection['users_procedures']);?>;
//var count_allspecialities     = <?php //echo count($collection['users_specialities']);?>;

var procedure_array          = <?php echo json_encode($procedure_array);?> 
var users_specialities_array = <?php echo json_encode($users_specialities_array);?>;
var procedure_html           = '<ul>';
var firstThreeProcedure      = '';
var count_allprocedures      = 0;
   $.each(procedure_array, function(k, v) {
     if(count_allprocedures < 3) {
    firstThreeProcedure += '<li><a href="#">'+v+'</a></li>';
    }
      procedure_html  += '<li><a href="#">'+v+'</a></li>';
      count_allprocedures++;
   });

 procedure_html  += '</ul>';
 $(".hide_ul").html(firstThreeProcedure);
 $('.tab_procedures').html(procedure_html);
 if(count_allprocedures > 3){
  $(".view_tabpocedure").click(function(){

        $(".hide_ul").hide(1000);
    });
$('.view_tabpocedure').text('See all '+ count_allprocedures +' procedures');
}
else
{
  $('.hide_ul').next('a').remove();
  //$('.view_tabpocedure').text('Above are total  procedures');
}
// BELOW CONDITIONS.
var arr = '';
arr = <?php echo ($collection['users_conditions'] ? $collection['users_conditions']: true);?>; 
if(arr != 1)
{
    var count_conditions     = 0;
    var firstThreeConditions = '';
    var conditions_html      = '<ul>';

      $.each( arr, function( index, value ) {
        if(count_conditions < 3) {
    firstThreeConditions += '<li><a href="#">'+value.condition_name+'</a></li>';
    }
        conditions_html  += '<li><a href="#">'+value.condition_name+'</a></li>';
        count_conditions ++;
       });

     conditions_html  += '</ul>';
     $(".conditions_ul").html(firstThreeConditions);
     $('.tab_conditions').html(conditions_html);
      if(count_conditions < 3) {
     $(".view_tabconditions").click(function(){
        $(".conditions_ul").hide(1000);
      });
    $('.view_tabconditions').text('See all '+count_conditions+' conditions');
  }
  else
  {
   $(".conditions_ul").next('a').remove();
   // $('.view_tabconditions').text('Above are total conditions');
  }
 }
//------------------------CONDITIONS ENDS HERE-----------------------------------
var specialities_html     = '<ul>';
var count_allspecialities = 0;
firstThreeSpecialities     = '';      
   $.each(users_specialities_array, function(k, v) {
     if(count_allspecialities < 3) {
    firstThreeSpecialities += '<li><a href="#">'+v+'</a></li>';
    }
      specialities_html  += '<li><a href="#">'+v+'</a></li>';
      count_allspecialities++;
   });
 specialities_html  += '</ul>';
 
 $('.specialities_top_ul').html(specialities_html);
 $('.tab_specialities').html(specialities_html);
 if(count_allspecialities > 3){
 $(".view_tabspecialities").click(function(){
 $(".specialities_top_ul").hide(1000);});

$('.view_tabspecialities').text('See all '+ count_allspecialities +' Specialities');
}else{
  $('.specialities_top_ul').next('a').remove();
 //$('.view_tabspecialities').text('Above are total Specialities'); 
}
//-------------ASSOCIATIONS CODE-----------------------------------
<?php 
if(count($collection['users_associations'])> 0 ):
  $assoc_html = '';
foreach($collection['users_associations'] as $associations):
  $assoc_html .='<ul><li><img src="'.url().'/images/associations/'.$associations->assoc_logo.'" alt="'.$associations->name.'" title="'.$associations->name.'" class="img-responsive"></li></ul>';
endforeach;
endif;

  $business_avatar = '<li><img src="'.url().'/images/catalog/'.$collection['business_profile']->avatar.'"  alt="'.$collection['business_profile']->business_name.'" title="'.$collection['business_profile']->business_name.'" class="img-responsive"></li>';
?>
var assoc_html = '<?php echo $assoc_html;?>';
$('.hospital-right').html(assoc_html);

$('.association_logos').html('<?php echo $business_avatar;?>');

$('.hospital-left').prepend( '<?php echo "<h4>".$collection['business_profile']->business_name."</h4><address><p>".$collection['business_profile']->business_address."</p></address>" ?>' );


<?php $address = $collection['business_profile']->business_address;
 //'Bacha+Khan+University,Charsadda+District,Khyber+Pakhtunkhwa,Pakistan';
//https://maps.googleapis.com/maps/api/staticmap?center=Australia&size=640x400&style=element:labels|visibility:off&style=element:geometry.stroke|visibility:off&style=feature:landscape|element:geometry|saturation:-100&style=feature:water|saturation:-100|invert_lightness:true&key=
?>
var gmap = '<?php echo '<img src="https://maps.googleapis.com/maps/api/staticmap?center='.$address.'&zoom=13&size=440x240&maptype=roadmap&markers=color:red%7Clabel:S%7C'.$address.'&key=AIzaSyCG1SQaFGcPwop48cv4frngJrn-DssuRC4"/>';?>';
$('.gmap').html(gmap);
  
//*******************TOGGLE SCRIPT ENDS HERE**************************


//--------------------------
$('.phonoanddirection').click(function(e) {
  e.preventDefault();
  $(".dr-tabs li").removeClass("active");
  $('#litab02').addClass('active');
  $(".tabbable .tab-content .tab-pane").removeClass("active");
  $('#tab02').addClass('active'); 
  $('html, body').animate({
    scrollTop: $(".phone_address").offset().top
}, 1000);
});

$('.enquiry').click(function(e){
  e.preventDefault(); 
  $('#enquirymsg').html('');
  $.ajax({ 
     url: '{{url()}}/checkUserIfLogin',
        type: "get", 
         beforeSend: function()
          {
            $('#patient_enquiry').modal('toggle');
            $('#enquiry_modal_body').html('loading...');

          },      
    success:  function (response) {           
        if (response == '0')
        { 
      $('#enquiry_modal_body').html('<form name="enquiry_form" id="enquiry_form" class="form-horizontal enquiry_form" role="form" method="POST" action=""><meta name="_token" content="{{app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token())}}" id="enquiry_token" />  <input name="doctor_id" id="doctor_id" value="{{ $collection['doctors'][0]->user_id}}" type="hidden"> </input> <div id="enquirymsg"></div><div class="list-group list-group-sm"><div class="col-md-12"> <div class="form-group"><label class="col-sm-2 control-label" for="inputEmail3">Name</label><div class="col-sm-10"><input type="text" class="form-control" name="enquiry_name" id="enquiry_name" value="{{ old('enquiry_name') }}" placeholder="Enter Name"></div></div></div><div class="col-md-12"><div class="form-group"><label class="col-sm-2 control-label" for="inputEmail3">Email</label><div class="col-sm-10"><input type="email" class="form-control" name="enquiry_email" id="enquiry_email" value="{{ old('enquiry_email') }}" placeholder="Enter Email"></div></div></div><div class="col-md-12"><div class="form-group"><label class="col-sm-2 control-label" for="inputEmail3">Phone</label><div class="col-sm-10"><input type="text" class="form-control" name="enquiry_phone" id="enquiry_phone" value="{{ old('enquiry_phone') }}" placeholder="Enter Phone"></div></div></div><div class="col-md-12"><label class="col-sm-2 control-label" for="inputEmail3">Type Enquiry</label><div class="col-sm-10"><textarea class="form-control animated" cols="50" id="enquiry_message" name="enquiry_message" placeholder="Enter Enquiry...." rows="5" style="margin-bottom:30px" ></textarea></div></div></div><button type="submit" name="enquiry_submit" id="enquiry_submit" class="btn btn-lg btn-primary btn-block">Submit Enquiry</button><div class="line line-dashed"></div></form>');
        }
  
  if (response == '1')
  { 
    $('#enquiry_modal_body').html('<form name="enquiry_form" id="enquiry_form" class="form-validation enquiry_form" role="form" method="POST" action=""><meta name="_token" content="{{app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token())}}" id="enquiry_token" /><input name="doctor_id" id="doctor_id" value="{{ $collection['doctors'][0]->user_id}}" type="hidden"> </input> <div id="enquirymsg"></div><div class="list-group list-group-sm"><div class="col-md-12" ng-app="myEnquiry" ><textarea class="form-control animated" cols="50" id="enquiry_message" name="enquiry_message" placeholder="Enter Enquiry...." rows="5" style="margin-bottom:30px"  ></textarea><span ng-bind="200 - enquiry_message.Comment.body.length"></span> Remaining</div></div><button type="submit" name="enquiry_submit" id="enquiry_submit" class="btn btn-lg btn-primary btn-block">Submit Enquiry</button><div class="line line-dashed"></div></form>');
  }
          
              
     }// SUCCESS ENDS HERE.
 }); 
});
 // submit query form.


 $(document).on('submit','.enquiry_form',function(e) {
    // get all the inputs into an array. 
    var $inputs = $('.enquiry_form :input');
    e.preventDefault();
    // not sure if you wanted this, but I thought I'd add it.
    // get an associative array of just the values.
    var formData = {};
    $inputs.each(function() {
        formData[this.name] = $(this).val();
    }); 
    if(formData.enquiry_message == '')
    { 
      $('#enquirymsg').html('<div class="callout callout-danger"><p>Please enter enquiry message.</p></div>');
    }
    else 
    {
        
        $.ajaxSetup({
                headers: {
                    'X-XSRF-Token': $('meta[id="enquiry_token"]').attr('content')
                }
            }); 
      $.ajax({
        url: '{{url()}}/submitEnquiry',
        type: "post",
        data: formData,
        dataType: 'json',
         beforeSend: function()
          {
            $('#enquiry_submit').html('sending...');
          },
        success: function(data){
          if(data.status == 'fail')
          {
         $('#enquirymsg').html('<div class="callout callout-danger"><p>'+data.msg+'</p></div>');
           $('#enquiry_submit').html('Submit Enquiry');  
       }
       if(data.status == 'success') {
         $('#enquiry_modal_body').html('<div class="alert alert-success alert-dismissable"><p>'+data.msg+'</p></div>');
       $('#enquiry_submit').hide();  

       }
                
          }// END OF SUCCESS FUNCTION.
      }); 
     
    }// end of 
});


//$('#scheduale_date').datepicker({
//  language: 'en',
//}); APPOINTMENT SCRIPT IS BELOW .

$('#scheduale_date').val($.datepicker.formatDate('dd-mm-yy', new Date()));///GET CURRENT DATE AND POPULATE THE HIDDEN FIELD.
var minDate = $.datepicker.formatDate('yy-mm-dd', new Date());// THE START DATE FOR CALANDER FO APPOONTMENT

Date.prototype.calcFullMonths = function(monthOffset) {
  //copy the date
  var dt = new Date(this);
  dt.setMonth(dt.getMonth() + monthOffset);
  return dt;
}; 

<?php  
foreach($collection['offDays'] as $offDays)
{
  switch($offDays->days)
  {
    case "sunday":
    $offdaysArray[] = 0;
    break;
    case "monday":
    $offdaysArray[] = 1;
    break;
    case "tuesday":
    $offdaysArray[] = 2;
    break;
    case "wednesday":
    $offdaysArray[] = 3;
    break;
    case "thursday":
    $offdaysArray[] = 4;
    break;
    case "friday":
    $offdaysArray[] = 5;
    break;
    case "saturday":
    $offdaysArray[] = 6;
    break;
    }

  //$date      = new DateTime($offDays->days);
  //$thisMonth = $date->format('m');
  //while ($date->format('m') === $thisMonth) {
     // $offdaysArray[] =  $date->format('d');
      // $date->modify('next '.$offDays->days);
      // break;
    // }
}   
?>
var weekday_unchecked_value = <?php echo json_encode($offdaysArray); ?>;

// reference taken http://stackoverflow.com/questions/38515423/modify-datetimepicker-dynamically-with-javascript
//$("#meeting_datetime").data("DateTimePicker").enabledHours([meeting_time_moment.hours()])
//$("#meeting_datetime").data("DateTimePicker").stepping(60)
 
 //$day = 'Fri';
//echo $nmonth = date('j',strtotime($day));
  var add3MonthWithCurrDate = moment((new Date()).calcFullMonths(4)).format('YYYY-MM-DD'); 
$('#datetimepicker12').datetimepicker({
      format     : 'DD-MM-YYYY',
      inline     : true,
      sideBySide : true,
      minDate    : '-'+minDate,//yesterday is minimum date(for today use 0 or -1970/01/01)
      maxDate    : '+'+add3MonthWithCurrDate,//tomorrow is maximum date calendar
       
    }).on('dp.change',function(e){

  var clicked_date = e.date.format('DD-MM-YYYY');
  $('#scheduale_date').val(clicked_date);

//#################AJAX CALLS FOR APPOINTMENT BY CLICKING A DATE.#############################
var msg = '';
            var formData = {
                schedule_date     : $('input[name=scheduale_date]').val(),
                profile_doctor_id : $('input[name=profile_doctor_id]').val(),doctors_id : $('input[name=doctors_id]').val()
            }  
            if($('input[name=scheduale_date]').val() == '')
              { $( "#scheduale_date" ).parent( "#datetimepicker3" ).addClass( "has-error"); 
               $( "#scheduale_date" ).focus();
               $("#appointmentmsg").html('<div class="callout callout-danger"><p>Please chose a date.</p></div>');
                return false;
              }
       else
      { 
        $.ajaxSetup({
          headers: {
                    'X-XSRF-Token': $('meta[name="_token_schedule"]').attr('content')
                   }
            }); 
      $.ajax({
        url: '{{url()}}/schedualAppointment',
        type: "post",
        data: formData,
         beforeSend: function()
          {
            $('#scheduale_submit').html('Checking...');
          },
        success: function(data){
          switch (data)
                    {
                        case '1':
                            msg = 'Doctor is not available on this day.';
                             $('#scheduale_submit').html('Done');

                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                 $('#myTbody').html('');
                        break;
                        case '2':
                           msg = 'Appointments are full.Please select another day.';
                            $('#scheduale_submit').html('Done');

                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                 $('#myTbody').html('');
                        break;
                        case '3': 
                           msg = 'You have an appoinment on this day.';
                            $('#scheduale_submit').html('Done');

                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                 $('#myTbody').html('');
                        break;
                        case '4': 
                           msg = 'Please select coming date.';
                           $('#scheduale_submit').html('Done');
                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                 $('#myTbody').html('');
                        break;
                        case '5': 
                           msg = 'Not allowed.';
                           $('#scheduale_submit').html('Done');

                $("#appointmentmsg").html('<div class="callout callout-danger"><p>'+msg+'</p></div>');
                        break; 

                       default: 
                       var url = document.getElementById("url").textContent; 
                      // $('#myTbody').html('<img src="'+url+'/images/loader.gif">');
                       $("#appointmentmsg").html('');
                       $('#scheduale_submit').html('Done'); 
                       // msg = data;
                       $('#myTbody').html('<h2>Doctor Schedule.</h2><p>Chose yourdesire Time.</p><table class="table table-bordered"><tbody >'+data+'</tbody></table>');
                    }
          // else {
             

           //}
          }
      }); 
          
       }
      
//################END OF AJAX APPOINTMENT####################################
  });



$("#datetimepicker12").data("DateTimePicker").daysOfWeekDisabled(weekday_unchecked_value);

});// DOCUMENT READY FUNCTION ENDS HERE.

</script>
@stop

@section('content') 
<!-- appoint ment message -->
<div class="modal fade" id="registrationform" tabindex="-1" role="dialog" aria-labelledby="registrationform" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Registeration</h4>
          </div>
          <div class="modal-body text-center" id="modal-body"> 
 
            <form name="new_form" id="new_form" class="form-validation" role="form" method="POST" action=""> 

           <meta name="_token" content="{{ app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token()) }}" id="new_token" />

          <div id="new_signuperrormsg"  >
 

          </div>

          <div class="list-group list-group-sm">
            
            <div class="list-group-item">
              <input type="email" class="form-control no-border" name="new_email" id="new_email" value="{{ old('email') }}" placeholder="Email" required>

            </div>
  
            <input type="hidden" name="role" value="12" id="role"></input>
             

            <div class="list-group-item">
              <input type="password" class="form-control no-border" name="new_password" placeholder="Password" required>
            </div>
            <div class="list-group-item">
                <input type="password" class="form-control no-border" name="new_password_confirmation" placeholder="Confirm Password" required>
            </div>
          </div>


          <button type="submit" name="new_submit" id="new_submit" class="btn btn-lg btn-primary btn-block">
            Sign up
          </button>
          <div class="line line-dashed"></div>

        </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
          </div>
        </div>
      </div>
    </div>



    <!-- appoint LOGIN FORM . -->
<div class="modal fade" id="newloginform" tabindex="-1" role="dialog" aria-labelledby="newloginform" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close newlogin_closebtn" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Please Login</h4>
          </div>
          <div class="modal-body text-center" id="modal-body"> 
 
            <form name="newlogin_form" id="newlogin_form" class="form-validation" role="form" method="POST" action=""> 

           <meta name="_token" content="{{ app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token()) }}" id="newlogin_token" />

          <div id="newlogin_signuperrormsg"  >
 

          </div>

          <div class="list-group list-group-sm">
            
            <div class="list-group-item">
              <input type="email" class="form-control no-border" name="newlogin_email" id="newlogin_email" value="{{ old('newlogin_email') }}" placeholder="Email" required>

            </div>  

            <div class="list-group-item">
              <input type="password" class="form-control no-border" name="newlogin_password" placeholder="Password" required>
            </div> 
          </div>


          <button type="submit" name="newlogin_submit" id="newlogin_submit" class="btn btn-lg btn-primary btn-block">
            login
          </button>
          <div class="line line-dashed"></div>

        </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default newlogin_closebtn" data-dismiss="modal">Close</button>
            
          </div>
        </div>
      </div>
    </div>

    <!--INQUERY FORM STARTS HERE.--> 
<div class="modal fade" id="patient_enquiry" tabindex="-1" role="dialog" aria-labelledby="patient_enquery" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Enquery Form</h4>
          </div>

          <div id="enquirymsg"></div>

          <div class="modal-body text-center" id="enquiry_modal_body"> 
 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
          </div>
        </div>
      </div>
    </div>


    <!--END ENQUERY FORM.-->

<!-- appoint ment message -->
<div class="modal fade" id="appointmentModal" tabindex="-1" role="dialog" aria-labelledby="appointmentModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Message!</h4>
          </div>
          <div class="modal-body text-center" id="appointmentModal-modal-body"> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
          </div>
        </div>
      </div>
    </div>
<!-- GALLERY MODAL STARTS HERE.-->
<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="galleryModal" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Click On image to view Full Size.</h4>
          </div>
          <div class="modal-body">
            <h3>Gallery Images</h3>

          <div id="links"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
          </div>
        </div>
      </div>
    </div>

<!-- GALLERY MODAL ENDS HERE.-->
@include('headerListingDetail') 
<!-- Doctor Detail Profile start here -->
<div class="dr-det-prof">
<div class="container">

<!-- MODAL TO SHOW ALBUM GALLERY .
http://jsfiddle.net/sherbrow/thlyb/ -->
 <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Click On album to see images.</h4>
          </div>
          <div class="modal-body"> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
          </div>
        </div>
      </div>
    </div>

<!-- END ALBUM SHOW MODAL -->
<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
     <div class="row">
     <ul class="breadcrumb "> 

<?php if(\App\Helpers\MyHelper::getFieldName('state', $collection['doctors'][0]->state_id,'id','state_name') != '0'):
 ?>
<li><a href="{{ url('searchesbystates').'/'.$collection['doctors'][0]->state_id}}?sort_by=id">{{\App\Helpers\MyHelper::getFieldName('state', $collection['doctors'][0]->state_id,'id','state_name')}}</a> <span class="divider">»</span></li>
<?php endif;
 if(\App\Helpers\MyHelper::getFieldName('city', $collection['doctors'][0]->city_id,'id','city_name') != '0'):
 ?>
<li><a href="{{ url('searchesbystates').'/'.$collection['doctors'][0]->state_id.'?state_citie='.$collection['doctors'][0]->city_id}}&sort_by=id">{{\App\Helpers\MyHelper::getFieldName('city', $collection['doctors'][0]->city_id,'id','city_name')}}</a> <span class="divider">»</span></li>
 <?php endif;?>
 
<li><a href="{{ url('searchesbystates').'/'.$collection['doctors'][0]->state_id.'?state_citie='.$collection['doctors'][0]->city_id.'&'.'catagory='.$collection['doctors'][0]->specialities_id}}&sort_by=id">{{\App\Helpers\MyHelper::getFieldName('specialities', $collection['doctors'][0]->specialities_id,'id','name')}}</a> <span class="divider">»</span></li>
 

<li>{{$collection['doctors'][0]->wholename}}</li>
</ul> 
  </div></div>
  <!--BREADCURMS ENDS -->
  <div class="container"> 
    <div class="row">

      <div class="col-md-3 col-sm-3 col-xs-12 prof-img">
      <?php
      if($collection['doctors'][0]->image_of_doctor == '')
      {
        if($collection['doctors'][0]->gender == 'Male')
        $image_url = url().'/images/m4.png';
      else $image_url = url().'/images/f1.jpg';
      }else  $image_url = url().'/images/catalog/'.$collection['doctors'][0]->image_of_doctor;       

      ?>
        <figure> <img src="{{ $image_url }}" alt="detail dr image" class="img-responsive"> </figure>
      </div>
      <div class="col-md-9 col-sm-9 col-xs-12 prof-disc">
        <h1>{{ $collection['doctors'][0]->wholename}} , {{ $collection['doctors'][0]->education}} </h1>
       @if(count($collection['users_procedures']) !=0) <span class="abt-dr"> Specializes in : <br/>
@foreach($collection['users_procedures'] as $users_procedures)
     {{$users_procedures->procedure_name}},   
@endforeach
@endif
          <br/>
        <ul>
          <li>{{ $collection['doctors'][0]->gender}}</li>
          <li>Age  {{ \App\Helpers\MyHelper::GetAge($collection['doctors'][0]->user_dob)}} </li>
        </ul>
        </span>
        <div class="dr-det-left">
          <p>Patient Satisfaction</p>
          <input name="user_id" id="user_id" value="{{ $collection['doctors'][0]->user_id}}" type="hidden"> </input> 

          <div class="stars">
                <?php $rating = round($collection['averageUserReviews']);?>
                <?php for($istars =1; $istars<=5; $istars++):
                  if($rating >=1)
                  {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star';
                  }else {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star-empty';
                  }
                  ?>
                <span class="<?php echo $class;?>"></span> 
                <?php $rating --;
                endfor; ?>
                </div>
          <span class="link-sp"><a href="#" class="dr-links">{{count($collection['userReviews'])}} responses</a></span> <span class="link-sp"><a href="#" class="dr-links leaveareview"  data-id="{{$collection['doctors'][0]->user_id}}" data-value="rattings">Take a survey</a></span> </div>
        <div class="dr-det-right"> <span class="link-sp"><a href="#" class="dr-links">{{$collection['business_profile']->business_name}}</a></span>
          <p>{{$collection['business_profile']->business_address}}<br>
            {{$collection['business_profile']->business_address2}}</p>
          <span class="link-sp"><a href="#" class="dr-links phonoanddirection">Phone Number & Directions</a></span> </div>
        <div class="btn-links">
@if ( \Cartalyst\Sentinel\Native\Facades\Sentinel::check() === false )
         
      <!--  <a  href="#newloginform" role="button" class="btn" data-toggle="modal"  data-remote-target="#newloginform .modal-body" >Existing PATIENT</a> -->
@endif
        <!--<a href="#enquiry" class="btn enquiry"  >Other Enquirys</a>--> </div>
      </div>
    </div>
  </div>
</div>
<!-- Doctor Detail Profile End here -->

<!-- welcome nationwide start here -->
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 hidden-xs tab-links">
      <ul class="nav nav-tabs dr-tabs">
        <li class="active"><a href="#tab01" data-toggle="tab"><em class="fa fa-cog"></em> Overview</a></li>
        <li class="" id="litab02"><a href="#tab02" data-toggle="tab"><em class="fa fa-map-marker"></em> Phone& Address</a></li>
        <li class=""><a href="#tab03" data-toggle="tab"><em class="fa fa-pencil-square-o"></em> Experience</a></li>
        <li class=""><a href="#tab04" data-toggle="tab"><em class="fa fa-h-square"></em> Hospital Quality</a></li>
        <li class="" id="litab05"><a href="#tab05" data-toggle="tab"><em class="fa fa-comments-o"></em> Patient Satisfaction</a></li>
      </ul>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12 left-content">



      <div class="tabbable hidden-xs">
        <div class="tab-content  dr-tab-cont">
          <div class="tab-pane active" id="tab01">
            <div class="row">
              <div class="col-xs-12 dr-title"> <span>Key things you need to know about</span>
                <h2>{{ $collection['doctors'][0]->wholename}}</h2>
              </div>
              <div class="col-xs-12 dr-det-sect phone-address">
                <div class="icon-colm"> <em class="fa fa-map-marker"></em>
                  <div class="det-title">
                    <h3>Phone & Address</h3>
                    <span>Check insurance plans, locations and make an appointment</span> </div>
                </div>
                <div class="dr-det-btm phone-address-bg">
                  <address>
                  <p>{{ $collection['doctors'][0]->wholename}}</p>

                  <p>{{$collection['business_profile']->business_address}}</p>
                  <p>{{$collection['business_profile']->business_address2}}</p>
                  <p>{{ $collection['doctors'][0]->contact_dr_phone_number}}</p>

                  </address>
                  <div style="border:#000"  class="gmap">
                  </div>


                  <div class="btn-links">

<a href="#basicModal" role="button" class="btn" data-toggle="modal" data-load-remote="{{ url() }}/getjasonAlbums/{{$collection['doctors'][0]->user_id}}" data-remote-target="#basicModal .modal-body">
        Album Gallery</a> 

@if ( \Cartalyst\Sentinel\Native\Facades\Sentinel::check() === false )
         
                 <!-- <a href="#registrationform" role="button" class="btn" data-toggle="modal"  data-remote-target="#registrationform .modal-body" >NEW PATIENT</a> <a href="#newloginform"  role="button" class="btn" data-toggle="modal"  data-remote-target="#newloginform .modal-body" >Existing PATIENT</a> -->
@endif
                  <!-- <a href="#" class="btn">Other Enquiry</a> --> </div>

                </div>
              </div>
              <div class="col-xs-12 dr-det-sect experience-colm">
                <div class="icon-colm"> <em class="fa fa-pencil-square-o"></em>
                  <div class="det-title">
                    <h3>experience</h3>
                    <span>Research training, expertise and qualifications</span> </div>
                </div>
                <div class="dr-det-btm">
                  <div class="dr-exp">
                  <div class="exp-links">
                    <h5>Dr. {{ $collection['doctors'][0]->wholename}}'s Specialties</h5>
                    <ul class="specialities_top_ul"> 
                    </ul>

                      <a data-toggle="collapse"  href="#tab_specialities1" class="view-all view_tabspecialities"> 0 specialty</a>
                    
               <div id="tab_specialities1" class="collapse tab_specialities"></div>
 
                    </div>

                    <div class="exp-links">
                    <h5>Procedures Dr. {{ $collection['doctors'][0]->wholename}} Performs</h5>
                  <ul class="hide_ul">
                       @foreach($collection['users_procedures'] as $users_procedures)
                  <li><a href="#">  {{$users_procedures->procedure_name}} </a></li>  
                     @endforeach
                   
                    </ul>

                    <a data-toggle="collapse"  href="#tab_procedures1" class="view-all view_tabpocedure"> 0 procedures</a>
                    
                    <div id="tab_procedures1" class="collapse tab_procedures">
 

                    </div>
                    </div>
                    <div class="exp-links">
                    <h5>Conditions Dr. {{ $collection['doctors'][0]->wholename}} Treats</h5>
                    <ul class="conditions_ul"> 
                    </ul>
                    <a data-toggle="collapse"  href="#tab_conditions1" class="view-all view_tabconditions"> 0 conditions</a>
                    <div id="tab_conditions1" class="collapse tab_conditions"></div>

                    </div>
 
                  </div>
                  @if(@$collection['latest_review']->comment != '')
                  <div  class="quote-colm"> <em class="fa fa-quote-left"></em>
                    <p>{!!@$collection['latest_review']->comment !!}</p>
                  </div>
                  @endif
                </div>
              </div>
              <div class="col-xs-12 dr-det-sect hospital-quality">
                <div class="icon-colm"> <em class="fa fa-h-square"></em>
                  <div class="det-title">
                    <h3>Hospital Quality</h3>
                    <span>The right hospital is as important as the right doctor</span> </div>
                </div>
                <div class="dr-det-btm hospital-quality-bg">
                  <div class="hospital-left">
                    
                    <ul class="association_logos">
                   
                    </ul>
                  </div>
                  <div class="hospital-right">
                  

                  </div>
                </div>
              </div>
              <div class="col-xs-12 dr-det-sect patience-setisfiction">
                <div class="icon-colm"> <em class="fa fa-comments-o"></em>
                  <div class="det-title">
                    <h3>Patient Satisfaction</h3>
                    <span>Patients' feedback on their experience with Dr. {{ $collection['doctors'][0]->wholename}}</span> </div>
                </div>
                <div class="dr-det-btm patience-setisf-bg">
                  <div class="reviews-disc">
                    <p>Likelihood of recommending Dr. {{ $collection['doctors'][0]->wholename}} to family and friends is 4.9 out of 5</p>
                  </div>
                  <div class="reviews-star"> 
                    <div class="stars">
                <?php $rating = round($collection['averageUserReviews']);?>

                <?php for($istars =1; $istars<=5; $istars++):
                  if($rating >=1)
                  {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star';
                  }else {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star-empty';
                  }
                  ?>

                <span class="<?php echo $class;?>"></span> 

                <?php $rating --;
                endfor; ?>
                 
                </div>  
                  </div>
                  <div class="survey-results">
                    <h4>See More Survey Results</h4>
                    <ul>  
                      <li><a href="#" class="scheduling-appointments" data-id="{{$collection['doctors'][0]->user_id}}" data-value="appointment" data-value="appointment">Scheduling Appointments</a></li>
                    </ul>
                  </div>
                  <div class="take-survey">
                    <p>{{count($collection['userReviews'])}} responses</p>
                    <a href="#"  data-id="{{$collection['doctors'][0]->user_id}}" class="leaveareview">Take a Review</a> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tab02">
          <div class="row">
          <div class="col-xs-12 dr-det-sect phone-address">
                <div class="icon-colm"> <em class="fa fa-map-marker"></em>
                  <div class="det-title phone_address">
                    <h3>Phone & Address</h3>
                     </div>
                </div>
                <div class="dr-det-btm phone-address-bg">
                  <address>
                  <p>{{ $collection['doctors'][0]->wholename}}</p> 
                  <p>{{$collection['business_profile']->business_address}}</p>
                  <p>{{$collection['business_profile']->business_address2}}</p>
                  <p>{{ $collection['doctors'][0]->contact_dr_phone_number}}</p>

                  </address>
 <div  class="gmap">
 </div>


                  <div class="btn-links"> 
                  @if ( \Cartalyst\Sentinel\Native\Facades\Sentinel::check() === false )
      <!--<a href="#registrationform" role="button" class="btn" data-toggle="modal"  data-remote-target="#registrationform .modal-body" >NEW PATIENT</a> <a  href="#newloginform" role="button" class="btn" data-toggle="modal"  data-remote-target="#newloginform .modal-body" >Existing PATIENT</a>
<a href="#" class="btn">Other Enquiry</a>-->
         @endif
           </div>

                </div>
              </div>
          </div>
          </div>
          <div class="tab-pane" id="tab03">
          <div class="row">
          <div class="col-xs-12 dr-det-sect experience-colm">
                <div class="icon-colm"> <em class="fa fa-pencil-square-o"></em>
                  <div class="det-title">
                    <h3>experience</h3>
                    <span>Research training, expertise and qualifications</span> </div>
                </div>
                <div class="dr-det-btm">
                  <div class="dr-exp">
                  <div class="exp-links">
                    <h5>Dr. {{ $collection['doctors'][0]->wholename}}'s Specialties</h5>
                        <ul class="specialities_top_ul"> 
                    </ul>

                      <a data-toggle="collapse"  href="#tab_specialities2" class="view-all view_tabspecialities"> 0 specialty</a>
                    
               <div id="tab_specialities2" class="collapse tab_specialities"></div>
                    </div>

                    <div class="exp-links">
                    <h5>Procedures Dr. {{ $collection['doctors'][0]->wholename}} Performs</h5>
                    <ul class="hide_ul">
                       @foreach($collection['users_procedures'] as $users_procedures)
                  <li><a href="#">  {{$users_procedures->procedure_name}} </a></li>  
                     @endforeach
                   
                    </ul>

                    <a data-toggle="collapse"  href="#tab_procedures2" class="view-all view_tabpocedure"> 0 procedures</a>
                    
                    <div id="tab_procedures2" class="collapse tab_procedures">
 

                    </div>

                    </div>
                    <div class="exp-links">
                    <h5>Conditions Dr. {{ $collection['doctors'][0]->wholename}} Treats</h5>
                    
                    <ul class="conditions_ul">

                    </ul>
                    <a data-toggle="collapse"  href="#tab_conditions2" class="view-all view_tabconditions"> 0 conditions</a>
                    <div id="tab_conditions2" class="collapse tab_conditions"></div>
                    </div> 
                  </div>
                  <div  class="quote-colm"> <em class="fa fa-quote-left"></em>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
                  </div>
                </div>
              </div>
          </div>
          </div>
          <div class="tab-pane" id="tab04">
          <div class="row">
          <div class="col-xs-12 dr-det-sect hospital-quality">
                <div class="icon-colm"> <em class="fa fa-h-square"></em>
                  <div class="det-title">
                    <h3>Hospital Quality</h3>
                    <span>The right hospital is as important as the right doctor</span> </div>
                </div>
                <div class="dr-det-btm hospital-quality-bg">
                  <div class="hospital-left">

                    <ul class="association_logos">
                    
                    </ul>
                  </div>
                  <div class="hospital-right">
                 

                  </div>
                </div>
              </div>
          </div>
          </div>
          <div class="tab-pane" id="tab05">
          <div class="row">
          <div class="col-xs-12 dr-det-sect patience-setisfiction">
                <div class="icon-colm"> <em class="fa fa-comments-o"></em>
                  <div class="det-title">
                    <h3>Patient Satisfaction</h3>
                    <span>Patients' feedback on their experience with Dr. {{ $collection['doctors'][0]->wholename}}</span> </div>
                </div>
                <div class="dr-det-btm patience-setisf-bg">
                  <div class="reviews-disc">
                    <p>Likelihood of recommending Dr. {{ $collection['doctors'][0]->wholename}} to family and friends is 4.9 out of 5</p>
                  </div>
                  <div class="reviews-star">
                    <!--<ul class="pt-satisf">
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star-half-o"></a></li>
                      <li><a href="#" class="fa fa-star-o"></a></li>
                    </ul>-->
                     
                    <div class="stars">
                <?php $rating = round($collection['averageUserReviews']);?>

                <?php for($istars =1; $istars<=5; $istars++):
                  if($rating >=1)
                  {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star';
                  }else {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star-empty';
                  }
                  ?>

                <span class="<?php echo $class;?>"></span> 

                <?php $rating --;
                endfor; ?>
                 
                </div>
                  </div>
                  <div class="survey-results">
                    <h4>See More Survey Results</h4>
                    <ul> 
                      <li><a href="#" class="scheduling-appointments"   data-id="{{$collection['doctors'][0]->user_id}}" data-value="appointment">Scheduling Appointments</a></li>
                    </ul>
                  </div>
                  <div class="take-survey">
                    <p>{{count($collection['userReviews'])}} responses</p>
                    <a href="#"  data-id="{{$collection['doctors'][0]->user_id}}" class="leaveareview" data-value="rattings">Take a survey</a>
                       <p> <a href="#" id="leaveareview" data-id="{{$collection['doctors'][0]->user_id}}" class="leaveareview" data-value="rattings">Leave a review</a></p> </div>
                </div>
              </div>
             <!-- comments-contents Starts here-->
              <div class="col-xs-12" id="comments-contents">

              <div  class="quote-colm" id="userauthentication" style="display: none;">  
                  
                      <form class="form-inline" role="form" action=""  id="loginform" name="loginform" method="post">
             <input type="hidden" name="loginfor" id="loginfor" value="">
              <input name="doctor_id" id="doctor_id" value="{{ $collection['doctors'][0]->user_id}}" type="hidden">

                      <div class="col-md-12" id="signinmsg"></div>
  <meta name="_token" content="{{ app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token()) }}" />
                <div class="form-group">
                    <label class="sr-only" for="inputEmail">Email</label>
                    <input type="email" name="login_email" id="login_email" class="form-control"   placeholder="Email" autocomplete="off">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="inputPassword">Password</label>
                    <input type="password" class="form-control" id="login_password" name="login_password" placeholder="Password" autocomplete="off">
                </div> 
                <button type="submit" id="login_submit" class="btn btn-primary">Login</button>
    </form> <!--LGIN FORM ENDS HERE-->

    <br>
    <div class="col-md-8">
    <form  id="signupform" name="signupform" action="" method="post" >
<div class="col-md-12" id="signuperrormsg"></div>
              <meta name="_token" content="{{ app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token()) }}" id="signup_token" />
                <div class="form-group">
                    <label for="inputEmail">Email</label>
                    <input type="email" class="form-control" id="signup_email" name="signup_email"  placeholder="Email" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="inputPassword">Password</label>
                    <input type="password" class="form-control" id="signup_password" name="signup_password" placeholder="Password" autocomplete="off">
                </div> 
                 <div class="form-group">
                 <label for="inputPassword">Confirm Password</label>
                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" autocomplete="off">
                </div> 



                <button type="submit" class="btn btn-primary" id="signupbtn" name="signupbtn">Sign Up</button>
            </form> <!--SIGNUP FORM ENDS HERE-->

            </div>
  </div> <!--Authentications ends here.-->

   <!--SCHEDULE HTML FORM STARTS HEER--> 

  <div  class="quote-colm" id="scheduleform" style="display: none">   



    <div class="col-md-12">
    <h4>Schedule An Applintment On The Open Days</h4>
      <div class="well well-sm">         
            <div class="row" id="post-review-box">
            <div class="col-md-12" id="appointmentmsg"></div>
                <div class="col-md-12">

 <form accept-charset="UTF-8" action="" method="post"> 
  <meta name="_token_schedule" content="{{ app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token()) }}" />
<input name="doctors_id" id="doctors_id" value="{{ $collection['doctors'][0]->user_id}}" type="hidden">
    <input name="profile_doctor_id" id="profile_doctor_id" value="{{ $collection['doctors'][0]->id}}" type="hidden">
              <div class="row"> 
                <div class="col-md-12"><label class="control-label">Check appointment by clicking a date or below button</label></div>
              <div class="col-md-12">
 <div style="overflow:hidden;">
    <div class="form-group">
       
            <div class="col-md-12 datetimepicker12">
                <div id="datetimepicker12" ></div>
            </div>
        
    </div> 
</div>
      <input type="hidden" class="form-control" id="scheduale_date" name="scheduale_date" value="" />
                 
                 </div>
                 <div class="col-md-2 col-md-offset-4">
            <button class="btn btn-success btn-lg" id="scheduale_submit" type="button">Check Appointment</button></div>
             </div> 




                    </form>
                </div>
            </div><!--post-review-box ENDS-->



            <div class="row" id="post-review-box">
            <div class="col-md-12" id="myTbody">
           </div>
            </div>
        </div> 
         
    </div>
  </div><!--SCHEDULING HTML ENDS HERE-->




















                
   <!--COMMENTS AND RATING STARTS HERE-->
    @if(count($collection['userReviews']) > 0)
     <div  class="quote-colm">  

    <div class="col-md-12">
      <div class="well well-sm"> 
        
            <div class="row" id="post-review-box">
             
             <div class="col-md-12" style="padding-bottom: 2px"> 
              <h4>Aggregate comments of each visitor.</h4></div>
                 

                @foreach($collection['userReviews'] as $reviews)

                


                <div class="col-md-12" style="padding-bottom: 10px">
                <div class="col-md-8"> {{$reviews->visitor_name}}- {{$reviews->timestamps}}<br/>
 {{$reviews->comment}} 
                </div>
                
                <div class="stars col-md-4">
                <?php $rating = $reviews->rating;?>

                <?php for($istars =1; $istars<=5; $istars++):
                  if($rating >=1)
                  {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star';
                  }
                  else {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star-empty';
                  }
                  ?>

                <span class="<?php echo $class;?>"></span> 

                <?php $rating --;
                endfor; ?>
                 
                </div> 
                </div> 

                

                @endforeach
               
            </div>

             <!------------------------------>
                <div class="row" id="post-review-box"> 
                <div class="col-md-12" style="padding-bottom: 2px"> 
              <h4>Total Average Responses. </h4></div> 
                <div class="col-md-12">
                <div class="col-md-12">
                <div class="col-md-8">Ease of scheduling urgent appointments ({{$collection['averageReviewsAll']->total_behav}} Responses)</div>
                <div class="col-md-4">
                <div class="stars">
                <?php $behav = $collection['averageReviewsAll']->behav;?>

                <?php for($istars =1; $istars<=5; $istars++):
                  if($behav >=1)
                  {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star';
                  }else {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star-empty';
                  }
                  ?>

                <span class="<?php echo $class;?>"></span> 

                <?php $behav --;
                endfor; ?>
                 </div></div></div>
              <div class="col-md-12">
              <div class="col-md-8">Office environment, cleanliness, comfort, etc ({{$collection['averageReviewsAll']->total_available}} Responses)</div>
                <div class="col-md-4">
                  <div class="stars">
                <?php $available = $collection['averageReviewsAll']->available;?>

                <?php for($istars =1; $istars<=5; $istars++):
                  if($available >=1)
                  {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star';
                  }else {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star-empty';
                  }
                  ?>

                <span class="<?php echo $class;?>"></span> 

                <?php $available --;
                endfor; ?>
                 </div></div></div>

                 <div class="col-md-12">
                 <div class="col-md-8">Staff friendliness and courteousness({{$collection['averageReviewsAll']->total_envi}} Responses)</div>
                <div class="col-md-4">
                  <div class="stars">
                <?php $envi = $collection['averageReviewsAll']->envi;?>

                <?php for($istars =1; $istars<=5; $istars++):
                  if($envi >=1)
                  {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star';
                  }else {
                    $class = 'glyphicon .glyphicon-star-empty glyphicon-star-empty';
                  }
                  ?>

                <span class="<?php echo $class;?>"></span> 

                <?php $envi --;
                endfor; ?>
                 </div>
                 </div></div>

                </div>
                </div>
                <!------------------------------>
        </div> 
         
    </div>
  </div>
 @endif
   <!--COMMENTS ENDS HERE-->

   <!--COMMENT FORM STARTS HEER--> 

  <div  class="quote-colm" id="commentsform" style="display: none">  

    <div class="col-md-12">
    <h4>Post a reviw</h4>
      <div class="well well-sm">         
            <div class="row" id="post-review-box">
                <div class="col-md-12">
                    <form accept-charset="UTF-8" action="{{ url() }}/makereview" method="post">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <input name="doctor_id" id="doctor_id" value="{{ $collection['doctors'][0]->user_id}}" type="hidden">
                        <input id="ratings-hidden" name="rating" type="hidden">
                        <input id="ratings-hidden1" name="rating1" type="hidden"> 
                        <input id="ratings-hidden2" name="rating2" type="hidden">   
                        <?php //echo $collection['loginUserReview'];?>
                        <div class="text-left">

                        <div class="col-md-12">
                        <div class="col-md-8">Ease of scheduling urgent appointments</div>
                          
                          <div class="stars starrr" id="starrr"  data-rating="0"></div>
                            
                          </div>

                          <div class="col-md-12">
                          <div class="col-md-8">Office environment, cleanliness, comfort, etc</div>
                            <div class="stars starrr" id="starrr1" data-rating="0"> </div></div>
                        <div class="col-md-12">
                      <div class="col-md-8">Staff friendliness and courteousness</div>
                      <div class="stars starrr" id="starrr2" data-rating="0"> </div></div>
                            
                        </div>
                        <div class="col-md-12" ng-app="myShoppingLists" >
          <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5" style="margin-bottom:30px" required ng-model="comment.Comment.body" maxlength="200"></textarea>

          <span ng-bind="200 - comment.Comment.body.length"></span> Remaining</div>

                         <div class="col-md-12">
                        <div class="text-right">
          <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
            <span class="glyphicon glyphicon-remove"></span>Cancel</a>
            <button class="btn btn-success btn-lg" type="submit">Submit</button>
                            </div>
                         </div>

                    </form>
                </div>
            </div>
        </div> 
         
    </div>
  </div>
                  <!--COMMENTS FORm ENDS HERE.-->


        

</div>




          </div>
          </div>
        </div>
      </div>

      <div class="accordion visible-xs dr-tab-cont" id="accordion2">
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                  Overview
                </a>
              </div>
              <div id="collapseOne" class="accordion-body  collapse in">
                <div class="accordion-inner">
                <div class="row">
              <div class="col-xs-12 dr-title"> <span>Key things you need to know about</span>
                <h2>{{ $collection['doctors'][0]->wholename}}</h2>
              </div>
              <div class="col-xs-12 dr-det-sect phone-address">
                <div class="icon-colm"> <em class="fa fa-map-marker"></em>
                  <div class="det-title">
                    <h3>Phone & Address</h3>
                    <span>Check insurance plans, locations and make an appointment</span> </div>
                </div>
                <div class="dr-det-btm phone-address-bg">
                  <address>
                  <p>{{ $collection['doctors'][0]->wholename}}</p>

                  <p>{{$collection['business_profile']->business_address}}</p>
                  <p>{{$collection['business_profile']->business_address2}}</p>
                  <p>{{ $collection['doctors'][0]->contact_dr_phone_number}}</p>

                  </address>
                  <div  class="gmap" >
                 
                  </div>


                  <div class="btn-links"> @if ( \Cartalyst\Sentinel\Native\Facades\Sentinel::check() === false )
         <a  href="#registrationform" role="button" class="btn" data-toggle="modal"  data-remote-target="#registrationform .modal-body" >NEW PATIENT</a> <a  href="#newloginform" role="button" class="btn" data-toggle="modal"  data-remote-target="#newloginform .modal-body" >Existing PATIENT</a> @endif
         <a href="#" class="btn enquiry">Other Enquiry</a> </div>

                </div>
              </div>
              <div class="col-xs-12 dr-det-sect experience-colm">
                <div class="icon-colm"> <em class="fa fa-pencil-square-o"></em>
                  <div class="det-title">
                    <h3>experience</h3>
                    <span>Research training, expertise and qualifications</span> </div>
                </div>
                <div class="dr-det-btm">
                  <div class="dr-exp">
                  <div class="exp-links">
                    <h5>Dr. {{ $collection['doctors'][0]->wholename}}'s Specialties</h5>
                       <ul class="specialities_top_ul"> 
                    </ul>

                      <a data-toggle="collapse"  href="#tab_specialities3" class="view-all view_tabspecialities"> 0 specialty</a>
                    
               <div id="tab_specialities3" class="collapse tab_specialities"></div>
                    </div>

                    <div class="exp-links">
                    <h5>Procedures Dr. {{ $collection['doctors'][0]->wholename}} Performs</h5>

              <ul class="hide_ul">
                       @foreach($collection['users_procedures'] as $users_procedures)
                  <li><a href="#">  {{$users_procedures->procedure_name}} </a></li>  
                     @endforeach
                   
                    </ul>

                    <a data-toggle="collapse"  href="#tab_procedures3" class="view-all view_tabpocedure"> 0 procedures</a>
                    
                    <div id="tab_procedures3" class="collapse tab_procedures">
 

                    </div>

                    </div>
                    <div class="exp-links">
                    <h5>Conditions Dr. {{ $collection['doctors'][0]->wholename}} Treats</h5>
                   
                    <ul class="conditions_ul">
                      

                    </ul>
                    <a data-toggle="collapse"  href="#tab_conditions3" class="view-all view_tabconditions"> 0 conditions</a>
                    <div id="tab_conditions3" class="collapse tab_conditions"></div>
                    </div>
 
                  </div>
                  <div  class="quote-colm"> <em class="fa fa-quote-left"></em>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 dr-det-sect hospital-quality">
                <div class="icon-colm"> <em class="fa fa-h-square"></em>
                  <div class="det-title">
                    <h3>Hospital Quality</h3>
                    <span>The right hospital is as important as the right doctor</span> </div>
                </div>
                <div class="dr-det-btm hospital-quality-bg">
                  <div class="hospital-left">

                    <ul class="association_logos">
                    

                    </ul>
                  </div>
                  <div class="hospital-right">
                

                  </div>
                </div>
              </div>
              <div class="col-xs-12 dr-det-sect patience-setisfiction">
                <div class="icon-colm"> <em class="fa fa-comments-o"></em>
                  <div class="det-title">
                    <h3>Patient Satisfaction</h3>
                    <span>Patients' feedback on their experience with Dr. {{ $collection['doctors'][0]->wholename}}</span> </div>
                </div>
                <div class="dr-det-btm patience-setisf-bg">
                  <div class="reviews-disc">
                    <p>Likelihood of recommending Dr. {{ $collection['doctors'][0]->wholename}} to family and friends is 4.9 out of 5</p>
                  </div>
                  <div class="reviews-star">
                    <ul class="pt-satisf">
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star-half-o"></a></li>
                      <li><a href="#" class="fa fa-star-o"></a></li>
                    </ul>
                  </div>
                  <div class="survey-results">
                    <h4>See More Survey Results</h4>
                    <ul> 
                      <li><a href="#" class="scheduling-appointments"   data-id="{{$collection['doctors'][0]->user_id}}" data-value="appointment">Scheduling Appointments</a></li>
                    </ul>
                  </div>
                  <div class="take-survey">
                    <p>{{count($collection['userReviews'])}} responses</p>
                    <a href="#"  data-id="{{$collection['doctors'][0]->user_id}}" class="leaveareview" data-value="rattings">Take a survey</a> </div>
                </div>
              </div>
            </div>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                 Phone& Address
                </a>
              </div>
              <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                <div class="row">
                <div class="col-xs-12 dr-det-sect phone-address">
                <div class="icon-colm"> <em class="fa fa-map-marker"></em>
                  <div class="det-title">
                    <h3>Phone & Address</h3>
                    <span>Check insurance plans, locations and make an appointment</span> </div>
                </div>
                <div class="dr-det-btm phone-address-bg">
                  <address>
                  <p>{{ $collection['doctors'][0]->wholename}}</p> 
                  <p>{{$collection['business_profile']->business_address}}</p>
                  <p>{{$collection['business_profile']->business_address2}}</p>
                  <p>{{ $collection['doctors'][0]->contact_dr_phone_number}}</p> 
                  </address>
                  <div  class="gmap">
                  </div>


                  <div class="btn-links">@if ( \Cartalyst\Sentinel\Native\Facades\Sentinel::check() === false )
          <a  href="#registrationform" role="button" class="btn" data-toggle="modal"  data-remote-target="#registrationform .modal-body" >NEW PATIENT</a> <a  href="#newloginform" role="button" class="btn" data-toggle="modal"  data-remote-target="#newloginform .modal-body" >Existing PATIENT</a> @endif <a href="#" class="btn enquiry">Other Enquiry</a> </div>

                </div>
              </div>
                </div>
              </div>
            </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                Experience
                </a>
              </div>
              <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner">
                <div class="row">
                 <div class="col-xs-12 dr-det-sect experience-colm">
                <div class="icon-colm"> <em class="fa fa-pencil-square-o"></em>
                  <div class="det-title">
                    <h3>experience</h3>
                    <span>Research training, expertise and qualifications</span> </div>
                </div>
                <div class="dr-det-btm">
                  <div class="dr-exp">
                  <div class="exp-links">
                    <h5>Dr. {{ $collection['doctors'][0]->wholename}}'s Specialties</h5>
                        <ul class="specialities_top_ul"> 
                    </ul>

                      <a data-toggle="collapse"  href="#tab_specialities4" class="view-all view_tabspecialities"> 0 specialty</a>
                    
               <div id="tab_specialities4" class="collapse tab_specialities"></div>
                    </div>

                    <div class="exp-links">
                    <h5>Procedures Dr. {{ $collection['doctors'][0]->wholename}} Performs</h5>
                    <ul class="hide_ul">
                       @foreach($collection['users_procedures'] as $users_procedures)
                  <li><a href="#">  {{$users_procedures->procedure_name}} </a></li>  
                     @endforeach
                   
                    </ul>

                    <a data-toggle="collapse"  href="#tab_procedures4" class="view-all view_tabpocedure"> 0 procedures</a>
                    
                    <div id="tab_procedures4" class="collapse tab_procedures">
 

                    </div>
                    </div>
                    <div class="exp-links">
                    <h5>Conditions Dr. {{ $collection['doctors'][0]->wholename}} Treats</h5>
                   
                    <ul class="conditions_ul">

                    </ul>
                    <a data-toggle="collapse"  href="#tab_conditions4" class="view-all view_tabconditions"> 0 conditions</a>
                    <div id="tab_conditions4" class="collapse tab_conditions"></div>
                    </div>
 
                  </div>
                  <div  class="quote-colm"> <em class="fa fa-quote-left"></em>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
                  </div>
                </div>
              </div>
                </div>
              </div>
            </div>
            </div>

            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                Hospital Quality
                </a>
              </div>
              <div id="collapseFour" class="accordion-body collapse" >
                <div class="accordion-inner">
                <div class="row">
                 <div class="col-xs-12 dr-det-sect hospital-quality">
                <div class="icon-colm"> <em class="fa fa-h-square"></em>
                  <div class="det-title">
                    <h3>Hospital Quality</h3>
                    <span>The right hospital is as important as the right doctor</span> </div>
                </div>
                <div class="dr-det-btm hospital-quality-bg">
                  <div class="hospital-left">

                   <ul class="association_logos">
                   
                    </ul>
                  </div>
                  <div class="hospital-right"> 
                  </div>
                </div>
              </div>
                </div>
                </div>
              </div>
            </div>

            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                  Patient Satisfaction
                </a>
              </div>
              <div id="collapseFive" class="accordion-body collapse">
                <div class="accordion-inner">
                 <div class="col-xs-12 dr-det-sect patience-setisfiction">
                <div class="icon-colm"> <em class="fa fa-comments-o"></em>
                  <div class="det-title">
                    <h3>Patient Satisfaction</h3>
                    <span>Patients' feedback on their experience with Dr. {{ $collection['doctors'][0]->wholename}}</span> </div>
                </div>
                <div class="dr-det-btm patience-setisf-bg">
                  <div class="reviews-disc">
                    <p>Likelihood of recommending Dr. {{ $collection['doctors'][0]->wholename}} to family and friends is 4.9 out of 5</p>
                  </div>
                  <div class="reviews-star">
                    <ul class="pt-satisf">
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star"></a></li>
                      <li><a href="#" class="fa fa-star-half-o"></a></li>
                      <li><a href="#" class="fa fa-star-o"></a></li>
                    </ul>
                  </div>
                  <div class="survey-results">
                    <h4>See More Survey Results4</h4>
                    <ul>
                      <li><a href="#" class="scheduling-appointments"   data-id="{{$collection['doctors'][0]->user_id}}" data-value="appointment">Scheduling Appointments</a></li>
                    </ul>
                  </div>
                  <div class="take-survey">
                    <p>{{count($collection['userReviews'])}} responses</p>
                    <a href="#"  data-id="{{$collection['doctors'][0]->user_id}}" class="leaveareview" data-value="rattings">Take a survey</a> </div>
                </div>
              </div>
                </div>
              </div>
            </div>
          </div>
    </div>
    <aside class="col-md-4 col-sm-4 col-xs-12 sidebar">
      <div class="row">
        <div class="col-xs-12 more-specialist-colm">
        <div class="more-spec-bg">
          <h3>More {{\App\Helpers\MyHelper::getFieldName('specialities', $collection['doctors'][0]->specialities_id,'id','name')}} Specialists like Dr. {{ $collection['doctors'][0]->wholename}} at  {{$collection['business_profile']->business_name}}</h3>
          <div class="show-more-provider">
            <p>Showing {{count($collection['related_doctors'])}} providers who matches:</p>
            <ul>
              <li><a href="{{ url('searchesbystates').'/'.$collection['doctors'][0]->state_id.'?state_citie='.$collection['doctors'][0]->city_id.'&'.'catagory='.$collection['doctors'][0]->specialities_id}}&sort_by=id">{{\App\Helpers\MyHelper::getFieldName('specialities', $collection['doctors'][0]->specialities_id,'id','name')}} Specialist(s)</a></li>
              <li><a href="">Near Dr. {{ $collection['doctors'][0]->wholename}}'s Office</a> </li>
            </ul>
           </div>
             <div class="more-sergen-content">
            <?php
                  foreach ($collection['related_doctors'] as $key => $doc) {

                    if($doc->image_of_doctor == '')
                            {
                    if($doc->gender == 'Male')
                       $image_url = url().'/images/m4.png';
                       else $image_url = url().'/images/f1.jpg';
                      }else  $image_url = url().'/images/catalog/'.$doc->image_of_doctor;
                      ?>
            <div class="more-sergen-colm">
       <figure> <a href="{{ url() }}/doctorDetail/{{$doc->user_id}}"><img src="{{$image_url}}" alt="{{$doc->wholename}}" class="img-responsive"></a> </figure>
              <div class="more-sergen-disc">
                <h5><a href="{{ url() }}/doctorDetail/{{$doc->user_id}}">{{ $doc->wholename }}, {{$doc->education}}</a></h5>
                <p>{{$collection['business_profile']->business_name.' '.$collection['business_profile']->business_address}}</p>
               

                  <div class="stars-listing-doctors">
      
      @for ($i=1; $i <= 5 ; $i++)
      <span class="glyphicon glyphicon-star{{ ($i <= $doc->averageRating && $doc->approved == 1) ? '' : '-empty'}}"></span>
       @endfor</div>  

              </div>
            </div>
            <?php
                }
             ?>
            <!--<div class="more-sergen-colm">
              <figure> <a href="#"><img src="{{ url() }}/front_end/assets/img/specialist-img-1.jpg" alt="image" class="img-responsive"></a> </figure>
              <div class="more-sergen-disc">
                <h5><a href="#">Dr. Collin E. Brathwaite, MD</a></h5>
                <p>Winthrop Surgical Associates 120 Mineola Blvd Suite 320Mineola, NY 11501</p>
                <ul class="pt-satisf">
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star-half-o"></a></li>
                  <li><a href="#" class="fa fa-star-o"></a></li>
                </ul>
              </div>
            </div>
            <div class="more-sergen-colm">
              <figure> <a href="#"><img src="{{ url() }}/front_end/assets/img/specialist-img-1.jpg" alt="image" class="img-responsive"></a> </figure>
              <div class="more-sergen-disc">
                <h5><a href="#">Dr. Collin E. Brathwaite, MD</a></h5>
                <p>Winthrop Surgical Associates 120 Mineola Blvd Suite 320Mineola, NY 11501</p>
                <ul class="pt-satisf">
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star-half-o"></a></li>
                  <li><a href="#" class="fa fa-star-o"></a></li>
                </ul>
              </div>
            </div>
            <div class="more-sergen-colm">
              <figure> <a href="#"><img src="{{ url() }}/front_end/assets/img/specialist-img-1.jpg" alt="image" class="img-responsive"></a> </figure>
              <div class="more-sergen-disc">
                <h5><a href="#">Dr. Collin E. Brathwaite, MD</a></h5>
                <p>Winthrop Surgical Associates 120 Mineola Blvd Suite 320Mineola, NY 11501</p>
                <ul class="pt-satisf">
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star"></a></li>
                  <li><a href="#" class="fa fa-star-half-o"></a></li>
                  <li><a href="#" class="fa fa-star-o"></a></li>
                </ul>
              </div>
            </div>-->
            </div>
           </div>
          </div>
        <figure class="col-xs-12 online-directory"> <a href="#"><img src="{{ url() }}/front_end/assets/img/online-directory-img.jpg" alt="dr directory" class="img-responsive"></a> </figure>

      </div>
    </aside>
  </div>
</div>
<!-- benifit and features sections end here -->

<!-- footer ------------------------>


@endsection
