 @extends('layout.patientapp')
 @section('head')
  <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ URL::asset('plugins/fullcalendar/fullcalendar.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('plugins/fullcalendar/fullcalendar.print.css')}}" media="print">
     <link rel="stylesheet" href="{{ URL::asset('plugins/datepicker/datepicker3.css')}}">
 @stop
 @section('footer')
 <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script> 
 <script src="{{ URL::asset('plugins/fullcalendar/fullcalendar.min.js')}}"></script>
 <script src="{{ URL::asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>


 <script  type="text/javascript" charset="utf-8">

$(document).ready(function (){

  /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d    = date.getDate(),
        m        = date.getMonth(),
        y        = date.getFullYear();
/*Below calander is picked from the reference.
https://bootstrap-datepicker.readthedocs.io/en/stable/options.html#todayhighlight

*/

function convertDate(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
}

  $('#search_by_date').datepicker({
                    autoclose: true,    // It is false, by default
                    format: "dd/mm/yyyy",
                    todayHighlight: true});  

        $('#calendar').fullCalendar({

          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          buttonText: {
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
          },
          //Random default events
           events: {
                url: "{{url()}}/visitors/get-all-appointments/",
                error: function() {
                  $('#error').html('Could not find any appointments');
                  }
                },
          editable: true,
          droppable: true, // this allows things to be dropped onto the calendar !!!
          //Random default events
        eventClick:  function(event, jsEvent, view) {
        var contents = '<table class="table table-bordered"><tbody>';
        var checkStatus = '';
        var checkActive = '';
        $.ajax({
        url      : '{{url()}}/visitors/singleAppointment',
        type     : "get",
        data     : 'id='+event.id,
        dataType : 'json',
         beforeSend: function()
          {
            $('#showdetails').modal();
            $('#Heading').html('Error!');
            $('#modal-body'). html('<div class="text-center"><img src="{{ URL::asset('images/loader1.gif')}}"></div>'); 
          },
        success: function(data){
          $('#Heading').html('Detail Record.');
        switch (data.status) {
                      case "1":
                        checkStatus = 'Unapproved';
                        break;
                      case "2":
                        checkStatus = 'Approved';
                        break;
                      case "3":
                        checkStatus = 'Canceled';
                        break;
                      case "4":
                        checkStatus = 'Pending';
                        break;
                      default:
                         checkStatus = 'Pending';
                        break;
                    }  
                    (data.active == 1) ? checkActive='Meet Doctor' : checkActive='Missed Doctor';                
                  contents += '<tr><th>Full Name</th><td>'+data.first_name+' '+data.last_name+'</td></tr>';
                  contents += '<tr><th>Schedule Date</th><td>'+convertDate(data.schedule_date)+'</td></tr>';
                  contents += '<tr><th>Schedule Time</th><td>'+data.schedule_time+'</td></tr>';
                  contents += '<tr><th>Status</th><td>'+checkStatus +'</td></tr>';
                  contents += '<tr><th>Doctor Appointment Status</th><td>'+checkActive+'</td></tr>';
                  contents += '</tbody></table>';                      
                      $('#modal-body').html(contents);
                      $('#showdetails').modal();
                     
          },
          error: function( jqXhr, textStatus, errorThrown ){
            $('#Heading').html('Error!');
            $('#modal-body').html( errorThrown );
            $('#showdetails').modal();
    }
      }); // AJAX ENDS HERE.

          },

          
          drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
            }

          }
        });

});// DOCUMENT . READY ENDS HERE.

</script>

 @stop
 @section('content')
 <!-- Modal Dialog  FOR VIEW --> 
<div class="modal fade" id="showdetails" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Your Details</h4>
      </div>
      <div class="modal-body">  
        <p id="modal-body">Be patient contents are loading.</p>
      </div>
    </div>
  </div>
</div>

<!-- Modal Dialog  FOR EDIT COMMENTS --> 
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">

     <form action="/visitors/update" method="post" accept-charset="utf-8">
     <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit Your Details</h4>
      </div>
      <div class="modal-body loader_div" >
      </div>
          <div class="modal-body modal_body" >
            <input class="form-control" id="id" name="id" type="hidden" placeholder="" value="">
             <input class="form-control" id="rating" name="rating" type="hidden" placeholder="" value="">
               <input class="form-control" id="approved" name="approved" type="hidden" placeholder="" value="">

        <input class="form-control" id="doctor_id" name="doctor_id" type="hidden" placeholder="" value="">
        <input class="form-control" id="user_id" name="user_id" type="hidden" placeholder="" value=""> 
        <div class="form-group">
        <textarea rows="2" class="form-control" placeholder="Comments" id="comments" name="comment"></textarea></div></div>

          <div class="modal-footer">
          <div class="row text-center">
        <button type="submit" class="btn btn-warning btn-lg"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
</div>

      </div>

    </form>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Appointments With Doctors.<small></small> </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">  
            

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">You Have {{count($collection['allAppointments'])}} Appointments With Doctors</h3> 
                </div>
                

             @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>

                    <th>Sr.No</th>
                    <th>Doctor's Name</th> 
                    <th>Date</th> 
                    <th>Time</th> 
                    <th>Current Status</th> 
                    
                  </tr>
                </thead>
                <tbody>
                
                   <?php $counter = 1;?>
                    @foreach($collection['allAppointments'] as $doctor)
                  
                  <tr>
                  <td>{{$counter}}</td>
                  <td>{{$doctor->first_name.' '.$doctor->last_name}}</td>
                    <td>
                   @if($doctor->schedule_date == '')
                        Null
                   @else
                   {{\Carbon\Carbon::createFromFormat('Y-m-d', $doctor->schedule_date)->format('d-m-Y')}}
                   @endif
                   </td>
                    <td>
                    @if($doctor->schedule_time == '')
                   Null
                   @else
                   {{\Carbon\Carbon::createFromFormat('H:i', $doctor->schedule_time)->format('h:i A')}}
                   @endif
                    </td>
                   <td>                     
                     <?php switch ($doctor->status) {
                      case 1:
                        echo 'Unapproved';
                        break;
                      case 2:
                        echo 'Approved';
                        break;
                      case 3:
                        echo 'Canceled';
                        break;
                      case 4:
                        echo 'Pending';
                        break;
                      default:
                         echo 'Pending';
                        break;
                    }?>
                   </td> 
                  </tr>
                  <?php $counter ++;?>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Sr.No</th>
                    <th>Doctor's Name</th> 
                    <th>Date</th>
                    <th>Time</th> 
                    <th>Status</th> 
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
          <!-- /.box --> 
        </div>
        <!-- /.col --> 
      </div>
      <!-- /.row --> 
    </section>
    <!-- /.content --> 


      <!-- Main content -->
        <section class="content">
          <div class="row"> 
             <div class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">Color Hints</h4>
                </div>
                <div class="box-body">
                  <!-- the events -->
                  <div id="external-events">
                    <div class="external-event bg-green">Nearest Appointments</div>
                    <div class="external-event bg-yellow">Passed Appointments</div>
                    <div class="external-event bg-aqua">Canceled Appointments</div>
                    <div class="external-event bg-light-blue">Coming Appointments</div>
                    <div class="external-event bg-red">Current day Appointments</div> 
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /. box --> 
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-body no-padding">
                  <!-- THE CALENDAR -->
                  <div id="calendar"></div>

                </div><!-- /.box-body -->

              </div><!-- /. box -->
            </div><!-- /.col -->

          </div><!-- /.row -->

        </section><!-- /.content -->


  </div>



@endsection

