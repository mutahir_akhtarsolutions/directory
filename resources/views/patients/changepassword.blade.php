 @extends('layout.patientapp')
@section('content')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Change Password 
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
           
  @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Error</strong>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif
   @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->

            <div class="box-body">
    <!--Comment the create or update profile upto here--> 
    <div class="padder">
      <!-- form for profile builder starts -->
      <div class="container"> 
<!--{{ url('profile') }}-->

    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Change Password </h3> 
          
  <form class="form-horizontal" role="form" method="POST" action="" autocomplete = "off">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="box-body">
                    <div class="form-group {{ $errors->first('password', ' has-error') }}">
                      <label for="inputEmail3" class="col-sm-4 control-label">Password</label>
                      <div class="col-sm-8">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password"><span style="color:#DD4B39" class="help-block">{{{ $errors->first('password', ':message') }}}</span>
                      </div>
                       
                    </div>
                    <div class="form-group {{ $errors->first('password_confirmation', ' has-error') }}">
                      <label for="inputPassword3" class="col-sm-4 control-label">Confirm Password</label>
                      <div class="col-sm-8">
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password"><span style="color:#DD4B39" class="help-block">{{{ $errors->first('password_confirmation', ':message') }}}</span>
                      </div>

                    </div> 
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Change</button>
                  </div><!-- /.box-footer -->
                </form>

        </div>
      </div>
    </div> 
          
        </div>

      </div>

    </div> 
 
          


            </div><!-- /.box-body -->
         <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
         

        </section><!-- /.content --> 
 </div><!-- /.box -->
 
@endsection