 @extends('layout.patientapp')
@section('content')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Update 
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"> 
                </h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
  @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
        <strong>Error</strong>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif
   @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->

            <div class="box-body">
    <!--Comment the create or update profile upto here--> 
    <div class="padder">
      <!-- form for profile builder starts -->
      <div class="container"> 
<!--{{ url('profile') }}-->

    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> General Visitor Information </h3>
            <form role="form" action="{{url()}}/visitors/profile/update" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" /> 
    <input type="hidden" name="profile_id" value="{{$collection['Profile']->id}}">

    <input type="hidden" name="user_id" value="{{$collection['Profile']->user_id}}">
          <div class="form-group">
            <label class="control-label">First Name</label>
            <input  maxlength="100" type="text" name="first_name" value="{{@$collection['first_name']}}"  class="form-control" placeholder="Enter First"  />
            <span class="help-block">{{ $errors->first('first_name', ':message')}}</span>
          </div>
          <div class="form-group">
            <label class="control-label">Last Name</label>
            <input  maxlength="100" type="text" name="last_name" value="{{@$collection['last_name']}}" required="required" class="form-control" placeholder="Enter Last"  />
             <span class="help-block">{{ $errors->first('last_name', ':message') }}</span>
          </div>
          <div class="form-group">
            <label class="control-label">Profile Image</label>
            <input ui-jq="filestyle" type="file" name="avatar" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
          </div>
           <div class="form-group">
            <img src="<?php echo url(); ?>/images/catalog/<?php echo @$collection['Profile'] ->avatar; ?>" alt="doctor image" height="60" width="60">
          </div> 
          <hr> 
          <div class="form-group">
            <label class="control-label">Contact Telephone</label>
            <input  maxlength="100" type="text" name="phone" value="{{ Input::old('phone', isset($collection['Profile'] ->phone) ? $collection['Profile'] ->phone : '' ) }}" required="required" class="form-control" placeholder="Enter Telephon"  />
          </div>
          <div class="form-group">
            <label class="control-label">Contact Mobile</label>
            <input  maxlength="100" type="text" name="mobile" value="{{ Input::old('mobile', isset($collection['Profile'] ->mobile) ? $collection['Profile'] ->mobile : '' ) }}" required="required" class="form-control" placeholder="Enter Mobile"  />
          </div>   
         <div class="form-group">
            <label class="control-label">Country</label>
            <select name="country_id"  class="form-control m-b" id="country_id">
              <option>select country</option>


              <?php foreach(@$collection['countries'] as $contry){ ?>

              <option value="<?php echo $contry->id; ?>" @if($contry->id == $collection['Profile']->country_id) selected="selected" @endif ><?php echo $contry->country_name; ?></option>
              <?php } ?>

           </select>
          </div>

          <div class="form-group">
            <label class="control-label">State</label>
            <select name="state_id" class="form-control m-b" id="state_id">
              <option>select state</option>

              <?php foreach($collection['states'] as $stat){ ?>

              <option value="<?php echo $stat->id; ?>" @if($stat->id == $collection['Profile']->state_id) selected="selected" @endif ><?php echo $stat->state_name; ?></option>
              <?php } ?>



           </select>
          </div>

          <div class="form-group">
            <label class="control-label">City</label>
            <select name="city_id" class="form-control m-b" id="city_id">
              <option>select city</option>
              <?php foreach($collection['cities'] as $city){ ?>

              <option value="<?php echo $city->id; ?>" @if($city->id == $collection['Profile']->city_id) selected="selected" @endif ><?php echo $city->city_name; ?></option>
              <?php } ?>
           </select>
          </div>

          <div class="form-group">
            <label class="control-label">Zip Code</label>
            <input maxlength="200" type="text" name="zip_code" value="{{ Input::old('zip_code', isset($collection['Profile'] ->zip_code) ? $collection['Profile'] ->zip_code : '' ) }}" required="required" class="form-control" placeholder="Enter Zip Code" />
          </div>    
 
          <button class="btn btn-success btn-lg pull-right" type="submit">Update</button>
          <!--<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>-->

  </form>

        </div>
      </div>
    </div> 
          
        </div>

      </div>

    </div> 
 
          


            </div><!-- /.box-body -->
            <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
         

        </section><!-- /.content --> 
 </div><!-- /.box -->
 
@endsection