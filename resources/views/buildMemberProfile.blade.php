 @extends('layout.appprofile')
@section('content')
 <style type="text/css"> 
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Update 
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
<p>{{ $mode == 'create' ? 'Create Profile' : 'Update profile' }} </p>
</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>

   @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->

            <div class="box-body">
    <!--Comment the create or update profile upto here--> 
    <div class="padder">
      <!-- form for profile builder starts -->
      <div class="container">

<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Step 1</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
        <p>Step 2</p>
      </div>
      <!--<div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Step 3</p>
      </div>-->
    </div>
  </div>
<?php if($mode =="update"){
    $action = url('updateProfile');
 }else{
   $action = url('profile');
 }
 ?>
<!--{{ url('profile') }}-->
  <form role="form" action="<?php echo $action;?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />
    <?Php
        if($mode !="update"){
    ?>
    <input type="hidden" name="user_id" value="{{ $data['mem_id'] }}" />
    <?php
     }else{
    ?>
    <input type="hidden" name="user_id" value="{{ $data['user_id'] }}" />
    <input type="hidden" name="id" value="{{ $data['id'] }}" />
    <?Php
      }
    ?>
    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> General member information (Step 1)</h3>
          <div class="form-group">
            <label class="control-label">Title</label>
            <input  maxlength="100" type="text" name="title" value="{{ Input::old('title', isset($data->title) ? $data->title : '' ) }}" required="required" class="form-control" placeholder="Enter phone"  />
          </div>
          <div class="form-group">
            <label class="control-label">First Name</label>
            <input  maxlength="100" type="text" name="first_name" value="<?php if(isset($data->first_name)){ echo $data->first_name;}else{  echo $data['first_name']; } ?>" required="required" class="form-control" placeholder="Enter mobile"  />
          </div>
          <div class="form-group">
            <label class="control-label">Last Name</label>
            <input  maxlength="100" type="text" name="last_name" value="<?php if(isset($data->last_name)){ echo $data->last_name;}else{  echo $data['last_name']; } ?>" required="required" class="form-control" placeholder="Enter mobile"  />
          </div>
          <div class="form-group">
            <label class="control-label">Profile Image</label>
            <input ui-jq="filestyle" type="file" name="avatar" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s">
          </div>
          <div class="form-group">
            <label class="control-label">Contact Telephone</label>
            <input  maxlength="100" type="text" name="phone" value="{{ Input::old('phone', isset($data->phone) ? $data->phone : '' ) }}" required="required" class="form-control" placeholder="Enter Country"  />
          </div>
          <div class="form-group">
            <label class="control-label">Contact Mobile</label>
            <input  maxlength="100" type="text" name="mobile" value="{{ Input::old('mobile', isset($data->mobile) ? $data->mobile : '' ) }}" required="required" class="form-control" placeholder="Enter Country"  />
          </div>



          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-2">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> General business Iinformation (Step 2)</h3>
          <div class="form-group">
            <label class="control-label">Business Name</label>
            <input maxlength="200" type="text" name="business_name" value="{{ Input::old('business_name', isset($data->business_name) ? $data->business_name : '' ) }}" required="required" class="form-control" placeholder="Business Name" />
          </div>

          <div class="form-group">
            <label class="control-label">Number of Doctors</label>
            <select name="no_of_doctors" class="form-control m-b">
              <option>select number of doctors </option>
              <?php for($i = 1; $i <= 10; $i++){  ?>
              <option> <?php echo $i; ?></option>
              <?php }  ?>

           </select>
          </div>

          <!--Opening hours start-->

                  <h2>Opening hours</h2>
                  <p>Please select the opening hours:</p>
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Days</th>
                        <th>Open</th>
                        <th>From</th>
                        <th>To</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Sunday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_sunday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                      </td>
                        <td>
                          <div class="form-group">
                          <select name="from_sunday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_sunday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Monday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_monday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="from_monday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_monday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Tuesday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_tuesday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="from_tuesday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_tuesday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                      </tr>

                      <tr>
                        <td>Wednesday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_wednesday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="from_wednesday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_wednesday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Thursday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_thursday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="from_thursday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_thursday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Friday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_friday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="from_friday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_friday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Saturday</td>
                        <td>
                          <div class="form-group">
                          <select name="opening_saturday" class="form-control m-b">
                            <option>select opening</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="from_saturday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                        <td>
                          <div class="form-group">
                          <select name="to_saturday" class="form-control m-b">
                            <option>select opening from</option>
                            <?php for($i = 0; $i <= 23; $i++){  ?>
                            <option> <?php echo $i; ?></option>
                            <?php }  ?>

                         </select>
                        </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>

<!--Opening hours end-->































          <div class="form-group">
            <label class="control-label">Business Address</label>
            <input maxlength="200" type="text" name="business_address" value="{{ Input::old('business_address', isset($data->business_address) ? $data->business_address : '' ) }}" required="required" class="form-control" placeholder="Business Address" />
            <br>
            <input maxlength="200" type="text" name="business_address2" value="{{ Input::old('business_address2', isset($data->business_address2) ? $data->business_address2 : '' ) }}" required="required" class="form-control" placeholder="Enter Other Business Address" />
          </div>

          <div class="form-group">
            <label class="control-label">Country</label>
            <select name="country_id"  class="form-control m-b" id="country_id">
              <option>select country</option>


              <?php foreach($country as $contry){ ?>

              <option value="<?php echo $contry->id; ?>"><?php echo $contry->country_name; ?></option>
              <?php } ?>

           </select>
          </div>

          <div class="form-group">
            <label class="control-label">State</label>
            <select name="state_id" class="form-control m-b" id="state_id">
              <option>select state</option>

              <?php foreach($state as $stat){ ?>

              <option value="<?php echo $stat->id; ?>"><?php echo $stat->state_name; ?></option>
              <?php } ?>



           </select>
          </div>

          <div class="form-group">
            <label class="control-label">City</label>
            <select name="city_id" class="form-control m-b" id="city_id">
              <option>select city</option>
              <?php foreach($cities as $city){ ?>

              <option value="<?php echo $city->id; ?>"><?php echo $city->city_name; ?></option>
              <?php } ?>
           </select>
          </div>

          <div class="form-group">
            <label class="control-label">Zip Code</label>
            <input maxlength="200" type="text" name="zip_code" value="{{ Input::old('zip_code', isset($data->zip_code) ? $data->zip_code : '' ) }}" required="required" class="form-control" placeholder="Enter Zip Code" />
          </div>

          <div class="form-group">
            <label class="control-label">Business Telephone</label>
            <input maxlength="200" type="text" name="business_telephone" value="{{ Input::old('business_telephone', isset($data->business_telephone) ? $data->business_telephone : '' ) }}" required="required" class="form-control" placeholder="Enter Business Telephone" />
          </div>

          <div class="form-group">
            <label class="control-label">Business Reception</label>
            <input maxlength="200" type="text" name="business_reception" value="{{ Input::old('business_reception', isset($data->business_reception) ? $data->business_reception : '' ) }}" required="required" class="form-control" placeholder="Enter Business Reception" />
          </div>

          <div class="form-group">
            <label class="control-label">Business Email</label>
            <input maxlength="200" type="text" name="business_email" value="{{ Input::old('business_email', isset($data->business_email) ? $data->business_email : '' ) }}" required="required" class="form-control" placeholder="Enter Business Email" />
          </div>

          <div class="form-group">
            <label class="control-label">Business Website</label>
            <input maxlength="200" type="text" name="business_website" value="{{ Input::old('business_website', isset($data->business_website) ? $data->business_website : '' ) }}" required="required" class="form-control" placeholder="Enter Business Website" />
          </div>






          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
          <!--<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>-->
        </div>
      </div>
    </div>
    <!--<div class="row setup-content" id="step-3">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Step 3</h3>
          <div class="form-group">
            <label class="control-label">Association</label>
            <input maxlength="200" type="text" name="association" value="{{ Input::old('association', isset($data->association) ? $data->association : '' ) }}" required="required" class="form-control" placeholder="Enter Association" />
          </div>
          <div class="form-group">
            <label class="control-label">Package Plan</label>
            <select name="package" class="form-control m-b">
              <option>option 1</option>
              <option>option 2</option>
              <option>option 3</option>
              <option>option 4</option>
           </select>
          </div>
          <div class="form-group">
            <label class="control-label">Payment Method</label>
            <select name="Payment-gateway" class="form-control m-b">
              <option>option 1</option>
              <option>option 2</option>
              <option>option 3</option>
              <option>option 4</option>
           </select>
          </div>
          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
        </div>
      </div>
    </div>-->
  </form>


            </div><!-- /.box-body -->
            <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 
@endsection