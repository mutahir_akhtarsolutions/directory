<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Appointment Request Notification.</h2>
<div><img src="http://directory.aladdinapps.com/front_end/assets/img/logo.png" alt=""></div>
<div>
   <p>Hello dear : {!! $first_name.' '.$last_name !!}</p> 
   <p>You request for appointment with doctor has been received by us.</p>
   <p>We are overviewing you request , you will soon be informed.</p>
</div>
</body>
</html>