<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/23/2015
 * Time: 8:15 PM
 */
 ?>

 @extends('app')

 {{-- Page content --}}
 @section('content')

<div class="container">
     
<div class="col-md-10 col-md-offset-2">
<div class="page-header">
        <h1>Catagories <span class="pull-right"><a href="{{ URL::to('catagory/create') }}" class="btn btn-warning">Create</a></span></h1>
     </div> 
    @if ($Catagories->count())
     Page {{ $Catagories->currentPage() }} of {{ $Catagories->lastPage() }}

     <div class="pull-right">
       {!! $Catagories->render() !!}
     </div>
     <br><br>

     <table class="table table-bordered">
        <thead>
            <th class="col-lg-5">Catagory</th>
            <th class="col-lg-5">Parent Catagory</th>
            <th class="col-lg-2">Actions</th>
        </thead>
        <tbody>
            @foreach ($Catagories as $Catagory)
            <tr>

                <td>{{ $Catagory->name }}</td>


                <?php if ($Catagory->parent_id != 0){ ?>


                <td>{{ $Catagory->getCatagory->name }}</td>
                <?php } else { ?>
                <td>{{ 'Null' }}</td>
                <?php } ?>

                <td>
                    <a class="btn btn-warning" href="{{ URL::to("catagory/{$Catagory->id}") }}">Edit</a>
                    <a class="btn btn-danger" href="{{ URL::to("catagory/{$Catagory->id}/delete") }}">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
     </table>

 Page {{ $Catagories->currentPage() }} of {{ $Catagories->lastPage() }}

     <div class="pull-right">
        {!! $Catagories->render() !!}
     </div>
     @else
     <div class="well">

        Nothing to show here.

     </div>
    @endif
</div>
</div>

 @endsection
