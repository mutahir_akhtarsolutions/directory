<?php
/**
 * Created by PhpStorm.
 * User: AMJAD
 * Date: 8/23/2015
 * Time: 8:17 PM
 */

 ?>


 @extends('app')

 {{-- Page content --}}
 @section('content')

 <div class="container">

     <div class="page-header">
        <h1>{{ $mode == 'create' ? 'Create catagory' : 'Update catagory' }} <small>{{ $mode === 'update' ? $catagory->name : null }}</small></h1>
     </div>

     <form method="post" action="">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />





<div class="form-group{{ $errors->first('ParentCatagory', ' has-error') }}">
  <label for="ParentCatagory">Parent Catagory</label>

        <select class="form-control" name="Catagory">
          <option value="0">{{'Select any parent Catagory'}}</option>
          <?php if($mode== 'update') {?>

            @foreach($catagories as $catagry)
              <option value="{{$catagry->id}}" <?php if($catagry->id == $catagory->parent_id){ ?> selected="selected" <?php }?>   >{{$catagry->name}}</option>
            @endforeach
            <?php }else{?>
        @foreach($catagory as $cata)
             <option value="{{$cata->id}}">{{$cata->name}}</option>
         @endforeach
         <?php }?>
        </select>
  </div>








        <div class="form-group{{ $errors->first('name', ' has-error') }}">

            <label for="name">Sub Catagory</label>

            <input type="text"  class="form-control" name="name" id="sub_catagory" value="{{ Input::old('name', isset($catagory->name) ? $catagory->name : '') }}" placeholder="Enter the sub Catagory.">

            <span class="help-block">{{{ $errors->first('name', ':message') }}}</span>

        </div>



        <button type="submit" class="btn btn-default">Submit</button>

     </form>
 </div>

 @stop
