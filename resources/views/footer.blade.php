<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-6 footer-colm latest-post-colm">
        <h4>Latest Forum Post</h4>
        <div class="latest-forum-post">
          <p><a href="#">There are many variations of passages of Lorem Ipsum available</a></p>
          <ul>
            <li><a href="#">24 May, 2014</a></li>
            <li><a href="#">15 Comments</a></li>
          </ul>
        </div>
        <div class="latest-forum-post">
          <p><a href="#">There are many variations of passages of Lorem Ipsum available</a></p>
          <ul>
            <li><a href="#">24 May, 2014</a></li>
            <li><a href="#">15 Comments</a></li>
          </ul>
        </div>
        <div class="latest-forum-post">
          <p><a href="#">There are many variations of passages of Lorem Ipsum available</a></p>
          <ul>
            <li><a href="#">24 May, 2014</a></li>
            <li><a href="#">15 Comments</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6 footer-colm quick-links">
        <h4>Quick Links</h4>
        <ul>
          <li><a href="#">There are many variations of pas</a></li>
          <li><a href="#">Sages Lorem Ipsum available</a></li>
          <li><a href="#">Majority have suffered alteration in</a></li>
          <li><a href="#">Injected humour, or randomised</a></li>
          <li><a href="#">Words which don't look even slightly</a></li>
          <li><a href="#">Believable. If you are</a></li>
          <li><a href="#">to use a Passage of Lorem Ipsum</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6 footer-colm get-in-touch">
        <h4>Get in Touch</h4>
        <address>
        <p>Miami | Fourt Lauderdale</p>
        <p><em class="fa fa-phone"></em> 1786 768 9385</p>
        <p><em class="fa fa-envelope-o"></em> <a href="#">contact@demolink.com</a></p>
        <p>Web: <a href="http://www.nationwidephysician.com">www.nationwidephysician.com</a></p>
        </address>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6 footer-colm newsletter">
        <div class="sub-newsletter">
        <h4>Subscribe Newsletter</h4>
        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. </p>
        <form>
          <input type="email" class="form-control" placeholder="Email Address">
          <input type="submit" class="btn" value="submit">
        </form>
        </div>
        <div class="social-links">
        <h4>Social Media</h4>
          <ul>
            <li><a href="#" class="fa fa-facebook" title="Facebook"></a></li>
            <li><a href="#" class="fa fa-twitter" title="Twitter"></a></li>
            <li><a href="#" class="fa fa-google-plus" title="Google-Plus"></a></li>
            <li><a href="#" class="fa fa-pinterest" title="Pinterest"></a></li>
            <li><a href="#" class="fa fa-linkedin" title="Linkedin"></a></li>
            <li><a href="#" class="fa fa-youtube-play" title="youtube"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="copyrights-sect">
  <div class="container">
    <p>COPYRIGHT 2014 <span>NATIONWIDEPHYSICIANS</span>, ALL RIGHTS RESERVED</p>

  </div>
</div>
</footer>

<script src="{{ url() }}/front_end/assets/js/jquery.js"></script>
<script src="{{ url() }}/front_end/assets/js/bootstrap.min.js"></script>
<script src="{{ url() }}/front_end/assets/js/application.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="{{ url() }}/front_end/assets/js/jquery.gmap.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ url() }}/front_end/assets/js/customjs.js"></script>

<!--js script starts-->
<script>
$(function() {
  var availableTags = new Array();
  <?php foreach($doctors as $key => $doctor){ ?>
        availableTags.push('<?php echo $doctor->first_name.' '.$doctor->last_name; ?>');
    <?php } ?>

  $( "#search_keyword" ).autocomplete({
    source: availableTags
  });
});
//scrip for combobox starts
(function( $ ) {
  $.widget( "custom.combobox", {
    _create: function() {
      this.wrapper = $( "<span>" )
        .addClass( "custom-combobox" )
        .insertAfter( this.element );

      this.element.hide();
      this._createAutocomplete();
      this._createShowAllButton();
    },

    _createAutocomplete: function() {
      var selected = this.element.children( ":selected" ),
        value = selected.val() ? selected.text() : "";

      this.input = $( "<input>" )
        .appendTo( this.wrapper )
        .val( value )
        .attr( "title", "" )
        .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
        .autocomplete({
          delay: 0,
          minLength: 0,
          source: $.proxy( this, "_source" )
        })
        .tooltip({
          tooltipClass: "ui-state-highlight"
        });

      this._on( this.input, {
        autocompleteselect: function( event, ui ) {
          ui.item.option.selected = true;
          this._trigger( "select", event, {
            item: ui.item.option
          });
        },

        autocompletechange: "_removeIfInvalid"
      });
    },

    _createShowAllButton: function() {
      var input = this.input,
        wasOpen = false;

      $( "<a>" )
        .attr( "tabIndex", -1 )
        .attr( "title", "Show All Catagories" )
        .tooltip()
        .appendTo( this.wrapper )
        .button({
          icons: {
            primary: "ui-icon-triangle-1-s"
          },
          text: false
        })
        .removeClass( "ui-corner-all" )
        .addClass( "custom-combobox-toggle ui-corner-right" )
        .mousedown(function() {
          wasOpen = input.autocomplete( "widget" ).is( ":visible" );
        })
        .click(function() {
          input.focus();

          // Close if already visible
          if ( wasOpen ) {
            return;
          }

          // Pass empty string as value to search for, displaying all results
          input.autocomplete( "search", "" );
        });
    },

    _source: function( request, response ) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
      response( this.element.children( "option" ).map(function() {
        var text = $( this ).text();
        if ( this.value && ( !request.term || matcher.test(text) ) )
          return {
            label: text,
            value: text,
            option: this
          };
      }) );
    },

    _removeIfInvalid: function( event, ui ) {

      // Selected an item, nothing to do
      if ( ui.item ) {
        return;
      }

      // Search for a match (case-insensitive)
      var value = this.input.val(),
        valueLowerCase = value.toLowerCase(),
        valid = false;
      this.element.children( "option" ).each(function() {
        if ( $( this ).text().toLowerCase() === valueLowerCase ) {
          this.selected = valid = true;
          return false;
        }
      });

      // Found a match, nothing to do
      if ( valid ) {
        return;
      }

      // Remove invalid value
      this.input
        .val( "" )
        .attr( "title", value + " didn't match any item" )
        .tooltip( "open" );
      this.element.val( "" );
      this._delay(function() {
        this.input.tooltip( "close" ).attr( "title", "" );
      }, 2500 );
      this.input.autocomplete( "instance" ).term = "";
    },

    _destroy: function() {
      this.wrapper.remove();
      this.element.show();
    }
  });
})( jQuery );

$(function() {
  $( "#catagory" ).combobox();
  $( "#toggle" ).click(function() {
    $( "#catagory" ).toggle();
  });
});
//script for combobox ends
(function( $ ) {
  $.widget( "custom.combobox", {
    _create: function() {
      this.wrapper = $( "<span>" )
        .addClass( "custom-combobox" )
        .insertAfter( this.element );

      this.element.hide();
      this._createAutocomplete();
      this._createShowAllButton();
    },

    _createAutocomplete: function() {
      var selected = this.element.children( ":selected" ),
        value = selected.val() ? selected.text() : "";

      this.input = $( "<input>" )
        .appendTo( this.wrapper )
        .val( value )
        .attr( "title", "" )
        .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
        .autocomplete({
          delay: 0,
          minLength: 0,
          source: $.proxy( this, "_source" )
        })
        .tooltip({
          tooltipClass: "ui-state-highlight"
        });

      this._on( this.input, {
        autocompleteselect: function( event, ui ) {
          ui.item.option.selected = true;
          this._trigger( "select", event, {
            item: ui.item.option
          });
        },

        autocompletechange: "_removeIfInvalid"
      });
    },

    _createShowAllButton: function() {
      var input = this.input,
        wasOpen = false;

      $( "<a>" )
        .attr( "tabIndex", -1 )
        .attr( "title", "Show All States" )
        .tooltip()
        .appendTo( this.wrapper )
        .button({
          icons: {
            primary: "ui-icon-triangle-1-s"
          },
          text: false
        })
        .removeClass( "ui-corner-all" )
        .addClass( "custom-combobox-toggle ui-corner-right" )
        .mousedown(function() {
          wasOpen = input.autocomplete( "widget" ).is( ":visible" );
        })
        .click(function() {
          input.focus();

          // Close if already visible
          if ( wasOpen ) {
            return;
          }

          // Pass empty string as value to search for, displaying all results
          input.autocomplete( "search", "" );
        });
    },

    _source: function( request, response ) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
      response( this.element.children( "option" ).map(function() {
        var text = $( this ).text();
        if ( this.value && ( !request.term || matcher.test(text) ) )
          return {
            label: text,
            value: text,
            option: this
          };
      }) );
    },

    _removeIfInvalid: function( event, ui ) {

      // Selected an item, nothing to do
      if ( ui.item ) {
        return;
      }

      // Search for a match (case-insensitive)
      var value = this.input.val(),
        valueLowerCase = value.toLowerCase(),
        valid = false;
      this.element.children( "option" ).each(function() {
        if ( $( this ).text().toLowerCase() === valueLowerCase ) {
          this.selected = valid = true;
          return false;
        }
      });

      // Found a match, nothing to do
      if ( valid ) {
        return;
      }

      // Remove invalid value
      this.input
        .val( "" )
        .attr( "title", value + " didn't match any item" )
        .tooltip( "open" );
      this.element.val( "" );
      this._delay(function() {
        this.input.tooltip( "close" ).attr( "title", "" );
      }, 2500 );
      this.input.autocomplete( "instance" ).term = "";
    },

    _destroy: function() {
      this.wrapper.remove();
      this.element.show();
    }
  });
})( jQuery );

$(function() {
  $( "#location" ).combobox();
  $( "#toggle" ).click(function() {
    $( "#location" ).toggle();
  });
});
// MUTAHIR REIVIEW POINTS.
 
                        $(document).ready(function () {
                            $("#demo3 .stars").click(function () {
                                  $.get("{{ url('starRatting') }}",{rate:$(this).val(),doctor:$('#user_id').val()},
                                    function(d){
                                    if(d>0)
                                    {
                                        alert('You already rated');
                                    }else{
                                        alert('Thanks For Rating');
                                    }
                                });
                                $(this).attr("checked");
                            });
                        }); 
</script>
</body>
</html>
