@extends('layout.memberapp')
@section('head')
<link rel="stylesheet" href="{{URL::asset('dist/build/css/bootstrap-datetimepicker.min.css') }}"> 
@stop
@section('footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js">
  
</script>
<script src="{{ URL::asset('dist/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">
            $(function () {
                $('.datetimepicker3').datetimepicker({
                    //format: 'LT',
                    format: 'H:mm'
                });
                 });
                 </script>

@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Update 
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section> 
        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><p> Update Dr.<?php echo $doctor->first_name.' '.$doctor->last_name; ?> </p>
</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
<div class="box-body">
  <form role="form" action="<?php echo url(); ?>/profiles/" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="_token" value="{{{ csrf_token() }}}" />

    <input type="hidden" name="profile_id" value="{{ $dr_Profile->id }}" />

    <div class="row" id="step-2">
      <div class="col-xs-8 col-md-offset-3">
        <div class="col-md-12">
          <h3> Schedule availability</h3> 

          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>

        </div>
      </div>
    </div>

  </form>

</div>
<!-- form for profile builder End -->


 </div><!-- /.box-body -->
            <div class="box-footer">
              Footer
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 
@endsection
