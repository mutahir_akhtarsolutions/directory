 @extends('layout.memberapp')
 @section('head')
<!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/all.css') }}">
@stop
 @section('footer')
<!-- iCheck 1.0.1 -->
<script src="{{ URL::asset('plugins/iCheck/icheck.min.js') }}"></script>

<script  type="text/javascript" charset="utf-8">
   $(function () {


          //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        }); 

        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

   //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
          var clicks = $(this).data('clicks');
       
          if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
          } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
          }
          $(this).data("clicks", !clicks);
        });
      });


</script>
@stop
@section('content')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            User's Permissions
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content"> 
 <form method="post" action="" name="">
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row"> 
            <div class="col-md-12">   
              <!-- iCheck -->
              <div class="box">
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->
                <div class="box-header">
                  <h3 class="box-title">User's Permissions</h3>
                </div>
                <div class="box-body">
                  <!-- Minimal style -->


                     <div class="col-md-12">  
                     <div class="form-group">
                    <label>
                   <!-- <input type="checkbox" id="checkAll"/>-->
                   <button class="btn btn-default btn-sm checkbox-toggle" type="button"><i class="fa fa-square-o"></i></button>
                     &nbsp; Check All.
                      </label> 

                      </div>
                     <div class="form-group"> 

                  </div> 
                  </div> 
   <div class="col-md-12">  
                  <!-- checkbox -->
                  <div class="form-group mailbox-messages">
                    <label>
                      <input type="checkbox" class="minimal" name="create" id="create" value="true" @if(@$access->user_create == 'true') checked @endif >
                     &nbsp; Create Table.
                      </label> 

</div>

                  <!-- checkbox -->
                <div class="form-group mailbox-messages">
                    <label>
                      <input type="checkbox" class="minimal"  name="update" id="update" value="true"  @if(@$access->user_update == 'true') checked @endif >
                     Update Table
                    </label> 
                    </div>

                  <!-- checkbox -->
                 <div class="form-group mailbox-messages">
                    <label>
                      <input type="checkbox" class="minimal"  name="view" id="view" value="true"  @if(@$access->user_view == 'true') checked @endif >
                     
                      View Table
                    </label> 
                   </div> 

                  <!-- checkbox -->
                 <div class="form-group mailbox-messages">
                    <label>
                      <input type="checkbox" class="minimal"  name="delete" id="delete" value="true"  @if(@$access->user_delete == 'true') checked @endif >
                     Delete Table
                    </label> 
                    </div>

                  <!-- checkbox -->
                <div class="form-group mailbox-messages">
                    <label>
                      <input type="checkbox" class="minimal"  name="active" id="active" value="true"   @if(@$access->user_delete == 'true') checked @endif >
                     
                     Active / Inactive
                    </label> 
                    </div>
                     </div>
                </div><!-- /.box-body -->

<div class="box-footer">
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                  </div><!-- /.col-md-12 (right) --> 
              </div><!-- /.box -->

            
          </div><!-- /.row -->
</form>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
 
@endsection