 @extends('layout.memberapp')
@section('head')
<!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/all.css') }}">
@stop
 @section('footer')
<!-- iCheck 1.0.1 -->
<script src="{{ URL::asset('plugins/iCheck/icheck.min.js') }}"></script>


<script  type="text/javascript" charset="utf-8">
   $(function () {
   //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
          var clicks = $(this).data('clicks');
       
          if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
          } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
          }
          $(this).data("clicks", !clicks);
        });
      });


</script>
 @stop
@section('content')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            User's Conditions
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content"> 
 <form method="post" action="" name="">
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row"> 
            <div class="col-md-12">  

              <!-- iCheck -->
              <div class="box">

              <div class="col-md-12">  
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->
                <div class="box-header">
                  <h3 class="box-title">User's Conditions Treated.</h3>
                </div>
                </div>
                <div class="box-body">
                  <!-- Minimal style -->
                <div class="col-md-12">  
                     <div class="form-group">
                    <label>
                   <!-- <input type="checkbox" id="checkAll"/>-->
                   <button class="btn btn-default btn-sm checkbox-toggle" type="button"><i class="fa fa-square-o"></i></button>
                     &nbsp; Check All.
                      </label> 

                      </div>
                     <div class="form-group"> 

                  </div> 
                  </div>
                  <div class="col-md-4 mailbox-messages">

                  <?php $divcounter = 1; ?>  
             @foreach($collection['allCondtions'] as $allCondtions)

             <?php if($divcounter%7 == 0) {
              echo '</div><div class="col-md-4 mailbox-messages">';
                $divcounter = 1;
              }
              ?>
                  <!-- checkbox -->
                  <div class="form-group">
                    <label>
                      <input type="checkbox" class="minimal"  name="condition_id[]" value="{{$allCondtions->id}}" <?php if(sizeof($collection['user_conditions']) !=0):
                      if(in_array($allCondtions->id, $collection['user_conditions'])): ?>
                      checked="checked"
                    <?php endif; endif; ?>>
                     {{$allCondtions->condition_name}}
                    </label> 
                    </div> 
                    <?php $divcounter ++;?>

@endforeach 
 </div> 
                </div><!-- /.box-body -->

<div class="box-footer">
<div class="col-md-4 col-md-offset-5">
                    <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                  </div> 
              </div><!-- /.box -->
            </div><!-- /.col (right) -->
          </div><!-- /.row -->
</form>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
 
@endsection