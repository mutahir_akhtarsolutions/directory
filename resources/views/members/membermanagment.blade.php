 @extends('layout.memberapp')
@section('content')

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>All My Doctors<small>Managment</small> </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">  
            

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">You can add {{$Profile['no_of_doctors']-count($doctors)}} more doctors</h3>

            <div class="col-md-12">
            <div class="col-md-6"></div><div class="col-md-6">
              <div class="col-md-10"></div> 
              <div class="col-md-2"> @if($Profile['no_of_doctors']-count($doctors) > 0)
                <button class="btn btn-block btn-info" onclick="window.location.href='{{ url('/profiles/completesingleProfile/'. $Profile['id']) }}'">Add New </button>@endif</div> </div></div>
                </div>
                

             @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                    <strong>Success</strong> {{ $message }}
                </div>
              @endif
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr><th>Sr.No</th>
                    <th>Name</th>
                    <th>Qualification</th>
                    <th>Phone</th>
                    <th>Afiliation</th>
                    <th>Options</th>
                    
                  </tr>
                </thead>
                <tbody>
                <?php $counter = 1;?>
                    @foreach($doctors as $doctor)
                  
                  <tr><td>{{$counter}}</td>
                    <td>{{$doctor->member->first_name}}&nbsp; {{$doctor->member->last_name}}</td>
                    <td>{{$doctor->education}}</td>
                    <td>{{$doctor->contact_dr_phone_number}}</td>
                    <td>{{$doctor->affiliation}}</td>
                    <td><div class="btn-group">
                      <button class="btn btn-info" type="button">Action</button>
                      <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ url('/profiles/editCompleteProfile').'/'. $doctor->id}}">Pesonal Information</a></li>
                        <li><a href="{{ url('/profiles/editSchedule').'/'. $doctor->id }}">Schedule Availability</a></li>
                       <li><a href="{{ url('/profiles/editInsurance').'/'. $doctor->id }}">Insurance</a></li>
                       <li><a href="{{ url('/profiles/conditions').'/'. $doctor->user_id }}">Conditions Treated</a></a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/profiles/permissions').'/'. $doctor->user_id }}">Give Permissions</a></li>
                         <li><a href="{{ url('/profiles/comments').'/'. $doctor->user_id }}">Show Comments</a></li>
                         <li><a href="{{ url('/profiles/trasferhistory').'/'. $doctor->id }}">Transfer History</a></li>
 <li><a href="{{ url('/profiles/appintments').'/'. $doctor->id }}">All Appointments</a></li>
                      </ul>
                    </div></td>
                  </tr>
                  <?php $counter++;?>
                  @endforeach
                  
                  
                </tbody>
                <tfoot>
                  <tr>
                    <th>Sr.No</th>
                    <th>Name</th>
                    <th>Qualification</th>
                    <th>Phone</th>
                    <th>Afiliation</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
          <!-- /.box --> 
        </div>
        <!-- /.col --> 
      </div>
      <!-- /.row --> 
    </section>
    <!-- /.content --> 
  </div>



@endsection

