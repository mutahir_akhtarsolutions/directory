<?php
use Illuminate\Database\Seeder;

class SentinelTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->truncate();
		//disable foreign key check for this connection before running seeders
  	DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('roles')->truncate();
		// supposed to only apply to a single connection and reset it's self
    // but I like to explicitly undo what I've done for clarity
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		DB::table('role_users')->truncate();
    DB::table('activations')->truncate();
    DB::table('persistences')->truncate();

		$adminRole = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Administrator',
            'slug' => 'administrator',
            'permissions' => [
                'admin' => true,
            ]
        ]);

        $patientsRole = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Patients',
            'slug' => 'patients',
        ]);

		$admin = [
			'email'    => 'admin@directory-script.local',
			'password' => '123456',
		];

		$patients = [

			[
				'email'    => 'user1@directory-script.local',
				'password' => '123456',
			],

			[
				'email'    => 'user2@directory-script.local',
				'password' => '123456',
			],

			[
				'email'    => 'user3@directory-script.local',
				'password' => 'demo123',
			],

		];

		$adminUser = Sentinel::registerAndActivate($admin)->roles()->attach($adminRole);

		foreach ($patients as $patient)
		{
            Sentinel::registerAndActivate($patient)->roles()->attach($patientsRole);
		}
	}

}
