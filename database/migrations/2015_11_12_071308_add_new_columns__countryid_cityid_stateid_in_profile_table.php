<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsCountryidCityidStateidInProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('profile', function($table)
      {
        $table->string('zip_code', 100);
        $table->string('contact_dr_phone_number', 100);
        $table->string('image_of_doctor', 100);
        $table->integer('state_id')->unsigned()->default(0);
        $table->integer('country_id')->unsigned()->default(0);
        $table->integer('city_id')->unsigned()->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile');
    }
}
