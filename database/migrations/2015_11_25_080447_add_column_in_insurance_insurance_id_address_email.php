<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInInsuranceInsuranceIdAddressEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //adding column to profile
        Schema::table('insurance', function($table)
              {

                $table->string('insurance_id', 45);
                $table->string('insurance_address', 45);
                $table->string('insurance_phone', 45);
                $table->string('insurance_email', 45);
                $table->string('insurance_url', 45);
              });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
