<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsLetterMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('newsletter_messages', function (Blueprint $table) { 
           $table->increments('id')->index();
           $table->string('subject',255);
           $table->text('body');
           $table->tinyInteger('active')->default(1);
           $table->timestamp('date_sent');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('newsletter_messages');
    }
}
