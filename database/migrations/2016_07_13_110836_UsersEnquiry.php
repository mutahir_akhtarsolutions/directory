<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersEnquiry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {  
        Schema::create('enquiries', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('name');
            $table->string('email');
            $table->string('phone');   
            $table->string('subject');
            $table->integer('user_id')->default(0);
            $table->foreign('user_id')->references('id')->on('users'); 
            $table->text('contents')->nullable();
            $table->integer('status')->default(0);
            $table->integer('sort_order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enquiries');
    }
}
