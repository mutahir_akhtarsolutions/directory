<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersToCreditCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('users_to_creditcards', function (Blueprint $table) { 
            $table->integer('user_id')->unsigned();
            $table->integer('credit_card_id')->unsigned();
            $table->timestamps();
            $table->foreign('credit_card_id')->references('id')->on('credit_card')
            ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('user')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
