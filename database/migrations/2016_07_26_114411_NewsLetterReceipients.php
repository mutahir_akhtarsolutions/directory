<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsLetterReceipients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
       public function up()
    {
          Schema::create('newsletter_recipients', function (Blueprint $table) { 
           $table->increments('id')->index();
           $table->string('first_name',255)->nullable();
           $table->string('last_name',255)->nullable();
           $table->string('email',255)->unique();
           $table->tinyInteger('active')->default(1); 
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('newsletter_recipients');
    }
}
