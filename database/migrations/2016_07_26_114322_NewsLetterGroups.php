<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsLetterGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('newsletter_groups', function (Blueprint $table) { 
           $table->increments('id')->index();
           $table->string('group_name',255);
           $table->text('group_description');
           $table->tinyInteger('group_public')->default(1); 
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('newsletter_groups');
    }
}
