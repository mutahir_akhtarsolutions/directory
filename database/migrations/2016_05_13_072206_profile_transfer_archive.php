<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfileTransferArchive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('profile_to_transferhistory', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->integer('old_parent')->unsigned();
            $table->integer('new_parent')->unsigned();
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('profile')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
