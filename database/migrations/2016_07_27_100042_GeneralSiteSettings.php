<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeneralSiteSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
          Schema::create('sitesettings', function (Blueprint $table) { 
           $table->increments('id')->index();
           $table->string('site_title',255)->nullable();
           $table->string('site_copyright',255)->nullable();
           $table->string('site_email',255)->unique();
           $table->string('contact_email',255)->nullable();
           $table->string('general_email',255)->nullable();
           $table->string('contact_phone',255)->nullable();
           $table->string('contact_cell',255)->nullable();
           $table->string('site_url',255)->nullable();
           $table->text('address')->nullable();
           $table->text('site_keywords')->nullable();
           $table->text('site_metatags')->nullable();
           $table->text('site_metadescriptions')->nullable();
           $table->string('site_facon',255)->nullable(); 
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('generalsite_settings');
    }
}
