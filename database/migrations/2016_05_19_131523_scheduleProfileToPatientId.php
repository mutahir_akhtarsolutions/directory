<?php

use Illuminate\Database\Migrations\Migration;

class ScheduleProfileToPatientId extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('schedule_profile_to_patient', function ($table) {
				 $table->smallInteger('status');
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}
}
