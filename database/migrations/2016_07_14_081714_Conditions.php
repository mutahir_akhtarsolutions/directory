<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Conditions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('condition_name', 200); 
            $table->integer('specialitie_id')->unsigned()->index();
            $table->foreign('specialitie_id')->references('id')->on('specialities')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
