<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScheduleProfilesToPatient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('schedule_profile_to_patient', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_doctor_id')->unsigned();
            $table->integer('user_patient_id')->unsigned();
            $table->date('schedule_date');;
            $table->timestamps();
            $table->foreign('profile_doctor_id')->references('id')->on('profile')
            ->onDelete('cascade');
            $table->foreign('user_patient_id')->references('id')->on('user')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
