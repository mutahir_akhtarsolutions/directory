<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsInProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('profile', function($table)
          {
            $table->string('business_name');
            $table->integer('No-of-doctors')->unsigned()->default(0);
            $table->string('business_address');
            $table->string('business_telephone');
            $table->string('business_reception');
            $table->string('business_email');
            $table->string('business_website');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
