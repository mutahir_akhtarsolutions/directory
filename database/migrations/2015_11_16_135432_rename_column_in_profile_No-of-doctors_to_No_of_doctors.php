<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnInProfileNoOfDoctorsToNoOfDoctors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //drop the column in profile table
      Schema::table('profile', function($table)
              {
                  $table->dropColumn('No-of-doctors');
              });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('profile');
    }
}
