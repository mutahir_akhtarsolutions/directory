<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsLetterRecipientsGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
          Schema::create('newsletter_recipients_groups', function (Blueprint $table) { 
         $table->tinyInteger('user_id')->unsigned()->index();
         $table->tinyInteger('group_id')->unsigned()->index();
         $table->timestamps();
         $table->foreign('user_id')->references('id')
         ->on('newsletter_recipients');
         $table->foreign('group_id')->references('id')
         ->on('newsletter_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('newsletter_recipients_groups');
    }

}
