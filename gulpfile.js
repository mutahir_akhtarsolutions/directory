var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 var bowerDir = './resources/assets/';

 var lessPaths = [
     bowerDir + "bootstrap/less"
 ];


elixir(function(mix) {
    //mix.sass('app.scss');
	//mix.less('app.less');
	mix.less('app.less', 'public/assets/css', { paths: lessPaths })
			.scripts([
					'bootstrap/dist/js/bootstrap.min.js',
					//'bootstrap-select/dist/js/bootstrap-select.min.js
			], 'public/assets/js/vendor.js', bowerDir)
			//.copy('resources/assets/js/app.js', 'public/js/app.js'
			.copy(bowerDir + 'bootstrap/fonts', 'public/assets/fonts')
});
